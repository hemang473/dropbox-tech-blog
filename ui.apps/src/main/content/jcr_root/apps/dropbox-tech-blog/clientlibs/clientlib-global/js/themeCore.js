/*! jQuery v3.3.1 | (c) JS Foundation and other contributors | jquery.org/license */ ! function (e, t) {
  "use strict";
  "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
    if (!e.document) throw new Error("jQuery requires a window with a document");
    return t(e)
  } : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
  "use strict";
  var n = [],
    r = e.document,
    i = Object.getPrototypeOf,
    o = n.slice,
    a = n.concat,
    s = n.push,
    u = n.indexOf,
    l = {},
    c = l.toString,
    f = l.hasOwnProperty,
    p = f.toString,
    d = p.call(Object),
    h = {},
    g = function e(t) {
      return "function" == typeof t && "number" != typeof t.nodeType
    },
    y = function e(t) {
      return null != t && t === t.window
    },
    v = {
      type: !0,
      src: !0,
      noModule: !0
    };

  function m(e, t, n) {
    var i, o = (t = t || r).createElement("script");
    if (o.text = e, n)
      for (i in v) n[i] && (o[i] = n[i]);
    t.head.appendChild(o).parentNode.removeChild(o)
  }

  function x(e) {
    return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? l[c.call(e)] || "object" : typeof e
  }
  var b = "3.3.1",
    w = function (e, t) {
      return new w.fn.init(e, t)
    },
    T = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
  w.fn = w.prototype = {
    jquery: "3.3.1",
    constructor: w,
    length: 0,
    toArray: function () {
      return o.call(this)
    },
    get: function (e) {
      return null == e ? o.call(this) : e < 0 ? this[e + this.length] : this[e]
    },
    pushStack: function (e) {
      var t = w.merge(this.constructor(), e);
      return t.prevObject = this, t
    },
    each: function (e) {
      return w.each(this, e)
    },
    map: function (e) {
      return this.pushStack(w.map(this, function (t, n) {
        return e.call(t, n, t)
      }))
    },
    slice: function () {
      return this.pushStack(o.apply(this, arguments))
    },
    first: function () {
      return this.eq(0)
    },
    last: function () {
      return this.eq(-1)
    },
    eq: function (e) {
      var t = this.length,
        n = +e + (e < 0 ? t : 0);
      return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
    },
    end: function () {
      return this.prevObject || this.constructor()
    },
    push: s,
    sort: n.sort,
    splice: n.splice
  }, w.extend = w.fn.extend = function () {
    var e, t, n, r, i, o, a = arguments[0] || {},
      s = 1,
      u = arguments.length,
      l = !1;
    for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || g(a) || (a = {}), s === u && (a = this, s--); s < u; s++)
      if (null != (e = arguments[s]))
        for (t in e) n = a[t], a !== (r = e[t]) && (l && r && (w.isPlainObject(r) || (i = Array.isArray(r))) ? (i ? (i = !1, o = n && Array.isArray(n) ? n : []) : o = n && w.isPlainObject(n) ? n : {}, a[t] = w.extend(l, o, r)) : void 0 !== r && (a[t] = r));
    return a
  }, w.extend({
    expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function (e) {
      throw new Error(e)
    },
    noop: function () {},
    isPlainObject: function (e) {
      var t, n;
      return !(!e || "[object Object]" !== c.call(e)) && (!(t = i(e)) || "function" == typeof (n = f.call(t, "constructor") && t.constructor) && p.call(n) === d)
    },
    isEmptyObject: function (e) {
      var t;
      for (t in e) return !1;
      return !0
    },
    globalEval: function (e) {
      m(e)
    },
    each: function (e, t) {
      var n, r = 0;
      if (C(e)) {
        for (n = e.length; r < n; r++)
          if (!1 === t.call(e[r], r, e[r])) break
      } else
        for (r in e)
          if (!1 === t.call(e[r], r, e[r])) break;
      return e
    },
    trim: function (e) {
      return null == e ? "" : (e + "").replace(T, "")
    },
    makeArray: function (e, t) {
      var n = t || [];
      return null != e && (C(Object(e)) ? w.merge(n, "string" == typeof e ? [e] : e) : s.call(n, e)), n
    },
    inArray: function (e, t, n) {
      return null == t ? -1 : u.call(t, e, n)
    },
    merge: function (e, t) {
      for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
      return e.length = i, e
    },
    grep: function (e, t, n) {
      for (var r, i = [], o = 0, a = e.length, s = !n; o < a; o++)(r = !t(e[o], o)) !== s && i.push(e[o]);
      return i
    },
    map: function (e, t, n) {
      var r, i, o = 0,
        s = [];
      if (C(e))
        for (r = e.length; o < r; o++) null != (i = t(e[o], o, n)) && s.push(i);
      else
        for (o in e) null != (i = t(e[o], o, n)) && s.push(i);
      return a.apply([], s)
    },
    guid: 1,
    support: h
  }), "function" == typeof Symbol && (w.fn[Symbol.iterator] = n[Symbol.iterator]), w.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
    l["[object " + t + "]"] = t.toLowerCase()
  });

  function C(e) {
    var t = !!e && "length" in e && e.length,
      n = x(e);
    return !g(e) && !y(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
  }
  var E = function (e) {
    var t, n, r, i, o, a, s, u, l, c, f, p, d, h, g, y, v, m, x, b = "sizzle" + 1 * new Date,
      w = e.document,
      T = 0,
      C = 0,
      E = ae(),
      k = ae(),
      S = ae(),
      D = function (e, t) {
        return e === t && (f = !0), 0
      },
      N = {}.hasOwnProperty,
      A = [],
      j = A.pop,
      q = A.push,
      L = A.push,
      H = A.slice,
      O = function (e, t) {
        for (var n = 0, r = e.length; n < r; n++)
          if (e[n] === t) return n;
        return -1
      },
      P = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
      M = "[\\x20\\t\\r\\n\\f]",
      R = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
      I = "\\[" + M + "*(" + R + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + R + "))|)" + M + "*\\]",
      W = ":(" + R + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + I + ")*)|.*)\\)|)",
      $ = new RegExp(M + "+", "g"),
      B = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
      F = new RegExp("^" + M + "*," + M + "*"),
      _ = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
      z = new RegExp("=" + M + "*([^\\]'\"]*?)" + M + "*\\]", "g"),
      X = new RegExp(W),
      U = new RegExp("^" + R + "$"),
      V = {
        ID: new RegExp("^#(" + R + ")"),
        CLASS: new RegExp("^\\.(" + R + ")"),
        TAG: new RegExp("^(" + R + "|[*])"),
        ATTR: new RegExp("^" + I),
        PSEUDO: new RegExp("^" + W),
        CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
        bool: new RegExp("^(?:" + P + ")$", "i"),
        needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
      },
      G = /^(?:input|select|textarea|button)$/i,
      Y = /^h\d$/i,
      Q = /^[^{]+\{\s*\[native \w/,
      J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      K = /[+~]/,
      Z = new RegExp("\\\\([\\da-f]{1,6}" + M + "?|(" + M + ")|.)", "ig"),
      ee = function (e, t, n) {
        var r = "0x" + t - 65536;
        return r !== r || n ? t : r < 0 ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
      },
      te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
      ne = function (e, t) {
        return t ? "\0" === e ? "\ufffd" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
      },
      re = function () {
        p()
      },
      ie = me(function (e) {
        return !0 === e.disabled && ("form" in e || "label" in e)
      }, {
        dir: "parentNode",
        next: "legend"
      });
    try {
      L.apply(A = H.call(w.childNodes), w.childNodes), A[w.childNodes.length].nodeType
    } catch (e) {
      L = {
        apply: A.length ? function (e, t) {
          q.apply(e, H.call(t))
        } : function (e, t) {
          var n = e.length,
            r = 0;
          while (e[n++] = t[r++]);
          e.length = n - 1
        }
      }
    }

    function oe(e, t, r, i) {
      var o, s, l, c, f, h, v, m = t && t.ownerDocument,
        T = t ? t.nodeType : 9;
      if (r = r || [], "string" != typeof e || !e || 1 !== T && 9 !== T && 11 !== T) return r;
      if (!i && ((t ? t.ownerDocument || t : w) !== d && p(t), t = t || d, g)) {
        if (11 !== T && (f = J.exec(e)))
          if (o = f[1]) {
            if (9 === T) {
              if (!(l = t.getElementById(o))) return r;
              if (l.id === o) return r.push(l), r
            } else if (m && (l = m.getElementById(o)) && x(t, l) && l.id === o) return r.push(l), r
          } else {
            if (f[2]) return L.apply(r, t.getElementsByTagName(e)), r;
            if ((o = f[3]) && n.getElementsByClassName && t.getElementsByClassName) return L.apply(r, t.getElementsByClassName(o)), r
          } if (n.qsa && !S[e + " "] && (!y || !y.test(e))) {
          if (1 !== T) m = t, v = e;
          else if ("object" !== t.nodeName.toLowerCase()) {
            (c = t.getAttribute("id")) ? c = c.replace(te, ne): t.setAttribute("id", c = b), s = (h = a(e)).length;
            while (s--) h[s] = "#" + c + " " + ve(h[s]);
            v = h.join(","), m = K.test(e) && ge(t.parentNode) || t
          }
          if (v) try {
            return L.apply(r, m.querySelectorAll(v)), r
          } catch (e) {} finally {
            c === b && t.removeAttribute("id")
          }
        }
      }
      return u(e.replace(B, "$1"), t, r, i)
    }

    function ae() {
      var e = [];

      function t(n, i) {
        return e.push(n + " ") > r.cacheLength && delete t[e.shift()], t[n + " "] = i
      }
      return t
    }

    function se(e) {
      return e[b] = !0, e
    }

    function ue(e) {
      var t = d.createElement("fieldset");
      try {
        return !!e(t)
      } catch (e) {
        return !1
      } finally {
        t.parentNode && t.parentNode.removeChild(t), t = null
      }
    }

    function le(e, t) {
      var n = e.split("|"),
        i = n.length;
      while (i--) r.attrHandle[n[i]] = t
    }

    function ce(e, t) {
      var n = t && e,
        r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
      if (r) return r;
      if (n)
        while (n = n.nextSibling)
          if (n === t) return -1;
      return e ? 1 : -1
    }

    function fe(e) {
      return function (t) {
        return "input" === t.nodeName.toLowerCase() && t.type === e
      }
    }

    function pe(e) {
      return function (t) {
        var n = t.nodeName.toLowerCase();
        return ("input" === n || "button" === n) && t.type === e
      }
    }

    function de(e) {
      return function (t) {
        return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && ie(t) === e : t.disabled === e : "label" in t && t.disabled === e
      }
    }

    function he(e) {
      return se(function (t) {
        return t = +t, se(function (n, r) {
          var i, o = e([], n.length, t),
            a = o.length;
          while (a--) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
        })
      })
    }

    function ge(e) {
      return e && "undefined" != typeof e.getElementsByTagName && e
    }
    n = oe.support = {}, o = oe.isXML = function (e) {
      var t = e && (e.ownerDocument || e).documentElement;
      return !!t && "HTML" !== t.nodeName
    }, p = oe.setDocument = function (e) {
      var t, i, a = e ? e.ownerDocument || e : w;
      return a !== d && 9 === a.nodeType && a.documentElement ? (d = a, h = d.documentElement, g = !o(d), w !== d && (i = d.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", re, !1) : i.attachEvent && i.attachEvent("onunload", re)), n.attributes = ue(function (e) {
        return e.className = "i", !e.getAttribute("className")
      }), n.getElementsByTagName = ue(function (e) {
        return e.appendChild(d.createComment("")), !e.getElementsByTagName("*").length
      }), n.getElementsByClassName = Q.test(d.getElementsByClassName), n.getById = ue(function (e) {
        return h.appendChild(e).id = b, !d.getElementsByName || !d.getElementsByName(b).length
      }), n.getById ? (r.filter.ID = function (e) {
        var t = e.replace(Z, ee);
        return function (e) {
          return e.getAttribute("id") === t
        }
      }, r.find.ID = function (e, t) {
        if ("undefined" != typeof t.getElementById && g) {
          var n = t.getElementById(e);
          return n ? [n] : []
        }
      }) : (r.filter.ID = function (e) {
        var t = e.replace(Z, ee);
        return function (e) {
          var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
          return n && n.value === t
        }
      }, r.find.ID = function (e, t) {
        if ("undefined" != typeof t.getElementById && g) {
          var n, r, i, o = t.getElementById(e);
          if (o) {
            if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
            i = t.getElementsByName(e), r = 0;
            while (o = i[r++])
              if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
          }
          return []
        }
      }), r.find.TAG = n.getElementsByTagName ? function (e, t) {
        return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : n.qsa ? t.querySelectorAll(e) : void 0
      } : function (e, t) {
        var n, r = [],
          i = 0,
          o = t.getElementsByTagName(e);
        if ("*" === e) {
          while (n = o[i++]) 1 === n.nodeType && r.push(n);
          return r
        }
        return o
      }, r.find.CLASS = n.getElementsByClassName && function (e, t) {
        if ("undefined" != typeof t.getElementsByClassName && g) return t.getElementsByClassName(e)
      }, v = [], y = [], (n.qsa = Q.test(d.querySelectorAll)) && (ue(function (e) {
        h.appendChild(e).innerHTML = "<a id='" + b + "'></a><select id='" + b + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && y.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || y.push("\\[" + M + "*(?:value|" + P + ")"), e.querySelectorAll("[id~=" + b + "-]").length || y.push("~="), e.querySelectorAll(":checked").length || y.push(":checked"), e.querySelectorAll("a#" + b + "+*").length || y.push(".#.+[+~]")
      }), ue(function (e) {
        e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
        var t = d.createElement("input");
        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && y.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && y.push(":enabled", ":disabled"), h.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && y.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), y.push(",.*:")
      })), (n.matchesSelector = Q.test(m = h.matches || h.webkitMatchesSelector || h.mozMatchesSelector || h.oMatchesSelector || h.msMatchesSelector)) && ue(function (e) {
        n.disconnectedMatch = m.call(e, "*"), m.call(e, "[s!='']:x"), v.push("!=", W)
      }), y = y.length && new RegExp(y.join("|")), v = v.length && new RegExp(v.join("|")), t = Q.test(h.compareDocumentPosition), x = t || Q.test(h.contains) ? function (e, t) {
        var n = 9 === e.nodeType ? e.documentElement : e,
          r = t && t.parentNode;
        return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
      } : function (e, t) {
        if (t)
          while (t = t.parentNode)
            if (t === e) return !0;
        return !1
      }, D = t ? function (e, t) {
        if (e === t) return f = !0, 0;
        var r = !e.compareDocumentPosition - !t.compareDocumentPosition;
        return r || (1 & (r = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !n.sortDetached && t.compareDocumentPosition(e) === r ? e === d || e.ownerDocument === w && x(w, e) ? -1 : t === d || t.ownerDocument === w && x(w, t) ? 1 : c ? O(c, e) - O(c, t) : 0 : 4 & r ? -1 : 1)
      } : function (e, t) {
        if (e === t) return f = !0, 0;
        var n, r = 0,
          i = e.parentNode,
          o = t.parentNode,
          a = [e],
          s = [t];
        if (!i || !o) return e === d ? -1 : t === d ? 1 : i ? -1 : o ? 1 : c ? O(c, e) - O(c, t) : 0;
        if (i === o) return ce(e, t);
        n = e;
        while (n = n.parentNode) a.unshift(n);
        n = t;
        while (n = n.parentNode) s.unshift(n);
        while (a[r] === s[r]) r++;
        return r ? ce(a[r], s[r]) : a[r] === w ? -1 : s[r] === w ? 1 : 0
      }, d) : d
    }, oe.matches = function (e, t) {
      return oe(e, null, null, t)
    }, oe.matchesSelector = function (e, t) {
      if ((e.ownerDocument || e) !== d && p(e), t = t.replace(z, "='$1']"), n.matchesSelector && g && !S[t + " "] && (!v || !v.test(t)) && (!y || !y.test(t))) try {
        var r = m.call(e, t);
        if (r || n.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
      } catch (e) {}
      return oe(t, d, null, [e]).length > 0
    }, oe.contains = function (e, t) {
      return (e.ownerDocument || e) !== d && p(e), x(e, t)
    }, oe.attr = function (e, t) {
      (e.ownerDocument || e) !== d && p(e);
      var i = r.attrHandle[t.toLowerCase()],
        o = i && N.call(r.attrHandle, t.toLowerCase()) ? i(e, t, !g) : void 0;
      return void 0 !== o ? o : n.attributes || !g ? e.getAttribute(t) : (o = e.getAttributeNode(t)) && o.specified ? o.value : null
    }, oe.escape = function (e) {
      return (e + "").replace(te, ne)
    }, oe.error = function (e) {
      throw new Error("Syntax error, unrecognized expression: " + e)
    }, oe.uniqueSort = function (e) {
      var t, r = [],
        i = 0,
        o = 0;
      if (f = !n.detectDuplicates, c = !n.sortStable && e.slice(0), e.sort(D), f) {
        while (t = e[o++]) t === e[o] && (i = r.push(o));
        while (i--) e.splice(r[i], 1)
      }
      return c = null, e
    }, i = oe.getText = function (e) {
      var t, n = "",
        r = 0,
        o = e.nodeType;
      if (o) {
        if (1 === o || 9 === o || 11 === o) {
          if ("string" == typeof e.textContent) return e.textContent;
          for (e = e.firstChild; e; e = e.nextSibling) n += i(e)
        } else if (3 === o || 4 === o) return e.nodeValue
      } else
        while (t = e[r++]) n += i(t);
      return n
    }, (r = oe.selectors = {
      cacheLength: 50,
      createPseudo: se,
      match: V,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function (e) {
          return e[1] = e[1].replace(Z, ee), e[3] = (e[3] || e[4] || e[5] || "").replace(Z, ee), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
        },
        CHILD: function (e) {
          return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || oe.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && oe.error(e[0]), e
        },
        PSEUDO: function (e) {
          var t, n = !e[6] && e[2];
          return V.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = a(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
        }
      },
      filter: {
        TAG: function (e) {
          var t = e.replace(Z, ee).toLowerCase();
          return "*" === e ? function () {
            return !0
          } : function (e) {
            return e.nodeName && e.nodeName.toLowerCase() === t
          }
        },
        CLASS: function (e) {
          var t = E[e + " "];
          return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && E(e, function (e) {
            return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
          })
        },
        ATTR: function (e, t, n) {
          return function (r) {
            var i = oe.attr(r, e);
            return null == i ? "!=" === t : !t || (i += "", "=" === t ? i === n : "!=" === t ? i !== n : "^=" === t ? n && 0 === i.indexOf(n) : "*=" === t ? n && i.indexOf(n) > -1 : "$=" === t ? n && i.slice(-n.length) === n : "~=" === t ? (" " + i.replace($, " ") + " ").indexOf(n) > -1 : "|=" === t && (i === n || i.slice(0, n.length + 1) === n + "-"))
          }
        },
        CHILD: function (e, t, n, r, i) {
          var o = "nth" !== e.slice(0, 3),
            a = "last" !== e.slice(-4),
            s = "of-type" === t;
          return 1 === r && 0 === i ? function (e) {
            return !!e.parentNode
          } : function (t, n, u) {
            var l, c, f, p, d, h, g = o !== a ? "nextSibling" : "previousSibling",
              y = t.parentNode,
              v = s && t.nodeName.toLowerCase(),
              m = !u && !s,
              x = !1;
            if (y) {
              if (o) {
                while (g) {
                  p = t;
                  while (p = p[g])
                    if (s ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) return !1;
                  h = g = "only" === e && !h && "nextSibling"
                }
                return !0
              }
              if (h = [a ? y.firstChild : y.lastChild], a && m) {
                x = (d = (l = (c = (f = (p = y)[b] || (p[b] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] || [])[0] === T && l[1]) && l[2], p = d && y.childNodes[d];
                while (p = ++d && p && p[g] || (x = d = 0) || h.pop())
                  if (1 === p.nodeType && ++x && p === t) {
                    c[e] = [T, d, x];
                    break
                  }
              } else if (m && (x = d = (l = (c = (f = (p = t)[b] || (p[b] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] || [])[0] === T && l[1]), !1 === x)
                while (p = ++d && p && p[g] || (x = d = 0) || h.pop())
                  if ((s ? p.nodeName.toLowerCase() === v : 1 === p.nodeType) && ++x && (m && ((c = (f = p[b] || (p[b] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] = [T, x]), p === t)) break;
              return (x -= i) === r || x % r == 0 && x / r >= 0
            }
          }
        },
        PSEUDO: function (e, t) {
          var n, i = r.pseudos[e] || r.setFilters[e.toLowerCase()] || oe.error("unsupported pseudo: " + e);
          return i[b] ? i(t) : i.length > 1 ? (n = [e, e, "", t], r.setFilters.hasOwnProperty(e.toLowerCase()) ? se(function (e, n) {
            var r, o = i(e, t),
              a = o.length;
            while (a--) e[r = O(e, o[a])] = !(n[r] = o[a])
          }) : function (e) {
            return i(e, 0, n)
          }) : i
        }
      },
      pseudos: {
        not: se(function (e) {
          var t = [],
            n = [],
            r = s(e.replace(B, "$1"));
          return r[b] ? se(function (e, t, n, i) {
            var o, a = r(e, null, i, []),
              s = e.length;
            while (s--)(o = a[s]) && (e[s] = !(t[s] = o))
          }) : function (e, i, o) {
            return t[0] = e, r(t, null, o, n), t[0] = null, !n.pop()
          }
        }),
        has: se(function (e) {
          return function (t) {
            return oe(e, t).length > 0
          }
        }),
        contains: se(function (e) {
          return e = e.replace(Z, ee),
            function (t) {
              return (t.textContent || t.innerText || i(t)).indexOf(e) > -1
            }
        }),
        lang: se(function (e) {
          return U.test(e || "") || oe.error("unsupported lang: " + e), e = e.replace(Z, ee).toLowerCase(),
            function (t) {
              var n;
              do {
                if (n = g ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
              } while ((t = t.parentNode) && 1 === t.nodeType);
              return !1
            }
        }),
        target: function (t) {
          var n = e.location && e.location.hash;
          return n && n.slice(1) === t.id
        },
        root: function (e) {
          return e === h
        },
        focus: function (e) {
          return e === d.activeElement && (!d.hasFocus || d.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
        },
        enabled: de(!1),
        disabled: de(!0),
        checked: function (e) {
          var t = e.nodeName.toLowerCase();
          return "input" === t && !!e.checked || "option" === t && !!e.selected
        },
        selected: function (e) {
          return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
        },
        empty: function (e) {
          for (e = e.firstChild; e; e = e.nextSibling)
            if (e.nodeType < 6) return !1;
          return !0
        },
        parent: function (e) {
          return !r.pseudos.empty(e)
        },
        header: function (e) {
          return Y.test(e.nodeName)
        },
        input: function (e) {
          return G.test(e.nodeName)
        },
        button: function (e) {
          var t = e.nodeName.toLowerCase();
          return "input" === t && "button" === e.type || "button" === t
        },
        text: function (e) {
          var t;
          return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
        },
        first: he(function () {
          return [0]
        }),
        last: he(function (e, t) {
          return [t - 1]
        }),
        eq: he(function (e, t, n) {
          return [n < 0 ? n + t : n]
        }),
        even: he(function (e, t) {
          for (var n = 0; n < t; n += 2) e.push(n);
          return e
        }),
        odd: he(function (e, t) {
          for (var n = 1; n < t; n += 2) e.push(n);
          return e
        }),
        lt: he(function (e, t, n) {
          for (var r = n < 0 ? n + t : n; --r >= 0;) e.push(r);
          return e
        }),
        gt: he(function (e, t, n) {
          for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
          return e
        })
      }
    }).pseudos.nth = r.pseudos.eq;
    for (t in {
        radio: !0,
        checkbox: !0,
        file: !0,
        password: !0,
        image: !0
      }) r.pseudos[t] = fe(t);
    for (t in {
        submit: !0,
        reset: !0
      }) r.pseudos[t] = pe(t);

    function ye() {}
    ye.prototype = r.filters = r.pseudos, r.setFilters = new ye, a = oe.tokenize = function (e, t) {
      var n, i, o, a, s, u, l, c = k[e + " "];
      if (c) return t ? 0 : c.slice(0);
      s = e, u = [], l = r.preFilter;
      while (s) {
        n && !(i = F.exec(s)) || (i && (s = s.slice(i[0].length) || s), u.push(o = [])), n = !1, (i = _.exec(s)) && (n = i.shift(), o.push({
          value: n,
          type: i[0].replace(B, " ")
        }), s = s.slice(n.length));
        for (a in r.filter) !(i = V[a].exec(s)) || l[a] && !(i = l[a](i)) || (n = i.shift(), o.push({
          value: n,
          type: a,
          matches: i
        }), s = s.slice(n.length));
        if (!n) break
      }
      return t ? s.length : s ? oe.error(e) : k(e, u).slice(0)
    };

    function ve(e) {
      for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
      return r
    }

    function me(e, t, n) {
      var r = t.dir,
        i = t.next,
        o = i || r,
        a = n && "parentNode" === o,
        s = C++;
      return t.first ? function (t, n, i) {
        while (t = t[r])
          if (1 === t.nodeType || a) return e(t, n, i);
        return !1
      } : function (t, n, u) {
        var l, c, f, p = [T, s];
        if (u) {
          while (t = t[r])
            if ((1 === t.nodeType || a) && e(t, n, u)) return !0
        } else
          while (t = t[r])
            if (1 === t.nodeType || a)
              if (f = t[b] || (t[b] = {}), c = f[t.uniqueID] || (f[t.uniqueID] = {}), i && i === t.nodeName.toLowerCase()) t = t[r] || t;
              else {
                if ((l = c[o]) && l[0] === T && l[1] === s) return p[2] = l[2];
                if (c[o] = p, p[2] = e(t, n, u)) return !0
              } return !1
      }
    }

    function xe(e) {
      return e.length > 1 ? function (t, n, r) {
        var i = e.length;
        while (i--)
          if (!e[i](t, n, r)) return !1;
        return !0
      } : e[0]
    }

    function be(e, t, n) {
      for (var r = 0, i = t.length; r < i; r++) oe(e, t[r], n);
      return n
    }

    function we(e, t, n, r, i) {
      for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++)(o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
      return a
    }

    function Te(e, t, n, r, i, o) {
      return r && !r[b] && (r = Te(r)), i && !i[b] && (i = Te(i, o)), se(function (o, a, s, u) {
        var l, c, f, p = [],
          d = [],
          h = a.length,
          g = o || be(t || "*", s.nodeType ? [s] : s, []),
          y = !e || !o && t ? g : we(g, p, e, s, u),
          v = n ? i || (o ? e : h || r) ? [] : a : y;
        if (n && n(y, v, s, u), r) {
          l = we(v, d), r(l, [], s, u), c = l.length;
          while (c--)(f = l[c]) && (v[d[c]] = !(y[d[c]] = f))
        }
        if (o) {
          if (i || e) {
            if (i) {
              l = [], c = v.length;
              while (c--)(f = v[c]) && l.push(y[c] = f);
              i(null, v = [], l, u)
            }
            c = v.length;
            while (c--)(f = v[c]) && (l = i ? O(o, f) : p[c]) > -1 && (o[l] = !(a[l] = f))
          }
        } else v = we(v === a ? v.splice(h, v.length) : v), i ? i(null, a, v, u) : L.apply(a, v)
      })
    }

    function Ce(e) {
      for (var t, n, i, o = e.length, a = r.relative[e[0].type], s = a || r.relative[" "], u = a ? 1 : 0, c = me(function (e) {
          return e === t
        }, s, !0), f = me(function (e) {
          return O(t, e) > -1
        }, s, !0), p = [function (e, n, r) {
          var i = !a && (r || n !== l) || ((t = n).nodeType ? c(e, n, r) : f(e, n, r));
          return t = null, i
        }]; u < o; u++)
        if (n = r.relative[e[u].type]) p = [me(xe(p), n)];
        else {
          if ((n = r.filter[e[u].type].apply(null, e[u].matches))[b]) {
            for (i = ++u; i < o; i++)
              if (r.relative[e[i].type]) break;
            return Te(u > 1 && xe(p), u > 1 && ve(e.slice(0, u - 1).concat({
              value: " " === e[u - 2].type ? "*" : ""
            })).replace(B, "$1"), n, u < i && Ce(e.slice(u, i)), i < o && Ce(e = e.slice(i)), i < o && ve(e))
          }
          p.push(n)
        } return xe(p)
    }

    function Ee(e, t) {
      var n = t.length > 0,
        i = e.length > 0,
        o = function (o, a, s, u, c) {
          var f, h, y, v = 0,
            m = "0",
            x = o && [],
            b = [],
            w = l,
            C = o || i && r.find.TAG("*", c),
            E = T += null == w ? 1 : Math.random() || .1,
            k = C.length;
          for (c && (l = a === d || a || c); m !== k && null != (f = C[m]); m++) {
            if (i && f) {
              h = 0, a || f.ownerDocument === d || (p(f), s = !g);
              while (y = e[h++])
                if (y(f, a || d, s)) {
                  u.push(f);
                  break
                } c && (T = E)
            }
            n && ((f = !y && f) && v--, o && x.push(f))
          }
          if (v += m, n && m !== v) {
            h = 0;
            while (y = t[h++]) y(x, b, a, s);
            if (o) {
              if (v > 0)
                while (m--) x[m] || b[m] || (b[m] = j.call(u));
              b = we(b)
            }
            L.apply(u, b), c && !o && b.length > 0 && v + t.length > 1 && oe.uniqueSort(u)
          }
          return c && (T = E, l = w), x
        };
      return n ? se(o) : o
    }
    return s = oe.compile = function (e, t) {
      var n, r = [],
        i = [],
        o = S[e + " "];
      if (!o) {
        t || (t = a(e)), n = t.length;
        while (n--)(o = Ce(t[n]))[b] ? r.push(o) : i.push(o);
        (o = S(e, Ee(i, r))).selector = e
      }
      return o
    }, u = oe.select = function (e, t, n, i) {
      var o, u, l, c, f, p = "function" == typeof e && e,
        d = !i && a(e = p.selector || e);
      if (n = n || [], 1 === d.length) {
        if ((u = d[0] = d[0].slice(0)).length > 2 && "ID" === (l = u[0]).type && 9 === t.nodeType && g && r.relative[u[1].type]) {
          if (!(t = (r.find.ID(l.matches[0].replace(Z, ee), t) || [])[0])) return n;
          p && (t = t.parentNode), e = e.slice(u.shift().value.length)
        }
        o = V.needsContext.test(e) ? 0 : u.length;
        while (o--) {
          if (l = u[o], r.relative[c = l.type]) break;
          if ((f = r.find[c]) && (i = f(l.matches[0].replace(Z, ee), K.test(u[0].type) && ge(t.parentNode) || t))) {
            if (u.splice(o, 1), !(e = i.length && ve(u))) return L.apply(n, i), n;
            break
          }
        }
      }
      return (p || s(e, d))(i, t, !g, n, !t || K.test(e) && ge(t.parentNode) || t), n
    }, n.sortStable = b.split("").sort(D).join("") === b, n.detectDuplicates = !!f, p(), n.sortDetached = ue(function (e) {
      return 1 & e.compareDocumentPosition(d.createElement("fieldset"))
    }), ue(function (e) {
      return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
    }) || le("type|href|height|width", function (e, t, n) {
      if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
    }), n.attributes && ue(function (e) {
      return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
    }) || le("value", function (e, t, n) {
      if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
    }), ue(function (e) {
      return null == e.getAttribute("disabled")
    }) || le(P, function (e, t, n) {
      var r;
      if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
    }), oe
  }(e);
  w.find = E, w.expr = E.selectors, w.expr[":"] = w.expr.pseudos, w.uniqueSort = w.unique = E.uniqueSort, w.text = E.getText, w.isXMLDoc = E.isXML, w.contains = E.contains, w.escapeSelector = E.escape;
  var k = function (e, t, n) {
      var r = [],
        i = void 0 !== n;
      while ((e = e[t]) && 9 !== e.nodeType)
        if (1 === e.nodeType) {
          if (i && w(e).is(n)) break;
          r.push(e)
        } return r
    },
    S = function (e, t) {
      for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
      return n
    },
    D = w.expr.match.needsContext;

  function N(e, t) {
    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
  }
  var A = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

  function j(e, t, n) {
    return g(t) ? w.grep(e, function (e, r) {
      return !!t.call(e, r, e) !== n
    }) : t.nodeType ? w.grep(e, function (e) {
      return e === t !== n
    }) : "string" != typeof t ? w.grep(e, function (e) {
      return u.call(t, e) > -1 !== n
    }) : w.filter(t, e, n)
  }
  w.filter = function (e, t, n) {
    var r = t[0];
    return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? w.find.matchesSelector(r, e) ? [r] : [] : w.find.matches(e, w.grep(t, function (e) {
      return 1 === e.nodeType
    }))
  }, w.fn.extend({
    find: function (e) {
      var t, n, r = this.length,
        i = this;
      if ("string" != typeof e) return this.pushStack(w(e).filter(function () {
        for (t = 0; t < r; t++)
          if (w.contains(i[t], this)) return !0
      }));
      for (n = this.pushStack([]), t = 0; t < r; t++) w.find(e, i[t], n);
      return r > 1 ? w.uniqueSort(n) : n
    },
    filter: function (e) {
      return this.pushStack(j(this, e || [], !1))
    },
    not: function (e) {
      return this.pushStack(j(this, e || [], !0))
    },
    is: function (e) {
      return !!j(this, "string" == typeof e && D.test(e) ? w(e) : e || [], !1).length
    }
  });
  var q, L = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
  (w.fn.init = function (e, t, n) {
    var i, o;
    if (!e) return this;
    if (n = n || q, "string" == typeof e) {
      if (!(i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : L.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
      if (i[1]) {
        if (t = t instanceof w ? t[0] : t, w.merge(this, w.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : r, !0)), A.test(i[1]) && w.isPlainObject(t))
          for (i in t) g(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
        return this
      }
      return (o = r.getElementById(i[2])) && (this[0] = o, this.length = 1), this
    }
    return e.nodeType ? (this[0] = e, this.length = 1, this) : g(e) ? void 0 !== n.ready ? n.ready(e) : e(w) : w.makeArray(e, this)
  }).prototype = w.fn, q = w(r);
  var H = /^(?:parents|prev(?:Until|All))/,
    O = {
      children: !0,
      contents: !0,
      next: !0,
      prev: !0
    };
  w.fn.extend({
    has: function (e) {
      var t = w(e, this),
        n = t.length;
      return this.filter(function () {
        for (var e = 0; e < n; e++)
          if (w.contains(this, t[e])) return !0
      })
    },
    closest: function (e, t) {
      var n, r = 0,
        i = this.length,
        o = [],
        a = "string" != typeof e && w(e);
      if (!D.test(e))
        for (; r < i; r++)
          for (n = this[r]; n && n !== t; n = n.parentNode)
            if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && w.find.matchesSelector(n, e))) {
              o.push(n);
              break
            } return this.pushStack(o.length > 1 ? w.uniqueSort(o) : o)
    },
    index: function (e) {
      return e ? "string" == typeof e ? u.call(w(e), this[0]) : u.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
    },
    add: function (e, t) {
      return this.pushStack(w.uniqueSort(w.merge(this.get(), w(e, t))))
    },
    addBack: function (e) {
      return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }
  });

  function P(e, t) {
    while ((e = e[t]) && 1 !== e.nodeType);
    return e
  }
  w.each({
    parent: function (e) {
      var t = e.parentNode;
      return t && 11 !== t.nodeType ? t : null
    },
    parents: function (e) {
      return k(e, "parentNode")
    },
    parentsUntil: function (e, t, n) {
      return k(e, "parentNode", n)
    },
    next: function (e) {
      return P(e, "nextSibling")
    },
    prev: function (e) {
      return P(e, "previousSibling")
    },
    nextAll: function (e) {
      return k(e, "nextSibling")
    },
    prevAll: function (e) {
      return k(e, "previousSibling")
    },
    nextUntil: function (e, t, n) {
      return k(e, "nextSibling", n)
    },
    prevUntil: function (e, t, n) {
      return k(e, "previousSibling", n)
    },
    siblings: function (e) {
      return S((e.parentNode || {}).firstChild, e)
    },
    children: function (e) {
      return S(e.firstChild)
    },
    contents: function (e) {
      return N(e, "iframe") ? e.contentDocument : (N(e, "template") && (e = e.content || e), w.merge([], e.childNodes))
    }
  }, function (e, t) {
    w.fn[e] = function (n, r) {
      var i = w.map(this, t, n);
      return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = w.filter(r, i)), this.length > 1 && (O[e] || w.uniqueSort(i), H.test(e) && i.reverse()), this.pushStack(i)
    }
  });
  var M = /[^\x20\t\r\n\f]+/g;

  function R(e) {
    var t = {};
    return w.each(e.match(M) || [], function (e, n) {
      t[n] = !0
    }), t
  }
  w.Callbacks = function (e) {
    e = "string" == typeof e ? R(e) : w.extend({}, e);
    var t, n, r, i, o = [],
      a = [],
      s = -1,
      u = function () {
        for (i = i || e.once, r = t = !0; a.length; s = -1) {
          n = a.shift();
          while (++s < o.length) !1 === o[s].apply(n[0], n[1]) && e.stopOnFalse && (s = o.length, n = !1)
        }
        e.memory || (n = !1), t = !1, i && (o = n ? [] : "")
      },
      l = {
        add: function () {
          return o && (n && !t && (s = o.length - 1, a.push(n)), function t(n) {
            w.each(n, function (n, r) {
              g(r) ? e.unique && l.has(r) || o.push(r) : r && r.length && "string" !== x(r) && t(r)
            })
          }(arguments), n && !t && u()), this
        },
        remove: function () {
          return w.each(arguments, function (e, t) {
            var n;
            while ((n = w.inArray(t, o, n)) > -1) o.splice(n, 1), n <= s && s--
          }), this
        },
        has: function (e) {
          return e ? w.inArray(e, o) > -1 : o.length > 0
        },
        empty: function () {
          return o && (o = []), this
        },
        disable: function () {
          return i = a = [], o = n = "", this
        },
        disabled: function () {
          return !o
        },
        lock: function () {
          return i = a = [], n || t || (o = n = ""), this
        },
        locked: function () {
          return !!i
        },
        fireWith: function (e, n) {
          return i || (n = [e, (n = n || []).slice ? n.slice() : n], a.push(n), t || u()), this
        },
        fire: function () {
          return l.fireWith(this, arguments), this
        },
        fired: function () {
          return !!r
        }
      };
    return l
  };

  function I(e) {
    return e
  }

  function W(e) {
    throw e
  }

  function $(e, t, n, r) {
    var i;
    try {
      e && g(i = e.promise) ? i.call(e).done(t).fail(n) : e && g(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
    } catch (e) {
      n.apply(void 0, [e])
    }
  }
  w.extend({
    Deferred: function (t) {
      var n = [
          ["notify", "progress", w.Callbacks("memory"), w.Callbacks("memory"), 2],
          ["resolve", "done", w.Callbacks("once memory"), w.Callbacks("once memory"), 0, "resolved"],
          ["reject", "fail", w.Callbacks("once memory"), w.Callbacks("once memory"), 1, "rejected"]
        ],
        r = "pending",
        i = {
          state: function () {
            return r
          },
          always: function () {
            return o.done(arguments).fail(arguments), this
          },
          "catch": function (e) {
            return i.then(null, e)
          },
          pipe: function () {
            var e = arguments;
            return w.Deferred(function (t) {
              w.each(n, function (n, r) {
                var i = g(e[r[4]]) && e[r[4]];
                o[r[1]](function () {
                  var e = i && i.apply(this, arguments);
                  e && g(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[r[0] + "With"](this, i ? [e] : arguments)
                })
              }), e = null
            }).promise()
          },
          then: function (t, r, i) {
            var o = 0;

            function a(t, n, r, i) {
              return function () {
                var s = this,
                  u = arguments,
                  l = function () {
                    var e, l;
                    if (!(t < o)) {
                      if ((e = r.apply(s, u)) === n.promise()) throw new TypeError("Thenable self-resolution");
                      l = e && ("object" == typeof e || "function" == typeof e) && e.then, g(l) ? i ? l.call(e, a(o, n, I, i), a(o, n, W, i)) : (o++, l.call(e, a(o, n, I, i), a(o, n, W, i), a(o, n, I, n.notifyWith))) : (r !== I && (s = void 0, u = [e]), (i || n.resolveWith)(s, u))
                    }
                  },
                  c = i ? l : function () {
                    try {
                      l()
                    } catch (e) {
                      w.Deferred.exceptionHook && w.Deferred.exceptionHook(e, c.stackTrace), t + 1 >= o && (r !== W && (s = void 0, u = [e]), n.rejectWith(s, u))
                    }
                  };
                t ? c() : (w.Deferred.getStackHook && (c.stackTrace = w.Deferred.getStackHook()), e.setTimeout(c))
              }
            }
            return w.Deferred(function (e) {
              n[0][3].add(a(0, e, g(i) ? i : I, e.notifyWith)), n[1][3].add(a(0, e, g(t) ? t : I)), n[2][3].add(a(0, e, g(r) ? r : W))
            }).promise()
          },
          promise: function (e) {
            return null != e ? w.extend(e, i) : i
          }
        },
        o = {};
      return w.each(n, function (e, t) {
        var a = t[2],
          s = t[5];
        i[t[1]] = a.add, s && a.add(function () {
          r = s
        }, n[3 - e][2].disable, n[3 - e][3].disable, n[0][2].lock, n[0][3].lock), a.add(t[3].fire), o[t[0]] = function () {
          return o[t[0] + "With"](this === o ? void 0 : this, arguments), this
        }, o[t[0] + "With"] = a.fireWith
      }), i.promise(o), t && t.call(o, o), o
    },
    when: function (e) {
      var t = arguments.length,
        n = t,
        r = Array(n),
        i = o.call(arguments),
        a = w.Deferred(),
        s = function (e) {
          return function (n) {
            r[e] = this, i[e] = arguments.length > 1 ? o.call(arguments) : n, --t || a.resolveWith(r, i)
          }
        };
      if (t <= 1 && ($(e, a.done(s(n)).resolve, a.reject, !t), "pending" === a.state() || g(i[n] && i[n].then))) return a.then();
      while (n--) $(i[n], s(n), a.reject);
      return a.promise()
    }
  });
  var B = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  w.Deferred.exceptionHook = function (t, n) {
    e.console && e.console.warn && t && B.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, n)
  }, w.readyException = function (t) {
    e.setTimeout(function () {
      throw t
    })
  };
  var F = w.Deferred();
  w.fn.ready = function (e) {
    return F.then(e)["catch"](function (e) {
      w.readyException(e)
    }), this
  }, w.extend({
    isReady: !1,
    readyWait: 1,
    ready: function (e) {
      (!0 === e ? --w.readyWait : w.isReady) || (w.isReady = !0, !0 !== e && --w.readyWait > 0 || F.resolveWith(r, [w]))
    }
  }), w.ready.then = F.then;

  function _() {
    r.removeEventListener("DOMContentLoaded", _), e.removeEventListener("load", _), w.ready()
  }
  "complete" === r.readyState || "loading" !== r.readyState && !r.documentElement.doScroll ? e.setTimeout(w.ready) : (r.addEventListener("DOMContentLoaded", _), e.addEventListener("load", _));
  var z = function (e, t, n, r, i, o, a) {
      var s = 0,
        u = e.length,
        l = null == n;
      if ("object" === x(n)) {
        i = !0;
        for (s in n) z(e, t, s, n[s], !0, o, a)
      } else if (void 0 !== r && (i = !0, g(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function (e, t, n) {
          return l.call(w(e), n)
        })), t))
        for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
      return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
    },
    X = /^-ms-/,
    U = /-([a-z])/g;

  function V(e, t) {
    return t.toUpperCase()
  }

  function G(e) {
    return e.replace(X, "ms-").replace(U, V)
  }
  var Y = function (e) {
    return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
  };

  function Q() {
    this.expando = w.expando + Q.uid++
  }
  Q.uid = 1, Q.prototype = {
    cache: function (e) {
      var t = e[this.expando];
      return t || (t = {}, Y(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
        value: t,
        configurable: !0
      }))), t
    },
    set: function (e, t, n) {
      var r, i = this.cache(e);
      if ("string" == typeof t) i[G(t)] = n;
      else
        for (r in t) i[G(r)] = t[r];
      return i
    },
    get: function (e, t) {
      return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][G(t)]
    },
    access: function (e, t, n) {
      return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
    },
    remove: function (e, t) {
      var n, r = e[this.expando];
      if (void 0 !== r) {
        if (void 0 !== t) {
          n = (t = Array.isArray(t) ? t.map(G) : (t = G(t)) in r ? [t] : t.match(M) || []).length;
          while (n--) delete r[t[n]]
        }(void 0 === t || w.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
      }
    },
    hasData: function (e) {
      var t = e[this.expando];
      return void 0 !== t && !w.isEmptyObject(t)
    }
  };
  var J = new Q,
    K = new Q,
    Z = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    ee = /[A-Z]/g;

  function te(e) {
    return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : Z.test(e) ? JSON.parse(e) : e)
  }

  function ne(e, t, n) {
    var r;
    if (void 0 === n && 1 === e.nodeType)
      if (r = "data-" + t.replace(ee, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(r))) {
        try {
          n = te(n)
        } catch (e) {}
        K.set(e, t, n)
      } else n = void 0;
    return n
  }
  w.extend({
    hasData: function (e) {
      return K.hasData(e) || J.hasData(e)
    },
    data: function (e, t, n) {
      return K.access(e, t, n)
    },
    removeData: function (e, t) {
      K.remove(e, t)
    },
    _data: function (e, t, n) {
      return J.access(e, t, n)
    },
    _removeData: function (e, t) {
      J.remove(e, t)
    }
  }), w.fn.extend({
    data: function (e, t) {
      var n, r, i, o = this[0],
        a = o && o.attributes;
      if (void 0 === e) {
        if (this.length && (i = K.get(o), 1 === o.nodeType && !J.get(o, "hasDataAttrs"))) {
          n = a.length;
          while (n--) a[n] && 0 === (r = a[n].name).indexOf("data-") && (r = G(r.slice(5)), ne(o, r, i[r]));
          J.set(o, "hasDataAttrs", !0)
        }
        return i
      }
      return "object" == typeof e ? this.each(function () {
        K.set(this, e)
      }) : z(this, function (t) {
        var n;
        if (o && void 0 === t) {
          if (void 0 !== (n = K.get(o, e))) return n;
          if (void 0 !== (n = ne(o, e))) return n
        } else this.each(function () {
          K.set(this, e, t)
        })
      }, null, t, arguments.length > 1, null, !0)
    },
    removeData: function (e) {
      return this.each(function () {
        K.remove(this, e)
      })
    }
  }), w.extend({
    queue: function (e, t, n) {
      var r;
      if (e) return t = (t || "fx") + "queue", r = J.get(e, t), n && (!r || Array.isArray(n) ? r = J.access(e, t, w.makeArray(n)) : r.push(n)), r || []
    },
    dequeue: function (e, t) {
      t = t || "fx";
      var n = w.queue(e, t),
        r = n.length,
        i = n.shift(),
        o = w._queueHooks(e, t),
        a = function () {
          w.dequeue(e, t)
        };
      "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
    },
    _queueHooks: function (e, t) {
      var n = t + "queueHooks";
      return J.get(e, n) || J.access(e, n, {
        empty: w.Callbacks("once memory").add(function () {
          J.remove(e, [t + "queue", n])
        })
      })
    }
  }), w.fn.extend({
    queue: function (e, t) {
      var n = 2;
      return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? w.queue(this[0], e) : void 0 === t ? this : this.each(function () {
        var n = w.queue(this, e, t);
        w._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && w.dequeue(this, e)
      })
    },
    dequeue: function (e) {
      return this.each(function () {
        w.dequeue(this, e)
      })
    },
    clearQueue: function (e) {
      return this.queue(e || "fx", [])
    },
    promise: function (e, t) {
      var n, r = 1,
        i = w.Deferred(),
        o = this,
        a = this.length,
        s = function () {
          --r || i.resolveWith(o, [o])
        };
      "string" != typeof e && (t = e, e = void 0), e = e || "fx";
      while (a--)(n = J.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
      return s(), i.promise(t)
    }
  });
  var re = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    ie = new RegExp("^(?:([+-])=|)(" + re + ")([a-z%]*)$", "i"),
    oe = ["Top", "Right", "Bottom", "Left"],
    ae = function (e, t) {
      return "none" === (e = t || e).style.display || "" === e.style.display && w.contains(e.ownerDocument, e) && "none" === w.css(e, "display")
    },
    se = function (e, t, n, r) {
      var i, o, a = {};
      for (o in t) a[o] = e.style[o], e.style[o] = t[o];
      i = n.apply(e, r || []);
      for (o in t) e.style[o] = a[o];
      return i
    };

  function ue(e, t, n, r) {
    var i, o, a = 20,
      s = r ? function () {
        return r.cur()
      } : function () {
        return w.css(e, t, "")
      },
      u = s(),
      l = n && n[3] || (w.cssNumber[t] ? "" : "px"),
      c = (w.cssNumber[t] || "px" !== l && +u) && ie.exec(w.css(e, t));
    if (c && c[3] !== l) {
      u /= 2, l = l || c[3], c = +u || 1;
      while (a--) w.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
      c *= 2, w.style(e, t, c + l), n = n || []
    }
    return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
  }
  var le = {};

  function ce(e) {
    var t, n = e.ownerDocument,
      r = e.nodeName,
      i = le[r];
    return i || (t = n.body.appendChild(n.createElement(r)), i = w.css(t, "display"), t.parentNode.removeChild(t), "none" === i && (i = "block"), le[r] = i, i)
  }

  function fe(e, t) {
    for (var n, r, i = [], o = 0, a = e.length; o < a; o++)(r = e[o]).style && (n = r.style.display, t ? ("none" === n && (i[o] = J.get(r, "display") || null, i[o] || (r.style.display = "")), "" === r.style.display && ae(r) && (i[o] = ce(r))) : "none" !== n && (i[o] = "none", J.set(r, "display", n)));
    for (o = 0; o < a; o++) null != i[o] && (e[o].style.display = i[o]);
    return e
  }
  w.fn.extend({
    show: function () {
      return fe(this, !0)
    },
    hide: function () {
      return fe(this)
    },
    toggle: function (e) {
      return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
        ae(this) ? w(this).show() : w(this).hide()
      })
    }
  });
  var pe = /^(?:checkbox|radio)$/i,
    de = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
    he = /^$|^module$|\/(?:java|ecma)script/i,
    ge = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      thead: [1, "<table>", "</table>"],
      col: [2, "<table><colgroup>", "</colgroup></table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: [0, "", ""]
    };
  ge.optgroup = ge.option, ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td;

  function ye(e, t) {
    var n;
    return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && N(e, t) ? w.merge([e], n) : n
  }

  function ve(e, t) {
    for (var n = 0, r = e.length; n < r; n++) J.set(e[n], "globalEval", !t || J.get(t[n], "globalEval"))
  }
  var me = /<|&#?\w+;/;

  function xe(e, t, n, r, i) {
    for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++)
      if ((o = e[d]) || 0 === o)
        if ("object" === x(o)) w.merge(p, o.nodeType ? [o] : o);
        else if (me.test(o)) {
      a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + w.htmlPrefilter(o) + u[2], c = u[0];
      while (c--) a = a.lastChild;
      w.merge(p, a.childNodes), (a = f.firstChild).textContent = ""
    } else p.push(t.createTextNode(o));
    f.textContent = "", d = 0;
    while (o = p[d++])
      if (r && w.inArray(o, r) > -1) i && i.push(o);
      else if (l = w.contains(o.ownerDocument, o), a = ye(f.appendChild(o), "script"), l && ve(a), n) {
      c = 0;
      while (o = a[c++]) he.test(o.type || "") && n.push(o)
    }
    return f
  }! function () {
    var e = r.createDocumentFragment().appendChild(r.createElement("div")),
      t = r.createElement("input");
    t.setAttribute("type", "radio"), t.setAttribute("checked", "checked"), t.setAttribute("name", "t"), e.appendChild(t), h.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", h.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
  }();
  var be = r.documentElement,
    we = /^key/,
    Te = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    Ce = /^([^.]*)(?:\.(.+)|)/;

  function Ee() {
    return !0
  }

  function ke() {
    return !1
  }

  function Se() {
    try {
      return r.activeElement
    } catch (e) {}
  }

  function De(e, t, n, r, i, o) {
    var a, s;
    if ("object" == typeof t) {
      "string" != typeof n && (r = r || n, n = void 0);
      for (s in t) De(e, s, n, r, t[s], o);
      return e
    }
    if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = ke;
    else if (!i) return e;
    return 1 === o && (a = i, (i = function (e) {
      return w().off(e), a.apply(this, arguments)
    }).guid = a.guid || (a.guid = w.guid++)), e.each(function () {
      w.event.add(this, t, i, r, n)
    })
  }
  w.event = {
    global: {},
    add: function (e, t, n, r, i) {
      var o, a, s, u, l, c, f, p, d, h, g, y = J.get(e);
      if (y) {
        n.handler && (n = (o = n).handler, i = o.selector), i && w.find.matchesSelector(be, i), n.guid || (n.guid = w.guid++), (u = y.events) || (u = y.events = {}), (a = y.handle) || (a = y.handle = function (t) {
          return "undefined" != typeof w && w.event.triggered !== t.type ? w.event.dispatch.apply(e, arguments) : void 0
        }), l = (t = (t || "").match(M) || [""]).length;
        while (l--) d = g = (s = Ce.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = w.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = w.event.special[d] || {}, c = w.extend({
          type: d,
          origType: g,
          data: r,
          handler: n,
          guid: n.guid,
          selector: i,
          needsContext: i && w.expr.match.needsContext.test(i),
          namespace: h.join(".")
        }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(e, r, h, a) || e.addEventListener && e.addEventListener(d, a)), f.add && (f.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), w.event.global[d] = !0)
      }
    },
    remove: function (e, t, n, r, i) {
      var o, a, s, u, l, c, f, p, d, h, g, y = J.hasData(e) && J.get(e);
      if (y && (u = y.events)) {
        l = (t = (t || "").match(M) || [""]).length;
        while (l--)
          if (s = Ce.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
            f = w.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;
            while (o--) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
            a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, y.handle) || w.removeEvent(e, d, y.handle), delete u[d])
          } else
            for (d in u) w.event.remove(e, d + t[l], n, r, !0);
        w.isEmptyObject(u) && J.remove(e, "handle events")
      }
    },
    dispatch: function (e) {
      var t = w.event.fix(e),
        n, r, i, o, a, s, u = new Array(arguments.length),
        l = (J.get(this, "events") || {})[t.type] || [],
        c = w.event.special[t.type] || {};
      for (u[0] = t, n = 1; n < arguments.length; n++) u[n] = arguments[n];
      if (t.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, t)) {
        s = w.event.handlers.call(this, t, l), n = 0;
        while ((o = s[n++]) && !t.isPropagationStopped()) {
          t.currentTarget = o.elem, r = 0;
          while ((a = o.handlers[r++]) && !t.isImmediatePropagationStopped()) t.rnamespace && !t.rnamespace.test(a.namespace) || (t.handleObj = a, t.data = a.data, void 0 !== (i = ((w.event.special[a.origType] || {}).handle || a.handler).apply(o.elem, u)) && !1 === (t.result = i) && (t.preventDefault(), t.stopPropagation()))
        }
        return c.postDispatch && c.postDispatch.call(this, t), t.result
      }
    },
    handlers: function (e, t) {
      var n, r, i, o, a, s = [],
        u = t.delegateCount,
        l = e.target;
      if (u && l.nodeType && !("click" === e.type && e.button >= 1))
        for (; l !== this; l = l.parentNode || this)
          if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
            for (o = [], a = {}, n = 0; n < u; n++) void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? w(i, this).index(l) > -1 : w.find(i, this, null, [l]).length), a[i] && o.push(r);
            o.length && s.push({
              elem: l,
              handlers: o
            })
          } return l = this, u < t.length && s.push({
        elem: l,
        handlers: t.slice(u)
      }), s
    },
    addProp: function (e, t) {
      Object.defineProperty(w.Event.prototype, e, {
        enumerable: !0,
        configurable: !0,
        get: g(t) ? function () {
          if (this.originalEvent) return t(this.originalEvent)
        } : function () {
          if (this.originalEvent) return this.originalEvent[e]
        },
        set: function (t) {
          Object.defineProperty(this, e, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: t
          })
        }
      })
    },
    fix: function (e) {
      return e[w.expando] ? e : new w.Event(e)
    },
    special: {
      load: {
        noBubble: !0
      },
      focus: {
        trigger: function () {
          if (this !== Se() && this.focus) return this.focus(), !1
        },
        delegateType: "focusin"
      },
      blur: {
        trigger: function () {
          if (this === Se() && this.blur) return this.blur(), !1
        },
        delegateType: "focusout"
      },
      click: {
        trigger: function () {
          if ("checkbox" === this.type && this.click && N(this, "input")) return this.click(), !1
        },
        _default: function (e) {
          return N(e.target, "a")
        }
      },
      beforeunload: {
        postDispatch: function (e) {
          void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
        }
      }
    }
  }, w.removeEvent = function (e, t, n) {
    e.removeEventListener && e.removeEventListener(t, n)
  }, w.Event = function (e, t) {
    if (!(this instanceof w.Event)) return new w.Event(e, t);
    e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Ee : ke, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && w.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[w.expando] = !0
  }, w.Event.prototype = {
    constructor: w.Event,
    isDefaultPrevented: ke,
    isPropagationStopped: ke,
    isImmediatePropagationStopped: ke,
    isSimulated: !1,
    preventDefault: function () {
      var e = this.originalEvent;
      this.isDefaultPrevented = Ee, e && !this.isSimulated && e.preventDefault()
    },
    stopPropagation: function () {
      var e = this.originalEvent;
      this.isPropagationStopped = Ee, e && !this.isSimulated && e.stopPropagation()
    },
    stopImmediatePropagation: function () {
      var e = this.originalEvent;
      this.isImmediatePropagationStopped = Ee, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
    }
  }, w.each({
    altKey: !0,
    bubbles: !0,
    cancelable: !0,
    changedTouches: !0,
    ctrlKey: !0,
    detail: !0,
    eventPhase: !0,
    metaKey: !0,
    pageX: !0,
    pageY: !0,
    shiftKey: !0,
    view: !0,
    "char": !0,
    charCode: !0,
    key: !0,
    keyCode: !0,
    button: !0,
    buttons: !0,
    clientX: !0,
    clientY: !0,
    offsetX: !0,
    offsetY: !0,
    pointerId: !0,
    pointerType: !0,
    screenX: !0,
    screenY: !0,
    targetTouches: !0,
    toElement: !0,
    touches: !0,
    which: function (e) {
      var t = e.button;
      return null == e.which && we.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Te.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
    }
  }, w.event.addProp), w.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (e, t) {
    w.event.special[e] = {
      delegateType: t,
      bindType: t,
      handle: function (e) {
        var n, r = this,
          i = e.relatedTarget,
          o = e.handleObj;
        return i && (i === r || w.contains(r, i)) || (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
      }
    }
  }), w.fn.extend({
    on: function (e, t, n, r) {
      return De(this, e, t, n, r)
    },
    one: function (e, t, n, r) {
      return De(this, e, t, n, r, 1)
    },
    off: function (e, t, n) {
      var r, i;
      if (e && e.preventDefault && e.handleObj) return r = e.handleObj, w(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
      if ("object" == typeof e) {
        for (i in e) this.off(i, t, e[i]);
        return this
      }
      return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = ke), this.each(function () {
        w.event.remove(this, e, n, t)
      })
    }
  });
  var Ne = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
    Ae = /<script|<style|<link/i,
    je = /checked\s*(?:[^=]|=\s*.checked.)/i,
    qe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function Le(e, t) {
    return N(e, "table") && N(11 !== t.nodeType ? t : t.firstChild, "tr") ? w(e).children("tbody")[0] || e : e
  }

  function He(e) {
    return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
  }

  function Oe(e) {
    return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e
  }

  function Pe(e, t) {
    var n, r, i, o, a, s, u, l;
    if (1 === t.nodeType) {
      if (J.hasData(e) && (o = J.access(e), a = J.set(t, o), l = o.events)) {
        delete a.handle, a.events = {};
        for (i in l)
          for (n = 0, r = l[i].length; n < r; n++) w.event.add(t, i, l[i][n])
      }
      K.hasData(e) && (s = K.access(e), u = w.extend({}, s), K.set(t, u))
    }
  }

  function Me(e, t) {
    var n = t.nodeName.toLowerCase();
    "input" === n && pe.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
  }

  function Re(e, t, n, r) {
    t = a.apply([], t);
    var i, o, s, u, l, c, f = 0,
      p = e.length,
      d = p - 1,
      y = t[0],
      v = g(y);
    if (v || p > 1 && "string" == typeof y && !h.checkClone && je.test(y)) return e.each(function (i) {
      var o = e.eq(i);
      v && (t[0] = y.call(this, i, o.html())), Re(o, t, n, r)
    });
    if (p && (i = xe(t, e[0].ownerDocument, !1, e, r), o = i.firstChild, 1 === i.childNodes.length && (i = o), o || r)) {
      for (u = (s = w.map(ye(i, "script"), He)).length; f < p; f++) l = i, f !== d && (l = w.clone(l, !0, !0), u && w.merge(s, ye(l, "script"))), n.call(e[f], l, f);
      if (u)
        for (c = s[s.length - 1].ownerDocument, w.map(s, Oe), f = 0; f < u; f++) l = s[f], he.test(l.type || "") && !J.access(l, "globalEval") && w.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? w._evalUrl && w._evalUrl(l.src) : m(l.textContent.replace(qe, ""), c, l))
    }
    return e
  }

  function Ie(e, t, n) {
    for (var r, i = t ? w.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || w.cleanData(ye(r)), r.parentNode && (n && w.contains(r.ownerDocument, r) && ve(ye(r, "script")), r.parentNode.removeChild(r));
    return e
  }
  w.extend({
    htmlPrefilter: function (e) {
      return e.replace(Ne, "<$1></$2>")
    },
    clone: function (e, t, n) {
      var r, i, o, a, s = e.cloneNode(!0),
        u = w.contains(e.ownerDocument, e);
      if (!(h.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || w.isXMLDoc(e)))
        for (a = ye(s), r = 0, i = (o = ye(e)).length; r < i; r++) Me(o[r], a[r]);
      if (t)
        if (n)
          for (o = o || ye(e), a = a || ye(s), r = 0, i = o.length; r < i; r++) Pe(o[r], a[r]);
        else Pe(e, s);
      return (a = ye(s, "script")).length > 0 && ve(a, !u && ye(e, "script")), s
    },
    cleanData: function (e) {
      for (var t, n, r, i = w.event.special, o = 0; void 0 !== (n = e[o]); o++)
        if (Y(n)) {
          if (t = n[J.expando]) {
            if (t.events)
              for (r in t.events) i[r] ? w.event.remove(n, r) : w.removeEvent(n, r, t.handle);
            n[J.expando] = void 0
          }
          n[K.expando] && (n[K.expando] = void 0)
        }
    }
  }), w.fn.extend({
    detach: function (e) {
      return Ie(this, e, !0)
    },
    remove: function (e) {
      return Ie(this, e)
    },
    text: function (e) {
      return z(this, function (e) {
        return void 0 === e ? w.text(this) : this.empty().each(function () {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
        })
      }, null, e, arguments.length)
    },
    append: function () {
      return Re(this, arguments, function (e) {
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Le(this, e).appendChild(e)
      })
    },
    prepend: function () {
      return Re(this, arguments, function (e) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var t = Le(this, e);
          t.insertBefore(e, t.firstChild)
        }
      })
    },
    before: function () {
      return Re(this, arguments, function (e) {
        this.parentNode && this.parentNode.insertBefore(e, this)
      })
    },
    after: function () {
      return Re(this, arguments, function (e) {
        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
      })
    },
    empty: function () {
      for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (w.cleanData(ye(e, !1)), e.textContent = "");
      return this
    },
    clone: function (e, t) {
      return e = null != e && e, t = null == t ? e : t, this.map(function () {
        return w.clone(this, e, t)
      })
    },
    html: function (e) {
      return z(this, function (e) {
        var t = this[0] || {},
          n = 0,
          r = this.length;
        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
        if ("string" == typeof e && !Ae.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
          e = w.htmlPrefilter(e);
          try {
            for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (w.cleanData(ye(t, !1)), t.innerHTML = e);
            t = 0
          } catch (e) {}
        }
        t && this.empty().append(e)
      }, null, e, arguments.length)
    },
    replaceWith: function () {
      var e = [];
      return Re(this, arguments, function (t) {
        var n = this.parentNode;
        w.inArray(this, e) < 0 && (w.cleanData(ye(this)), n && n.replaceChild(t, this))
      }, e)
    }
  }), w.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (e, t) {
    w.fn[e] = function (e) {
      for (var n, r = [], i = w(e), o = i.length - 1, a = 0; a <= o; a++) n = a === o ? this : this.clone(!0), w(i[a])[t](n), s.apply(r, n.get());
      return this.pushStack(r)
    }
  });
  var We = new RegExp("^(" + re + ")(?!px)[a-z%]+$", "i"),
    $e = function (t) {
      var n = t.ownerDocument.defaultView;
      return n && n.opener || (n = e), n.getComputedStyle(t)
    },
    Be = new RegExp(oe.join("|"), "i");
  ! function () {
    function t() {
      if (c) {
        l.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", c.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", be.appendChild(l).appendChild(c);
        var t = e.getComputedStyle(c);
        i = "1%" !== t.top, u = 12 === n(t.marginLeft), c.style.right = "60%", s = 36 === n(t.right), o = 36 === n(t.width), c.style.position = "absolute", a = 36 === c.offsetWidth || "absolute", be.removeChild(l), c = null
      }
    }

    function n(e) {
      return Math.round(parseFloat(e))
    }
    var i, o, a, s, u, l = r.createElement("div"),
      c = r.createElement("div");
    c.style && (c.style.backgroundClip = "content-box", c.cloneNode(!0).style.backgroundClip = "", h.clearCloneStyle = "content-box" === c.style.backgroundClip, w.extend(h, {
      boxSizingReliable: function () {
        return t(), o
      },
      pixelBoxStyles: function () {
        return t(), s
      },
      pixelPosition: function () {
        return t(), i
      },
      reliableMarginLeft: function () {
        return t(), u
      },
      scrollboxSize: function () {
        return t(), a
      }
    }))
  }();

  function Fe(e, t, n) {
    var r, i, o, a, s = e.style;
    return (n = n || $e(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || w.contains(e.ownerDocument, e) || (a = w.style(e, t)), !h.pixelBoxStyles() && We.test(a) && Be.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
  }

  function _e(e, t) {
    return {
      get: function () {
        if (!e()) return (this.get = t).apply(this, arguments);
        delete this.get
      }
    }
  }
  var ze = /^(none|table(?!-c[ea]).+)/,
    Xe = /^--/,
    Ue = {
      position: "absolute",
      visibility: "hidden",
      display: "block"
    },
    Ve = {
      letterSpacing: "0",
      fontWeight: "400"
    },
    Ge = ["Webkit", "Moz", "ms"],
    Ye = r.createElement("div").style;

  function Qe(e) {
    if (e in Ye) return e;
    var t = e[0].toUpperCase() + e.slice(1),
      n = Ge.length;
    while (n--)
      if ((e = Ge[n] + t) in Ye) return e
  }

  function Je(e) {
    var t = w.cssProps[e];
    return t || (t = w.cssProps[e] = Qe(e) || e), t
  }

  function Ke(e, t, n) {
    var r = ie.exec(t);
    return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
  }

  function Ze(e, t, n, r, i, o) {
    var a = "width" === t ? 1 : 0,
      s = 0,
      u = 0;
    if (n === (r ? "border" : "content")) return 0;
    for (; a < 4; a += 2) "margin" === n && (u += w.css(e, n + oe[a], !0, i)), r ? ("content" === n && (u -= w.css(e, "padding" + oe[a], !0, i)), "margin" !== n && (u -= w.css(e, "border" + oe[a] + "Width", !0, i))) : (u += w.css(e, "padding" + oe[a], !0, i), "padding" !== n ? u += w.css(e, "border" + oe[a] + "Width", !0, i) : s += w.css(e, "border" + oe[a] + "Width", !0, i));
    return !r && o >= 0 && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5))), u
  }

  function et(e, t, n) {
    var r = $e(e),
      i = Fe(e, t, r),
      o = "border-box" === w.css(e, "boxSizing", !1, r),
      a = o;
    if (We.test(i)) {
      if (!n) return i;
      i = "auto"
    }
    return a = a && (h.boxSizingReliable() || i === e.style[t]), ("auto" === i || !parseFloat(i) && "inline" === w.css(e, "display", !1, r)) && (i = e["offset" + t[0].toUpperCase() + t.slice(1)], a = !0), (i = parseFloat(i) || 0) + Ze(e, t, n || (o ? "border" : "content"), a, r, i) + "px"
  }
  w.extend({
    cssHooks: {
      opacity: {
        get: function (e, t) {
          if (t) {
            var n = Fe(e, "opacity");
            return "" === n ? "1" : n
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {},
    style: function (e, t, n, r) {
      if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
        var i, o, a, s = G(t),
          u = Xe.test(t),
          l = e.style;
        if (u || (t = Je(s)), a = w.cssHooks[t] || w.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
        "string" == (o = typeof n) && (i = ie.exec(n)) && i[1] && (n = ue(e, t, i), o = "number"), null != n && n === n && ("number" === o && (n += i && i[3] || (w.cssNumber[s] ? "" : "px")), h.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))
      }
    },
    css: function (e, t, n, r) {
      var i, o, a, s = G(t);
      return Xe.test(t) || (t = Je(s)), (a = w.cssHooks[t] || w.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = Fe(e, t, r)), "normal" === i && t in Ve && (i = Ve[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
    }
  }), w.each(["height", "width"], function (e, t) {
    w.cssHooks[t] = {
      get: function (e, n, r) {
        if (n) return !ze.test(w.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? et(e, t, r) : se(e, Ue, function () {
          return et(e, t, r)
        })
      },
      set: function (e, n, r) {
        var i, o = $e(e),
          a = "border-box" === w.css(e, "boxSizing", !1, o),
          s = r && Ze(e, t, r, a, o);
        return a && h.scrollboxSize() === o.position && (s -= Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - parseFloat(o[t]) - Ze(e, t, "border", !1, o) - .5)), s && (i = ie.exec(n)) && "px" !== (i[3] || "px") && (e.style[t] = n, n = w.css(e, t)), Ke(e, n, s)
      }
    }
  }), w.cssHooks.marginLeft = _e(h.reliableMarginLeft, function (e, t) {
    if (t) return (parseFloat(Fe(e, "marginLeft")) || e.getBoundingClientRect().left - se(e, {
      marginLeft: 0
    }, function () {
      return e.getBoundingClientRect().left
    })) + "px"
  }), w.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (e, t) {
    w.cssHooks[e + t] = {
      expand: function (n) {
        for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; r < 4; r++) i[e + oe[r] + t] = o[r] || o[r - 2] || o[0];
        return i
      }
    }, "margin" !== e && (w.cssHooks[e + t].set = Ke)
  }), w.fn.extend({
    css: function (e, t) {
      return z(this, function (e, t, n) {
        var r, i, o = {},
          a = 0;
        if (Array.isArray(t)) {
          for (r = $e(e), i = t.length; a < i; a++) o[t[a]] = w.css(e, t[a], !1, r);
          return o
        }
        return void 0 !== n ? w.style(e, t, n) : w.css(e, t)
      }, e, t, arguments.length > 1)
    }
  });

  function tt(e, t, n, r, i) {
    return new tt.prototype.init(e, t, n, r, i)
  }
  w.Tween = tt, tt.prototype = {
    constructor: tt,
    init: function (e, t, n, r, i, o) {
      this.elem = e, this.prop = n, this.easing = i || w.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (w.cssNumber[n] ? "" : "px")
    },
    cur: function () {
      var e = tt.propHooks[this.prop];
      return e && e.get ? e.get(this) : tt.propHooks._default.get(this)
    },
    run: function (e) {
      var t, n = tt.propHooks[this.prop];
      return this.options.duration ? this.pos = t = w.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : tt.propHooks._default.set(this), this
    }
  }, tt.prototype.init.prototype = tt.prototype, tt.propHooks = {
    _default: {
      get: function (e) {
        var t;
        return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = w.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
      },
      set: function (e) {
        w.fx.step[e.prop] ? w.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[w.cssProps[e.prop]] && !w.cssHooks[e.prop] ? e.elem[e.prop] = e.now : w.style(e.elem, e.prop, e.now + e.unit)
      }
    }
  }, tt.propHooks.scrollTop = tt.propHooks.scrollLeft = {
    set: function (e) {
      e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
    }
  }, w.easing = {
    linear: function (e) {
      return e
    },
    swing: function (e) {
      return .5 - Math.cos(e * Math.PI) / 2
    },
    _default: "swing"
  }, w.fx = tt.prototype.init, w.fx.step = {};
  var nt, rt, it = /^(?:toggle|show|hide)$/,
    ot = /queueHooks$/;

  function at() {
    rt && (!1 === r.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(at) : e.setTimeout(at, w.fx.interval), w.fx.tick())
  }

  function st() {
    return e.setTimeout(function () {
      nt = void 0
    }), nt = Date.now()
  }

  function ut(e, t) {
    var n, r = 0,
      i = {
        height: e
      };
    for (t = t ? 1 : 0; r < 4; r += 2 - t) i["margin" + (n = oe[r])] = i["padding" + n] = e;
    return t && (i.opacity = i.width = e), i
  }

  function lt(e, t, n) {
    for (var r, i = (pt.tweeners[t] || []).concat(pt.tweeners["*"]), o = 0, a = i.length; o < a; o++)
      if (r = i[o].call(n, t, e)) return r
  }

  function ct(e, t, n) {
    var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t,
      p = this,
      d = {},
      h = e.style,
      g = e.nodeType && ae(e),
      y = J.get(e, "fxshow");
    n.queue || (null == (a = w._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function () {
      a.unqueued || s()
    }), a.unqueued++, p.always(function () {
      p.always(function () {
        a.unqueued--, w.queue(e, "fx").length || a.empty.fire()
      })
    }));
    for (r in t)
      if (i = t[r], it.test(i)) {
        if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
          if ("show" !== i || !y || void 0 === y[r]) continue;
          g = !0
        }
        d[r] = y && y[r] || w.style(e, r)
      } if ((u = !w.isEmptyObject(t)) || !w.isEmptyObject(d)) {
      f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = y && y.display) && (l = J.get(e, "display")), "none" === (c = w.css(e, "display")) && (l ? c = l : (fe([e], !0), l = e.style.display || l, c = w.css(e, "display"), fe([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === w.css(e, "float") && (u || (p.done(function () {
        h.display = l
      }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
        h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
      })), u = !1;
      for (r in d) u || (y ? "hidden" in y && (g = y.hidden) : y = J.access(e, "fxshow", {
        display: l
      }), o && (y.hidden = !g), g && fe([e], !0), p.done(function () {
        g || fe([e]), J.remove(e, "fxshow");
        for (r in d) w.style(e, r, d[r])
      })), u = lt(g ? y[r] : 0, r, p), r in y || (y[r] = u.start, g && (u.end = u.start, u.start = 0))
    }
  }

  function ft(e, t) {
    var n, r, i, o, a;
    for (n in e)
      if (r = G(n), i = t[r], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = w.cssHooks[r]) && "expand" in a) {
        o = a.expand(o), delete e[r];
        for (n in o) n in e || (e[n] = o[n], t[n] = i)
      } else t[r] = i
  }

  function pt(e, t, n) {
    var r, i, o = 0,
      a = pt.prefilters.length,
      s = w.Deferred().always(function () {
        delete u.elem
      }),
      u = function () {
        if (i) return !1;
        for (var t = nt || st(), n = Math.max(0, l.startTime + l.duration - t), r = 1 - (n / l.duration || 0), o = 0, a = l.tweens.length; o < a; o++) l.tweens[o].run(r);
        return s.notifyWith(e, [l, r, n]), r < 1 && a ? n : (a || s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l]), !1)
      },
      l = s.promise({
        elem: e,
        props: w.extend({}, t),
        opts: w.extend(!0, {
          specialEasing: {},
          easing: w.easing._default
        }, n),
        originalProperties: t,
        originalOptions: n,
        startTime: nt || st(),
        duration: n.duration,
        tweens: [],
        createTween: function (t, n) {
          var r = w.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
          return l.tweens.push(r), r
        },
        stop: function (t) {
          var n = 0,
            r = t ? l.tweens.length : 0;
          if (i) return this;
          for (i = !0; n < r; n++) l.tweens[n].run(1);
          return t ? (s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l, t])) : s.rejectWith(e, [l, t]), this
        }
      }),
      c = l.props;
    for (ft(c, l.opts.specialEasing); o < a; o++)
      if (r = pt.prefilters[o].call(l, e, c, l.opts)) return g(r.stop) && (w._queueHooks(l.elem, l.opts.queue).stop = r.stop.bind(r)), r;
    return w.map(c, lt, l), g(l.opts.start) && l.opts.start.call(e, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), w.fx.timer(w.extend(u, {
      elem: e,
      anim: l,
      queue: l.opts.queue
    })), l
  }
  w.Animation = w.extend(pt, {
      tweeners: {
        "*": [function (e, t) {
          var n = this.createTween(e, t);
          return ue(n.elem, e, ie.exec(t), n), n
        }]
      },
      tweener: function (e, t) {
        g(e) ? (t = e, e = ["*"]) : e = e.match(M);
        for (var n, r = 0, i = e.length; r < i; r++) n = e[r], pt.tweeners[n] = pt.tweeners[n] || [], pt.tweeners[n].unshift(t)
      },
      prefilters: [ct],
      prefilter: function (e, t) {
        t ? pt.prefilters.unshift(e) : pt.prefilters.push(e)
      }
    }), w.speed = function (e, t, n) {
      var r = e && "object" == typeof e ? w.extend({}, e) : {
        complete: n || !n && t || g(e) && e,
        duration: e,
        easing: n && t || t && !g(t) && t
      };
      return w.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in w.fx.speeds ? r.duration = w.fx.speeds[r.duration] : r.duration = w.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function () {
        g(r.old) && r.old.call(this), r.queue && w.dequeue(this, r.queue)
      }, r
    }, w.fn.extend({
      fadeTo: function (e, t, n, r) {
        return this.filter(ae).css("opacity", 0).show().end().animate({
          opacity: t
        }, e, n, r)
      },
      animate: function (e, t, n, r) {
        var i = w.isEmptyObject(e),
          o = w.speed(t, n, r),
          a = function () {
            var t = pt(this, w.extend({}, e), o);
            (i || J.get(this, "finish")) && t.stop(!0)
          };
        return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
      },
      stop: function (e, t, n) {
        var r = function (e) {
          var t = e.stop;
          delete e.stop, t(n)
        };
        return "string" != typeof e && (n = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function () {
          var t = !0,
            i = null != e && e + "queueHooks",
            o = w.timers,
            a = J.get(this);
          if (i) a[i] && a[i].stop && r(a[i]);
          else
            for (i in a) a[i] && a[i].stop && ot.test(i) && r(a[i]);
          for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
          !t && n || w.dequeue(this, e)
        })
      },
      finish: function (e) {
        return !1 !== e && (e = e || "fx"), this.each(function () {
          var t, n = J.get(this),
            r = n[e + "queue"],
            i = n[e + "queueHooks"],
            o = w.timers,
            a = r ? r.length : 0;
          for (n.finish = !0, w.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
          for (t = 0; t < a; t++) r[t] && r[t].finish && r[t].finish.call(this);
          delete n.finish
        })
      }
    }), w.each(["toggle", "show", "hide"], function (e, t) {
      var n = w.fn[t];
      w.fn[t] = function (e, r, i) {
        return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(ut(t, !0), e, r, i)
      }
    }), w.each({
      slideDown: ut("show"),
      slideUp: ut("hide"),
      slideToggle: ut("toggle"),
      fadeIn: {
        opacity: "show"
      },
      fadeOut: {
        opacity: "hide"
      },
      fadeToggle: {
        opacity: "toggle"
      }
    }, function (e, t) {
      w.fn[e] = function (e, n, r) {
        return this.animate(t, e, n, r)
      }
    }), w.timers = [], w.fx.tick = function () {
      var e, t = 0,
        n = w.timers;
      for (nt = Date.now(); t < n.length; t++)(e = n[t])() || n[t] !== e || n.splice(t--, 1);
      n.length || w.fx.stop(), nt = void 0
    }, w.fx.timer = function (e) {
      w.timers.push(e), w.fx.start()
    }, w.fx.interval = 13, w.fx.start = function () {
      rt || (rt = !0, at())
    }, w.fx.stop = function () {
      rt = null
    }, w.fx.speeds = {
      slow: 600,
      fast: 200,
      _default: 400
    }, w.fn.delay = function (t, n) {
      return t = w.fx ? w.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function (n, r) {
        var i = e.setTimeout(n, t);
        r.stop = function () {
          e.clearTimeout(i)
        }
      })
    },
    function () {
      var e = r.createElement("input"),
        t = r.createElement("select").appendChild(r.createElement("option"));
      e.type = "checkbox", h.checkOn = "" !== e.value, h.optSelected = t.selected, (e = r.createElement("input")).value = "t", e.type = "radio", h.radioValue = "t" === e.value
    }();
  var dt, ht = w.expr.attrHandle;
  w.fn.extend({
    attr: function (e, t) {
      return z(this, w.attr, e, t, arguments.length > 1)
    },
    removeAttr: function (e) {
      return this.each(function () {
        w.removeAttr(this, e)
      })
    }
  }), w.extend({
    attr: function (e, t, n) {
      var r, i, o = e.nodeType;
      if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? w.prop(e, t, n) : (1 === o && w.isXMLDoc(e) || (i = w.attrHooks[t.toLowerCase()] || (w.expr.match.bool.test(t) ? dt : void 0)), void 0 !== n ? null === n ? void w.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = w.find.attr(e, t)) ? void 0 : r)
    },
    attrHooks: {
      type: {
        set: function (e, t) {
          if (!h.radioValue && "radio" === t && N(e, "input")) {
            var n = e.value;
            return e.setAttribute("type", t), n && (e.value = n), t
          }
        }
      }
    },
    removeAttr: function (e, t) {
      var n, r = 0,
        i = t && t.match(M);
      if (i && 1 === e.nodeType)
        while (n = i[r++]) e.removeAttribute(n)
    }
  }), dt = {
    set: function (e, t, n) {
      return !1 === t ? w.removeAttr(e, n) : e.setAttribute(n, n), n
    }
  }, w.each(w.expr.match.bool.source.match(/\w+/g), function (e, t) {
    var n = ht[t] || w.find.attr;
    ht[t] = function (e, t, r) {
      var i, o, a = t.toLowerCase();
      return r || (o = ht[a], ht[a] = i, i = null != n(e, t, r) ? a : null, ht[a] = o), i
    }
  });
  var gt = /^(?:input|select|textarea|button)$/i,
    yt = /^(?:a|area)$/i;
  w.fn.extend({
    prop: function (e, t) {
      return z(this, w.prop, e, t, arguments.length > 1)
    },
    removeProp: function (e) {
      return this.each(function () {
        delete this[w.propFix[e] || e]
      })
    }
  }), w.extend({
    prop: function (e, t, n) {
      var r, i, o = e.nodeType;
      if (3 !== o && 8 !== o && 2 !== o) return 1 === o && w.isXMLDoc(e) || (t = w.propFix[t] || t, i = w.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
    },
    propHooks: {
      tabIndex: {
        get: function (e) {
          var t = w.find.attr(e, "tabindex");
          return t ? parseInt(t, 10) : gt.test(e.nodeName) || yt.test(e.nodeName) && e.href ? 0 : -1
        }
      }
    },
    propFix: {
      "for": "htmlFor",
      "class": "className"
    }
  }), h.optSelected || (w.propHooks.selected = {
    get: function (e) {
      var t = e.parentNode;
      return t && t.parentNode && t.parentNode.selectedIndex, null
    },
    set: function (e) {
      var t = e.parentNode;
      t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
    }
  }), w.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    w.propFix[this.toLowerCase()] = this
  });

  function vt(e) {
    return (e.match(M) || []).join(" ")
  }

  function mt(e) {
    return e.getAttribute && e.getAttribute("class") || ""
  }

  function xt(e) {
    return Array.isArray(e) ? e : "string" == typeof e ? e.match(M) || [] : []
  }
  w.fn.extend({
    addClass: function (e) {
      var t, n, r, i, o, a, s, u = 0;
      if (g(e)) return this.each(function (t) {
        w(this).addClass(e.call(this, t, mt(this)))
      });
      if ((t = xt(e)).length)
        while (n = this[u++])
          if (i = mt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
            a = 0;
            while (o = t[a++]) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
            i !== (s = vt(r)) && n.setAttribute("class", s)
          } return this
    },
    removeClass: function (e) {
      var t, n, r, i, o, a, s, u = 0;
      if (g(e)) return this.each(function (t) {
        w(this).removeClass(e.call(this, t, mt(this)))
      });
      if (!arguments.length) return this.attr("class", "");
      if ((t = xt(e)).length)
        while (n = this[u++])
          if (i = mt(n), r = 1 === n.nodeType && " " + vt(i) + " ") {
            a = 0;
            while (o = t[a++])
              while (r.indexOf(" " + o + " ") > -1) r = r.replace(" " + o + " ", " ");
            i !== (s = vt(r)) && n.setAttribute("class", s)
          } return this
    },
    toggleClass: function (e, t) {
      var n = typeof e,
        r = "string" === n || Array.isArray(e);
      return "boolean" == typeof t && r ? t ? this.addClass(e) : this.removeClass(e) : g(e) ? this.each(function (n) {
        w(this).toggleClass(e.call(this, n, mt(this), t), t)
      }) : this.each(function () {
        var t, i, o, a;
        if (r) {
          i = 0, o = w(this), a = xt(e);
          while (t = a[i++]) o.hasClass(t) ? o.removeClass(t) : o.addClass(t)
        } else void 0 !== e && "boolean" !== n || ((t = mt(this)) && J.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : J.get(this, "__className__") || ""))
      })
    },
    hasClass: function (e) {
      var t, n, r = 0;
      t = " " + e + " ";
      while (n = this[r++])
        if (1 === n.nodeType && (" " + vt(mt(n)) + " ").indexOf(t) > -1) return !0;
      return !1
    }
  });
  var bt = /\r/g;
  w.fn.extend({
    val: function (e) {
      var t, n, r, i = this[0]; {
        if (arguments.length) return r = g(e), this.each(function (n) {
          var i;
          1 === this.nodeType && (null == (i = r ? e.call(this, n, w(this).val()) : e) ? i = "" : "number" == typeof i ? i += "" : Array.isArray(i) && (i = w.map(i, function (e) {
            return null == e ? "" : e + ""
          })), (t = w.valHooks[this.type] || w.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
        });
        if (i) return (t = w.valHooks[i.type] || w.valHooks[i.nodeName.toLowerCase()]) && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : "string" == typeof (n = i.value) ? n.replace(bt, "") : null == n ? "" : n
      }
    }
  }), w.extend({
    valHooks: {
      option: {
        get: function (e) {
          var t = w.find.attr(e, "value");
          return null != t ? t : vt(w.text(e))
        }
      },
      select: {
        get: function (e) {
          var t, n, r, i = e.options,
            o = e.selectedIndex,
            a = "select-one" === e.type,
            s = a ? null : [],
            u = a ? o + 1 : i.length;
          for (r = o < 0 ? u : a ? o : 0; r < u; r++)
            if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !N(n.parentNode, "optgroup"))) {
              if (t = w(n).val(), a) return t;
              s.push(t)
            } return s
        },
        set: function (e, t) {
          var n, r, i = e.options,
            o = w.makeArray(t),
            a = i.length;
          while (a--)((r = i[a]).selected = w.inArray(w.valHooks.option.get(r), o) > -1) && (n = !0);
          return n || (e.selectedIndex = -1), o
        }
      }
    }
  }), w.each(["radio", "checkbox"], function () {
    w.valHooks[this] = {
      set: function (e, t) {
        if (Array.isArray(t)) return e.checked = w.inArray(w(e).val(), t) > -1
      }
    }, h.checkOn || (w.valHooks[this].get = function (e) {
      return null === e.getAttribute("value") ? "on" : e.value
    })
  }), h.focusin = "onfocusin" in e;
  var wt = /^(?:focusinfocus|focusoutblur)$/,
    Tt = function (e) {
      e.stopPropagation()
    };
  w.extend(w.event, {
    trigger: function (t, n, i, o) {
      var a, s, u, l, c, p, d, h, v = [i || r],
        m = f.call(t, "type") ? t.type : t,
        x = f.call(t, "namespace") ? t.namespace.split(".") : [];
      if (s = h = u = i = i || r, 3 !== i.nodeType && 8 !== i.nodeType && !wt.test(m + w.event.triggered) && (m.indexOf(".") > -1 && (m = (x = m.split(".")).shift(), x.sort()), c = m.indexOf(":") < 0 && "on" + m, t = t[w.expando] ? t : new w.Event(m, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = x.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + x.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : w.makeArray(n, [t]), d = w.event.special[m] || {}, o || !d.trigger || !1 !== d.trigger.apply(i, n))) {
        if (!o && !d.noBubble && !y(i)) {
          for (l = d.delegateType || m, wt.test(l + m) || (s = s.parentNode); s; s = s.parentNode) v.push(s), u = s;
          u === (i.ownerDocument || r) && v.push(u.defaultView || u.parentWindow || e)
        }
        a = 0;
        while ((s = v[a++]) && !t.isPropagationStopped()) h = s, t.type = a > 1 ? l : d.bindType || m, (p = (J.get(s, "events") || {})[t.type] && J.get(s, "handle")) && p.apply(s, n), (p = c && s[c]) && p.apply && Y(s) && (t.result = p.apply(s, n), !1 === t.result && t.preventDefault());
        return t.type = m, o || t.isDefaultPrevented() || d._default && !1 !== d._default.apply(v.pop(), n) || !Y(i) || c && g(i[m]) && !y(i) && ((u = i[c]) && (i[c] = null), w.event.triggered = m, t.isPropagationStopped() && h.addEventListener(m, Tt), i[m](), t.isPropagationStopped() && h.removeEventListener(m, Tt), w.event.triggered = void 0, u && (i[c] = u)), t.result
      }
    },
    simulate: function (e, t, n) {
      var r = w.extend(new w.Event, n, {
        type: e,
        isSimulated: !0
      });
      w.event.trigger(r, null, t)
    }
  }), w.fn.extend({
    trigger: function (e, t) {
      return this.each(function () {
        w.event.trigger(e, t, this)
      })
    },
    triggerHandler: function (e, t) {
      var n = this[0];
      if (n) return w.event.trigger(e, t, n, !0)
    }
  }), h.focusin || w.each({
    focus: "focusin",
    blur: "focusout"
  }, function (e, t) {
    var n = function (e) {
      w.event.simulate(t, e.target, w.event.fix(e))
    };
    w.event.special[t] = {
      setup: function () {
        var r = this.ownerDocument || this,
          i = J.access(r, t);
        i || r.addEventListener(e, n, !0), J.access(r, t, (i || 0) + 1)
      },
      teardown: function () {
        var r = this.ownerDocument || this,
          i = J.access(r, t) - 1;
        i ? J.access(r, t, i) : (r.removeEventListener(e, n, !0), J.remove(r, t))
      }
    }
  });
  var Ct = e.location,
    Et = Date.now(),
    kt = /\?/;
  w.parseXML = function (t) {
    var n;
    if (!t || "string" != typeof t) return null;
    try {
      n = (new e.DOMParser).parseFromString(t, "text/xml")
    } catch (e) {
      n = void 0
    }
    return n && !n.getElementsByTagName("parsererror").length || w.error("Invalid XML: " + t), n
  };
  var St = /\[\]$/,
    Dt = /\r?\n/g,
    Nt = /^(?:submit|button|image|reset|file)$/i,
    At = /^(?:input|select|textarea|keygen)/i;

  function jt(e, t, n, r) {
    var i;
    if (Array.isArray(t)) w.each(t, function (t, i) {
      n || St.test(e) ? r(e, i) : jt(e + "[" + ("object" == typeof i && null != i ? t : "") + "]", i, n, r)
    });
    else if (n || "object" !== x(t)) r(e, t);
    else
      for (i in t) jt(e + "[" + i + "]", t[i], n, r)
  }
  w.param = function (e, t) {
    var n, r = [],
      i = function (e, t) {
        var n = g(t) ? t() : t;
        r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
      };
    if (Array.isArray(e) || e.jquery && !w.isPlainObject(e)) w.each(e, function () {
      i(this.name, this.value)
    });
    else
      for (n in e) jt(n, e[n], t, i);
    return r.join("&")
  }, w.fn.extend({
    serialize: function () {
      return w.param(this.serializeArray())
    },
    serializeArray: function () {
      return this.map(function () {
        var e = w.prop(this, "elements");
        return e ? w.makeArray(e) : this
      }).filter(function () {
        var e = this.type;
        return this.name && !w(this).is(":disabled") && At.test(this.nodeName) && !Nt.test(e) && (this.checked || !pe.test(e))
      }).map(function (e, t) {
        var n = w(this).val();
        return null == n ? null : Array.isArray(n) ? w.map(n, function (e) {
          return {
            name: t.name,
            value: e.replace(Dt, "\r\n")
          }
        }) : {
          name: t.name,
          value: n.replace(Dt, "\r\n")
        }
      }).get()
    }
  });
  var qt = /%20/g,
    Lt = /#.*$/,
    Ht = /([?&])_=[^&]*/,
    Ot = /^(.*?):[ \t]*([^\r\n]*)$/gm,
    Pt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
    Mt = /^(?:GET|HEAD)$/,
    Rt = /^\/\//,
    It = {},
    Wt = {},
    $t = "*/".concat("*"),
    Bt = r.createElement("a");
  Bt.href = Ct.href;

  function Ft(e) {
    return function (t, n) {
      "string" != typeof t && (n = t, t = "*");
      var r, i = 0,
        o = t.toLowerCase().match(M) || [];
      if (g(n))
        while (r = o[i++]) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
    }
  }

  function _t(e, t, n, r) {
    var i = {},
      o = e === Wt;

    function a(s) {
      var u;
      return i[s] = !0, w.each(e[s] || [], function (e, s) {
        var l = s(t, n, r);
        return "string" != typeof l || o || i[l] ? o ? !(u = l) : void 0 : (t.dataTypes.unshift(l), a(l), !1)
      }), u
    }
    return a(t.dataTypes[0]) || !i["*"] && a("*")
  }

  function zt(e, t) {
    var n, r, i = w.ajaxSettings.flatOptions || {};
    for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
    return r && w.extend(!0, e, r), e
  }

  function Xt(e, t, n) {
    var r, i, o, a, s = e.contents,
      u = e.dataTypes;
    while ("*" === u[0]) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
    if (r)
      for (i in s)
        if (s[i] && s[i].test(r)) {
          u.unshift(i);
          break
        } if (u[0] in n) o = u[0];
    else {
      for (i in n) {
        if (!u[0] || e.converters[i + " " + u[0]]) {
          o = i;
          break
        }
        a || (a = i)
      }
      o = o || a
    }
    if (o) return o !== u[0] && u.unshift(o), n[o]
  }

  function Ut(e, t, n, r) {
    var i, o, a, s, u, l = {},
      c = e.dataTypes.slice();
    if (c[1])
      for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
    o = c.shift();
    while (o)
      if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())
        if ("*" === o) o = u;
        else if ("*" !== u && u !== o) {
      if (!(a = l[u + " " + o] || l["* " + o]))
        for (i in l)
          if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
            !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
            break
          } if (!0 !== a)
        if (a && e["throws"]) t = a(t);
        else try {
          t = a(t)
        } catch (e) {
          return {
            state: "parsererror",
            error: a ? e : "No conversion from " + u + " to " + o
          }
        }
    }
    return {
      state: "success",
      data: t
    }
  }
  w.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: Ct.href,
      type: "GET",
      isLocal: Pt.test(Ct.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": $t,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": w.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function (e, t) {
      return t ? zt(zt(e, w.ajaxSettings), t) : zt(w.ajaxSettings, e)
    },
    ajaxPrefilter: Ft(It),
    ajaxTransport: Ft(Wt),
    ajax: function (t, n) {
      "object" == typeof t && (n = t, t = void 0), n = n || {};
      var i, o, a, s, u, l, c, f, p, d, h = w.ajaxSetup({}, n),
        g = h.context || h,
        y = h.context && (g.nodeType || g.jquery) ? w(g) : w.event,
        v = w.Deferred(),
        m = w.Callbacks("once memory"),
        x = h.statusCode || {},
        b = {},
        T = {},
        C = "canceled",
        E = {
          readyState: 0,
          getResponseHeader: function (e) {
            var t;
            if (c) {
              if (!s) {
                s = {};
                while (t = Ot.exec(a)) s[t[1].toLowerCase()] = t[2]
              }
              t = s[e.toLowerCase()]
            }
            return null == t ? null : t
          },
          getAllResponseHeaders: function () {
            return c ? a : null
          },
          setRequestHeader: function (e, t) {
            return null == c && (e = T[e.toLowerCase()] = T[e.toLowerCase()] || e, b[e] = t), this
          },
          overrideMimeType: function (e) {
            return null == c && (h.mimeType = e), this
          },
          statusCode: function (e) {
            var t;
            if (e)
              if (c) E.always(e[E.status]);
              else
                for (t in e) x[t] = [x[t], e[t]];
            return this
          },
          abort: function (e) {
            var t = e || C;
            return i && i.abort(t), k(0, t), this
          }
        };
      if (v.promise(E), h.url = ((t || h.url || Ct.href) + "").replace(Rt, Ct.protocol + "//"), h.type = n.method || n.type || h.method || h.type, h.dataTypes = (h.dataType || "*").toLowerCase().match(M) || [""], null == h.crossDomain) {
        l = r.createElement("a");
        try {
          l.href = h.url, l.href = l.href, h.crossDomain = Bt.protocol + "//" + Bt.host != l.protocol + "//" + l.host
        } catch (e) {
          h.crossDomain = !0
        }
      }
      if (h.data && h.processData && "string" != typeof h.data && (h.data = w.param(h.data, h.traditional)), _t(It, h, n, E), c) return E;
      (f = w.event && h.global) && 0 == w.active++ && w.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Mt.test(h.type), o = h.url.replace(Lt, ""), h.hasContent ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(qt, "+")) : (d = h.url.slice(o.length), h.data && (h.processData || "string" == typeof h.data) && (o += (kt.test(o) ? "&" : "?") + h.data, delete h.data), !1 === h.cache && (o = o.replace(Ht, "$1"), d = (kt.test(o) ? "&" : "?") + "_=" + Et++ + d), h.url = o + d), h.ifModified && (w.lastModified[o] && E.setRequestHeader("If-Modified-Since", w.lastModified[o]), w.etag[o] && E.setRequestHeader("If-None-Match", w.etag[o])), (h.data && h.hasContent && !1 !== h.contentType || n.contentType) && E.setRequestHeader("Content-Type", h.contentType), E.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + $t + "; q=0.01" : "") : h.accepts["*"]);
      for (p in h.headers) E.setRequestHeader(p, h.headers[p]);
      if (h.beforeSend && (!1 === h.beforeSend.call(g, E, h) || c)) return E.abort();
      if (C = "abort", m.add(h.complete), E.done(h.success), E.fail(h.error), i = _t(Wt, h, n, E)) {
        if (E.readyState = 1, f && y.trigger("ajaxSend", [E, h]), c) return E;
        h.async && h.timeout > 0 && (u = e.setTimeout(function () {
          E.abort("timeout")
        }, h.timeout));
        try {
          c = !1, i.send(b, k)
        } catch (e) {
          if (c) throw e;
          k(-1, e)
        }
      } else k(-1, "No Transport");

      function k(t, n, r, s) {
        var l, p, d, b, T, C = n;
        c || (c = !0, u && e.clearTimeout(u), i = void 0, a = s || "", E.readyState = t > 0 ? 4 : 0, l = t >= 200 && t < 300 || 304 === t, r && (b = Xt(h, E, r)), b = Ut(h, b, E, l), l ? (h.ifModified && ((T = E.getResponseHeader("Last-Modified")) && (w.lastModified[o] = T), (T = E.getResponseHeader("etag")) && (w.etag[o] = T)), 204 === t || "HEAD" === h.type ? C = "nocontent" : 304 === t ? C = "notmodified" : (C = b.state, p = b.data, l = !(d = b.error))) : (d = C, !t && C || (C = "error", t < 0 && (t = 0))), E.status = t, E.statusText = (n || C) + "", l ? v.resolveWith(g, [p, C, E]) : v.rejectWith(g, [E, C, d]), E.statusCode(x), x = void 0, f && y.trigger(l ? "ajaxSuccess" : "ajaxError", [E, h, l ? p : d]), m.fireWith(g, [E, C]), f && (y.trigger("ajaxComplete", [E, h]), --w.active || w.event.trigger("ajaxStop")))
      }
      return E
    },
    getJSON: function (e, t, n) {
      return w.get(e, t, n, "json")
    },
    getScript: function (e, t) {
      return w.get(e, void 0, t, "script")
    }
  }), w.each(["get", "post"], function (e, t) {
    w[t] = function (e, n, r, i) {
      return g(n) && (i = i || r, r = n, n = void 0), w.ajax(w.extend({
        url: e,
        type: t,
        dataType: i,
        data: n,
        success: r
      }, w.isPlainObject(e) && e))
    }
  }), w._evalUrl = function (e) {
    return w.ajax({
      url: e,
      type: "GET",
      dataType: "script",
      cache: !0,
      async: !1,
      global: !1,
      "throws": !0
    })
  }, w.fn.extend({
    wrapAll: function (e) {
      var t;
      return this[0] && (g(e) && (e = e.call(this[0])), t = w(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
        var e = this;
        while (e.firstElementChild) e = e.firstElementChild;
        return e
      }).append(this)), this
    },
    wrapInner: function (e) {
      return g(e) ? this.each(function (t) {
        w(this).wrapInner(e.call(this, t))
      }) : this.each(function () {
        var t = w(this),
          n = t.contents();
        n.length ? n.wrapAll(e) : t.append(e)
      })
    },
    wrap: function (e) {
      var t = g(e);
      return this.each(function (n) {
        w(this).wrapAll(t ? e.call(this, n) : e)
      })
    },
    unwrap: function (e) {
      return this.parent(e).not("body").each(function () {
        w(this).replaceWith(this.childNodes)
      }), this
    }
  }), w.expr.pseudos.hidden = function (e) {
    return !w.expr.pseudos.visible(e)
  }, w.expr.pseudos.visible = function (e) {
    return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
  }, w.ajaxSettings.xhr = function () {
    try {
      return new e.XMLHttpRequest
    } catch (e) {}
  };
  var Vt = {
      0: 200,
      1223: 204
    },
    Gt = w.ajaxSettings.xhr();
  h.cors = !!Gt && "withCredentials" in Gt, h.ajax = Gt = !!Gt, w.ajaxTransport(function (t) {
    var n, r;
    if (h.cors || Gt && !t.crossDomain) return {
      send: function (i, o) {
        var a, s = t.xhr();
        if (s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
          for (a in t.xhrFields) s[a] = t.xhrFields[a];
        t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
        for (a in i) s.setRequestHeader(a, i[a]);
        n = function (e) {
          return function () {
            n && (n = r = s.onload = s.onerror = s.onabort = s.ontimeout = s.onreadystatechange = null, "abort" === e ? s.abort() : "error" === e ? "number" != typeof s.status ? o(0, "error") : o(s.status, s.statusText) : o(Vt[s.status] || s.status, s.statusText, "text" !== (s.responseType || "text") || "string" != typeof s.responseText ? {
              binary: s.response
            } : {
              text: s.responseText
            }, s.getAllResponseHeaders()))
          }
        }, s.onload = n(), r = s.onerror = s.ontimeout = n("error"), void 0 !== s.onabort ? s.onabort = r : s.onreadystatechange = function () {
          4 === s.readyState && e.setTimeout(function () {
            n && r()
          })
        }, n = n("abort");
        try {
          s.send(t.hasContent && t.data || null)
        } catch (e) {
          if (n) throw e
        }
      },
      abort: function () {
        n && n()
      }
    }
  }), w.ajaxPrefilter(function (e) {
    e.crossDomain && (e.contents.script = !1)
  }), w.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function (e) {
        return w.globalEval(e), e
      }
    }
  }), w.ajaxPrefilter("script", function (e) {
    void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
  }), w.ajaxTransport("script", function (e) {
    if (e.crossDomain) {
      var t, n;
      return {
        send: function (i, o) {
          t = w("<script>").prop({
            charset: e.scriptCharset,
            src: e.url
          }).on("load error", n = function (e) {
            t.remove(), n = null, e && o("error" === e.type ? 404 : 200, e.type)
          }), r.head.appendChild(t[0])
        },
        abort: function () {
          n && n()
        }
      }
    }
  });
  var Yt = [],
    Qt = /(=)\?(?=&|$)|\?\?/;
  w.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      var e = Yt.pop() || w.expando + "_" + Et++;
      return this[e] = !0, e
    }
  }), w.ajaxPrefilter("json jsonp", function (t, n, r) {
    var i, o, a, s = !1 !== t.jsonp && (Qt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Qt.test(t.data) && "data");
    if (s || "jsonp" === t.dataTypes[0]) return i = t.jsonpCallback = g(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Qt, "$1" + i) : !1 !== t.jsonp && (t.url += (kt.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function () {
      return a || w.error(i + " was not called"), a[0]
    }, t.dataTypes[0] = "json", o = e[i], e[i] = function () {
      a = arguments
    }, r.always(function () {
      void 0 === o ? w(e).removeProp(i) : e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, Yt.push(i)), a && g(o) && o(a[0]), a = o = void 0
    }), "script"
  }), h.createHTMLDocument = function () {
    var e = r.implementation.createHTMLDocument("").body;
    return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
  }(), w.parseHTML = function (e, t, n) {
    if ("string" != typeof e) return [];
    "boolean" == typeof t && (n = t, t = !1);
    var i, o, a;
    return t || (h.createHTMLDocument ? ((i = (t = r.implementation.createHTMLDocument("")).createElement("base")).href = r.location.href, t.head.appendChild(i)) : t = r), o = A.exec(e), a = !n && [], o ? [t.createElement(o[1])] : (o = xe([e], t, a), a && a.length && w(a).remove(), w.merge([], o.childNodes))
  }, w.fn.load = function (e, t, n) {
    var r, i, o, a = this,
      s = e.indexOf(" ");
    return s > -1 && (r = vt(e.slice(s)), e = e.slice(0, s)), g(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), a.length > 0 && w.ajax({
      url: e,
      type: i || "GET",
      dataType: "html",
      data: t
    }).done(function (e) {
      o = arguments, a.html(r ? w("<div>").append(w.parseHTML(e)).find(r) : e)
    }).always(n && function (e, t) {
      a.each(function () {
        n.apply(this, o || [e.responseText, t, e])
      })
    }), this
  }, w.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
    w.fn[t] = function (e) {
      return this.on(t, e)
    }
  }), w.expr.pseudos.animated = function (e) {
    return w.grep(w.timers, function (t) {
      return e === t.elem
    }).length
  }, w.offset = {
    setOffset: function (e, t, n) {
      var r, i, o, a, s, u, l, c = w.css(e, "position"),
        f = w(e),
        p = {};
      "static" === c && (e.style.position = "relative"), s = f.offset(), o = w.css(e, "top"), u = w.css(e, "left"), (l = ("absolute" === c || "fixed" === c) && (o + u).indexOf("auto") > -1) ? (a = (r = f.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), g(t) && (t = t.call(e, n, w.extend({}, s))), null != t.top && (p.top = t.top - s.top + a), null != t.left && (p.left = t.left - s.left + i), "using" in t ? t.using.call(e, p) : f.css(p)
    }
  }, w.fn.extend({
    offset: function (e) {
      if (arguments.length) return void 0 === e ? this : this.each(function (t) {
        w.offset.setOffset(this, e, t)
      });
      var t, n, r = this[0];
      if (r) return r.getClientRects().length ? (t = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
        top: t.top + n.pageYOffset,
        left: t.left + n.pageXOffset
      }) : {
        top: 0,
        left: 0
      }
    },
    position: function () {
      if (this[0]) {
        var e, t, n, r = this[0],
          i = {
            top: 0,
            left: 0
          };
        if ("fixed" === w.css(r, "position")) t = r.getBoundingClientRect();
        else {
          t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;
          while (e && (e === n.body || e === n.documentElement) && "static" === w.css(e, "position")) e = e.parentNode;
          e && e !== r && 1 === e.nodeType && ((i = w(e).offset()).top += w.css(e, "borderTopWidth", !0), i.left += w.css(e, "borderLeftWidth", !0))
        }
        return {
          top: t.top - i.top - w.css(r, "marginTop", !0),
          left: t.left - i.left - w.css(r, "marginLeft", !0)
        }
      }
    },
    offsetParent: function () {
      return this.map(function () {
        var e = this.offsetParent;
        while (e && "static" === w.css(e, "position")) e = e.offsetParent;
        return e || be
      })
    }
  }), w.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (e, t) {
    var n = "pageYOffset" === t;
    w.fn[e] = function (r) {
      return z(this, function (e, r, i) {
        var o;
        if (y(e) ? o = e : 9 === e.nodeType && (o = e.defaultView), void 0 === i) return o ? o[t] : e[r];
        o ? o.scrollTo(n ? o.pageXOffset : i, n ? i : o.pageYOffset) : e[r] = i
      }, e, r, arguments.length)
    }
  }), w.each(["top", "left"], function (e, t) {
    w.cssHooks[t] = _e(h.pixelPosition, function (e, n) {
      if (n) return n = Fe(e, t), We.test(n) ? w(e).position()[t] + "px" : n
    })
  }), w.each({
    Height: "height",
    Width: "width"
  }, function (e, t) {
    w.each({
      padding: "inner" + e,
      content: t,
      "": "outer" + e
    }, function (n, r) {
      w.fn[r] = function (i, o) {
        var a = arguments.length && (n || "boolean" != typeof i),
          s = n || (!0 === i || !0 === o ? "margin" : "border");
        return z(this, function (t, n, i) {
          var o;
          return y(t) ? 0 === r.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? w.css(t, n, s) : w.style(t, n, i, s)
        }, t, a ? i : void 0, a)
      }
    })
  }), w.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, t) {
    w.fn[t] = function (e, n) {
      return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
    }
  }), w.fn.extend({
    hover: function (e, t) {
      return this.mouseenter(e).mouseleave(t || e)
    }
  }), w.fn.extend({
    bind: function (e, t, n) {
      return this.on(e, null, t, n)
    },
    unbind: function (e, t) {
      return this.off(e, null, t)
    },
    delegate: function (e, t, n, r) {
      return this.on(t, e, n, r)
    },
    undelegate: function (e, t, n) {
      return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
    }
  }), w.proxy = function (e, t) {
    var n, r, i;
    if ("string" == typeof t && (n = e[t], t = e, e = n), g(e)) return r = o.call(arguments, 2), i = function () {
      return e.apply(t || this, r.concat(o.call(arguments)))
    }, i.guid = e.guid = e.guid || w.guid++, i
  }, w.holdReady = function (e) {
    e ? w.readyWait++ : w.ready(!0)
  }, w.isArray = Array.isArray, w.parseJSON = JSON.parse, w.nodeName = N, w.isFunction = g, w.isWindow = y, w.camelCase = G, w.type = x, w.now = Date.now, w.isNumeric = function (e) {
    var t = w.type(e);
    return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
  }, "function" == typeof define && define.amd && define("jquery", [], function () {
    return w
  });
  var Jt = e.jQuery,
    Kt = e.$;
  return w.noConflict = function (t) {
    return e.$ === w && (e.$ = Kt), t && e.jQuery === w && (e.jQuery = Jt), w
  }, t || (e.jQuery = e.$ = w), w
});

/*!
 * Bootstrap v4.3.1 (https://getbootstrap.com/)
 * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
! function (t, e) {
  "object" == typeof exports && "undefined" != typeof module ? e(exports, require("jquery")) : "function" == typeof define && define.amd ? define(["exports", "jquery"], e) : e((t = t || self).bootstrap = {}, t.jQuery)
}(this, function (t, p) {
  "use strict";

  function i(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
    }
  }

  function s(t, e, n) {
    return e && i(t.prototype, e), n && i(t, n), t
  }

  function l(o) {
    for (var t = 1; t < arguments.length; t++) {
      var r = null != arguments[t] ? arguments[t] : {},
        e = Object.keys(r);
      "function" == typeof Object.getOwnPropertySymbols && (e = e.concat(Object.getOwnPropertySymbols(r).filter(function (t) {
        return Object.getOwnPropertyDescriptor(r, t).enumerable
      }))), e.forEach(function (t) {
        var e, n, i;
        e = o, i = r[n = t], n in e ? Object.defineProperty(e, n, {
          value: i,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }) : e[n] = i
      })
    }
    return o
  }
  p = p && p.hasOwnProperty("default") ? p.default : p;
  var e = "transitionend";

  function n(t) {
    var e = this,
      n = !1;
    return p(this).one(m.TRANSITION_END, function () {
      n = !0
    }), setTimeout(function () {
      n || m.triggerTransitionEnd(e)
    }, t), this
  }
  var m = {
    TRANSITION_END: "bsTransitionEnd",
    getUID: function (t) {
      for (; t += ~~(1e6 * Math.random()), document.getElementById(t););
      return t
    },
    getSelectorFromElement: function (t) {
      var e = t.getAttribute("data-target");
      if (!e || "#" === e) {
        var n = t.getAttribute("href");
        e = n && "#" !== n ? n.trim() : ""
      }
      try {
        return document.querySelector(e) ? e : null
      } catch (t) {
        return null
      }
    },
    getTransitionDurationFromElement: function (t) {
      if (!t) return 0;
      var e = p(t).css("transition-duration"),
        n = p(t).css("transition-delay"),
        i = parseFloat(e),
        o = parseFloat(n);
      return i || o ? (e = e.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(e) + parseFloat(n))) : 0
    },
    reflow: function (t) {
      return t.offsetHeight
    },
    triggerTransitionEnd: function (t) {
      p(t).trigger(e)
    },
    supportsTransitionEnd: function () {
      return Boolean(e)
    },
    isElement: function (t) {
      return (t[0] || t).nodeType
    },
    typeCheckConfig: function (t, e, n) {
      for (var i in n)
        if (Object.prototype.hasOwnProperty.call(n, i)) {
          var o = n[i],
            r = e[i],
            s = r && m.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
          if (!new RegExp(o).test(s)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".')
        } var a
    },
    findShadowRoot: function (t) {
      if (!document.documentElement.attachShadow) return null;
      if ("function" != typeof t.getRootNode) return t instanceof ShadowRoot ? t : t.parentNode ? m.findShadowRoot(t.parentNode) : null;
      var e = t.getRootNode();
      return e instanceof ShadowRoot ? e : null
    }
  };
  p.fn.emulateTransitionEnd = n, p.event.special[m.TRANSITION_END] = {
    bindType: e,
    delegateType: e,
    handle: function (t) {
      if (p(t.target).is(this)) return t.handleObj.handler.apply(this, arguments)
    }
  };
  var o = "alert",
    r = "bs.alert",
    a = "." + r,
    c = p.fn[o],
    h = {
      CLOSE: "close" + a,
      CLOSED: "closed" + a,
      CLICK_DATA_API: "click" + a + ".data-api"
    },
    u = "alert",
    f = "fade",
    d = "show",
    g = function () {
      function i(t) {
        this._element = t
      }
      var t = i.prototype;
      return t.close = function (t) {
        var e = this._element;
        t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
      }, t.dispose = function () {
        p.removeData(this._element, r), this._element = null
      }, t._getRootElement = function (t) {
        var e = m.getSelectorFromElement(t),
          n = !1;
        return e && (n = document.querySelector(e)), n || (n = p(t).closest("." + u)[0]), n
      }, t._triggerCloseEvent = function (t) {
        var e = p.Event(h.CLOSE);
        return p(t).trigger(e), e
      }, t._removeElement = function (e) {
        var n = this;
        if (p(e).removeClass(d), p(e).hasClass(f)) {
          var t = m.getTransitionDurationFromElement(e);
          p(e).one(m.TRANSITION_END, function (t) {
            return n._destroyElement(e, t)
          }).emulateTransitionEnd(t)
        } else this._destroyElement(e)
      }, t._destroyElement = function (t) {
        p(t).detach().trigger(h.CLOSED).remove()
      }, i._jQueryInterface = function (n) {
        return this.each(function () {
          var t = p(this),
            e = t.data(r);
          e || (e = new i(this), t.data(r, e)), "close" === n && e[n](this)
        })
      }, i._handleDismiss = function (e) {
        return function (t) {
          t && t.preventDefault(), e.close(this)
        }
      }, s(i, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }]), i
    }();
  p(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', g._handleDismiss(new g)), p.fn[o] = g._jQueryInterface, p.fn[o].Constructor = g, p.fn[o].noConflict = function () {
    return p.fn[o] = c, g._jQueryInterface
  };
  var _ = "button",
    v = "bs.button",
    y = "." + v,
    E = ".data-api",
    b = p.fn[_],
    w = "active",
    C = "btn",
    T = "focus",
    S = '[data-toggle^="button"]',
    D = '[data-toggle="buttons"]',
    I = 'input:not([type="hidden"])',
    A = ".active",
    O = ".btn",
    N = {
      CLICK_DATA_API: "click" + y + E,
      FOCUS_BLUR_DATA_API: "focus" + y + E + " blur" + y + E
    },
    k = function () {
      function n(t) {
        this._element = t
      }
      var t = n.prototype;
      return t.toggle = function () {
        var t = !0,
          e = !0,
          n = p(this._element).closest(D)[0];
        if (n) {
          var i = this._element.querySelector(I);
          if (i) {
            if ("radio" === i.type)
              if (i.checked && this._element.classList.contains(w)) t = !1;
              else {
                var o = n.querySelector(A);
                o && p(o).removeClass(w)
              } if (t) {
              if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;
              i.checked = !this._element.classList.contains(w), p(i).trigger("change")
            }
            i.focus(), e = !1
          }
        }
        e && this._element.setAttribute("aria-pressed", !this._element.classList.contains(w)), t && p(this._element).toggleClass(w)
      }, t.dispose = function () {
        p.removeData(this._element, v), this._element = null
      }, n._jQueryInterface = function (e) {
        return this.each(function () {
          var t = p(this).data(v);
          t || (t = new n(this), p(this).data(v, t)), "toggle" === e && t[e]()
        })
      }, s(n, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }]), n
    }();
  p(document).on(N.CLICK_DATA_API, S, function (t) {
    t.preventDefault();
    var e = t.target;
    p(e).hasClass(C) || (e = p(e).closest(O)), k._jQueryInterface.call(p(e), "toggle")
  }).on(N.FOCUS_BLUR_DATA_API, S, function (t) {
    var e = p(t.target).closest(O)[0];
    p(e).toggleClass(T, /^focus(in)?$/.test(t.type))
  }), p.fn[_] = k._jQueryInterface, p.fn[_].Constructor = k, p.fn[_].noConflict = function () {
    return p.fn[_] = b, k._jQueryInterface
  };
  var L = "carousel",
    x = "bs.carousel",
    P = "." + x,
    H = ".data-api",
    j = p.fn[L],
    R = {
      interval: 5e3,
      keyboard: !0,
      slide: !1,
      pause: "hover",
      wrap: !0,
      touch: !0
    },
    F = {
      interval: "(number|boolean)",
      keyboard: "boolean",
      slide: "(boolean|string)",
      pause: "(string|boolean)",
      wrap: "boolean",
      touch: "boolean"
    },
    M = "next",
    W = "prev",
    U = "left",
    B = "right",
    q = {
      SLIDE: "slide" + P,
      SLID: "slid" + P,
      KEYDOWN: "keydown" + P,
      MOUSEENTER: "mouseenter" + P,
      MOUSELEAVE: "mouseleave" + P,
      TOUCHSTART: "touchstart" + P,
      TOUCHMOVE: "touchmove" + P,
      TOUCHEND: "touchend" + P,
      POINTERDOWN: "pointerdown" + P,
      POINTERUP: "pointerup" + P,
      DRAG_START: "dragstart" + P,
      LOAD_DATA_API: "load" + P + H,
      CLICK_DATA_API: "click" + P + H
    },
    K = "carousel",
    Q = "active",
    V = "slide",
    Y = "carousel-item-right",
    z = "carousel-item-left",
    X = "carousel-item-next",
    G = "carousel-item-prev",
    $ = "pointer-event",
    J = ".active",
    Z = ".active.carousel-item",
    tt = ".carousel-item",
    et = ".carousel-item img",
    nt = ".carousel-item-next, .carousel-item-prev",
    it = ".carousel-indicators",
    ot = "[data-slide], [data-slide-to]",
    rt = '[data-ride="carousel"]',
    st = {
      TOUCH: "touch",
      PEN: "pen"
    },
    at = function () {
      function r(t, e) {
        this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(e), this._element = t, this._indicatorsElement = this._element.querySelector(it), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners()
      }
      var t = r.prototype;
      return t.next = function () {
        this._isSliding || this._slide(M)
      }, t.nextWhenVisible = function () {
        !document.hidden && p(this._element).is(":visible") && "hidden" !== p(this._element).css("visibility") && this.next()
      }, t.prev = function () {
        this._isSliding || this._slide(W)
      }, t.pause = function (t) {
        t || (this._isPaused = !0), this._element.querySelector(nt) && (m.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
      }, t.cycle = function (t) {
        t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
      }, t.to = function (t) {
        var e = this;
        this._activeElement = this._element.querySelector(Z);
        var n = this._getItemIndex(this._activeElement);
        if (!(t > this._items.length - 1 || t < 0))
          if (this._isSliding) p(this._element).one(q.SLID, function () {
            return e.to(t)
          });
          else {
            if (n === t) return this.pause(), void this.cycle();
            var i = n < t ? M : W;
            this._slide(i, this._items[t])
          }
      }, t.dispose = function () {
        p(this._element).off(P), p.removeData(this._element, x), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
      }, t._getConfig = function (t) {
        return t = l({}, R, t), m.typeCheckConfig(L, t, F), t
      }, t._handleSwipe = function () {
        var t = Math.abs(this.touchDeltaX);
        if (!(t <= 40)) {
          var e = t / this.touchDeltaX;
          0 < e && this.prev(), e < 0 && this.next()
        }
      }, t._addEventListeners = function () {
        var e = this;
        this._config.keyboard && p(this._element).on(q.KEYDOWN, function (t) {
          return e._keydown(t)
        }), "hover" === this._config.pause && p(this._element).on(q.MOUSEENTER, function (t) {
          return e.pause(t)
        }).on(q.MOUSELEAVE, function (t) {
          return e.cycle(t)
        }), this._config.touch && this._addTouchEventListeners()
      }, t._addTouchEventListeners = function () {
        var n = this;
        if (this._touchSupported) {
          var e = function (t) {
              n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] ? n.touchStartX = t.originalEvent.clientX : n._pointerEvent || (n.touchStartX = t.originalEvent.touches[0].clientX)
            },
            i = function (t) {
              n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] && (n.touchDeltaX = t.originalEvent.clientX - n.touchStartX), n._handleSwipe(), "hover" === n._config.pause && (n.pause(), n.touchTimeout && clearTimeout(n.touchTimeout), n.touchTimeout = setTimeout(function (t) {
                return n.cycle(t)
              }, 500 + n._config.interval))
            };
          p(this._element.querySelectorAll(et)).on(q.DRAG_START, function (t) {
            return t.preventDefault()
          }), this._pointerEvent ? (p(this._element).on(q.POINTERDOWN, function (t) {
            return e(t)
          }), p(this._element).on(q.POINTERUP, function (t) {
            return i(t)
          }), this._element.classList.add($)) : (p(this._element).on(q.TOUCHSTART, function (t) {
            return e(t)
          }), p(this._element).on(q.TOUCHMOVE, function (t) {
            var e;
            (e = t).originalEvent.touches && 1 < e.originalEvent.touches.length ? n.touchDeltaX = 0 : n.touchDeltaX = e.originalEvent.touches[0].clientX - n.touchStartX
          }), p(this._element).on(q.TOUCHEND, function (t) {
            return i(t)
          }))
        }
      }, t._keydown = function (t) {
        if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
          case 37:
            t.preventDefault(), this.prev();
            break;
          case 39:
            t.preventDefault(), this.next()
        }
      }, t._getItemIndex = function (t) {
        return this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll(tt)) : [], this._items.indexOf(t)
      }, t._getItemByDirection = function (t, e) {
        var n = t === M,
          i = t === W,
          o = this._getItemIndex(e),
          r = this._items.length - 1;
        if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
        var s = (o + (t === W ? -1 : 1)) % this._items.length;
        return -1 === s ? this._items[this._items.length - 1] : this._items[s]
      }, t._triggerSlideEvent = function (t, e) {
        var n = this._getItemIndex(t),
          i = this._getItemIndex(this._element.querySelector(Z)),
          o = p.Event(q.SLIDE, {
            relatedTarget: t,
            direction: e,
            from: i,
            to: n
          });
        return p(this._element).trigger(o), o
      }, t._setActiveIndicatorElement = function (t) {
        if (this._indicatorsElement) {
          var e = [].slice.call(this._indicatorsElement.querySelectorAll(J));
          p(e).removeClass(Q);
          var n = this._indicatorsElement.children[this._getItemIndex(t)];
          n && p(n).addClass(Q)
        }
      }, t._slide = function (t, e) {
        var n, i, o, r = this,
          s = this._element.querySelector(Z),
          a = this._getItemIndex(s),
          l = e || s && this._getItemByDirection(t, s),
          c = this._getItemIndex(l),
          h = Boolean(this._interval);
        if (o = t === M ? (n = z, i = X, U) : (n = Y, i = G, B), l && p(l).hasClass(Q)) this._isSliding = !1;
        else if (!this._triggerSlideEvent(l, o).isDefaultPrevented() && s && l) {
          this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(l);
          var u = p.Event(q.SLID, {
            relatedTarget: l,
            direction: o,
            from: a,
            to: c
          });
          if (p(this._element).hasClass(V)) {
            p(l).addClass(i), m.reflow(l), p(s).addClass(n), p(l).addClass(n);
            var f = parseInt(l.getAttribute("data-interval"), 10);
            this._config.interval = f ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, f) : this._config.defaultInterval || this._config.interval;
            var d = m.getTransitionDurationFromElement(s);
            p(s).one(m.TRANSITION_END, function () {
              p(l).removeClass(n + " " + i).addClass(Q), p(s).removeClass(Q + " " + i + " " + n), r._isSliding = !1, setTimeout(function () {
                return p(r._element).trigger(u)
              }, 0)
            }).emulateTransitionEnd(d)
          } else p(s).removeClass(Q), p(l).addClass(Q), this._isSliding = !1, p(this._element).trigger(u);
          h && this.cycle()
        }
      }, r._jQueryInterface = function (i) {
        return this.each(function () {
          var t = p(this).data(x),
            e = l({}, R, p(this).data());
          "object" == typeof i && (e = l({}, e, i));
          var n = "string" == typeof i ? i : e.slide;
          if (t || (t = new r(this, e), p(this).data(x, t)), "number" == typeof i) t.to(i);
          else if ("string" == typeof n) {
            if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
            t[n]()
          } else e.interval && e.ride && (t.pause(), t.cycle())
        })
      }, r._dataApiClickHandler = function (t) {
        var e = m.getSelectorFromElement(this);
        if (e) {
          var n = p(e)[0];
          if (n && p(n).hasClass(K)) {
            var i = l({}, p(n).data(), p(this).data()),
              o = this.getAttribute("data-slide-to");
            o && (i.interval = !1), r._jQueryInterface.call(p(n), i), o && p(n).data(x).to(o), t.preventDefault()
          }
        }
      }, s(r, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return R
        }
      }]), r
    }();
  p(document).on(q.CLICK_DATA_API, ot, at._dataApiClickHandler), p(window).on(q.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(rt)), e = 0, n = t.length; e < n; e++) {
      var i = p(t[e]);
      at._jQueryInterface.call(i, i.data())
    }
  }), p.fn[L] = at._jQueryInterface, p.fn[L].Constructor = at, p.fn[L].noConflict = function () {
    return p.fn[L] = j, at._jQueryInterface
  };
  var lt = "collapse",
    ct = "bs.collapse",
    ht = "." + ct,
    ut = p.fn[lt],
    ft = {
      toggle: !0,
      parent: ""
    },
    dt = {
      toggle: "boolean",
      parent: "(string|element)"
    },
    pt = {
      SHOW: "show" + ht,
      SHOWN: "shown" + ht,
      HIDE: "hide" + ht,
      HIDDEN: "hidden" + ht,
      CLICK_DATA_API: "click" + ht + ".data-api"
    },
    mt = "show",
    gt = "collapse",
    _t = "collapsing",
    vt = "collapsed",
    yt = "width",
    Et = "height",
    bt = ".show, .collapsing",
    wt = '[data-toggle="collapse"]',
    Ct = function () {
      function a(e, t) {
        this._isTransitioning = !1, this._element = e, this._config = this._getConfig(t), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));
        for (var n = [].slice.call(document.querySelectorAll(wt)), i = 0, o = n.length; i < o; i++) {
          var r = n[i],
            s = m.getSelectorFromElement(r),
            a = [].slice.call(document.querySelectorAll(s)).filter(function (t) {
              return t === e
            });
          null !== s && 0 < a.length && (this._selector = s, this._triggerArray.push(r))
        }
        this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
      }
      var t = a.prototype;
      return t.toggle = function () {
        p(this._element).hasClass(mt) ? this.hide() : this.show()
      }, t.show = function () {
        var t, e, n = this;
        if (!this._isTransitioning && !p(this._element).hasClass(mt) && (this._parent && 0 === (t = [].slice.call(this._parent.querySelectorAll(bt)).filter(function (t) {
            return "string" == typeof n._config.parent ? t.getAttribute("data-parent") === n._config.parent : t.classList.contains(gt)
          })).length && (t = null), !(t && (e = p(t).not(this._selector).data(ct)) && e._isTransitioning))) {
          var i = p.Event(pt.SHOW);
          if (p(this._element).trigger(i), !i.isDefaultPrevented()) {
            t && (a._jQueryInterface.call(p(t).not(this._selector), "hide"), e || p(t).data(ct, null));
            var o = this._getDimension();
            p(this._element).removeClass(gt).addClass(_t), this._element.style[o] = 0, this._triggerArray.length && p(this._triggerArray).removeClass(vt).attr("aria-expanded", !0), this.setTransitioning(!0);
            var r = "scroll" + (o[0].toUpperCase() + o.slice(1)),
              s = m.getTransitionDurationFromElement(this._element);
            p(this._element).one(m.TRANSITION_END, function () {
              p(n._element).removeClass(_t).addClass(gt).addClass(mt), n._element.style[o] = "", n.setTransitioning(!1), p(n._element).trigger(pt.SHOWN)
            }).emulateTransitionEnd(s), this._element.style[o] = this._element[r] + "px"
          }
        }
      }, t.hide = function () {
        var t = this;
        if (!this._isTransitioning && p(this._element).hasClass(mt)) {
          var e = p.Event(pt.HIDE);
          if (p(this._element).trigger(e), !e.isDefaultPrevented()) {
            var n = this._getDimension();
            this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", m.reflow(this._element), p(this._element).addClass(_t).removeClass(gt).removeClass(mt);
            var i = this._triggerArray.length;
            if (0 < i)
              for (var o = 0; o < i; o++) {
                var r = this._triggerArray[o],
                  s = m.getSelectorFromElement(r);
                if (null !== s) p([].slice.call(document.querySelectorAll(s))).hasClass(mt) || p(r).addClass(vt).attr("aria-expanded", !1)
              }
            this.setTransitioning(!0);
            this._element.style[n] = "";
            var a = m.getTransitionDurationFromElement(this._element);
            p(this._element).one(m.TRANSITION_END, function () {
              t.setTransitioning(!1), p(t._element).removeClass(_t).addClass(gt).trigger(pt.HIDDEN)
            }).emulateTransitionEnd(a)
          }
        }
      }, t.setTransitioning = function (t) {
        this._isTransitioning = t
      }, t.dispose = function () {
        p.removeData(this._element, ct), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
      }, t._getConfig = function (t) {
        return (t = l({}, ft, t)).toggle = Boolean(t.toggle), m.typeCheckConfig(lt, t, dt), t
      }, t._getDimension = function () {
        return p(this._element).hasClass(yt) ? yt : Et
      }, t._getParent = function () {
        var t, n = this;
        m.isElement(this._config.parent) ? (t = this._config.parent, "undefined" != typeof this._config.parent.jquery && (t = this._config.parent[0])) : t = document.querySelector(this._config.parent);
        var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
          i = [].slice.call(t.querySelectorAll(e));
        return p(i).each(function (t, e) {
          n._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e])
        }), t
      }, t._addAriaAndCollapsedClass = function (t, e) {
        var n = p(t).hasClass(mt);
        e.length && p(e).toggleClass(vt, !n).attr("aria-expanded", n)
      }, a._getTargetFromElement = function (t) {
        var e = m.getSelectorFromElement(t);
        return e ? document.querySelector(e) : null
      }, a._jQueryInterface = function (i) {
        return this.each(function () {
          var t = p(this),
            e = t.data(ct),
            n = l({}, ft, t.data(), "object" == typeof i && i ? i : {});
          if (!e && n.toggle && /show|hide/.test(i) && (n.toggle = !1), e || (e = new a(this, n), t.data(ct, e)), "string" == typeof i) {
            if ("undefined" == typeof e[i]) throw new TypeError('No method named "' + i + '"');
            e[i]()
          }
        })
      }, s(a, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return ft
        }
      }]), a
    }();
  p(document).on(pt.CLICK_DATA_API, wt, function (t) {
    "A" === t.currentTarget.tagName && t.preventDefault();
    var n = p(this),
      e = m.getSelectorFromElement(this),
      i = [].slice.call(document.querySelectorAll(e));
    p(i).each(function () {
      var t = p(this),
        e = t.data(ct) ? "toggle" : n.data();
      Ct._jQueryInterface.call(t, e)
    })
  }), p.fn[lt] = Ct._jQueryInterface, p.fn[lt].Constructor = Ct, p.fn[lt].noConflict = function () {
    return p.fn[lt] = ut, Ct._jQueryInterface
  };
  for (var Tt = "undefined" != typeof window && "undefined" != typeof document, St = ["Edge", "Trident", "Firefox"], Dt = 0, It = 0; It < St.length; It += 1)
    if (Tt && 0 <= navigator.userAgent.indexOf(St[It])) {
      Dt = 1;
      break
    } var At = Tt && window.Promise ? function (t) {
    var e = !1;
    return function () {
      e || (e = !0, window.Promise.resolve().then(function () {
        e = !1, t()
      }))
    }
  } : function (t) {
    var e = !1;
    return function () {
      e || (e = !0, setTimeout(function () {
        e = !1, t()
      }, Dt))
    }
  };

  function Ot(t) {
    return t && "[object Function]" === {}.toString.call(t)
  }

  function Nt(t, e) {
    if (1 !== t.nodeType) return [];
    var n = t.ownerDocument.defaultView.getComputedStyle(t, null);
    return e ? n[e] : n
  }

  function kt(t) {
    return "HTML" === t.nodeName ? t : t.parentNode || t.host
  }

  function Lt(t) {
    if (!t) return document.body;
    switch (t.nodeName) {
      case "HTML":
      case "BODY":
        return t.ownerDocument.body;
      case "#document":
        return t.body
    }
    var e = Nt(t),
      n = e.overflow,
      i = e.overflowX,
      o = e.overflowY;
    return /(auto|scroll|overlay)/.test(n + o + i) ? t : Lt(kt(t))
  }
  var xt = Tt && !(!window.MSInputMethodContext || !document.documentMode),
    Pt = Tt && /MSIE 10/.test(navigator.userAgent);

  function Ht(t) {
    return 11 === t ? xt : 10 === t ? Pt : xt || Pt
  }

  function jt(t) {
    if (!t) return document.documentElement;
    for (var e = Ht(10) ? document.body : null, n = t.offsetParent || null; n === e && t.nextElementSibling;) n = (t = t.nextElementSibling).offsetParent;
    var i = n && n.nodeName;
    return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TH", "TD", "TABLE"].indexOf(n.nodeName) && "static" === Nt(n, "position") ? jt(n) : n : t ? t.ownerDocument.documentElement : document.documentElement
  }

  function Rt(t) {
    return null !== t.parentNode ? Rt(t.parentNode) : t
  }

  function Ft(t, e) {
    if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;
    var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
      i = n ? t : e,
      o = n ? e : t,
      r = document.createRange();
    r.setStart(i, 0), r.setEnd(o, 0);
    var s, a, l = r.commonAncestorContainer;
    if (t !== l && e !== l || i.contains(o)) return "BODY" === (a = (s = l).nodeName) || "HTML" !== a && jt(s.firstElementChild) !== s ? jt(l) : l;
    var c = Rt(t);
    return c.host ? Ft(c.host, e) : Ft(t, Rt(e).host)
  }

  function Mt(t) {
    var e = "top" === (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top") ? "scrollTop" : "scrollLeft",
      n = t.nodeName;
    if ("BODY" !== n && "HTML" !== n) return t[e];
    var i = t.ownerDocument.documentElement;
    return (t.ownerDocument.scrollingElement || i)[e]
  }

  function Wt(t, e) {
    var n = "x" === e ? "Left" : "Top",
      i = "Left" === n ? "Right" : "Bottom";
    return parseFloat(t["border" + n + "Width"], 10) + parseFloat(t["border" + i + "Width"], 10)
  }

  function Ut(t, e, n, i) {
    return Math.max(e["offset" + t], e["scroll" + t], n["client" + t], n["offset" + t], n["scroll" + t], Ht(10) ? parseInt(n["offset" + t]) + parseInt(i["margin" + ("Height" === t ? "Top" : "Left")]) + parseInt(i["margin" + ("Height" === t ? "Bottom" : "Right")]) : 0)
  }

  function Bt(t) {
    var e = t.body,
      n = t.documentElement,
      i = Ht(10) && getComputedStyle(n);
    return {
      height: Ut("Height", e, n, i),
      width: Ut("Width", e, n, i)
    }
  }
  var qt = function () {
      function i(t, e) {
        for (var n = 0; n < e.length; n++) {
          var i = e[n];
          i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
      }
      return function (t, e, n) {
        return e && i(t.prototype, e), n && i(t, n), t
      }
    }(),
    Kt = function (t, e, n) {
      return e in t ? Object.defineProperty(t, e, {
        value: n,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }) : t[e] = n, t
    },
    Qt = Object.assign || function (t) {
      for (var e = 1; e < arguments.length; e++) {
        var n = arguments[e];
        for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i])
      }
      return t
    };

  function Vt(t) {
    return Qt({}, t, {
      right: t.left + t.width,
      bottom: t.top + t.height
    })
  }

  function Yt(t) {
    var e = {};
    try {
      if (Ht(10)) {
        e = t.getBoundingClientRect();
        var n = Mt(t, "top"),
          i = Mt(t, "left");
        e.top += n, e.left += i, e.bottom += n, e.right += i
      } else e = t.getBoundingClientRect()
    } catch (t) {}
    var o = {
        left: e.left,
        top: e.top,
        width: e.right - e.left,
        height: e.bottom - e.top
      },
      r = "HTML" === t.nodeName ? Bt(t.ownerDocument) : {},
      s = r.width || t.clientWidth || o.right - o.left,
      a = r.height || t.clientHeight || o.bottom - o.top,
      l = t.offsetWidth - s,
      c = t.offsetHeight - a;
    if (l || c) {
      var h = Nt(t);
      l -= Wt(h, "x"), c -= Wt(h, "y"), o.width -= l, o.height -= c
    }
    return Vt(o)
  }

  function zt(t, e) {
    var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
      i = Ht(10),
      o = "HTML" === e.nodeName,
      r = Yt(t),
      s = Yt(e),
      a = Lt(t),
      l = Nt(e),
      c = parseFloat(l.borderTopWidth, 10),
      h = parseFloat(l.borderLeftWidth, 10);
    n && o && (s.top = Math.max(s.top, 0), s.left = Math.max(s.left, 0));
    var u = Vt({
      top: r.top - s.top - c,
      left: r.left - s.left - h,
      width: r.width,
      height: r.height
    });
    if (u.marginTop = 0, u.marginLeft = 0, !i && o) {
      var f = parseFloat(l.marginTop, 10),
        d = parseFloat(l.marginLeft, 10);
      u.top -= c - f, u.bottom -= c - f, u.left -= h - d, u.right -= h - d, u.marginTop = f, u.marginLeft = d
    }
    return (i && !n ? e.contains(a) : e === a && "BODY" !== a.nodeName) && (u = function (t, e) {
      var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        i = Mt(e, "top"),
        o = Mt(e, "left"),
        r = n ? -1 : 1;
      return t.top += i * r, t.bottom += i * r, t.left += o * r, t.right += o * r, t
    }(u, e)), u
  }

  function Xt(t) {
    if (!t || !t.parentElement || Ht()) return document.documentElement;
    for (var e = t.parentElement; e && "none" === Nt(e, "transform");) e = e.parentElement;
    return e || document.documentElement
  }

  function Gt(t, e, n, i) {
    var o = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
      r = {
        top: 0,
        left: 0
      },
      s = o ? Xt(t) : Ft(t, e);
    if ("viewport" === i) r = function (t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        n = t.ownerDocument.documentElement,
        i = zt(t, n),
        o = Math.max(n.clientWidth, window.innerWidth || 0),
        r = Math.max(n.clientHeight, window.innerHeight || 0),
        s = e ? 0 : Mt(n),
        a = e ? 0 : Mt(n, "left");
      return Vt({
        top: s - i.top + i.marginTop,
        left: a - i.left + i.marginLeft,
        width: o,
        height: r
      })
    }(s, o);
    else {
      var a = void 0;
      "scrollParent" === i ? "BODY" === (a = Lt(kt(e))).nodeName && (a = t.ownerDocument.documentElement) : a = "window" === i ? t.ownerDocument.documentElement : i;
      var l = zt(a, s, o);
      if ("HTML" !== a.nodeName || function t(e) {
          var n = e.nodeName;
          if ("BODY" === n || "HTML" === n) return !1;
          if ("fixed" === Nt(e, "position")) return !0;
          var i = kt(e);
          return !!i && t(i)
        }(s)) r = l;
      else {
        var c = Bt(t.ownerDocument),
          h = c.height,
          u = c.width;
        r.top += l.top - l.marginTop, r.bottom = h + l.top, r.left += l.left - l.marginLeft, r.right = u + l.left
      }
    }
    var f = "number" == typeof (n = n || 0);
    return r.left += f ? n : n.left || 0, r.top += f ? n : n.top || 0, r.right -= f ? n : n.right || 0, r.bottom -= f ? n : n.bottom || 0, r
  }

  function $t(t, e, i, n, o) {
    var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
    if (-1 === t.indexOf("auto")) return t;
    var s = Gt(i, n, r, o),
      a = {
        top: {
          width: s.width,
          height: e.top - s.top
        },
        right: {
          width: s.right - e.right,
          height: s.height
        },
        bottom: {
          width: s.width,
          height: s.bottom - e.bottom
        },
        left: {
          width: e.left - s.left,
          height: s.height
        }
      },
      l = Object.keys(a).map(function (t) {
        return Qt({
          key: t
        }, a[t], {
          area: (e = a[t], e.width * e.height)
        });
        var e
      }).sort(function (t, e) {
        return e.area - t.area
      }),
      c = l.filter(function (t) {
        var e = t.width,
          n = t.height;
        return e >= i.clientWidth && n >= i.clientHeight
      }),
      h = 0 < c.length ? c[0].key : l[0].key,
      u = t.split("-")[1];
    return h + (u ? "-" + u : "")
  }

  function Jt(t, e, n) {
    var i = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null;
    return zt(n, i ? Xt(e) : Ft(e, n), i)
  }

  function Zt(t) {
    var e = t.ownerDocument.defaultView.getComputedStyle(t),
      n = parseFloat(e.marginTop || 0) + parseFloat(e.marginBottom || 0),
      i = parseFloat(e.marginLeft || 0) + parseFloat(e.marginRight || 0);
    return {
      width: t.offsetWidth + i,
      height: t.offsetHeight + n
    }
  }

  function te(t) {
    var e = {
      left: "right",
      right: "left",
      bottom: "top",
      top: "bottom"
    };
    return t.replace(/left|right|bottom|top/g, function (t) {
      return e[t]
    })
  }

  function ee(t, e, n) {
    n = n.split("-")[0];
    var i = Zt(t),
      o = {
        width: i.width,
        height: i.height
      },
      r = -1 !== ["right", "left"].indexOf(n),
      s = r ? "top" : "left",
      a = r ? "left" : "top",
      l = r ? "height" : "width",
      c = r ? "width" : "height";
    return o[s] = e[s] + e[l] / 2 - i[l] / 2, o[a] = n === a ? e[a] - i[c] : e[te(a)], o
  }

  function ne(t, e) {
    return Array.prototype.find ? t.find(e) : t.filter(e)[0]
  }

  function ie(t, n, e) {
    return (void 0 === e ? t : t.slice(0, function (t, e, n) {
      if (Array.prototype.findIndex) return t.findIndex(function (t) {
        return t[e] === n
      });
      var i = ne(t, function (t) {
        return t[e] === n
      });
      return t.indexOf(i)
    }(t, "name", e))).forEach(function (t) {
      t.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
      var e = t.function || t.fn;
      t.enabled && Ot(e) && (n.offsets.popper = Vt(n.offsets.popper), n.offsets.reference = Vt(n.offsets.reference), n = e(n, t))
    }), n
  }

  function oe(t, n) {
    return t.some(function (t) {
      var e = t.name;
      return t.enabled && e === n
    })
  }

  function re(t) {
    for (var e = [!1, "ms", "Webkit", "Moz", "O"], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0; i < e.length; i++) {
      var o = e[i],
        r = o ? "" + o + n : t;
      if ("undefined" != typeof document.body.style[r]) return r
    }
    return null
  }

  function se(t) {
    var e = t.ownerDocument;
    return e ? e.defaultView : window
  }

  function ae(t, e, n, i) {
    n.updateBound = i, se(t).addEventListener("resize", n.updateBound, {
      passive: !0
    });
    var o = Lt(t);
    return function t(e, n, i, o) {
      var r = "BODY" === e.nodeName,
        s = r ? e.ownerDocument.defaultView : e;
      s.addEventListener(n, i, {
        passive: !0
      }), r || t(Lt(s.parentNode), n, i, o), o.push(s)
    }(o, "scroll", n.updateBound, n.scrollParents), n.scrollElement = o, n.eventsEnabled = !0, n
  }

  function le() {
    var t, e;
    this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = (t = this.reference, e = this.state, se(t).removeEventListener("resize", e.updateBound), e.scrollParents.forEach(function (t) {
      t.removeEventListener("scroll", e.updateBound)
    }), e.updateBound = null, e.scrollParents = [], e.scrollElement = null, e.eventsEnabled = !1, e))
  }

  function ce(t) {
    return "" !== t && !isNaN(parseFloat(t)) && isFinite(t)
  }

  function he(n, i) {
    Object.keys(i).forEach(function (t) {
      var e = ""; - 1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(t) && ce(i[t]) && (e = "px"), n.style[t] = i[t] + e
    })
  }
  var ue = Tt && /Firefox/i.test(navigator.userAgent);

  function fe(t, e, n) {
    var i = ne(t, function (t) {
        return t.name === e
      }),
      o = !!i && t.some(function (t) {
        return t.name === n && t.enabled && t.order < i.order
      });
    if (!o) {
      var r = "`" + e + "`",
        s = "`" + n + "`";
      console.warn(s + " modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!")
    }
    return o
  }
  var de = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
    pe = de.slice(3);

  function me(t) {
    var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
      n = pe.indexOf(t),
      i = pe.slice(n + 1).concat(pe.slice(0, n));
    return e ? i.reverse() : i
  }
  var ge = "flip",
    _e = "clockwise",
    ve = "counterclockwise";

  function ye(t, o, r, e) {
    var s = [0, 0],
      a = -1 !== ["right", "left"].indexOf(e),
      n = t.split(/(\+|\-)/).map(function (t) {
        return t.trim()
      }),
      i = n.indexOf(ne(n, function (t) {
        return -1 !== t.search(/,|\s/)
      }));
    n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
    var l = /\s*,\s*|\s+/,
      c = -1 !== i ? [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))] : [n];
    return (c = c.map(function (t, e) {
      var n = (1 === e ? !a : a) ? "height" : "width",
        i = !1;
      return t.reduce(function (t, e) {
        return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, i = !0, t) : i ? (t[t.length - 1] += e, i = !1, t) : t.concat(e)
      }, []).map(function (t) {
        return function (t, e, n, i) {
          var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
            r = +o[1],
            s = o[2];
          if (!r) return t;
          if (0 !== s.indexOf("%")) return "vh" !== s && "vw" !== s ? r : ("vh" === s ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * r;
          var a = void 0;
          switch (s) {
            case "%p":
              a = n;
              break;
            case "%":
            case "%r":
            default:
              a = i
          }
          return Vt(a)[e] / 100 * r
        }(t, n, o, r)
      })
    })).forEach(function (n, i) {
      n.forEach(function (t, e) {
        ce(t) && (s[i] += t * ("-" === n[e - 1] ? -1 : 1))
      })
    }), s
  }
  var Ee = {
      placement: "bottom",
      positionFixed: !1,
      eventsEnabled: !0,
      removeOnDestroy: !1,
      onCreate: function () {},
      onUpdate: function () {},
      modifiers: {
        shift: {
          order: 100,
          enabled: !0,
          fn: function (t) {
            var e = t.placement,
              n = e.split("-")[0],
              i = e.split("-")[1];
            if (i) {
              var o = t.offsets,
                r = o.reference,
                s = o.popper,
                a = -1 !== ["bottom", "top"].indexOf(n),
                l = a ? "left" : "top",
                c = a ? "width" : "height",
                h = {
                  start: Kt({}, l, r[l]),
                  end: Kt({}, l, r[l] + r[c] - s[c])
                };
              t.offsets.popper = Qt({}, s, h[i])
            }
            return t
          }
        },
        offset: {
          order: 200,
          enabled: !0,
          fn: function (t, e) {
            var n = e.offset,
              i = t.placement,
              o = t.offsets,
              r = o.popper,
              s = o.reference,
              a = i.split("-")[0],
              l = void 0;
            return l = ce(+n) ? [+n, 0] : ye(n, r, s, a), "left" === a ? (r.top += l[0], r.left -= l[1]) : "right" === a ? (r.top += l[0], r.left += l[1]) : "top" === a ? (r.left += l[0], r.top -= l[1]) : "bottom" === a && (r.left += l[0], r.top += l[1]), t.popper = r, t
          },
          offset: 0
        },
        preventOverflow: {
          order: 300,
          enabled: !0,
          fn: function (t, i) {
            var e = i.boundariesElement || jt(t.instance.popper);
            t.instance.reference === e && (e = jt(e));
            var n = re("transform"),
              o = t.instance.popper.style,
              r = o.top,
              s = o.left,
              a = o[n];
            o.top = "", o.left = "", o[n] = "";
            var l = Gt(t.instance.popper, t.instance.reference, i.padding, e, t.positionFixed);
            o.top = r, o.left = s, o[n] = a, i.boundaries = l;
            var c = i.priority,
              h = t.offsets.popper,
              u = {
                primary: function (t) {
                  var e = h[t];
                  return h[t] < l[t] && !i.escapeWithReference && (e = Math.max(h[t], l[t])), Kt({}, t, e)
                },
                secondary: function (t) {
                  var e = "right" === t ? "left" : "top",
                    n = h[e];
                  return h[t] > l[t] && !i.escapeWithReference && (n = Math.min(h[e], l[t] - ("right" === t ? h.width : h.height))), Kt({}, e, n)
                }
              };
            return c.forEach(function (t) {
              var e = -1 !== ["left", "top"].indexOf(t) ? "primary" : "secondary";
              h = Qt({}, h, u[e](t))
            }), t.offsets.popper = h, t
          },
          priority: ["left", "right", "top", "bottom"],
          padding: 5,
          boundariesElement: "scrollParent"
        },
        keepTogether: {
          order: 400,
          enabled: !0,
          fn: function (t) {
            var e = t.offsets,
              n = e.popper,
              i = e.reference,
              o = t.placement.split("-")[0],
              r = Math.floor,
              s = -1 !== ["top", "bottom"].indexOf(o),
              a = s ? "right" : "bottom",
              l = s ? "left" : "top",
              c = s ? "width" : "height";
            return n[a] < r(i[l]) && (t.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (t.offsets.popper[l] = r(i[a])), t
          }
        },
        arrow: {
          order: 500,
          enabled: !0,
          fn: function (t, e) {
            var n;
            if (!fe(t.instance.modifiers, "arrow", "keepTogether")) return t;
            var i = e.element;
            if ("string" == typeof i) {
              if (!(i = t.instance.popper.querySelector(i))) return t
            } else if (!t.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;
            var o = t.placement.split("-")[0],
              r = t.offsets,
              s = r.popper,
              a = r.reference,
              l = -1 !== ["left", "right"].indexOf(o),
              c = l ? "height" : "width",
              h = l ? "Top" : "Left",
              u = h.toLowerCase(),
              f = l ? "left" : "top",
              d = l ? "bottom" : "right",
              p = Zt(i)[c];
            a[d] - p < s[u] && (t.offsets.popper[u] -= s[u] - (a[d] - p)), a[u] + p > s[d] && (t.offsets.popper[u] += a[u] + p - s[d]), t.offsets.popper = Vt(t.offsets.popper);
            var m = a[u] + a[c] / 2 - p / 2,
              g = Nt(t.instance.popper),
              _ = parseFloat(g["margin" + h], 10),
              v = parseFloat(g["border" + h + "Width"], 10),
              y = m - t.offsets.popper[u] - _ - v;
            return y = Math.max(Math.min(s[c] - p, y), 0), t.arrowElement = i, t.offsets.arrow = (Kt(n = {}, u, Math.round(y)), Kt(n, f, ""), n), t
          },
          element: "[x-arrow]"
        },
        flip: {
          order: 600,
          enabled: !0,
          fn: function (p, m) {
            if (oe(p.instance.modifiers, "inner")) return p;
            if (p.flipped && p.placement === p.originalPlacement) return p;
            var g = Gt(p.instance.popper, p.instance.reference, m.padding, m.boundariesElement, p.positionFixed),
              _ = p.placement.split("-")[0],
              v = te(_),
              y = p.placement.split("-")[1] || "",
              E = [];
            switch (m.behavior) {
              case ge:
                E = [_, v];
                break;
              case _e:
                E = me(_);
                break;
              case ve:
                E = me(_, !0);
                break;
              default:
                E = m.behavior
            }
            return E.forEach(function (t, e) {
              if (_ !== t || E.length === e + 1) return p;
              _ = p.placement.split("-")[0], v = te(_);
              var n, i = p.offsets.popper,
                o = p.offsets.reference,
                r = Math.floor,
                s = "left" === _ && r(i.right) > r(o.left) || "right" === _ && r(i.left) < r(o.right) || "top" === _ && r(i.bottom) > r(o.top) || "bottom" === _ && r(i.top) < r(o.bottom),
                a = r(i.left) < r(g.left),
                l = r(i.right) > r(g.right),
                c = r(i.top) < r(g.top),
                h = r(i.bottom) > r(g.bottom),
                u = "left" === _ && a || "right" === _ && l || "top" === _ && c || "bottom" === _ && h,
                f = -1 !== ["top", "bottom"].indexOf(_),
                d = !!m.flipVariations && (f && "start" === y && a || f && "end" === y && l || !f && "start" === y && c || !f && "end" === y && h);
              (s || u || d) && (p.flipped = !0, (s || u) && (_ = E[e + 1]), d && (y = "end" === (n = y) ? "start" : "start" === n ? "end" : n), p.placement = _ + (y ? "-" + y : ""), p.offsets.popper = Qt({}, p.offsets.popper, ee(p.instance.popper, p.offsets.reference, p.placement)), p = ie(p.instance.modifiers, p, "flip"))
            }), p
          },
          behavior: "flip",
          padding: 5,
          boundariesElement: "viewport"
        },
        inner: {
          order: 700,
          enabled: !1,
          fn: function (t) {
            var e = t.placement,
              n = e.split("-")[0],
              i = t.offsets,
              o = i.popper,
              r = i.reference,
              s = -1 !== ["left", "right"].indexOf(n),
              a = -1 === ["top", "left"].indexOf(n);
            return o[s ? "left" : "top"] = r[n] - (a ? o[s ? "width" : "height"] : 0), t.placement = te(e), t.offsets.popper = Vt(o), t
          }
        },
        hide: {
          order: 800,
          enabled: !0,
          fn: function (t) {
            if (!fe(t.instance.modifiers, "hide", "preventOverflow")) return t;
            var e = t.offsets.reference,
              n = ne(t.instance.modifiers, function (t) {
                return "preventOverflow" === t.name
              }).boundaries;
            if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
              if (!0 === t.hide) return t;
              t.hide = !0, t.attributes["x-out-of-boundaries"] = ""
            } else {
              if (!1 === t.hide) return t;
              t.hide = !1, t.attributes["x-out-of-boundaries"] = !1
            }
            return t
          }
        },
        computeStyle: {
          order: 850,
          enabled: !0,
          fn: function (t, e) {
            var n = e.x,
              i = e.y,
              o = t.offsets.popper,
              r = ne(t.instance.modifiers, function (t) {
                return "applyStyle" === t.name
              }).gpuAcceleration;
            void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
            var s, a, l, c, h, u, f, d, p, m, g, _, v, y, E = void 0 !== r ? r : e.gpuAcceleration,
              b = jt(t.instance.popper),
              w = Yt(b),
              C = {
                position: o.position
              },
              T = (s = t, a = window.devicePixelRatio < 2 || !ue, l = s.offsets, c = l.popper, h = l.reference, u = Math.round, f = Math.floor, d = function (t) {
                return t
              }, p = u(h.width), m = u(c.width), g = -1 !== ["left", "right"].indexOf(s.placement), _ = -1 !== s.placement.indexOf("-"), y = a ? u : d, {
                left: (v = a ? g || _ || p % 2 == m % 2 ? u : f : d)(p % 2 == 1 && m % 2 == 1 && !_ && a ? c.left - 1 : c.left),
                top: y(c.top),
                bottom: y(c.bottom),
                right: v(c.right)
              }),
              S = "bottom" === n ? "top" : "bottom",
              D = "right" === i ? "left" : "right",
              I = re("transform"),
              A = void 0,
              O = void 0;
            if (O = "bottom" === S ? "HTML" === b.nodeName ? -b.clientHeight + T.bottom : -w.height + T.bottom : T.top, A = "right" === D ? "HTML" === b.nodeName ? -b.clientWidth + T.right : -w.width + T.right : T.left, E && I) C[I] = "translate3d(" + A + "px, " + O + "px, 0)", C[S] = 0, C[D] = 0, C.willChange = "transform";
            else {
              var N = "bottom" === S ? -1 : 1,
                k = "right" === D ? -1 : 1;
              C[S] = O * N, C[D] = A * k, C.willChange = S + ", " + D
            }
            var L = {
              "x-placement": t.placement
            };
            return t.attributes = Qt({}, L, t.attributes), t.styles = Qt({}, C, t.styles), t.arrowStyles = Qt({}, t.offsets.arrow, t.arrowStyles), t
          },
          gpuAcceleration: !0,
          x: "bottom",
          y: "right"
        },
        applyStyle: {
          order: 900,
          enabled: !0,
          fn: function (t) {
            var e, n;
            return he(t.instance.popper, t.styles), e = t.instance.popper, n = t.attributes, Object.keys(n).forEach(function (t) {
              !1 !== n[t] ? e.setAttribute(t, n[t]) : e.removeAttribute(t)
            }), t.arrowElement && Object.keys(t.arrowStyles).length && he(t.arrowElement, t.arrowStyles), t
          },
          onLoad: function (t, e, n, i, o) {
            var r = Jt(o, e, t, n.positionFixed),
              s = $t(n.placement, r, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
            return e.setAttribute("x-placement", s), he(e, {
              position: n.positionFixed ? "fixed" : "absolute"
            }), n
          },
          gpuAcceleration: void 0
        }
      }
    },
    be = function () {
      function r(t, e) {
        var n = this,
          i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
        ! function (t, e) {
          if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }(this, r), this.scheduleUpdate = function () {
          return requestAnimationFrame(n.update)
        }, this.update = At(this.update.bind(this)), this.options = Qt({}, r.Defaults, i), this.state = {
          isDestroyed: !1,
          isCreated: !1,
          scrollParents: []
        }, this.reference = t && t.jquery ? t[0] : t, this.popper = e && e.jquery ? e[0] : e, this.options.modifiers = {}, Object.keys(Qt({}, r.Defaults.modifiers, i.modifiers)).forEach(function (t) {
          n.options.modifiers[t] = Qt({}, r.Defaults.modifiers[t] || {}, i.modifiers ? i.modifiers[t] : {})
        }), this.modifiers = Object.keys(this.options.modifiers).map(function (t) {
          return Qt({
            name: t
          }, n.options.modifiers[t])
        }).sort(function (t, e) {
          return t.order - e.order
        }), this.modifiers.forEach(function (t) {
          t.enabled && Ot(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state)
        }), this.update();
        var o = this.options.eventsEnabled;
        o && this.enableEventListeners(), this.state.eventsEnabled = o
      }
      return qt(r, [{
        key: "update",
        value: function () {
          return function () {
            if (!this.state.isDestroyed) {
              var t = {
                instance: this,
                styles: {},
                arrowStyles: {},
                attributes: {},
                flipped: !1,
                offsets: {}
              };
              t.offsets.reference = Jt(this.state, this.popper, this.reference, this.options.positionFixed), t.placement = $t(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.positionFixed = this.options.positionFixed, t.offsets.popper = ee(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", t = ie(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t))
            }
          }.call(this)
        }
      }, {
        key: "destroy",
        value: function () {
          return function () {
            return this.state.isDestroyed = !0, oe(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", this.popper.style[re("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
          }.call(this)
        }
      }, {
        key: "enableEventListeners",
        value: function () {
          return function () {
            this.state.eventsEnabled || (this.state = ae(this.reference, this.options, this.state, this.scheduleUpdate))
          }.call(this)
        }
      }, {
        key: "disableEventListeners",
        value: function () {
          return le.call(this)
        }
      }]), r
    }();
  be.Utils = ("undefined" != typeof window ? window : global).PopperUtils, be.placements = de, be.Defaults = Ee;
  var we = "dropdown",
    Ce = "bs.dropdown",
    Te = "." + Ce,
    Se = ".data-api",
    De = p.fn[we],
    Ie = new RegExp("38|40|27"),
    Ae = {
      HIDE: "hide" + Te,
      HIDDEN: "hidden" + Te,
      SHOW: "show" + Te,
      SHOWN: "shown" + Te,
      CLICK: "click" + Te,
      CLICK_DATA_API: "click" + Te + Se,
      KEYDOWN_DATA_API: "keydown" + Te + Se,
      KEYUP_DATA_API: "keyup" + Te + Se
    },
    Oe = "disabled",
    Ne = "show",
    ke = "dropup",
    Le = "dropright",
    xe = "dropleft",
    Pe = "dropdown-menu-right",
    He = "position-static",
    je = '[data-toggle="dropdown"]',
    Re = ".dropdown form",
    Fe = ".dropdown-menu",
    Me = ".navbar-nav",
    We = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
    Ue = "top-start",
    Be = "top-end",
    qe = "bottom-start",
    Ke = "bottom-end",
    Qe = "right-start",
    Ve = "left-start",
    Ye = {
      offset: 0,
      flip: !0,
      boundary: "scrollParent",
      reference: "toggle",
      display: "dynamic"
    },
    ze = {
      offset: "(number|string|function)",
      flip: "boolean",
      boundary: "(string|element)",
      reference: "(string|element)",
      display: "string"
    },
    Xe = function () {
      function c(t, e) {
        this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
      }
      var t = c.prototype;
      return t.toggle = function () {
        if (!this._element.disabled && !p(this._element).hasClass(Oe)) {
          var t = c._getParentFromElement(this._element),
            e = p(this._menu).hasClass(Ne);
          if (c._clearMenus(), !e) {
            var n = {
                relatedTarget: this._element
              },
              i = p.Event(Ae.SHOW, n);
            if (p(t).trigger(i), !i.isDefaultPrevented()) {
              if (!this._inNavbar) {
                if ("undefined" == typeof be) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
                var o = this._element;
                "parent" === this._config.reference ? o = t : m.isElement(this._config.reference) && (o = this._config.reference, "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && p(t).addClass(He), this._popper = new be(o, this._menu, this._getPopperConfig())
              }
              "ontouchstart" in document.documentElement && 0 === p(t).closest(Me).length && p(document.body).children().on("mouseover", null, p.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), p(this._menu).toggleClass(Ne), p(t).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, n))
            }
          }
        }
      }, t.show = function () {
        if (!(this._element.disabled || p(this._element).hasClass(Oe) || p(this._menu).hasClass(Ne))) {
          var t = {
              relatedTarget: this._element
            },
            e = p.Event(Ae.SHOW, t),
            n = c._getParentFromElement(this._element);
          p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, t)))
        }
      }, t.hide = function () {
        if (!this._element.disabled && !p(this._element).hasClass(Oe) && p(this._menu).hasClass(Ne)) {
          var t = {
              relatedTarget: this._element
            },
            e = p.Event(Ae.HIDE, t),
            n = c._getParentFromElement(this._element);
          p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.HIDDEN, t)))
        }
      }, t.dispose = function () {
        p.removeData(this._element, Ce), p(this._element).off(Te), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null)
      }, t.update = function () {
        this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
      }, t._addEventListeners = function () {
        var e = this;
        p(this._element).on(Ae.CLICK, function (t) {
          t.preventDefault(), t.stopPropagation(), e.toggle()
        })
      }, t._getConfig = function (t) {
        return t = l({}, this.constructor.Default, p(this._element).data(), t), m.typeCheckConfig(we, t, this.constructor.DefaultType), t
      }, t._getMenuElement = function () {
        if (!this._menu) {
          var t = c._getParentFromElement(this._element);
          t && (this._menu = t.querySelector(Fe))
        }
        return this._menu
      }, t._getPlacement = function () {
        var t = p(this._element.parentNode),
          e = qe;
        return t.hasClass(ke) ? (e = Ue, p(this._menu).hasClass(Pe) && (e = Be)) : t.hasClass(Le) ? e = Qe : t.hasClass(xe) ? e = Ve : p(this._menu).hasClass(Pe) && (e = Ke), e
      }, t._detectNavbar = function () {
        return 0 < p(this._element).closest(".navbar").length
      }, t._getOffset = function () {
        var e = this,
          t = {};
        return "function" == typeof this._config.offset ? t.fn = function (t) {
          return t.offsets = l({}, t.offsets, e._config.offset(t.offsets, e._element) || {}), t
        } : t.offset = this._config.offset, t
      }, t._getPopperConfig = function () {
        var t = {
          placement: this._getPlacement(),
          modifiers: {
            offset: this._getOffset(),
            flip: {
              enabled: this._config.flip
            },
            preventOverflow: {
              boundariesElement: this._config.boundary
            }
          }
        };
        return "static" === this._config.display && (t.modifiers.applyStyle = {
          enabled: !1
        }), t
      }, c._jQueryInterface = function (e) {
        return this.each(function () {
          var t = p(this).data(Ce);
          if (t || (t = new c(this, "object" == typeof e ? e : null), p(this).data(Ce, t)), "string" == typeof e) {
            if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
            t[e]()
          }
        })
      }, c._clearMenus = function (t) {
        if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which))
          for (var e = [].slice.call(document.querySelectorAll(je)), n = 0, i = e.length; n < i; n++) {
            var o = c._getParentFromElement(e[n]),
              r = p(e[n]).data(Ce),
              s = {
                relatedTarget: e[n]
              };
            if (t && "click" === t.type && (s.clickEvent = t), r) {
              var a = r._menu;
              if (p(o).hasClass(Ne) && !(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && p.contains(o, t.target))) {
                var l = p.Event(Ae.HIDE, s);
                p(o).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), e[n].setAttribute("aria-expanded", "false"), p(a).removeClass(Ne), p(o).removeClass(Ne).trigger(p.Event(Ae.HIDDEN, s)))
              }
            }
          }
      }, c._getParentFromElement = function (t) {
        var e, n = m.getSelectorFromElement(t);
        return n && (e = document.querySelector(n)), e || t.parentNode
      }, c._dataApiKeydownHandler = function (t) {
        if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || p(t.target).closest(Fe).length)) : Ie.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !p(this).hasClass(Oe))) {
          var e = c._getParentFromElement(this),
            n = p(e).hasClass(Ne);
          if (n && (!n || 27 !== t.which && 32 !== t.which)) {
            var i = [].slice.call(e.querySelectorAll(We));
            if (0 !== i.length) {
              var o = i.indexOf(t.target);
              38 === t.which && 0 < o && o--, 40 === t.which && o < i.length - 1 && o++, o < 0 && (o = 0), i[o].focus()
            }
          } else {
            if (27 === t.which) {
              var r = e.querySelector(je);
              p(r).trigger("focus")
            }
            p(this).trigger("click")
          }
        }
      }, s(c, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return Ye
        }
      }, {
        key: "DefaultType",
        get: function () {
          return ze
        }
      }]), c
    }();
  p(document).on(Ae.KEYDOWN_DATA_API, je, Xe._dataApiKeydownHandler).on(Ae.KEYDOWN_DATA_API, Fe, Xe._dataApiKeydownHandler).on(Ae.CLICK_DATA_API + " " + Ae.KEYUP_DATA_API, Xe._clearMenus).on(Ae.CLICK_DATA_API, je, function (t) {
    t.preventDefault(), t.stopPropagation(), Xe._jQueryInterface.call(p(this), "toggle")
  }).on(Ae.CLICK_DATA_API, Re, function (t) {
    t.stopPropagation()
  }), p.fn[we] = Xe._jQueryInterface, p.fn[we].Constructor = Xe, p.fn[we].noConflict = function () {
    return p.fn[we] = De, Xe._jQueryInterface
  };
  var Ge = "modal",
    $e = "bs.modal",
    Je = "." + $e,
    Ze = p.fn[Ge],
    tn = {
      backdrop: !0,
      keyboard: !0,
      focus: !0,
      show: !0
    },
    en = {
      backdrop: "(boolean|string)",
      keyboard: "boolean",
      focus: "boolean",
      show: "boolean"
    },
    nn = {
      HIDE: "hide" + Je,
      HIDDEN: "hidden" + Je,
      SHOW: "show" + Je,
      SHOWN: "shown" + Je,
      FOCUSIN: "focusin" + Je,
      RESIZE: "resize" + Je,
      CLICK_DISMISS: "click.dismiss" + Je,
      KEYDOWN_DISMISS: "keydown.dismiss" + Je,
      MOUSEUP_DISMISS: "mouseup.dismiss" + Je,
      MOUSEDOWN_DISMISS: "mousedown.dismiss" + Je,
      CLICK_DATA_API: "click" + Je + ".data-api"
    },
    on = "modal-dialog-scrollable",
    rn = "modal-scrollbar-measure",
    sn = "modal-backdrop",
    an = "modal-open",
    ln = "fade",
    cn = "show",
    hn = ".modal-dialog",
    un = ".modal-body",
    fn = '[data-toggle="modal"]',
    dn = '[data-dismiss="modal"]',
    pn = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
    mn = ".sticky-top",
    gn = function () {
      function o(t, e) {
        this._config = this._getConfig(e), this._element = t, this._dialog = t.querySelector(hn), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0
      }
      var t = o.prototype;
      return t.toggle = function (t) {
        return this._isShown ? this.hide() : this.show(t)
      }, t.show = function (t) {
        var e = this;
        if (!this._isShown && !this._isTransitioning) {
          p(this._element).hasClass(ln) && (this._isTransitioning = !0);
          var n = p.Event(nn.SHOW, {
            relatedTarget: t
          });
          p(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), p(this._element).on(nn.CLICK_DISMISS, dn, function (t) {
            return e.hide(t)
          }), p(this._dialog).on(nn.MOUSEDOWN_DISMISS, function () {
            p(e._element).one(nn.MOUSEUP_DISMISS, function (t) {
              p(t.target).is(e._element) && (e._ignoreBackdropClick = !0)
            })
          }), this._showBackdrop(function () {
            return e._showElement(t)
          }))
        }
      }, t.hide = function (t) {
        var e = this;
        if (t && t.preventDefault(), this._isShown && !this._isTransitioning) {
          var n = p.Event(nn.HIDE);
          if (p(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
            this._isShown = !1;
            var i = p(this._element).hasClass(ln);
            if (i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), p(document).off(nn.FOCUSIN), p(this._element).removeClass(cn), p(this._element).off(nn.CLICK_DISMISS), p(this._dialog).off(nn.MOUSEDOWN_DISMISS), i) {
              var o = m.getTransitionDurationFromElement(this._element);
              p(this._element).one(m.TRANSITION_END, function (t) {
                return e._hideModal(t)
              }).emulateTransitionEnd(o)
            } else this._hideModal()
          }
        }
      }, t.dispose = function () {
        [window, this._element, this._dialog].forEach(function (t) {
          return p(t).off(Je)
        }), p(document).off(nn.FOCUSIN), p.removeData(this._element, $e), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null
      }, t.handleUpdate = function () {
        this._adjustDialog()
      }, t._getConfig = function (t) {
        return t = l({}, tn, t), m.typeCheckConfig(Ge, t, en), t
      }, t._showElement = function (t) {
        var e = this,
          n = p(this._element).hasClass(ln);
        this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), p(this._dialog).hasClass(on) ? this._dialog.querySelector(un).scrollTop = 0 : this._element.scrollTop = 0, n && m.reflow(this._element), p(this._element).addClass(cn), this._config.focus && this._enforceFocus();
        var i = p.Event(nn.SHOWN, {
            relatedTarget: t
          }),
          o = function () {
            e._config.focus && e._element.focus(), e._isTransitioning = !1, p(e._element).trigger(i)
          };
        if (n) {
          var r = m.getTransitionDurationFromElement(this._dialog);
          p(this._dialog).one(m.TRANSITION_END, o).emulateTransitionEnd(r)
        } else o()
      }, t._enforceFocus = function () {
        var e = this;
        p(document).off(nn.FOCUSIN).on(nn.FOCUSIN, function (t) {
          document !== t.target && e._element !== t.target && 0 === p(e._element).has(t.target).length && e._element.focus()
        })
      }, t._setEscapeEvent = function () {
        var e = this;
        this._isShown && this._config.keyboard ? p(this._element).on(nn.KEYDOWN_DISMISS, function (t) {
          27 === t.which && (t.preventDefault(), e.hide())
        }) : this._isShown || p(this._element).off(nn.KEYDOWN_DISMISS)
      }, t._setResizeEvent = function () {
        var e = this;
        this._isShown ? p(window).on(nn.RESIZE, function (t) {
          return e.handleUpdate(t)
        }) : p(window).off(nn.RESIZE)
      }, t._hideModal = function () {
        var t = this;
        this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
          p(document.body).removeClass(an), t._resetAdjustments(), t._resetScrollbar(), p(t._element).trigger(nn.HIDDEN)
        })
      }, t._removeBackdrop = function () {
        this._backdrop && (p(this._backdrop).remove(), this._backdrop = null)
      }, t._showBackdrop = function (t) {
        var e = this,
          n = p(this._element).hasClass(ln) ? ln : "";
        if (this._isShown && this._config.backdrop) {
          if (this._backdrop = document.createElement("div"), this._backdrop.className = sn, n && this._backdrop.classList.add(n), p(this._backdrop).appendTo(document.body), p(this._element).on(nn.CLICK_DISMISS, function (t) {
              e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide())
            }), n && m.reflow(this._backdrop), p(this._backdrop).addClass(cn), !t) return;
          if (!n) return void t();
          var i = m.getTransitionDurationFromElement(this._backdrop);
          p(this._backdrop).one(m.TRANSITION_END, t).emulateTransitionEnd(i)
        } else if (!this._isShown && this._backdrop) {
          p(this._backdrop).removeClass(cn);
          var o = function () {
            e._removeBackdrop(), t && t()
          };
          if (p(this._element).hasClass(ln)) {
            var r = m.getTransitionDurationFromElement(this._backdrop);
            p(this._backdrop).one(m.TRANSITION_END, o).emulateTransitionEnd(r)
          } else o()
        } else t && t()
      }, t._adjustDialog = function () {
        var t = this._element.scrollHeight > document.documentElement.clientHeight;
        !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
      }, t._resetAdjustments = function () {
        this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
      }, t._checkScrollbar = function () {
        var t = document.body.getBoundingClientRect();
        this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
      }, t._setScrollbar = function () {
        var o = this;
        if (this._isBodyOverflowing) {
          var t = [].slice.call(document.querySelectorAll(pn)),
            e = [].slice.call(document.querySelectorAll(mn));
          p(t).each(function (t, e) {
            var n = e.style.paddingRight,
              i = p(e).css("padding-right");
            p(e).data("padding-right", n).css("padding-right", parseFloat(i) + o._scrollbarWidth + "px")
          }), p(e).each(function (t, e) {
            var n = e.style.marginRight,
              i = p(e).css("margin-right");
            p(e).data("margin-right", n).css("margin-right", parseFloat(i) - o._scrollbarWidth + "px")
          });
          var n = document.body.style.paddingRight,
            i = p(document.body).css("padding-right");
          p(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px")
        }
        p(document.body).addClass(an)
      }, t._resetScrollbar = function () {
        var t = [].slice.call(document.querySelectorAll(pn));
        p(t).each(function (t, e) {
          var n = p(e).data("padding-right");
          p(e).removeData("padding-right"), e.style.paddingRight = n || ""
        });
        var e = [].slice.call(document.querySelectorAll("" + mn));
        p(e).each(function (t, e) {
          var n = p(e).data("margin-right");
          "undefined" != typeof n && p(e).css("margin-right", n).removeData("margin-right")
        });
        var n = p(document.body).data("padding-right");
        p(document.body).removeData("padding-right"), document.body.style.paddingRight = n || ""
      }, t._getScrollbarWidth = function () {
        var t = document.createElement("div");
        t.className = rn, document.body.appendChild(t);
        var e = t.getBoundingClientRect().width - t.clientWidth;
        return document.body.removeChild(t), e
      }, o._jQueryInterface = function (n, i) {
        return this.each(function () {
          var t = p(this).data($e),
            e = l({}, tn, p(this).data(), "object" == typeof n && n ? n : {});
          if (t || (t = new o(this, e), p(this).data($e, t)), "string" == typeof n) {
            if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
            t[n](i)
          } else e.show && t.show(i)
        })
      }, s(o, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return tn
        }
      }]), o
    }();
  p(document).on(nn.CLICK_DATA_API, fn, function (t) {
    var e, n = this,
      i = m.getSelectorFromElement(this);
    i && (e = document.querySelector(i));
    var o = p(e).data($e) ? "toggle" : l({}, p(e).data(), p(this).data());
    "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
    var r = p(e).one(nn.SHOW, function (t) {
      t.isDefaultPrevented() || r.one(nn.HIDDEN, function () {
        p(n).is(":visible") && n.focus()
      })
    });
    gn._jQueryInterface.call(p(e), o, this)
  }), p.fn[Ge] = gn._jQueryInterface, p.fn[Ge].Constructor = gn, p.fn[Ge].noConflict = function () {
    return p.fn[Ge] = Ze, gn._jQueryInterface
  };
  var _n = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
    vn = {
      "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
      a: ["target", "href", "title", "rel"],
      area: [],
      b: [],
      br: [],
      col: [],
      code: [],
      div: [],
      em: [],
      hr: [],
      h1: [],
      h2: [],
      h3: [],
      h4: [],
      h5: [],
      h6: [],
      i: [],
      img: ["src", "alt", "title", "width", "height"],
      li: [],
      ol: [],
      p: [],
      pre: [],
      s: [],
      small: [],
      span: [],
      sub: [],
      sup: [],
      strong: [],
      u: [],
      ul: []
    },
    yn = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
    En = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

  function bn(t, s, e) {
    if (0 === t.length) return t;
    if (e && "function" == typeof e) return e(t);
    for (var n = (new window.DOMParser).parseFromString(t, "text/html"), a = Object.keys(s), l = [].slice.call(n.body.querySelectorAll("*")), i = function (t, e) {
        var n = l[t],
          i = n.nodeName.toLowerCase();
        if (-1 === a.indexOf(n.nodeName.toLowerCase())) return n.parentNode.removeChild(n), "continue";
        var o = [].slice.call(n.attributes),
          r = [].concat(s["*"] || [], s[i] || []);
        o.forEach(function (t) {
          (function (t, e) {
            var n = t.nodeName.toLowerCase();
            if (-1 !== e.indexOf(n)) return -1 === _n.indexOf(n) || Boolean(t.nodeValue.match(yn) || t.nodeValue.match(En));
            for (var i = e.filter(function (t) {
                return t instanceof RegExp
              }), o = 0, r = i.length; o < r; o++)
              if (n.match(i[o])) return !0;
            return !1
          })(t, r) || n.removeAttribute(t.nodeName)
        })
      }, o = 0, r = l.length; o < r; o++) i(o);
    return n.body.innerHTML
  }
  var wn = "tooltip",
    Cn = "bs.tooltip",
    Tn = "." + Cn,
    Sn = p.fn[wn],
    Dn = "bs-tooltip",
    In = new RegExp("(^|\\s)" + Dn + "\\S+", "g"),
    An = ["sanitize", "whiteList", "sanitizeFn"],
    On = {
      animation: "boolean",
      template: "string",
      title: "(string|element|function)",
      trigger: "string",
      delay: "(number|object)",
      html: "boolean",
      selector: "(string|boolean)",
      placement: "(string|function)",
      offset: "(number|string|function)",
      container: "(string|element|boolean)",
      fallbackPlacement: "(string|array)",
      boundary: "(string|element)",
      sanitize: "boolean",
      sanitizeFn: "(null|function)",
      whiteList: "object"
    },
    Nn = {
      AUTO: "auto",
      TOP: "top",
      RIGHT: "right",
      BOTTOM: "bottom",
      LEFT: "left"
    },
    kn = {
      animation: !0,
      template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
      trigger: "hover focus",
      title: "",
      delay: 0,
      html: !1,
      selector: !1,
      placement: "top",
      offset: 0,
      container: !1,
      fallbackPlacement: "flip",
      boundary: "scrollParent",
      sanitize: !0,
      sanitizeFn: null,
      whiteList: vn
    },
    Ln = "show",
    xn = "out",
    Pn = {
      HIDE: "hide" + Tn,
      HIDDEN: "hidden" + Tn,
      SHOW: "show" + Tn,
      SHOWN: "shown" + Tn,
      INSERTED: "inserted" + Tn,
      CLICK: "click" + Tn,
      FOCUSIN: "focusin" + Tn,
      FOCUSOUT: "focusout" + Tn,
      MOUSEENTER: "mouseenter" + Tn,
      MOUSELEAVE: "mouseleave" + Tn
    },
    Hn = "fade",
    jn = "show",
    Rn = ".tooltip-inner",
    Fn = ".arrow",
    Mn = "hover",
    Wn = "focus",
    Un = "click",
    Bn = "manual",
    qn = function () {
      function i(t, e) {
        if ("undefined" == typeof be) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
        this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners()
      }
      var t = i.prototype;
      return t.enable = function () {
        this._isEnabled = !0
      }, t.disable = function () {
        this._isEnabled = !1
      }, t.toggleEnabled = function () {
        this._isEnabled = !this._isEnabled
      }, t.toggle = function (t) {
        if (this._isEnabled)
          if (t) {
            var e = this.constructor.DATA_KEY,
              n = p(t.currentTarget).data(e);
            n || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n)
          } else {
            if (p(this.getTipElement()).hasClass(jn)) return void this._leave(null, this);
            this._enter(null, this)
          }
      }, t.dispose = function () {
        clearTimeout(this._timeout), p.removeData(this.element, this.constructor.DATA_KEY), p(this.element).off(this.constructor.EVENT_KEY), p(this.element).closest(".modal").off("hide.bs.modal"), this.tip && p(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
      }, t.show = function () {
        var e = this;
        if ("none" === p(this.element).css("display")) throw new Error("Please use show on visible elements");
        var t = p.Event(this.constructor.Event.SHOW);
        if (this.isWithContent() && this._isEnabled) {
          p(this.element).trigger(t);
          var n = m.findShadowRoot(this.element),
            i = p.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);
          if (t.isDefaultPrevented() || !i) return;
          var o = this.getTipElement(),
            r = m.getUID(this.constructor.NAME);
          o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && p(o).addClass(Hn);
          var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
            a = this._getAttachment(s);
          this.addAttachmentClass(a);
          var l = this._getContainer();
          p(o).data(this.constructor.DATA_KEY, this), p.contains(this.element.ownerDocument.documentElement, this.tip) || p(o).appendTo(l), p(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new be(this.element, o, {
            placement: a,
            modifiers: {
              offset: this._getOffset(),
              flip: {
                behavior: this.config.fallbackPlacement
              },
              arrow: {
                element: Fn
              },
              preventOverflow: {
                boundariesElement: this.config.boundary
              }
            },
            onCreate: function (t) {
              t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t)
            },
            onUpdate: function (t) {
              return e._handlePopperPlacementChange(t)
            }
          }), p(o).addClass(jn), "ontouchstart" in document.documentElement && p(document.body).children().on("mouseover", null, p.noop);
          var c = function () {
            e.config.animation && e._fixTransition();
            var t = e._hoverState;
            e._hoverState = null, p(e.element).trigger(e.constructor.Event.SHOWN), t === xn && e._leave(null, e)
          };
          if (p(this.tip).hasClass(Hn)) {
            var h = m.getTransitionDurationFromElement(this.tip);
            p(this.tip).one(m.TRANSITION_END, c).emulateTransitionEnd(h)
          } else c()
        }
      }, t.hide = function (t) {
        var e = this,
          n = this.getTipElement(),
          i = p.Event(this.constructor.Event.HIDE),
          o = function () {
            e._hoverState !== Ln && n.parentNode && n.parentNode.removeChild(n), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), p(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t()
          };
        if (p(this.element).trigger(i), !i.isDefaultPrevented()) {
          if (p(n).removeClass(jn), "ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), this._activeTrigger[Un] = !1, this._activeTrigger[Wn] = !1, this._activeTrigger[Mn] = !1, p(this.tip).hasClass(Hn)) {
            var r = m.getTransitionDurationFromElement(n);
            p(n).one(m.TRANSITION_END, o).emulateTransitionEnd(r)
          } else o();
          this._hoverState = ""
        }
      }, t.update = function () {
        null !== this._popper && this._popper.scheduleUpdate()
      }, t.isWithContent = function () {
        return Boolean(this.getTitle())
      }, t.addAttachmentClass = function (t) {
        p(this.getTipElement()).addClass(Dn + "-" + t)
      }, t.getTipElement = function () {
        return this.tip = this.tip || p(this.config.template)[0], this.tip
      }, t.setContent = function () {
        var t = this.getTipElement();
        this.setElementContent(p(t.querySelectorAll(Rn)), this.getTitle()), p(t).removeClass(Hn + " " + jn)
      }, t.setElementContent = function (t, e) {
        "object" != typeof e || !e.nodeType && !e.jquery ? this.config.html ? (this.config.sanitize && (e = bn(e, this.config.whiteList, this.config.sanitizeFn)), t.html(e)) : t.text(e) : this.config.html ? p(e).parent().is(t) || t.empty().append(e) : t.text(p(e).text())
      }, t.getTitle = function () {
        var t = this.element.getAttribute("data-original-title");
        return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t
      }, t._getOffset = function () {
        var e = this,
          t = {};
        return "function" == typeof this.config.offset ? t.fn = function (t) {
          return t.offsets = l({}, t.offsets, e.config.offset(t.offsets, e.element) || {}), t
        } : t.offset = this.config.offset, t
      }, t._getContainer = function () {
        return !1 === this.config.container ? document.body : m.isElement(this.config.container) ? p(this.config.container) : p(document).find(this.config.container)
      }, t._getAttachment = function (t) {
        return Nn[t.toUpperCase()]
      }, t._setListeners = function () {
        var i = this;
        this.config.trigger.split(" ").forEach(function (t) {
          if ("click" === t) p(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (t) {
            return i.toggle(t)
          });
          else if (t !== Bn) {
            var e = t === Mn ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
              n = t === Mn ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;
            p(i.element).on(e, i.config.selector, function (t) {
              return i._enter(t)
            }).on(n, i.config.selector, function (t) {
              return i._leave(t)
            })
          }
        }), p(this.element).closest(".modal").on("hide.bs.modal", function () {
          i.element && i.hide()
        }), this.config.selector ? this.config = l({}, this.config, {
          trigger: "manual",
          selector: ""
        }) : this._fixTitle()
      }, t._fixTitle = function () {
        var t = typeof this.element.getAttribute("data-original-title");
        (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
      }, t._enter = function (t, e) {
        var n = this.constructor.DATA_KEY;
        (e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusin" === t.type ? Wn : Mn] = !0), p(e.getTipElement()).hasClass(jn) || e._hoverState === Ln ? e._hoverState = Ln : (clearTimeout(e._timeout), e._hoverState = Ln, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
          e._hoverState === Ln && e.show()
        }, e.config.delay.show) : e.show())
      }, t._leave = function (t, e) {
        var n = this.constructor.DATA_KEY;
        (e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusout" === t.type ? Wn : Mn] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = xn, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
          e._hoverState === xn && e.hide()
        }, e.config.delay.hide) : e.hide())
      }, t._isWithActiveTrigger = function () {
        for (var t in this._activeTrigger)
          if (this._activeTrigger[t]) return !0;
        return !1
      }, t._getConfig = function (t) {
        var e = p(this.element).data();
        return Object.keys(e).forEach(function (t) {
          -1 !== An.indexOf(t) && delete e[t]
        }), "number" == typeof (t = l({}, this.constructor.Default, e, "object" == typeof t && t ? t : {})).delay && (t.delay = {
          show: t.delay,
          hide: t.delay
        }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), m.typeCheckConfig(wn, t, this.constructor.DefaultType), t.sanitize && (t.template = bn(t.template, t.whiteList, t.sanitizeFn)), t
      }, t._getDelegateConfig = function () {
        var t = {};
        if (this.config)
          for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
        return t
      }, t._cleanTipClass = function () {
        var t = p(this.getTipElement()),
          e = t.attr("class").match(In);
        null !== e && e.length && t.removeClass(e.join(""))
      }, t._handlePopperPlacementChange = function (t) {
        var e = t.instance;
        this.tip = e.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement))
      }, t._fixTransition = function () {
        var t = this.getTipElement(),
          e = this.config.animation;
        null === t.getAttribute("x-placement") && (p(t).removeClass(Hn), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e)
      }, i._jQueryInterface = function (n) {
        return this.each(function () {
          var t = p(this).data(Cn),
            e = "object" == typeof n && n;
          if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(Cn, t)), "string" == typeof n)) {
            if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
            t[n]()
          }
        })
      }, s(i, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return kn
        }
      }, {
        key: "NAME",
        get: function () {
          return wn
        }
      }, {
        key: "DATA_KEY",
        get: function () {
          return Cn
        }
      }, {
        key: "Event",
        get: function () {
          return Pn
        }
      }, {
        key: "EVENT_KEY",
        get: function () {
          return Tn
        }
      }, {
        key: "DefaultType",
        get: function () {
          return On
        }
      }]), i
    }();
  p.fn[wn] = qn._jQueryInterface, p.fn[wn].Constructor = qn, p.fn[wn].noConflict = function () {
    return p.fn[wn] = Sn, qn._jQueryInterface
  };
  var Kn = "popover",
    Qn = "bs.popover",
    Vn = "." + Qn,
    Yn = p.fn[Kn],
    zn = "bs-popover",
    Xn = new RegExp("(^|\\s)" + zn + "\\S+", "g"),
    Gn = l({}, qn.Default, {
      placement: "right",
      trigger: "click",
      content: "",
      template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
    }),
    $n = l({}, qn.DefaultType, {
      content: "(string|element|function)"
    }),
    Jn = "fade",
    Zn = "show",
    ti = ".popover-header",
    ei = ".popover-body",
    ni = {
      HIDE: "hide" + Vn,
      HIDDEN: "hidden" + Vn,
      SHOW: "show" + Vn,
      SHOWN: "shown" + Vn,
      INSERTED: "inserted" + Vn,
      CLICK: "click" + Vn,
      FOCUSIN: "focusin" + Vn,
      FOCUSOUT: "focusout" + Vn,
      MOUSEENTER: "mouseenter" + Vn,
      MOUSELEAVE: "mouseleave" + Vn
    },
    ii = function (t) {
      var e, n;

      function i() {
        return t.apply(this, arguments) || this
      }
      n = t, (e = i).prototype = Object.create(n.prototype), (e.prototype.constructor = e).__proto__ = n;
      var o = i.prototype;
      return o.isWithContent = function () {
        return this.getTitle() || this._getContent()
      }, o.addAttachmentClass = function (t) {
        p(this.getTipElement()).addClass(zn + "-" + t)
      }, o.getTipElement = function () {
        return this.tip = this.tip || p(this.config.template)[0], this.tip
      }, o.setContent = function () {
        var t = p(this.getTipElement());
        this.setElementContent(t.find(ti), this.getTitle());
        var e = this._getContent();
        "function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(ei), e), t.removeClass(Jn + " " + Zn)
      }, o._getContent = function () {
        return this.element.getAttribute("data-content") || this.config.content
      }, o._cleanTipClass = function () {
        var t = p(this.getTipElement()),
          e = t.attr("class").match(Xn);
        null !== e && 0 < e.length && t.removeClass(e.join(""))
      }, i._jQueryInterface = function (n) {
        return this.each(function () {
          var t = p(this).data(Qn),
            e = "object" == typeof n ? n : null;
          if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(Qn, t)), "string" == typeof n)) {
            if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
            t[n]()
          }
        })
      }, s(i, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return Gn
        }
      }, {
        key: "NAME",
        get: function () {
          return Kn
        }
      }, {
        key: "DATA_KEY",
        get: function () {
          return Qn
        }
      }, {
        key: "Event",
        get: function () {
          return ni
        }
      }, {
        key: "EVENT_KEY",
        get: function () {
          return Vn
        }
      }, {
        key: "DefaultType",
        get: function () {
          return $n
        }
      }]), i
    }(qn);
  p.fn[Kn] = ii._jQueryInterface, p.fn[Kn].Constructor = ii, p.fn[Kn].noConflict = function () {
    return p.fn[Kn] = Yn, ii._jQueryInterface
  };
  var oi = "scrollspy",
    ri = "bs.scrollspy",
    si = "." + ri,
    ai = p.fn[oi],
    li = {
      offset: 10,
      method: "auto",
      target: ""
    },
    ci = {
      offset: "number",
      method: "string",
      target: "(string|element)"
    },
    hi = {
      ACTIVATE: "activate" + si,
      SCROLL: "scroll" + si,
      LOAD_DATA_API: "load" + si + ".data-api"
    },
    ui = "dropdown-item",
    fi = "active",
    di = '[data-spy="scroll"]',
    pi = ".nav, .list-group",
    mi = ".nav-link",
    gi = ".nav-item",
    _i = ".list-group-item",
    vi = ".dropdown",
    yi = ".dropdown-item",
    Ei = ".dropdown-toggle",
    bi = "offset",
    wi = "position",
    Ci = function () {
      function n(t, e) {
        var n = this;
        this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + mi + "," + this._config.target + " " + _i + "," + this._config.target + " " + yi, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, p(this._scrollElement).on(hi.SCROLL, function (t) {
          return n._process(t)
        }), this.refresh(), this._process()
      }
      var t = n.prototype;
      return t.refresh = function () {
        var e = this,
          t = this._scrollElement === this._scrollElement.window ? bi : wi,
          o = "auto" === this._config.method ? t : this._config.method,
          r = o === wi ? this._getScrollTop() : 0;
        this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function (t) {
          var e, n = m.getSelectorFromElement(t);
          if (n && (e = document.querySelector(n)), e) {
            var i = e.getBoundingClientRect();
            if (i.width || i.height) return [p(e)[o]().top + r, n]
          }
          return null
        }).filter(function (t) {
          return t
        }).sort(function (t, e) {
          return t[0] - e[0]
        }).forEach(function (t) {
          e._offsets.push(t[0]), e._targets.push(t[1])
        })
      }, t.dispose = function () {
        p.removeData(this._element, ri), p(this._scrollElement).off(si), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
      }, t._getConfig = function (t) {
        if ("string" != typeof (t = l({}, li, "object" == typeof t && t ? t : {})).target) {
          var e = p(t.target).attr("id");
          e || (e = m.getUID(oi), p(t.target).attr("id", e)), t.target = "#" + e
        }
        return m.typeCheckConfig(oi, t, ci), t
      }, t._getScrollTop = function () {
        return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
      }, t._getScrollHeight = function () {
        return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
      }, t._getOffsetHeight = function () {
        return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
      }, t._process = function () {
        var t = this._getScrollTop() + this._config.offset,
          e = this._getScrollHeight(),
          n = this._config.offset + e - this._getOffsetHeight();
        if (this._scrollHeight !== e && this.refresh(), n <= t) {
          var i = this._targets[this._targets.length - 1];
          this._activeTarget !== i && this._activate(i)
        } else {
          if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();
          for (var o = this._offsets.length; o--;) {
            this._activeTarget !== this._targets[o] && t >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o])
          }
        }
      }, t._activate = function (e) {
        this._activeTarget = e, this._clear();
        var t = this._selector.split(",").map(function (t) {
            return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
          }),
          n = p([].slice.call(document.querySelectorAll(t.join(","))));
        n.hasClass(ui) ? (n.closest(vi).find(Ei).addClass(fi), n.addClass(fi)) : (n.addClass(fi), n.parents(pi).prev(mi + ", " + _i).addClass(fi), n.parents(pi).prev(gi).children(mi).addClass(fi)), p(this._scrollElement).trigger(hi.ACTIVATE, {
          relatedTarget: e
        })
      }, t._clear = function () {
        [].slice.call(document.querySelectorAll(this._selector)).filter(function (t) {
          return t.classList.contains(fi)
        }).forEach(function (t) {
          return t.classList.remove(fi)
        })
      }, n._jQueryInterface = function (e) {
        return this.each(function () {
          var t = p(this).data(ri);
          if (t || (t = new n(this, "object" == typeof e && e), p(this).data(ri, t)), "string" == typeof e) {
            if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
            t[e]()
          }
        })
      }, s(n, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "Default",
        get: function () {
          return li
        }
      }]), n
    }();
  p(window).on(hi.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(di)), e = t.length; e--;) {
      var n = p(t[e]);
      Ci._jQueryInterface.call(n, n.data())
    }
  }), p.fn[oi] = Ci._jQueryInterface, p.fn[oi].Constructor = Ci, p.fn[oi].noConflict = function () {
    return p.fn[oi] = ai, Ci._jQueryInterface
  };
  var Ti = "bs.tab",
    Si = "." + Ti,
    Di = p.fn.tab,
    Ii = {
      HIDE: "hide" + Si,
      HIDDEN: "hidden" + Si,
      SHOW: "show" + Si,
      SHOWN: "shown" + Si,
      CLICK_DATA_API: "click" + Si + ".data-api"
    },
    Ai = "dropdown-menu",
    Oi = "active",
    Ni = "disabled",
    ki = "fade",
    Li = "show",
    xi = ".dropdown",
    Pi = ".nav, .list-group",
    Hi = ".active",
    ji = "> li > .active",
    Ri = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
    Fi = ".dropdown-toggle",
    Mi = "> .dropdown-menu .active",
    Wi = function () {
      function i(t) {
        this._element = t
      }
      var t = i.prototype;
      return t.show = function () {
        var n = this;
        if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && p(this._element).hasClass(Oi) || p(this._element).hasClass(Ni))) {
          var t, i, e = p(this._element).closest(Pi)[0],
            o = m.getSelectorFromElement(this._element);
          if (e) {
            var r = "UL" === e.nodeName || "OL" === e.nodeName ? ji : Hi;
            i = (i = p.makeArray(p(e).find(r)))[i.length - 1]
          }
          var s = p.Event(Ii.HIDE, {
              relatedTarget: this._element
            }),
            a = p.Event(Ii.SHOW, {
              relatedTarget: i
            });
          if (i && p(i).trigger(s), p(this._element).trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
            o && (t = document.querySelector(o)), this._activate(this._element, e);
            var l = function () {
              var t = p.Event(Ii.HIDDEN, {
                  relatedTarget: n._element
                }),
                e = p.Event(Ii.SHOWN, {
                  relatedTarget: i
                });
              p(i).trigger(t), p(n._element).trigger(e)
            };
            t ? this._activate(t, t.parentNode, l) : l()
          }
        }
      }, t.dispose = function () {
        p.removeData(this._element, Ti), this._element = null
      }, t._activate = function (t, e, n) {
        var i = this,
          o = (!e || "UL" !== e.nodeName && "OL" !== e.nodeName ? p(e).children(Hi) : p(e).find(ji))[0],
          r = n && o && p(o).hasClass(ki),
          s = function () {
            return i._transitionComplete(t, o, n)
          };
        if (o && r) {
          var a = m.getTransitionDurationFromElement(o);
          p(o).removeClass(Li).one(m.TRANSITION_END, s).emulateTransitionEnd(a)
        } else s()
      }, t._transitionComplete = function (t, e, n) {
        if (e) {
          p(e).removeClass(Oi);
          var i = p(e.parentNode).find(Mi)[0];
          i && p(i).removeClass(Oi), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1)
        }
        if (p(t).addClass(Oi), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), m.reflow(t), t.classList.contains(ki) && t.classList.add(Li), t.parentNode && p(t.parentNode).hasClass(Ai)) {
          var o = p(t).closest(xi)[0];
          if (o) {
            var r = [].slice.call(o.querySelectorAll(Fi));
            p(r).addClass(Oi)
          }
          t.setAttribute("aria-expanded", !0)
        }
        n && n()
      }, i._jQueryInterface = function (n) {
        return this.each(function () {
          var t = p(this),
            e = t.data(Ti);
          if (e || (e = new i(this), t.data(Ti, e)), "string" == typeof n) {
            if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
            e[n]()
          }
        })
      }, s(i, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }]), i
    }();
  p(document).on(Ii.CLICK_DATA_API, Ri, function (t) {
    t.preventDefault(), Wi._jQueryInterface.call(p(this), "show")
  }), p.fn.tab = Wi._jQueryInterface, p.fn.tab.Constructor = Wi, p.fn.tab.noConflict = function () {
    return p.fn.tab = Di, Wi._jQueryInterface
  };
  var Ui = "toast",
    Bi = "bs.toast",
    qi = "." + Bi,
    Ki = p.fn[Ui],
    Qi = {
      CLICK_DISMISS: "click.dismiss" + qi,
      HIDE: "hide" + qi,
      HIDDEN: "hidden" + qi,
      SHOW: "show" + qi,
      SHOWN: "shown" + qi
    },
    Vi = "fade",
    Yi = "hide",
    zi = "show",
    Xi = "showing",
    Gi = {
      animation: "boolean",
      autohide: "boolean",
      delay: "number"
    },
    $i = {
      animation: !0,
      autohide: !0,
      delay: 500
    },
    Ji = '[data-dismiss="toast"]',
    Zi = function () {
      function i(t, e) {
        this._element = t, this._config = this._getConfig(e), this._timeout = null, this._setListeners()
      }
      var t = i.prototype;
      return t.show = function () {
        var t = this;
        p(this._element).trigger(Qi.SHOW), this._config.animation && this._element.classList.add(Vi);
        var e = function () {
          t._element.classList.remove(Xi), t._element.classList.add(zi), p(t._element).trigger(Qi.SHOWN), t._config.autohide && t.hide()
        };
        if (this._element.classList.remove(Yi), this._element.classList.add(Xi), this._config.animation) {
          var n = m.getTransitionDurationFromElement(this._element);
          p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n)
        } else e()
      }, t.hide = function (t) {
        var e = this;
        this._element.classList.contains(zi) && (p(this._element).trigger(Qi.HIDE), t ? this._close() : this._timeout = setTimeout(function () {
          e._close()
        }, this._config.delay))
      }, t.dispose = function () {
        clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(zi) && this._element.classList.remove(zi), p(this._element).off(Qi.CLICK_DISMISS), p.removeData(this._element, Bi), this._element = null, this._config = null
      }, t._getConfig = function (t) {
        return t = l({}, $i, p(this._element).data(), "object" == typeof t && t ? t : {}), m.typeCheckConfig(Ui, t, this.constructor.DefaultType), t
      }, t._setListeners = function () {
        var t = this;
        p(this._element).on(Qi.CLICK_DISMISS, Ji, function () {
          return t.hide(!0)
        })
      }, t._close = function () {
        var t = this,
          e = function () {
            t._element.classList.add(Yi), p(t._element).trigger(Qi.HIDDEN)
          };
        if (this._element.classList.remove(zi), this._config.animation) {
          var n = m.getTransitionDurationFromElement(this._element);
          p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n)
        } else e()
      }, i._jQueryInterface = function (n) {
        return this.each(function () {
          var t = p(this),
            e = t.data(Bi);
          if (e || (e = new i(this, "object" == typeof n && n), t.data(Bi, e)), "string" == typeof n) {
            if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
            e[n](this)
          }
        })
      }, s(i, null, [{
        key: "VERSION",
        get: function () {
          return "4.3.1"
        }
      }, {
        key: "DefaultType",
        get: function () {
          return Gi
        }
      }, {
        key: "Default",
        get: function () {
          return $i
        }
      }]), i
    }();
  p.fn[Ui] = Zi._jQueryInterface, p.fn[Ui].Constructor = Zi, p.fn[Ui].noConflict = function () {
      return p.fn[Ui] = Ki, Zi._jQueryInterface
    },
    function () {
      if ("undefined" == typeof p) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
      var t = p.fn.jquery.split(" ")[0].split(".");
      if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
    }(), t.Util = m, t.Alert = g, t.Button = k, t.Carousel = at, t.Collapse = Ct, t.Dropdown = Xe, t.Modal = gn, t.Popover = ii, t.Scrollspy = Ci, t.Tab = Wi, t.Toast = Zi, t.Tooltip = qn, Object.defineProperty(t, "__esModule", {
      value: !0
    })
});
//# sourceMappingURL=bootstrap.bundle.min.js.map
/*!
 * in-view 0.6.1 - Get notified when a DOM element enters or exits the viewport.
 * Copyright (c) 2016 Cam Wiegert <cam@camwiegert.com> - https://camwiegert.github.io/in-view
 * License: MIT
 */
! function (t, e) {
  "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.inView = e() : t.inView = e()
}(this, function () {
  return function (t) {
    function e(r) {
      if (n[r]) return n[r].exports;
      var i = n[r] = {
        exports: {},
        id: r,
        loaded: !1
      };
      return t[r].call(i.exports, i, i.exports, e), i.loaded = !0, i.exports
    }
    var n = {};
    return e.m = t, e.c = n, e.p = "", e(0)
  }([function (t, e, n) {
    "use strict";

    function r(t) {
      return t && t.__esModule ? t : {
        "default": t
      }
    }
    var i = n(2),
      o = r(i);
    t.exports = o["default"]
  }, function (t, e) {
    function n(t) {
      var e = typeof t;
      return null != t && ("object" == e || "function" == e)
    }
    t.exports = n
  }, function (t, e, n) {
    "use strict";

    function r(t) {
      return t && t.__esModule ? t : {
        "default": t
      }
    }
    Object.defineProperty(e, "__esModule", {
      value: !0
    });
    var i = n(9),
      o = r(i),
      u = n(3),
      f = r(u),
      s = n(4),
      c = function () {
        if ("undefined" != typeof window) {
          var t = 100,
            e = ["scroll", "resize", "load"],
            n = {
              history: []
            },
            r = {
              offset: {},
              threshold: 0,
              test: s.inViewport
            },
            i = (0, o["default"])(function () {
              n.history.forEach(function (t) {
                n[t].check()
              })
            }, t);
          e.forEach(function (t) {
            return addEventListener(t, i)
          }), window.MutationObserver && addEventListener("DOMContentLoaded", function () {
            new MutationObserver(i).observe(document.body, {
              attributes: !0,
              childList: !0,
              subtree: !0
            })
          });
          var u = function (t) {
            if ("string" == typeof t) {
              var e = [].slice.call(document.querySelectorAll(t));
              return n.history.indexOf(t) > -1 ? n[t].elements = e : (n[t] = (0, f["default"])(e, r), n.history.push(t)), n[t]
            }
          };
          return u.offset = function (t) {
            if (void 0 === t) return r.offset;
            var e = function (t) {
              return "number" == typeof t
            };
            return ["top", "right", "bottom", "left"].forEach(e(t) ? function (e) {
              return r.offset[e] = t
            } : function (n) {
              return e(t[n]) ? r.offset[n] = t[n] : null
            }), r.offset
          }, u.threshold = function (t) {
            return "number" == typeof t && t >= 0 && t <= 1 ? r.threshold = t : r.threshold
          }, u.test = function (t) {
            return "function" == typeof t ? r.test = t : r.test
          }, u.is = function (t) {
            return r.test(t, r)
          }, u.offset(0), u
        }
      };
    e["default"] = c()
  }, function (t, e) {
    "use strict";

    function n(t, e) {
      if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(e, "__esModule", {
      value: !0
    });
    var r = function () {
        function t(t, e) {
          for (var n = 0; n < e.length; n++) {
            var r = e[n];
            r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
          }
        }
        return function (e, n, r) {
          return n && t(e.prototype, n), r && t(e, r), e
        }
      }(),
      i = function () {
        function t(e, r) {
          n(this, t), this.options = r, this.elements = e, this.current = [], this.handlers = {
            enter: [],
            exit: []
          }, this.singles = {
            enter: [],
            exit: []
          }
        }
        return r(t, [{
          key: "check",
          value: function () {
            var t = this;
            return this.elements.forEach(function (e) {
              var n = t.options.test(e, t.options),
                r = t.current.indexOf(e),
                i = r > -1,
                o = n && !i,
                u = !n && i;
              o && (t.current.push(e), t.emit("enter", e)), u && (t.current.splice(r, 1), t.emit("exit", e))
            }), this
          }
        }, {
          key: "on",
          value: function (t, e) {
            return this.handlers[t].push(e), this
          }
        }, {
          key: "once",
          value: function (t, e) {
            return this.singles[t].unshift(e), this
          }
        }, {
          key: "emit",
          value: function (t, e) {
            for (; this.singles[t].length;) this.singles[t].pop()(e);
            for (var n = this.handlers[t].length; --n > -1;) this.handlers[t][n](e);
            return this
          }
        }]), t
      }();
    e["default"] = function (t, e) {
      return new i(t, e)
    }
  }, function (t, e) {
    "use strict";

    function n(t, e) {
      var n = t.getBoundingClientRect(),
        r = n.top,
        i = n.right,
        o = n.bottom,
        u = n.left,
        f = n.width,
        s = n.height,
        c = {
          t: o,
          r: window.innerWidth - u,
          b: window.innerHeight - r,
          l: i
        },
        a = {
          x: e.threshold * f,
          y: e.threshold * s
        };
      return c.t > e.offset.top + a.y && c.r > e.offset.right + a.x && c.b > e.offset.bottom + a.y && c.l > e.offset.left + a.x
    }
    Object.defineProperty(e, "__esModule", {
      value: !0
    }), e.inViewport = n
  }, function (t, e) {
    (function (e) {
      var n = "object" == typeof e && e && e.Object === Object && e;
      t.exports = n
    }).call(e, function () {
      return this
    }())
  }, function (t, e, n) {
    var r = n(5),
      i = "object" == typeof self && self && self.Object === Object && self,
      o = r || i || Function("return this")();
    t.exports = o
  }, function (t, e, n) {
    function r(t, e, n) {
      function r(e) {
        var n = x,
          r = m;
        return x = m = void 0, E = e, w = t.apply(r, n)
      }

      function a(t) {
        return E = t, j = setTimeout(h, e), M ? r(t) : w
      }

      function l(t) {
        var n = t - O,
          r = t - E,
          i = e - n;
        return _ ? c(i, g - r) : i
      }

      function d(t) {
        var n = t - O,
          r = t - E;
        return void 0 === O || n >= e || n < 0 || _ && r >= g
      }

      function h() {
        var t = o();
        return d(t) ? p(t) : void(j = setTimeout(h, l(t)))
      }

      function p(t) {
        return j = void 0, T && x ? r(t) : (x = m = void 0, w)
      }

      function v() {
        void 0 !== j && clearTimeout(j), E = 0, x = O = m = j = void 0
      }

      function y() {
        return void 0 === j ? w : p(o())
      }

      function b() {
        var t = o(),
          n = d(t);
        if (x = arguments, m = this, O = t, n) {
          if (void 0 === j) return a(O);
          if (_) return j = setTimeout(h, e), r(O)
        }
        return void 0 === j && (j = setTimeout(h, e)), w
      }
      var x, m, g, w, j, O, E = 0,
        M = !1,
        _ = !1,
        T = !0;
      if ("function" != typeof t) throw new TypeError(f);
      return e = u(e) || 0, i(n) && (M = !!n.leading, _ = "maxWait" in n, g = _ ? s(u(n.maxWait) || 0, e) : g, T = "trailing" in n ? !!n.trailing : T), b.cancel = v, b.flush = y, b
    }
    var i = n(1),
      o = n(8),
      u = n(10),
      f = "Expected a function",
      s = Math.max,
      c = Math.min;
    t.exports = r
  }, function (t, e, n) {
    var r = n(6),
      i = function () {
        return r.Date.now()
      };
    t.exports = i
  }, function (t, e, n) {
    function r(t, e, n) {
      var r = !0,
        f = !0;
      if ("function" != typeof t) throw new TypeError(u);
      return o(n) && (r = "leading" in n ? !!n.leading : r, f = "trailing" in n ? !!n.trailing : f), i(t, e, {
        leading: r,
        maxWait: e,
        trailing: f
      })
    }
    var i = n(7),
      o = n(1),
      u = "Expected a function";
    t.exports = r
  }, function (t, e) {
    function n(t) {
      return t
    }
    t.exports = n
  }])
});
/*
Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function () {
  var c, f;
  c = this.jQuery || window.jQuery;
  f = c(window);
  c.fn.stick_in_parent = function (b) {
    var A, w, B, n, p, J, k, E, t, K, q, L;
    null == b && (b = {});
    t = b.sticky_class;
    B = b.inner_scrolling;
    E = b.recalc_every;
    k = b.parent;
    p = b.offset_top;
    n = b.spacer;
    w = b.bottoming;
    null == p && (p = 0);
    null == k && (k = void 0);
    null == B && (B = !0);
    null == t && (t = "is_stuck");
    A = c(document);
    null == w && (w = !0);
    J = function (a) {
      var b;
      return window.getComputedStyle ? (a = window.getComputedStyle(a[0]), b = parseFloat(a.getPropertyValue("width")) + parseFloat(a.getPropertyValue("margin-left")) +
        parseFloat(a.getPropertyValue("margin-right")), "border-box" !== a.getPropertyValue("box-sizing") && (b += parseFloat(a.getPropertyValue("border-left-width")) + parseFloat(a.getPropertyValue("border-right-width")) + parseFloat(a.getPropertyValue("padding-left")) + parseFloat(a.getPropertyValue("padding-right"))), b) : a.outerWidth(!0)
    };
    K = function (a, b, q, C, F, u, r, G) {
      var v, H, m, D, I, d, g, x, y, z, h, l;
      if (!a.data("sticky_kit")) {
        a.data("sticky_kit", !0);
        I = A.height();
        g = a.parent();
        null != k && (g = g.closest(k));
        if (!g.length) throw "failed to find stick parent";
        v = m = !1;
        (h = null != n ? n && a.closest(n) : c("<div />")) && h.css("position", a.css("position"));
        x = function () {
          var d, f, e;
          if (!G && (I = A.height(), d = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), b = parseInt(g.css("padding-bottom"), 10), q = g.offset().top + d + f, C = g.height(), m && (v = m = !1, null == n && (a.insertAfter(h), h.detach()), a.css({
              position: "",
              top: "",
              width: "",
              bottom: ""
            }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - p, u = a.outerHeight(!0), r = a.css("float"), h && h.css({
              width: J(a),
              height: u,
              display: a.css("display"),
              "vertical-align": a.css("vertical-align"),
              "float": r
            }), e)) return l()
        };
        x();
        if (u !== C) return D = void 0, d = p, z = E, l = function () {
            var c, l, e, k;
            if (!G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + d > C + q, v && !k && (v = !1, a.css({
                  position: "fixed",
                  bottom: "",
                  top: d
                }).trigger("sticky_kit:unbottom"))), e < F && (m = !1, d = p, null == n && ("left" !== r && "right" !== r || a.insertAfter(h), h.detach()), c = {
                  position: "",
                  width: "",
                  top: ""
                }, a.css(c).removeClass(t).trigger("sticky_kit:unstick")),
                B && (c = f.height(), u + p > c && !v && (d -= l, d = Math.max(c - u, d), d = Math.min(p, d), m && a.css({
                  top: d + "px"
                })))) : e > F && (m = !0, c = {
                position: "fixed",
                top: d
              }, c.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(c).addClass(t), null == n && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + d > C + q), !v && k))) return v = !0, "static" === g.css("position") && g.css({
              position: "relative"
            }), a.css({
              position: "absolute",
              bottom: b,
              top: "auto"
            }).trigger("sticky_kit:bottom")
          },
          y = function () {
            x();
            return l()
          }, H = function () {
            G = !0;
            f.off("touchmove", l);
            f.off("scroll", l);
            f.off("resize", y);
            c(document.body).off("sticky_kit:recalc", y);
            a.off("sticky_kit:detach", H);
            a.removeData("sticky_kit");
            a.css({
              position: "",
              bottom: "",
              top: "",
              width: ""
            });
            g.position("position", "");
            if (m) return null == n && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t)
          }, f.on("touchmove", l), f.on("scroll", l), f.on("resize", y), c(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", H), setTimeout(l,
            0)
      }
    };
    q = 0;
    for (L = this.length; q < L; q++) b = this[q], K(c(b));
    return this
  }
}).call(this);

/**
 * SVGInjector v1.1.3 - Fast, caching, dynamic inline SVG DOM injection library
 * https://github.com/iconic/SVGInjector
 *
 * Copyright (c) 2014-2015 Waybury <hello@waybury.com>
 * @license MIT
 */
! function (t, e) {
  "use strict";

  function r(t) {
    t = t.split(" ");
    for (var e = {}, r = t.length, n = []; r--;) e.hasOwnProperty(t[r]) || (e[t[r]] = 1, n.unshift(t[r]));
    return n.join(" ")
  }
  var n = "file:" === t.location.protocol,
    i = e.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1"),
    o = Array.prototype.forEach || function (t, e) {
      if (void 0 === this || null === this || "function" != typeof t) throw new TypeError;
      var r, n = this.length >>> 0;
      for (r = 0; n > r; ++r) r in this && t.call(e, this[r], r, this)
    },
    a = {},
    l = 0,
    s = [],
    u = [],
    c = {},
    f = function (t) {
      return t.cloneNode(!0)
    },
    p = function (t, e) {
      u[t] = u[t] || [], u[t].push(e)
    },
    d = function (t) {
      for (var e = 0, r = u[t].length; r > e; e++) ! function (e) {
        setTimeout(function () {
          u[t][e](f(a[t]))
        }, 0)
      }(e)
    },
    v = function (e, r) {
      if (void 0 !== a[e]) a[e] instanceof SVGSVGElement ? r(f(a[e])) : p(e, r);
      else {
        if (!t.XMLHttpRequest) return r("Browser does not support XMLHttpRequest"), !1;
        a[e] = {}, p(e, r);
        var i = new XMLHttpRequest;
        i.onreadystatechange = function () {
          if (4 === i.readyState) {
            if (404 === i.status || null === i.responseXML) return r("Unable to load SVG file: " + e), n && r("Note: SVG injection ajax calls do not work locally without adjusting security setting in your browser. Or consider using a local webserver."), r(), !1;
            if (!(200 === i.status || n && 0 === i.status)) return r("There was a problem injecting the SVG: " + i.status + " " + i.statusText), !1;
            if (i.responseXML instanceof Document) a[e] = i.responseXML.documentElement;
            else if (DOMParser && DOMParser instanceof Function) {
              var t;
              try {
                var o = new DOMParser;
                t = o.parseFromString(i.responseText, "text/xml")
              } catch (l) {
                t = void 0
              }
              if (!t || t.getElementsByTagName("parsererror").length) return r("Unable to parse SVG file: " + e), !1;
              a[e] = t.documentElement
            }
            d(e)
          }
        }, i.open("GET", e), i.overrideMimeType && i.overrideMimeType("text/xml"), i.send()
      }
    },
    h = function (e, n, a, u) {
      var f = e.getAttribute("data-src") || e.getAttribute("src");
      if (!/\.svg/i.test(f)) return void u("Attempted to inject a file with a non-svg extension: " + f);
      if (!i) {
        var p = e.getAttribute("data-fallback") || e.getAttribute("data-png");
        return void(p ? (e.setAttribute("src", p), u(null)) : a ? (e.setAttribute("src", a + "/" + f.split("/").pop().replace(".svg", ".png")), u(null)) : u("This browser does not support SVG and no PNG fallback was defined."))
      } - 1 === s.indexOf(e) && (s.push(e), e.setAttribute("src", ""), v(f, function (i) {
        if ("undefined" == typeof i || "string" == typeof i) return u(i), !1;
        var a = e.getAttribute("id");
        a && i.setAttribute("id", a);
        var p = e.getAttribute("title");
        p && i.setAttribute("title", p);
        var d = [].concat(i.getAttribute("class") || [], "injected-svg", e.getAttribute("class") || []).join(" ");
        i.setAttribute("class", r(d));
        var v = e.getAttribute("style");
        v && i.setAttribute("style", v);
        var h = [].filter.call(e.attributes, function (t) {
          return /^data-\w[\w\-]*$/.test(t.name)
        });
        o.call(h, function (t) {
          t.name && t.value && i.setAttribute(t.name, t.value)
        });
        var g, m, b, y, A, w = {
          clipPath: ["clip-path"],
          "color-profile": ["color-profile"],
          cursor: ["cursor"],
          filter: ["filter"],
          linearGradient: ["fill", "stroke"],
          marker: ["marker", "marker-start", "marker-mid", "marker-end"],
          mask: ["mask"],
          pattern: ["fill", "stroke"],
          radialGradient: ["fill", "stroke"]
        };
        Object.keys(w).forEach(function (t) {
          g = t, b = w[t], m = i.querySelectorAll("defs " + g + "[id]");
          for (var e = 0, r = m.length; r > e; e++) {
            y = m[e].id, A = y + "-" + l;
            var n;
            o.call(b, function (t) {
              n = i.querySelectorAll("[" + t + '*="' + y + '"]');
              for (var e = 0, r = n.length; r > e; e++) n[e].setAttribute(t, "url(#" + A + ")")
            }), m[e].id = A
          }
        }), i.removeAttribute("xmlns:a");
        for (var x, S, k = i.querySelectorAll("script"), j = [], G = 0, T = k.length; T > G; G++) S = k[G].getAttribute("type"), S && "application/ecmascript" !== S && "application/javascript" !== S || (x = k[G].innerText || k[G].textContent, j.push(x), i.removeChild(k[G]));
        if (j.length > 0 && ("always" === n || "once" === n && !c[f])) {
          for (var M = 0, V = j.length; V > M; M++) new Function(j[M])(t);
          c[f] = !0
        }
        var E = i.querySelectorAll("style");
        o.call(E, function (t) {
          t.textContent += ""
        }), e.parentNode.replaceChild(i, e), delete s[s.indexOf(e)], e = null, l++, u(i)
      }))
    },
    g = function (t, e, r) {
      e = e || {};
      var n = e.evalScripts || "always",
        i = e.pngFallback || !1,
        a = e.each;
      if (void 0 !== t.length) {
        var l = 0;
        o.call(t, function (e) {
          h(e, n, i, function (e) {
            a && "function" == typeof a && a(e), r && t.length === ++l && r(l)
          })
        })
      } else t ? h(t, n, i, function (e) {
        a && "function" == typeof a && a(e), r && r(1), t = null
      }) : r && r(0)
    };
  "object" == typeof module && "object" == typeof module.exports ? module.exports = exports = g : "function" == typeof define && define.amd ? define(function () {
    return g
  }) : "object" == typeof t && (t.SVGInjector = g)
}(window, document);
//# sourceMappingURL=svg-injector.map.js

/**
 * jQuery CSS Customizable Scrollbar
 *
 * Copyright 2015, Yuriy Khabarov
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * If you found bug, please contact me via email <13real008@gmail.com>
 *
 * Compressed by http://jscompress.com/
 *
 * @author Yuriy Khabarov aka Gromo
 * @version 0.2.11
 * @url https://github.com/gromo/jquery.scrollbar/
 *
 */
! function (a, b) {
  "function" == typeof define && define.amd ? define(["jquery"], b) : b("undefined" != typeof exports ? require("jquery") : a.jQuery)
}(this, function (a) {
  "use strict";

  function h(b) {
    if (c.webkit && !b) return {
      height: 0,
      width: 0
    };
    if (!c.data.outer) {
      var d = {
        border: "none",
        "box-sizing": "content-box",
        height: "200px",
        margin: "0",
        padding: "0",
        width: "200px"
      };
      c.data.inner = a("<div>").css(a.extend({}, d)), c.data.outer = a("<div>").css(a.extend({
        left: "-1000px",
        overflow: "scroll",
        position: "absolute",
        top: "-1000px"
      }, d)).append(c.data.inner).appendTo("body")
    }
    return c.data.outer.scrollLeft(1e3).scrollTop(1e3), {
      height: Math.ceil(c.data.outer.offset().top - c.data.inner.offset().top || 0),
      width: Math.ceil(c.data.outer.offset().left - c.data.inner.offset().left || 0)
    }
  }

  function i() {
    var a = h(!0);
    return !(a.height || a.width)
  }

  function j(a) {
    var b = a.originalEvent;
    return (!b.axis || b.axis !== b.HORIZONTAL_AXIS) && !b.wheelDeltaX
  }
  var b = !1,
    c = {
      data: {
        index: 0,
        name: "scrollbar"
      },
      firefox: /firefox/i.test(navigator.userAgent),
      macosx: /mac/i.test(navigator.platform),
      msedge: /edge\/\d+/i.test(navigator.userAgent),
      msie: /(msie|trident)/i.test(navigator.userAgent),
      mobile: /android|webos|iphone|ipad|ipod|blackberry/i.test(navigator.userAgent),
      overlay: null,
      scroll: null,
      scrolls: [],
      webkit: /webkit/i.test(navigator.userAgent) && !/edge\/\d+/i.test(navigator.userAgent)
    };
  c.scrolls.add = function (a) {
    this.remove(a).push(a)
  }, c.scrolls.remove = function (b) {
    for (; a.inArray(b, this) >= 0;) this.splice(a.inArray(b, this), 1);
    return this
  };
  var d = {
      autoScrollSize: !0,
      autoUpdate: !0,
      debug: !1,
      disableBodyScroll: !1,
      duration: 200,
      ignoreMobile: !1,
      ignoreOverlay: !1,
      isRtl: !1,
      scrollStep: 30,
      showArrows: !1,
      stepScrolling: !0,
      scrollx: null,
      scrolly: null,
      onDestroy: null,
      onFallback: null,
      onInit: null,
      onScroll: null,
      onUpdate: null
    },
    e = function (b) {
      c.scroll || (c.overlay = i(), c.scroll = h(), g(), a(window).resize(function () {
        var a = !1;
        if (c.scroll && (c.scroll.height || c.scroll.width)) {
          var b = h();
          b.height === c.scroll.height && b.width === c.scroll.width || (c.scroll = b, a = !0)
        }
        g(a)
      })), this.container = b, this.namespace = ".scrollbar_" + c.data.index++, this.options = a.extend({}, d, window.jQueryScrollbarOptions || {}), this.scrollTo = null, this.scrollx = {}, this.scrolly = {}, b.data(c.data.name, this), c.scrolls.add(this)
    };
  e.prototype = {
    destroy: function () {
      if (this.wrapper) {
        this.container.removeData(c.data.name), c.scrolls.remove(this);
        var b = this.container.scrollLeft(),
          d = this.container.scrollTop();
        this.container.insertBefore(this.wrapper).css({
          height: "",
          margin: "",
          "max-height": ""
        }).removeClass("scroll-content scroll-scrollx_visible scroll-scrolly_visible").off(this.namespace).scrollLeft(b).scrollTop(d), this.scrollx.scroll.removeClass("scroll-scrollx_visible").find("div").addBack().off(this.namespace), this.scrolly.scroll.removeClass("scroll-scrolly_visible").find("div").addBack().off(this.namespace), this.wrapper.remove(), a(document).add("body").off(this.namespace), a.isFunction(this.options.onDestroy) && this.options.onDestroy.apply(this, [this.container])
      }
    },
    init: function (b) {
      var d = this,
        e = this.container,
        f = this.containerWrapper || e,
        g = this.namespace,
        h = a.extend(this.options, b || {}),
        i = {
          x: this.scrollx,
          y: this.scrolly
        },
        k = this.wrapper,
        l = {},
        m = {
          scrollLeft: e.scrollLeft(),
          scrollTop: e.scrollTop()
        };
      if (c.mobile && h.ignoreMobile || c.overlay && h.ignoreOverlay || c.macosx && !c.webkit) return a.isFunction(h.onFallback) && h.onFallback.apply(this, [e]), !1;
      if (k) l = {
        height: "auto",
        "margin-bottom": c.scroll.height * -1 + "px",
        "max-height": ""
      }, l[h.isRtl ? "margin-left" : "margin-right"] = c.scroll.width * -1 + "px", f.css(l);
      else {
        if (this.wrapper = k = a("<div>").addClass("scroll-wrapper").addClass(e.attr("class")).css("position", "absolute" === e.css("position") ? "absolute" : "relative").insertBefore(e).append(e), h.isRtl && k.addClass("scroll--rtl"), e.is("textarea") && (this.containerWrapper = f = a("<div>").insertBefore(e).append(e), k.addClass("scroll-textarea")), l = {
            height: "auto",
            "margin-bottom": c.scroll.height * -1 + "px",
            "max-height": ""
          }, l[h.isRtl ? "margin-left" : "margin-right"] = c.scroll.width * -1 + "px", f.addClass("scroll-content").css(l), e.on("scroll" + g, function (b) {
            var f = e.scrollLeft(),
              g = e.scrollTop();
            if (h.isRtl) switch (!0) {
              case c.firefox:
                f = Math.abs(f);
              case c.msedge || c.msie:
                f = e[0].scrollWidth - e[0].clientWidth - f
            }
            a.isFunction(h.onScroll) && h.onScroll.call(d, {
              maxScroll: i.y.maxScrollOffset,
              scroll: g,
              size: i.y.size,
              visible: i.y.visible
            }, {
              maxScroll: i.x.maxScrollOffset,
              scroll: f,
              size: i.x.size,
              visible: i.x.visible
            }), i.x.isVisible && i.x.scroll.bar.css("left", f * i.x.kx + "px"), i.y.isVisible && i.y.scroll.bar.css("top", g * i.y.kx + "px")
          }), k.on("scroll" + g, function () {
            k.scrollTop(0).scrollLeft(0)
          }), h.disableBodyScroll) {
          var n = function (a) {
            j(a) ? i.y.isVisible && i.y.mousewheel(a) : i.x.isVisible && i.x.mousewheel(a)
          };
          k.on("MozMousePixelScroll" + g, n), k.on("mousewheel" + g, n), c.mobile && k.on("touchstart" + g, function (b) {
            var c = b.originalEvent.touches && b.originalEvent.touches[0] || b,
              d = {
                pageX: c.pageX,
                pageY: c.pageY
              },
              f = {
                left: e.scrollLeft(),
                top: e.scrollTop()
              };
            a(document).on("touchmove" + g, function (a) {
              var b = a.originalEvent.targetTouches && a.originalEvent.targetTouches[0] || a;
              e.scrollLeft(f.left + d.pageX - b.pageX), e.scrollTop(f.top + d.pageY - b.pageY), a.preventDefault()
            }), a(document).on("touchend" + g, function () {
              a(document).off(g)
            })
          })
        }
        a.isFunction(h.onInit) && h.onInit.apply(this, [e])
      }
      a.each(i, function (b, f) {
        var k = null,
          l = 1,
          m = "x" === b ? "scrollLeft" : "scrollTop",
          n = h.scrollStep,
          o = function () {
            var a = e[m]();
            e[m](a + n), 1 == l && a + n >= p && (a = e[m]()), l == -1 && a + n <= p && (a = e[m]()), e[m]() == a && k && k()
          },
          p = 0;
        f.scroll || (f.scroll = d._getScroll(h["scroll" + b]).addClass("scroll-" + b), h.showArrows && f.scroll.addClass("scroll-element_arrows_visible"), f.mousewheel = function (a) {
          if (!f.isVisible || "x" === b && j(a)) return !0;
          if ("y" === b && !j(a)) return i.x.mousewheel(a), !0;
          var c = a.originalEvent.wheelDelta * -1 || a.originalEvent.detail,
            g = f.size - f.visible - f.offset;
          return c || ("x" === b && a.originalEvent.deltaX ? c = 40 * a.originalEvent.deltaX : "y" === b && a.originalEvent.deltaY && (c = 40 * a.originalEvent.deltaY)), (c > 0 && p < g || c < 0 && p > 0) && (p += c, p < 0 && (p = 0), p > g && (p = g), d.scrollTo = d.scrollTo || {}, d.scrollTo[m] = p, setTimeout(function () {
            d.scrollTo && (e.stop().animate(d.scrollTo, 240, "linear", function () {
              p = e[m]()
            }), d.scrollTo = null)
          }, 1)), a.preventDefault(), !1
        }, f.scroll.on("MozMousePixelScroll" + g, f.mousewheel).on("mousewheel" + g, f.mousewheel).on("mouseenter" + g, function () {
          p = e[m]()
        }), f.scroll.find(".scroll-arrow, .scroll-element_track").on("mousedown" + g, function (g) {
          if (1 != g.which) return !0;
          l = 1;
          var i = {
              eventOffset: g["x" === b ? "pageX" : "pageY"],
              maxScrollValue: f.size - f.visible - f.offset,
              scrollbarOffset: f.scroll.bar.offset()["x" === b ? "left" : "top"],
              scrollbarSize: f.scroll.bar["x" === b ? "outerWidth" : "outerHeight"]()
            },
            j = 0,
            q = 0;
          if (a(this).hasClass("scroll-arrow")) {
            if (l = a(this).hasClass("scroll-arrow_more") ? 1 : -1, n = h.scrollStep * l, p = l > 0 ? i.maxScrollValue : 0, h.isRtl) switch (!0) {
              case c.firefox:
                p = l > 0 ? 0 : i.maxScrollValue * -1;
                break;
              case c.msie || c.msedge:
            }
          } else l = i.eventOffset > i.scrollbarOffset + i.scrollbarSize ? 1 : i.eventOffset < i.scrollbarOffset ? -1 : 0, "x" === b && h.isRtl && (c.msie || c.msedge) && (l *= -1), n = Math.round(.75 * f.visible) * l, p = i.eventOffset - i.scrollbarOffset - (h.stepScrolling ? 1 == l ? i.scrollbarSize : 0 : Math.round(i.scrollbarSize / 2)), p = e[m]() + p / f.kx;
          return d.scrollTo = d.scrollTo || {}, d.scrollTo[m] = h.stepScrolling ? e[m]() + n : p, h.stepScrolling && (k = function () {
            p = e[m](), clearInterval(q), clearTimeout(j), j = 0, q = 0
          }, j = setTimeout(function () {
            q = setInterval(o, 40)
          }, h.duration + 100)), setTimeout(function () {
            d.scrollTo && (e.animate(d.scrollTo, h.duration), d.scrollTo = null)
          }, 1), d._handleMouseDown(k, g)
        }), f.scroll.bar.on("mousedown" + g, function (i) {
          if (1 != i.which) return !0;
          var j = i["x" === b ? "pageX" : "pageY"],
            k = e[m]();
          return f.scroll.addClass("scroll-draggable"), a(document).on("mousemove" + g, function (a) {
            var d = parseInt((a["x" === b ? "pageX" : "pageY"] - j) / f.kx, 10);
            "x" === b && h.isRtl && (c.msie || c.msedge) && (d *= -1), e[m](k + d)
          }), d._handleMouseDown(function () {
            f.scroll.removeClass("scroll-draggable"), p = e[m]()
          }, i)
        }))
      }), a.each(i, function (a, b) {
        var c = "scroll-scroll" + a + "_visible",
          d = "x" == a ? i.y : i.x;
        b.scroll.removeClass(c), d.scroll.removeClass(c), f.removeClass(c)
      }), a.each(i, function (b, c) {
        a.extend(c, "x" == b ? {
          offset: parseInt(e.css("left"), 10) || 0,
          size: e.prop("scrollWidth"),
          visible: k.width()
        } : {
          offset: parseInt(e.css("top"), 10) || 0,
          size: e.prop("scrollHeight"),
          visible: k.height()
        })
      }), this._updateScroll("x", this.scrollx), this._updateScroll("y", this.scrolly), a.isFunction(h.onUpdate) && h.onUpdate.apply(this, [e]), a.each(i, function (a, b) {
        var c = "x" === a ? "left" : "top",
          d = "x" === a ? "outerWidth" : "outerHeight",
          f = "x" === a ? "width" : "height",
          g = parseInt(e.css(c), 10) || 0,
          i = b.size,
          j = b.visible + g,
          k = b.scroll.size[d]() + (parseInt(b.scroll.size.css(c), 10) || 0);
        h.autoScrollSize && (b.scrollbarSize = parseInt(k * j / i, 10), b.scroll.bar.css(f, b.scrollbarSize + "px")), b.scrollbarSize = b.scroll.bar[d](), b.kx = (k - b.scrollbarSize) / (i - j) || 1, b.maxScrollOffset = i - j
      }), e.scrollLeft(m.scrollLeft).scrollTop(m.scrollTop).trigger("scroll")
    },
    _getScroll: function (b) {
      var c = {
        advanced: ['<div class="scroll-element">', '<div class="scroll-element_corner"></div>', '<div class="scroll-arrow scroll-arrow_less"></div>', '<div class="scroll-arrow scroll-arrow_more"></div>', '<div class="scroll-element_outer">', '<div class="scroll-element_size"></div>', '<div class="scroll-element_inner-wrapper">', '<div class="scroll-element_inner scroll-element_track">', '<div class="scroll-element_inner-bottom"></div>', "</div>", "</div>", '<div class="scroll-bar">', '<div class="scroll-bar_body">', '<div class="scroll-bar_body-inner"></div>', "</div>", '<div class="scroll-bar_bottom"></div>', '<div class="scroll-bar_center"></div>', "</div>", "</div>", "</div>"].join(""),
        simple: ['<div class="scroll-element">', '<div class="scroll-element_outer">', '<div class="scroll-element_size"></div>', '<div class="scroll-element_track"></div>', '<div class="scroll-bar"></div>', "</div>", "</div>"].join("")
      };
      return c[b] && (b = c[b]), b || (b = c.simple), b = "string" == typeof b ? a(b).appendTo(this.wrapper) : a(b), a.extend(b, {
        bar: b.find(".scroll-bar"),
        size: b.find(".scroll-element_size"),
        track: b.find(".scroll-element_track")
      }), b
    },
    _handleMouseDown: function (b, c) {
      var d = this.namespace;
      return a(document).on("blur" + d, function () {
        a(document).add("body").off(d), b && b()
      }), a(document).on("dragstart" + d, function (a) {
        return a.preventDefault(), !1
      }), a(document).on("mouseup" + d, function () {
        a(document).add("body").off(d), b && b()
      }), a("body").on("selectstart" + d, function (a) {
        return a.preventDefault(), !1
      }), c && c.preventDefault(), !1
    },
    _updateScroll: function (b, d) {
      var e = this.container,
        f = this.containerWrapper || e,
        g = "scroll-scroll" + b + "_visible",
        h = "x" === b ? this.scrolly : this.scrollx,
        i = parseInt(this.container.css("x" === b ? "left" : "top"), 10) || 0,
        j = this.wrapper,
        k = d.size,
        l = d.visible + i;
      d.isVisible = k - l > 1, d.isVisible ? (d.scroll.addClass(g), h.scroll.addClass(g), f.addClass(g)) : (d.scroll.removeClass(g), h.scroll.removeClass(g), f.removeClass(g)), "y" === b && (e.is("textarea") || k < l ? f.css({
        height: l + c.scroll.height + "px",
        "max-height": "none"
      }) : f.css({
        "max-height": l + c.scroll.height + "px"
      })), d.size == e.prop("scrollWidth") && h.size == e.prop("scrollHeight") && d.visible == j.width() && h.visible == j.height() && d.offset == (parseInt(e.css("left"), 10) || 0) && h.offset == (parseInt(e.css("top"), 10) || 0) || (a.extend(this.scrollx, {
        offset: parseInt(e.css("left"), 10) || 0,
        size: e.prop("scrollWidth"),
        visible: j.width()
      }), a.extend(this.scrolly, {
        offset: parseInt(e.css("top"), 10) || 0,
        size: this.container.prop("scrollHeight"),
        visible: j.height()
      }), this._updateScroll("x" === b ? "y" : "x", h))
    }
  };
  var f = e;
  a.fn.scrollbar = function (b, d) {
    return "string" != typeof b && (d = b, b = "init"), "undefined" == typeof d && (d = []), a.isArray(d) || (d = [d]), this.not("body, .scroll-wrapper").each(function () {
      var e = a(this),
        g = e.data(c.data.name);
      (g || "init" === b) && (g || (g = new f(e)), g[b] && g[b].apply(g, d))
    }), this
  }, a.fn.scrollbar.options = d;
  var g = function () {
    var a = 0,
      d = 0;
    return function (e) {
      var f, h, i, j, k, l, m;
      for (f = 0; f < c.scrolls.length; f++) j = c.scrolls[f], h = j.container, i = j.options, k = j.wrapper, l = j.scrollx, m = j.scrolly, (e || i.autoUpdate && k && k.is(":visible") && (h.prop("scrollWidth") != l.size || h.prop("scrollHeight") != m.size || k.width() != l.visible || k.height() != m.visible)) && (j.init(), i.debug && (window.console && console.log({
        scrollHeight: h.prop("scrollHeight") + ":" + j.scrolly.size,
        scrollWidth: h.prop("scrollWidth") + ":" + j.scrollx.size,
        visibleHeight: k.height() + ":" + j.scrolly.visible,
        visibleWidth: k.width() + ":" + j.scrollx.visible
      }, !0), d++));
      b && d > 10 ? (window.console && console.log("Scroll updates exceed 10"), g = function () {}) : (clearTimeout(a), a = setTimeout(g, 300))
    }
  }();
  window.angular && ! function (a) {
    a.module("jQueryScrollbar", []).provider("jQueryScrollbar", function () {
      var b = d;
      return {
        setOptions: function (c) {
          a.extend(b, c)
        },
        $get: function () {
          return {
            options: a.copy(b)
          }
        }
      }
    }).directive("jqueryScrollbar", ["jQueryScrollbar", "$parse", function (a, b) {
      return {
        restrict: "AC",
        link: function (c, d, e) {
          var f = b(e.jqueryScrollbar),
            g = f(c);
          d.scrollbar(g || a.options).on("$destroy", function () {
            d.scrollbar("destroy")
          })
        }
      }
    }])
  }(window.angular)
});
/*!
 * Scroll Lock v3.1.3
 * https://github.com/MohammadYounes/jquery-scrollLock
 *
 * Copyright (c) 2017 Mohammad Younes
 * Licensed under GPL 3.
 */
(function (n) {
  typeof define == "function" && define.amd ? define(["jquery"], n) : n(jQuery)
})(function (n) {
  "use strict";
  var i = {
      space: 32,
      pageup: 33,
      pagedown: 34,
      end: 35,
      home: 36,
      up: 38,
      down: 40
    },
    r = function (t, i) {
      var u = i.scrollTop(),
        h = i.prop("scrollHeight"),
        c = i.prop("clientHeight"),
        f = t.originalEvent.wheelDelta || -1 * t.originalEvent.detail || -1 * t.originalEvent.deltaY,
        r = 0,
        e, o, s;
      return t.type === "wheel" ? (e = i.height() / n(window).height(), r = t.originalEvent.deltaY * e) : this.options.touch && t.type === "touchmove" && (f = t.originalEvent.changedTouches[0].clientY - this.startClientY), s = (o = f > 0 && u + r <= 0) || f < 0 && u + r >= h - c, {
        prevent: s,
        top: o,
        scrollTop: u,
        deltaY: r
      }
    },
    u = function (n, t) {
      var u = t.scrollTop(),
        r = {
          top: !1,
          bottom: !1
        },
        f, e;
      return r.top = u === 0 && (n.keyCode === i.pageup || n.keyCode === i.home || n.keyCode === i.up), r.top || (f = t.prop("scrollHeight"), e = t.prop("clientHeight"), r.bottom = f === u + e && (n.keyCode === i.space || n.keyCode === i.pagedown || n.keyCode === i.end || n.keyCode === i.down)), r
    },
    t = function (i, r) {
      if (this.$element = i, this.options = n.extend({}, t.DEFAULTS, this.$element.data(), r), this.enabled = !0, this.startClientY = 0, this.options.unblock) this.$element.on(t.CORE.wheelEventName + t.NAMESPACE, this.options.unblock, n.proxy(t.CORE.unblockHandler, this));
      this.$element.on(t.CORE.wheelEventName + t.NAMESPACE, this.options.selector, n.proxy(t.CORE.handler, this));
      if (this.options.touch) {
        this.$element.on("touchstart" + t.NAMESPACE, this.options.selector, n.proxy(t.CORE.touchHandler, this));
        this.$element.on("touchmove" + t.NAMESPACE, this.options.selector, n.proxy(t.CORE.handler, this));
        if (this.options.unblock) this.$element.on("touchmove" + t.NAMESPACE, this.options.unblock, n.proxy(t.CORE.unblockHandler, this))
      }
      if (this.options.keyboard) {
        this.$element.attr("tabindex", this.options.keyboard.tabindex || 0);
        this.$element.on("keydown" + t.NAMESPACE, this.options.selector, n.proxy(t.CORE.keyboardHandler, this));
        if (this.options.unblock) this.$element.on("keydown" + t.NAMESPACE, this.options.unblock, n.proxy(t.CORE.unblockHandler, this))
      }
    },
    f;
  t.NAME = "ScrollLock";
  t.VERSION = "3.1.2";
  t.NAMESPACE = ".scrollLock";
  t.ANIMATION_NAMESPACE = t.NAMESPACE + ".effect";
  t.DEFAULTS = {
    strict: !1,
    strictFn: function (n) {
      return n.prop("scrollHeight") > n.prop("clientHeight")
    },
    selector: !1,
    animation: !1,
    touch: "ontouchstart" in window,
    keyboard: !1,
    unblock: !1
  };
  t.CORE = {
    wheelEventName: "onwheel" in document.createElement("div") ? "wheel" : document.onmousewheel !== undefined ? "mousewheel" : "DOMMouseScroll",
    animationEventName: ["webkitAnimationEnd", "mozAnimationEnd", "MSAnimationEnd", "oanimationend", "animationend"].join(t.ANIMATION_NAMESPACE + " ") + t.ANIMATION_NAMESPACE,
    unblockHandler: function (n) {
      n.__currentTarget = n.currentTarget
    },
    handler: function (i) {
      var f, u, e;
      this.enabled && !i.ctrlKey && (f = n(i.currentTarget), (this.options.strict !== !0 || this.options.strictFn(f)) && (i.stopPropagation(), u = n.proxy(r, this)(i, f), i.__currentTarget && (u.prevent &= n.proxy(r, this)(i, n(i.__currentTarget)).prevent), u.prevent && (i.preventDefault(), u.deltaY && f.scrollTop(u.scrollTop + u.deltaY), e = u.top ? "top" : "bottom", this.options.animation && setTimeout(t.CORE.animationHandler.bind(this, f, e), 0), f.trigger(n.Event(e + t.NAMESPACE)))))
    },
    touchHandler: function (n) {
      this.startClientY = n.originalEvent.touches[0].clientY
    },
    animationHandler: function (n, i) {
      var r = this.options.animation[i],
        u = this.options.animation.top + " " + this.options.animation.bottom;
      n.off(t.ANIMATION_NAMESPACE).removeClass(u).addClass(r).one(t.CORE.animationEventName, function () {
        n.removeClass(r)
      })
    },
    keyboardHandler: function (i) {
      var r = n(i.currentTarget),
        o = r.scrollTop(),
        f = u(i, r),
        e;
      return (i.__currentTarget && (e = u(i, n(i.__currentTarget)), f.top &= e.top, f.bottom &= e.bottom), f.top) ? (r.trigger(n.Event("top" + t.NAMESPACE)), this.options.animation && setTimeout(t.CORE.animationHandler.bind(this, r, "top"), 0), !1) : f.bottom ? (r.trigger(n.Event("bottom" + t.NAMESPACE)), this.options.animation && setTimeout(t.CORE.animationHandler.bind(this, r, "bottom"), 0), !1) : void 0
    }
  };
  t.prototype.toggleStrict = function () {
    this.options.strict = !this.options.strict
  };
  t.prototype.enable = function () {
    this.enabled = !0
  };
  t.prototype.disable = function () {
    this.enabled = !1
  };
  t.prototype.destroy = function () {
    this.disable();
    this.$element.off(t.NAMESPACE);
    this.$element = null;
    this.options = null
  };
  f = n.fn.scrollLock;
  n.fn.scrollLock = function (i) {
    return this.each(function () {
      var u = n(this),
        f = typeof i == "object" && i,
        r = u.data(t.NAME);
      (r || "destroy" !== i) && (r || u.data(t.NAME, r = new t(u, f)), typeof i == "string" && r[i]())
    })
  };
  n.fn.scrollLock.defaults = t.DEFAULTS;
  n.fn.scrollLock.noConflict = function () {
    return n.fn.scrollLock = f, this
  }
});
//# sourceMappingURL=jquery-scrollLock.min.js.map

/*!
 * imagesLoaded PACKAGED v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

! function (e, t) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t()
}("undefined" != typeof window ? window : this, function () {
  function e() {}
  var t = e.prototype;
  return t.on = function (e, t) {
    if (e && t) {
      var i = this._events = this._events || {},
        n = i[e] = i[e] || [];
      return n.indexOf(t) == -1 && n.push(t), this
    }
  }, t.once = function (e, t) {
    if (e && t) {
      this.on(e, t);
      var i = this._onceEvents = this._onceEvents || {},
        n = i[e] = i[e] || {};
      return n[t] = !0, this
    }
  }, t.off = function (e, t) {
    var i = this._events && this._events[e];
    if (i && i.length) {
      var n = i.indexOf(t);
      return n != -1 && i.splice(n, 1), this
    }
  }, t.emitEvent = function (e, t) {
    var i = this._events && this._events[e];
    if (i && i.length) {
      i = i.slice(0), t = t || [];
      for (var n = this._onceEvents && this._onceEvents[e], o = 0; o < i.length; o++) {
        var r = i[o],
          s = n && n[r];
        s && (this.off(e, r), delete n[r]), r.apply(this, t)
      }
      return this
    }
  }, t.allOff = function () {
    delete this._events, delete this._onceEvents
  }, e
}),
function (e, t) {
  "use strict";
  "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function (i) {
    return t(e, i)
  }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter)
}("undefined" != typeof window ? window : this, function (e, t) {
  function i(e, t) {
    for (var i in t) e[i] = t[i];
    return e
  }

  function n(e) {
    if (Array.isArray(e)) return e;
    var t = "object" == typeof e && "number" == typeof e.length;
    return t ? d.call(e) : [e]
  }

  function o(e, t, r) {
    if (!(this instanceof o)) return new o(e, t, r);
    var s = e;
    return "string" == typeof e && (s = document.querySelectorAll(e)), s ? (this.elements = n(s), this.options = i({}, this.options), "function" == typeof t ? r = t : i(this.options, t), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred), void setTimeout(this.check.bind(this))) : void a.error("Bad element for imagesLoaded " + (s || e))
  }

  function r(e) {
    this.img = e
  }

  function s(e, t) {
    this.url = e, this.element = t, this.img = new Image
  }
  var h = e.jQuery,
    a = e.console,
    d = Array.prototype.slice;
  o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this)
  }, o.prototype.addElementImages = function (e) {
    "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e);
    var t = e.nodeType;
    if (t && u[t]) {
      for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var o = i[n];
        this.addImage(o)
      }
      if ("string" == typeof this.options.background) {
        var r = e.querySelectorAll(this.options.background);
        for (n = 0; n < r.length; n++) {
          var s = r[n];
          this.addElementBackgroundImages(s)
        }
      }
    }
  };
  var u = {
    1: !0,
    9: !0,
    11: !0
  };
  return o.prototype.addElementBackgroundImages = function (e) {
    var t = getComputedStyle(e);
    if (t)
      for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) {
        var o = n && n[2];
        o && this.addBackground(o, e), n = i.exec(t.backgroundImage)
      }
  }, o.prototype.addImage = function (e) {
    var t = new r(e);
    this.images.push(t)
  }, o.prototype.addBackground = function (e, t) {
    var i = new s(e, t);
    this.images.push(i)
  }, o.prototype.check = function () {
    function e(e, i, n) {
      setTimeout(function () {
        t.progress(e, i, n)
      })
    }
    var t = this;
    return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) {
      t.once("progress", e), t.check()
    }) : void this.complete()
  }, o.prototype.progress = function (e, t, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, e, t)
  }, o.prototype.complete = function () {
    var e = this.hasAnyBroken ? "fail" : "done";
    if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var t = this.hasAnyBroken ? "reject" : "resolve";
      this.jqDeferred[t](this)
    }
  }, r.prototype = Object.create(t.prototype), r.prototype.check = function () {
    var e = this.getIsImageComplete();
    return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
  }, r.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth
  }, r.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.img, t])
  }, r.prototype.handleEvent = function (e) {
    var t = "on" + e.type;
    this[t] && this[t](e)
  }, r.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents()
  }, r.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents()
  }, r.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
    var e = this.getIsImageComplete();
    e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
  }, s.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, s.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.element, t])
  }, o.makeJQueryPlugin = function (t) {
    t = t || e.jQuery, t && (h = t, h.fn.imagesLoaded = function (e, t) {
      var i = new o(this, e, t);
      return i.jqDeferred.promise(h(this))
    })
  }, o.makeJQueryPlugin(), o
});

/*
 * anime.js v3.0.1
 * (c) 2019 Julian Garnier
 * Released under the MIT license
 * animejs.com
 */

! function (n, e) {
  "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : n.anime = e()
}(this, function () {
  "use strict";
  var n = {
      update: null,
      begin: null,
      loopBegin: null,
      changeBegin: null,
      change: null,
      changeComplete: null,
      loopComplete: null,
      complete: null,
      loop: 1,
      direction: "normal",
      autoplay: !0,
      timelineOffset: 0
    },
    e = {
      duration: 1e3,
      delay: 0,
      endDelay: 0,
      easing: "easeOutElastic(1, .5)",
      round: 0
    },
    r = ["translateX", "translateY", "translateZ", "rotate", "rotateX", "rotateY", "rotateZ", "scale", "scaleX", "scaleY", "scaleZ", "skew", "skewX", "skewY", "perspective"],
    t = {
      CSS: {},
      springs: {}
    };

  function a(n, e, r) {
    return Math.min(Math.max(n, e), r)
  }

  function o(n, e) {
    return n.indexOf(e) > -1
  }

  function i(n, e) {
    return n.apply(null, e)
  }
  var u = {
    arr: function (n) {
      return Array.isArray(n)
    },
    obj: function (n) {
      return o(Object.prototype.toString.call(n), "Object")
    },
    pth: function (n) {
      return u.obj(n) && n.hasOwnProperty("totalLength")
    },
    svg: function (n) {
      return n instanceof SVGElement
    },
    inp: function (n) {
      return n instanceof HTMLInputElement
    },
    dom: function (n) {
      return n.nodeType || u.svg(n)
    },
    str: function (n) {
      return "string" == typeof n
    },
    fnc: function (n) {
      return "function" == typeof n
    },
    und: function (n) {
      return void 0 === n
    },
    hex: function (n) {
      return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(n)
    },
    rgb: function (n) {
      return /^rgb/.test(n)
    },
    hsl: function (n) {
      return /^hsl/.test(n)
    },
    col: function (n) {
      return u.hex(n) || u.rgb(n) || u.hsl(n)
    },
    key: function (r) {
      return !n.hasOwnProperty(r) && !e.hasOwnProperty(r) && "targets" !== r && "keyframes" !== r
    }
  };

  function s(n) {
    var e = /\(([^)]+)\)/.exec(n);
    return e ? e[1].split(",").map(function (n) {
      return parseFloat(n)
    }) : []
  }

  function c(n, e) {
    var r = s(n),
      o = a(u.und(r[0]) ? 1 : r[0], .1, 100),
      i = a(u.und(r[1]) ? 100 : r[1], .1, 100),
      c = a(u.und(r[2]) ? 10 : r[2], .1, 100),
      f = a(u.und(r[3]) ? 0 : r[3], .1, 100),
      l = Math.sqrt(i / o),
      d = c / (2 * Math.sqrt(i * o)),
      p = d < 1 ? l * Math.sqrt(1 - d * d) : 0,
      v = 1,
      h = d < 1 ? (d * l - f) / p : -f + l;

    function g(n) {
      var r = e ? e * n / 1e3 : n;
      return r = d < 1 ? Math.exp(-r * d * l) * (v * Math.cos(p * r) + h * Math.sin(p * r)) : (v + h * r) * Math.exp(-r * l), 0 === n || 1 === n ? n : 1 - r
    }
    return e ? g : function () {
      var e = t.springs[n];
      if (e) return e;
      for (var r = 0, a = 0;;)
        if (1 === g(r += 1 / 6)) {
          if (++a >= 16) break
        } else a = 0;
      var o = r * (1 / 6) * 1e3;
      return t.springs[n] = o, o
    }
  }

  function f(n, e) {
    void 0 === n && (n = 1), void 0 === e && (e = .5);
    var r = a(n, 1, 10),
      t = a(e, .1, 2);
    return function (n) {
      return 0 === n || 1 === n ? n : -r * Math.pow(2, 10 * (n - 1)) * Math.sin((n - 1 - t / (2 * Math.PI) * Math.asin(1 / r)) * (2 * Math.PI) / t)
    }
  }

  function l(n) {
    return void 0 === n && (n = 10),
      function (e) {
        return Math.round(e * n) * (1 / n)
      }
  }
  var d = function () {
      var n = 11,
        e = 1 / (n - 1);

      function r(n, e) {
        return 1 - 3 * e + 3 * n
      }

      function t(n, e) {
        return 3 * e - 6 * n
      }

      function a(n) {
        return 3 * n
      }

      function o(n, e, o) {
        return ((r(e, o) * n + t(e, o)) * n + a(e)) * n
      }

      function i(n, e, o) {
        return 3 * r(e, o) * n * n + 2 * t(e, o) * n + a(e)
      }
      return function (r, t, a, u) {
        if (0 <= r && r <= 1 && 0 <= a && a <= 1) {
          var s = new Float32Array(n);
          if (r !== t || a !== u)
            for (var c = 0; c < n; ++c) s[c] = o(c * e, r, a);
          return function (n) {
            return r === t && a === u ? n : 0 === n || 1 === n ? n : o(f(n), t, u)
          }
        }

        function f(t) {
          for (var u = 0, c = 1, f = n - 1; c !== f && s[c] <= t; ++c) u += e;
          var l = u + (t - s[--c]) / (s[c + 1] - s[c]) * e,
            d = i(l, r, a);
          return d >= .001 ? function (n, e, r, t) {
            for (var a = 0; a < 4; ++a) {
              var u = i(e, r, t);
              if (0 === u) return e;
              e -= (o(e, r, t) - n) / u
            }
            return e
          }(t, l, r, a) : 0 === d ? l : function (n, e, r, t, a) {
            for (var i, u, s = 0;
              (i = o(u = e + (r - e) / 2, t, a) - n) > 0 ? r = u : e = u, Math.abs(i) > 1e-7 && ++s < 10;);
            return u
          }(t, u, u + e, r, a)
        }
      }
    }(),
    p = function () {
      var n = ["Quad", "Cubic", "Quart", "Quint", "Sine", "Expo", "Circ", "Back", "Elastic"],
        e = {
          In: [
            [.55, .085, .68, .53],
            [.55, .055, .675, .19],
            [.895, .03, .685, .22],
            [.755, .05, .855, .06],
            [.47, 0, .745, .715],
            [.95, .05, .795, .035],
            [.6, .04, .98, .335],
            [.6, -.28, .735, .045], f
          ],
          Out: [
            [.25, .46, .45, .94],
            [.215, .61, .355, 1],
            [.165, .84, .44, 1],
            [.23, 1, .32, 1],
            [.39, .575, .565, 1],
            [.19, 1, .22, 1],
            [.075, .82, .165, 1],
            [.175, .885, .32, 1.275],
            function (n, e) {
              return function (r) {
                return 1 - f(n, e)(1 - r)
              }
            }
          ],
          InOut: [
            [.455, .03, .515, .955],
            [.645, .045, .355, 1],
            [.77, 0, .175, 1],
            [.86, 0, .07, 1],
            [.445, .05, .55, .95],
            [1, 0, 0, 1],
            [.785, .135, .15, .86],
            [.68, -.55, .265, 1.55],
            function (n, e) {
              return function (r) {
                return r < .5 ? f(n, e)(2 * r) / 2 : 1 - f(n, e)(-2 * r + 2) / 2
              }
            }
          ]
        },
        r = {
          linear: [.25, .25, .75, .75]
        },
        t = function (t) {
          e[t].forEach(function (e, a) {
            r["ease" + t + n[a]] = e
          })
        };
      for (var a in e) t(a);
      return r
    }();

  function v(n, e) {
    if (u.fnc(n)) return n;
    var r = n.split("(")[0],
      t = p[r],
      a = s(n);
    switch (r) {
      case "spring":
        return c(n, e);
      case "cubicBezier":
        return i(d, a);
      case "steps":
        return i(l, a);
      default:
        return u.fnc(t) ? i(t, a) : i(d, t)
    }
  }

  function h(n) {
    try {
      return document.querySelectorAll(n)
    } catch (n) {
      return
    }
  }

  function g(n, e) {
    for (var r = n.length, t = arguments.length >= 2 ? arguments[1] : void 0, a = [], o = 0; o < r; o++)
      if (o in n) {
        var i = n[o];
        e.call(t, i, o, n) && a.push(i)
      } return a
  }

  function m(n) {
    return n.reduce(function (n, e) {
      return n.concat(u.arr(e) ? m(e) : e)
    }, [])
  }

  function y(n) {
    return u.arr(n) ? n : (u.str(n) && (n = h(n) || n), n instanceof NodeList || n instanceof HTMLCollection ? [].slice.call(n) : [n])
  }

  function b(n, e) {
    return n.some(function (n) {
      return n === e
    })
  }

  function x(n) {
    var e = {};
    for (var r in n) e[r] = n[r];
    return e
  }

  function M(n, e) {
    var r = x(n);
    for (var t in n) r[t] = e.hasOwnProperty(t) ? e[t] : n[t];
    return r
  }

  function w(n, e) {
    var r = x(n);
    for (var t in e) r[t] = u.und(n[t]) ? e[t] : n[t];
    return r
  }

  function k(n) {
    return u.rgb(n) ? (r = /rgb\((\d+,\s*[\d]+,\s*[\d]+)\)/g.exec(e = n)) ? "rgba(" + r[1] + ",1)" : e : u.hex(n) ? (t = n.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (n, e, r, t) {
      return e + e + r + r + t + t
    }), a = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t), "rgba(" + parseInt(a[1], 16) + "," + parseInt(a[2], 16) + "," + parseInt(a[3], 16) + ",1)") : u.hsl(n) ? function (n) {
      var e, r, t, a = /hsl\((\d+),\s*([\d.]+)%,\s*([\d.]+)%\)/g.exec(n) || /hsla\((\d+),\s*([\d.]+)%,\s*([\d.]+)%,\s*([\d.]+)\)/g.exec(n),
        o = parseInt(a[1], 10) / 360,
        i = parseInt(a[2], 10) / 100,
        u = parseInt(a[3], 10) / 100,
        s = a[4] || 1;

      function c(n, e, r) {
        return r < 0 && (r += 1), r > 1 && (r -= 1), r < 1 / 6 ? n + 6 * (e - n) * r : r < .5 ? e : r < 2 / 3 ? n + (e - n) * (2 / 3 - r) * 6 : n
      }
      if (0 == i) e = r = t = u;
      else {
        var f = u < .5 ? u * (1 + i) : u + i - u * i,
          l = 2 * u - f;
        e = c(l, f, o + 1 / 3), r = c(l, f, o), t = c(l, f, o - 1 / 3)
      }
      return "rgba(" + 255 * e + "," + 255 * r + "," + 255 * t + "," + s + ")"
    }(n) : void 0;
    var e, r, t, a
  }

  function C(n) {
    var e = /([\+\-]?[0-9#\.]+)(%|px|pt|em|rem|in|cm|mm|ex|ch|pc|vw|vh|vmin|vmax|deg|rad|turn)?$/.exec(n);
    if (e) return e[2]
  }

  function O(n, e) {
    return u.fnc(n) ? n(e.target, e.id, e.total) : n
  }

  function P(n, e) {
    return n.getAttribute(e)
  }

  function I(n, e, r) {
    if (b([r, "deg", "rad", "turn"], C(e))) return e;
    var a = t.CSS[e + r];
    if (!u.und(a)) return a;
    var o = document.createElement(n.tagName),
      i = n.parentNode && n.parentNode !== document ? n.parentNode : document.body;
    i.appendChild(o), o.style.position = "absolute", o.style.width = 100 + r;
    var s = 100 / o.offsetWidth;
    i.removeChild(o);
    var c = s * parseFloat(e);
    return t.CSS[e + r] = c, c
  }

  function B(n, e, r) {
    if (e in n.style) {
      var t = e.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase(),
        a = n.style[e] || getComputedStyle(n).getPropertyValue(t) || "0";
      return r ? I(n, a, r) : a
    }
  }

  function D(n, e) {
    return u.dom(n) && !u.inp(n) && (P(n, e) || u.svg(n) && n[e]) ? "attribute" : u.dom(n) && b(r, e) ? "transform" : u.dom(n) && "transform" !== e && B(n, e) ? "css" : null != n[e] ? "object" : void 0
  }

  function T(n) {
    if (u.dom(n)) {
      for (var e, r = n.style.transform || "", t = /(\w+)\(([^)]*)\)/g, a = new Map; e = t.exec(r);) a.set(e[1], e[2]);
      return a
    }
  }

  function F(n, e, r, t) {
    var a, i = o(e, "scale") ? 1 : 0 + (o(a = e, "translate") || "perspective" === a ? "px" : o(a, "rotate") || o(a, "skew") ? "deg" : void 0),
      u = T(n).get(e) || i;
    return r && (r.transforms.list.set(e, u), r.transforms.last = e), t ? I(n, u, t) : u
  }

  function N(n, e, r, t) {
    switch (D(n, e)) {
      case "transform":
        return F(n, e, t, r);
      case "css":
        return B(n, e, r);
      case "attribute":
        return P(n, e);
      default:
        return n[e] || 0
    }
  }

  function A(n, e) {
    var r = /^(\*=|\+=|-=)/.exec(n);
    if (!r) return n;
    var t = C(n) || 0,
      a = parseFloat(e),
      o = parseFloat(n.replace(r[0], ""));
    switch (r[0][0]) {
      case "+":
        return a + o + t;
      case "-":
        return a - o + t;
      case "*":
        return a * o + t
    }
  }

  function E(n, e) {
    if (u.col(n)) return k(n);
    var r = C(n),
      t = r ? n.substr(0, n.length - r.length) : n;
    return e && !/\s/g.test(n) ? t + e : t
  }

  function L(n, e) {
    return Math.sqrt(Math.pow(e.x - n.x, 2) + Math.pow(e.y - n.y, 2))
  }

  function S(n) {
    for (var e, r = n.points, t = 0, a = 0; a < r.numberOfItems; a++) {
      var o = r.getItem(a);
      a > 0 && (t += L(e, o)), e = o
    }
    return t
  }

  function j(n) {
    if (n.getTotalLength) return n.getTotalLength();
    switch (n.tagName.toLowerCase()) {
      case "circle":
        return o = n, 2 * Math.PI * P(o, "r");
      case "rect":
        return 2 * P(a = n, "width") + 2 * P(a, "height");
      case "line":
        return L({
          x: P(t = n, "x1"),
          y: P(t, "y1")
        }, {
          x: P(t, "x2"),
          y: P(t, "y2")
        });
      case "polyline":
        return S(n);
      case "polygon":
        return r = (e = n).points, S(e) + L(r.getItem(r.numberOfItems - 1), r.getItem(0))
    }
    var e, r, t, a, o
  }

  function q(n, e) {
    var r = e || {},
      t = r.el || function (n) {
        for (var e = n.parentNode; u.svg(e) && (e = e.parentNode, u.svg(e.parentNode)););
        return e
      }(n),
      a = t.getBoundingClientRect(),
      o = P(t, "viewBox"),
      i = a.width,
      s = a.height,
      c = r.viewBox || (o ? o.split(" ") : [0, 0, i, s]);
    return {
      el: t,
      viewBox: c,
      x: c[0] / 1,
      y: c[1] / 1,
      w: i / c[2],
      h: s / c[3]
    }
  }

  function $(n, e) {
    function r(r) {
      void 0 === r && (r = 0);
      var t = e + r >= 1 ? e + r : 0;
      return n.el.getPointAtLength(t)
    }
    var t = q(n.el, n.svg),
      a = r(),
      o = r(-1),
      i = r(1);
    switch (n.property) {
      case "x":
        return (a.x - t.x) * t.w;
      case "y":
        return (a.y - t.y) * t.h;
      case "angle":
        return 180 * Math.atan2(i.y - o.y, i.x - o.x) / Math.PI
    }
  }

  function X(n, e) {
    var r = /-?\d*\.?\d+/g,
      t = E(u.pth(n) ? n.totalLength : n, e) + "";
    return {
      original: t,
      numbers: t.match(r) ? t.match(r).map(Number) : [0],
      strings: u.str(n) || e ? t.split(r) : []
    }
  }

  function Y(n) {
    return g(n ? m(u.arr(n) ? n.map(y) : y(n)) : [], function (n, e, r) {
      return r.indexOf(n) === e
    })
  }

  function Z(n) {
    var e = Y(n);
    return e.map(function (n, r) {
      return {
        target: n,
        id: r,
        total: e.length,
        transforms: {
          list: T(n)
        }
      }
    })
  }

  function Q(n, e) {
    var r = x(e);
    if (/^spring/.test(r.easing) && (r.duration = c(r.easing)), u.arr(n)) {
      var t = n.length;
      2 === t && !u.obj(n[0]) ? n = {
        value: n
      } : u.fnc(e.duration) || (r.duration = e.duration / t)
    }
    var a = u.arr(n) ? n : [n];
    return a.map(function (n, r) {
      var t = u.obj(n) && !u.pth(n) ? n : {
        value: n
      };
      return u.und(t.delay) && (t.delay = r ? 0 : e.delay), u.und(t.endDelay) && (t.endDelay = r === a.length - 1 ? e.endDelay : 0), t
    }).map(function (n) {
      return w(n, r)
    })
  }

  function V(n, e) {
    var r = [],
      t = e.keyframes;
    for (var a in t && (e = w(function (n) {
        for (var e = g(m(n.map(function (n) {
            return Object.keys(n)
          })), function (n) {
            return u.key(n)
          }).reduce(function (n, e) {
            return n.indexOf(e) < 0 && n.push(e), n
          }, []), r = {}, t = function (t) {
            var a = e[t];
            r[a] = n.map(function (n) {
              var e = {};
              for (var r in n) u.key(r) ? r == a && (e.value = n[r]) : e[r] = n[r];
              return e
            })
          }, a = 0; a < e.length; a++) t(a);
        return r
      }(t), e)), e) u.key(a) && r.push({
      name: a,
      tweens: Q(e[a], n)
    });
    return r
  }

  function z(n, e) {
    var r;
    return n.tweens.map(function (t) {
      var a = function (n, e) {
          var r = {};
          for (var t in n) {
            var a = O(n[t], e);
            u.arr(a) && 1 === (a = a.map(function (n) {
              return O(n, e)
            })).length && (a = a[0]), r[t] = a
          }
          return r.duration = parseFloat(r.duration), r.delay = parseFloat(r.delay), r
        }(t, e),
        o = a.value,
        i = u.arr(o) ? o[1] : o,
        s = C(i),
        c = N(e.target, n.name, s, e),
        f = r ? r.to.original : c,
        l = u.arr(o) ? o[0] : f,
        d = C(l) || C(c),
        p = s || d;
      return u.und(i) && (i = f), a.from = X(l, p), a.to = X(A(i, l), p), a.start = r ? r.end : 0, a.end = a.start + a.delay + a.duration + a.endDelay, a.easing = v(a.easing, a.duration), a.isPath = u.pth(o), a.isColor = u.col(a.from.original), a.isColor && (a.round = 1), r = a, a
    })
  }
  var H = {
    css: function (n, e, r) {
      return n.style[e] = r
    },
    attribute: function (n, e, r) {
      return n.setAttribute(e, r)
    },
    object: function (n, e, r) {
      return n[e] = r
    },
    transform: function (n, e, r, t, a) {
      if (t.list.set(e, r), e === t.last || a) {
        var o = "";
        t.list.forEach(function (n, e) {
          o += e + "(" + n + ") "
        }), n.style.transform = o
      }
    }
  };

  function G(n, e) {
    Z(n).forEach(function (n) {
      for (var r in e) {
        var t = O(e[r], n),
          a = n.target,
          o = C(t),
          i = N(a, r, o, n),
          u = A(E(t, o || C(i)), i),
          s = D(a, r);
        H[s](a, r, u, n.transforms, !0)
      }
    })
  }

  function R(n, e) {
    return g(m(n.map(function (n) {
      return e.map(function (e) {
        return function (n, e) {
          var r = D(n.target, e.name);
          if (r) {
            var t = z(e, n),
              a = t[t.length - 1];
            return {
              type: r,
              property: e.name,
              animatable: n,
              tweens: t,
              duration: a.end,
              delay: t[0].delay,
              endDelay: a.endDelay
            }
          }
        }(n, e)
      })
    })), function (n) {
      return !u.und(n)
    })
  }

  function W(n, e) {
    var r = n.length,
      t = function (n) {
        return n.timelineOffset ? n.timelineOffset : 0
      },
      a = {};
    return a.duration = r ? Math.max.apply(Math, n.map(function (n) {
      return t(n) + n.duration
    })) : e.duration, a.delay = r ? Math.min.apply(Math, n.map(function (n) {
      return t(n) + n.delay
    })) : e.delay, a.endDelay = r ? a.duration - Math.max.apply(Math, n.map(function (n) {
      return t(n) + n.duration - n.endDelay
    })) : e.endDelay, a
  }
  var J = 0;
  var K, U = [],
    _ = [],
    nn = function () {
      function n() {
        K = requestAnimationFrame(e)
      }

      function e(e) {
        var r = U.length;
        if (r) {
          for (var t = 0; t < r;) {
            var a = U[t];
            if (a.paused) {
              var o = U.indexOf(a);
              o > -1 && (U.splice(o, 1), r = U.length)
            } else a.tick(e);
            t++
          }
          n()
        } else K = cancelAnimationFrame(K)
      }
      return n
    }();

  function en(r) {
    void 0 === r && (r = {});
    var t, o = 0,
      i = 0,
      u = 0,
      s = 0,
      c = null;

    function f(n) {
      var e = window.Promise && new Promise(function (n) {
        return c = n
      });
      return n.finished = e, e
    }
    var l, d, p, v, h, m, y, b, x = (d = M(n, l = r), p = M(e, l), v = V(p, l), h = Z(l.targets), m = R(h, v), y = W(m, p), b = J, J++, w(d, {
      id: b,
      children: [],
      animatables: h,
      animations: m,
      duration: y.duration,
      delay: y.delay,
      endDelay: y.endDelay
    }));
    f(x);

    function k() {
      var n = x.direction;
      "alternate" !== n && (x.direction = "normal" !== n ? "normal" : "reverse"), x.reversed = !x.reversed, t.forEach(function (n) {
        return n.reversed = x.reversed
      })
    }

    function C(n) {
      return x.reversed ? x.duration - n : n
    }

    function O() {
      o = 0, i = C(x.currentTime) * (1 / en.speed)
    }

    function P(n, e) {
      e && e.seek(n - e.timelineOffset)
    }

    function I(n) {
      for (var e = 0, r = x.animations, t = r.length; e < t;) {
        var o = r[e],
          i = o.animatable,
          u = o.tweens,
          s = u.length - 1,
          c = u[s];
        s && (c = g(u, function (e) {
          return n < e.end
        })[0] || c);
        for (var f = a(n - c.start - c.delay, 0, c.duration) / c.duration, l = isNaN(f) ? 1 : c.easing(f), d = c.to.strings, p = c.round, v = [], h = c.to.numbers.length, m = void 0, y = 0; y < h; y++) {
          var b = void 0,
            M = c.to.numbers[y],
            w = c.from.numbers[y] || 0;
          b = c.isPath ? $(c.value, l * M) : w + l * (M - w), p && (c.isColor && y > 2 || (b = Math.round(b * p) / p)), v.push(b)
        }
        var k = d.length;
        if (k) {
          m = d[0];
          for (var C = 0; C < k; C++) {
            d[C];
            var O = d[C + 1],
              P = v[C];
            isNaN(P) || (m += O ? P + O : P + " ")
          }
        } else m = v[0];
        H[o.type](i.target, o.property, m, i.transforms), o.currentValue = m, e++
      }
    }

    function B(n) {
      x[n] && !x.passThrough && x[n](x)
    }

    function D(n) {
      var e = x.duration,
        r = x.delay,
        l = e - x.endDelay,
        d = C(n);
      x.progress = a(d / e * 100, 0, 100), x.reversePlayback = d < x.currentTime, t && function (n) {
        if (x.reversePlayback)
          for (var e = s; e--;) P(n, t[e]);
        else
          for (var r = 0; r < s; r++) P(n, t[r])
      }(d), !x.began && x.currentTime > 0 && (x.began = !0, B("begin"), B("loopBegin")), d <= r && 0 !== x.currentTime && I(0), (d >= l && x.currentTime !== e || !e) && I(e), d > r && d < l ? (x.changeBegan || (x.changeBegan = !0, x.changeCompleted = !1, B("changeBegin")), B("change"), I(d)) : x.changeBegan && (x.changeCompleted = !0, x.changeBegan = !1, B("changeComplete")), x.currentTime = a(d, 0, e), x.began && B("update"), n >= e && (i = 0, x.remaining && !0 !== x.remaining && x.remaining--, x.remaining ? (o = u, B("loopComplete"), B("loopBegin"), "alternate" === x.direction && k()) : (x.paused = !0, x.completed || (x.completed = !0, B("loopComplete"), B("complete"), !x.passThrough && "Promise" in window && (c(), f(x)))))
    }
    return x.reset = function () {
      var n = x.direction;
      x.passThrough = !1, x.currentTime = 0, x.progress = 0, x.paused = !0, x.began = !1, x.changeBegan = !1, x.completed = !1, x.changeCompleted = !1, x.reversePlayback = !1, x.reversed = "reverse" === n, x.remaining = x.loop, t = x.children;
      for (var e = s = t.length; e--;) x.children[e].reset();
      (x.reversed && !0 !== x.loop || "alternate" === n && 1 === x.loop) && x.remaining++, I(0)
    }, x.set = function (n, e) {
      return G(n, e), x
    }, x.tick = function (n) {
      u = n, o || (o = u), D((u + (i - o)) * en.speed)
    }, x.seek = function (n) {
      D(C(n))
    }, x.pause = function () {
      x.paused = !0, O()
    }, x.play = function () {
      x.paused && (x.completed && x.reset(), x.paused = !1, U.push(x), O(), K || nn())
    }, x.reverse = function () {
      k(), O()
    }, x.restart = function () {
      x.reset(), x.play()
    }, x.reset(), x.autoplay && x.play(), x
  }

  function rn(n, e) {
    for (var r = e.length; r--;) b(n, e[r].animatable.target) && e.splice(r, 1)
  }
  return "undefined" != typeof document && document.addEventListener("visibilitychange", function () {
    document.hidden ? (U.forEach(function (n) {
      return n.pause()
    }), _ = U.slice(0), U = []) : _.forEach(function (n) {
      return n.play()
    })
  }), en.version = "3.0.1", en.speed = 1, en.running = U, en.remove = function (n) {
    for (var e = Y(n), r = U.length; r--;) {
      var t = U[r],
        a = t.animations,
        o = t.children;
      rn(e, a);
      for (var i = o.length; i--;) {
        var u = o[i],
          s = u.animations;
        rn(e, s), s.length || u.children.length || o.splice(i, 1)
      }
      a.length || o.length || t.pause()
    }
  }, en.get = N, en.set = G, en.convertPx = I, en.path = function (n, e) {
    var r = u.str(n) ? h(n)[0] : n,
      t = e || 100;
    return function (n) {
      return {
        property: n,
        el: r,
        svg: q(r),
        totalLength: j(r) * (t / 100)
      }
    }
  }, en.setDashoffset = function (n) {
    var e = j(n);
    return n.setAttribute("stroke-dasharray", e), e
  }, en.stagger = function (n, e) {
    void 0 === e && (e = {});
    var r = e.direction || "normal",
      t = e.easing ? v(e.easing) : null,
      a = e.grid,
      o = e.axis,
      i = e.from || 0,
      s = "first" === i,
      c = "center" === i,
      f = "last" === i,
      l = u.arr(n),
      d = l ? parseFloat(n[0]) : parseFloat(n),
      p = l ? parseFloat(n[1]) : 0,
      h = C(l ? n[1] : n) || 0,
      g = e.start || 0 + (l ? d : 0),
      m = [],
      y = 0;
    return function (n, e, u) {
      if (s && (i = 0), c && (i = (u - 1) / 2), f && (i = u - 1), !m.length) {
        for (var v = 0; v < u; v++) {
          if (a) {
            var b = c ? (a[0] - 1) / 2 : i % a[0],
              x = c ? (a[1] - 1) / 2 : Math.floor(i / a[0]),
              M = b - v % a[0],
              w = x - Math.floor(v / a[0]),
              k = Math.sqrt(M * M + w * w);
            "x" === o && (k = -M), "y" === o && (k = -w), m.push(k)
          } else m.push(Math.abs(i - v));
          y = Math.max.apply(Math, m)
        }
        t && (m = m.map(function (n) {
          return t(n / y) * y
        })), "reverse" === r && (m = m.map(function (n) {
          return o ? n < 0 ? -1 * n : -n : Math.abs(y - n)
        }))
      }
      return g + (l ? (p - d) / y : d) * (Math.round(100 * m[e]) / 100) + h
    }
  }, en.timeline = function (n) {
    void 0 === n && (n = {});
    var r = en(n);
    return r.duration = 0, r.add = function (t, a) {
      var o = U.indexOf(r),
        i = r.children;

      function s(n) {
        n.passThrough = !0
      }
      o > -1 && U.splice(o, 1);
      for (var c = 0; c < i.length; c++) s(i[c]);
      var f = w(t, M(e, n));
      f.targets = f.targets || n.targets;
      var l = r.duration;
      f.autoplay = !1, f.direction = r.direction, f.timelineOffset = u.und(a) ? l : A(a, l), s(r), r.seek(f.timelineOffset);
      var d = en(f);
      s(d), i.push(d);
      var p = W(i, n);
      return r.delay = p.delay, r.endDelay = p.endDelay, r.duration = p.duration, r.seek(0), r.reset(), r.autoplay && r.play(), r
    }, r
  }, en.easing = v, en.penner = p, en.random = function (n, e) {
    return Math.floor(Math.random() * (e - n + 1)) + n
  }, en
});

/**
 * vivus - JavaScript library to make drawing animation on SVG
 * @version v0.4.4
 * @link https://github.com/maxwellito/vivus
 * @license MIT
 */
"use strict";
! function () {
  function t(t) {
    if ("undefined" == typeof t) throw new Error('Pathformer [constructor]: "element" parameter is required');
    if (t.constructor === String && (t = document.getElementById(t), !t)) throw new Error('Pathformer [constructor]: "element" parameter is not related to an existing ID');
    if (!(t instanceof window.SVGElement || t instanceof window.SVGGElement || /^svg$/i.test(t.nodeName))) throw new Error('Pathformer [constructor]: "element" parameter must be a string or a SVGelement');
    this.el = t, this.scan(t)
  }

  function e(t, e, n) {
    r(), this.isReady = !1, this.setElement(t, e), this.setOptions(e), this.setCallback(n), this.isReady && this.init()
  }
  t.prototype.TYPES = ["line", "ellipse", "circle", "polygon", "polyline", "rect"], t.prototype.ATTR_WATCH = ["cx", "cy", "points", "r", "rx", "ry", "x", "x1", "x2", "y", "y1", "y2"], t.prototype.scan = function (t) {
    for (var e, r, n, i, a = t.querySelectorAll(this.TYPES.join(",")), o = 0; o < a.length; o++) r = a[o], e = this[r.tagName.toLowerCase() + "ToPath"], n = e(this.parseAttr(r.attributes)), i = this.pathMaker(r, n), r.parentNode.replaceChild(i, r)
  }, t.prototype.lineToPath = function (t) {
    var e = {},
      r = t.x1 || 0,
      n = t.y1 || 0,
      i = t.x2 || 0,
      a = t.y2 || 0;
    return e.d = "M" + r + "," + n + "L" + i + "," + a, e
  }, t.prototype.rectToPath = function (t) {
    var e = {},
      r = parseFloat(t.x) || 0,
      n = parseFloat(t.y) || 0,
      i = parseFloat(t.width) || 0,
      a = parseFloat(t.height) || 0;
    if (t.rx || t.ry) {
      var o = parseInt(t.rx, 10) || -1,
        s = parseInt(t.ry, 10) || -1;
      o = Math.min(Math.max(0 > o ? s : o, 0), i / 2), s = Math.min(Math.max(0 > s ? o : s, 0), a / 2), e.d = "M " + (r + o) + "," + n + " L " + (r + i - o) + "," + n + " A " + o + "," + s + ",0,0,1," + (r + i) + "," + (n + s) + " L " + (r + i) + "," + (n + a - s) + " A " + o + "," + s + ",0,0,1," + (r + i - o) + "," + (n + a) + " L " + (r + o) + "," + (n + a) + " A " + o + "," + s + ",0,0,1," + r + "," + (n + a - s) + " L " + r + "," + (n + s) + " A " + o + "," + s + ",0,0,1," + (r + o) + "," + n
    } else e.d = "M" + r + " " + n + " L" + (r + i) + " " + n + " L" + (r + i) + " " + (n + a) + " L" + r + " " + (n + a) + " Z";
    return e
  }, t.prototype.polylineToPath = function (t) {
    var e, r, n = {},
      i = t.points.trim().split(" ");
    if (-1 === t.points.indexOf(",")) {
      var a = [];
      for (e = 0; e < i.length; e += 2) a.push(i[e] + "," + i[e + 1]);
      i = a
    }
    for (r = "M" + i[0], e = 1; e < i.length; e++) - 1 !== i[e].indexOf(",") && (r += "L" + i[e]);
    return n.d = r, n
  }, t.prototype.polygonToPath = function (e) {
    var r = t.prototype.polylineToPath(e);
    return r.d += "Z", r
  }, t.prototype.ellipseToPath = function (t) {
    var e = {},
      r = parseFloat(t.rx) || 0,
      n = parseFloat(t.ry) || 0,
      i = parseFloat(t.cx) || 0,
      a = parseFloat(t.cy) || 0,
      o = i - r,
      s = a,
      h = parseFloat(i) + parseFloat(r),
      l = a;
    return e.d = "M" + o + "," + s + "A" + r + "," + n + " 0,1,1 " + h + "," + l + "A" + r + "," + n + " 0,1,1 " + o + "," + l, e
  }, t.prototype.circleToPath = function (t) {
    var e = {},
      r = parseFloat(t.r) || 0,
      n = parseFloat(t.cx) || 0,
      i = parseFloat(t.cy) || 0,
      a = n - r,
      o = i,
      s = parseFloat(n) + parseFloat(r),
      h = i;
    return e.d = "M" + a + "," + o + "A" + r + "," + r + " 0,1,1 " + s + "," + h + "A" + r + "," + r + " 0,1,1 " + a + "," + h, e
  }, t.prototype.pathMaker = function (t, e) {
    var r, n, i = document.createElementNS("http://www.w3.org/2000/svg", "path");
    for (r = 0; r < t.attributes.length; r++) n = t.attributes[r], -1 === this.ATTR_WATCH.indexOf(n.name) && i.setAttribute(n.name, n.value);
    for (r in e) i.setAttribute(r, e[r]);
    return i
  }, t.prototype.parseAttr = function (t) {
    for (var e, r = {}, n = 0; n < t.length; n++) {
      if (e = t[n], -1 !== this.ATTR_WATCH.indexOf(e.name) && -1 !== e.value.indexOf("%")) throw new Error("Pathformer [parseAttr]: a SVG shape got values in percentage. This cannot be transformed into 'path' tags. Please use 'viewBox'.");
      r[e.name] = e.value
    }
    return r
  };
  var r, n, i, a;
  e.LINEAR = function (t) {
    return t
  }, e.EASE = function (t) {
    return -Math.cos(t * Math.PI) / 2 + .5
  }, e.EASE_OUT = function (t) {
    return 1 - Math.pow(1 - t, 3)
  }, e.EASE_IN = function (t) {
    return Math.pow(t, 3)
  }, e.EASE_OUT_BOUNCE = function (t) {
    var e = -Math.cos(.5 * t * Math.PI) + 1,
      r = Math.pow(e, 1.5),
      n = Math.pow(1 - t, 2),
      i = -Math.abs(Math.cos(2.5 * r * Math.PI)) + 1;
    return 1 - n + i * n
  }, e.prototype.setElement = function (t, e) {
    var r, n;
    if ("undefined" == typeof t) throw new Error('Vivus [constructor]: "element" parameter is required');
    if (t.constructor === String && (t = document.getElementById(t), !t)) throw new Error('Vivus [constructor]: "element" parameter is not related to an existing ID');
    if (this.parentEl = t, e && e.file) {
      var n = this;
      r = function () {
        var t = document.createElement("div");
        t.innerHTML = this.responseText;
        var r = t.querySelector("svg");
        if (!r) throw new Error("Vivus [load]: Cannot find the SVG in the loaded file : " + e.file);
        n.el = r, n.el.setAttribute("width", "100%"), n.el.setAttribute("height", "100%"), n.parentEl.appendChild(n.el), n.isReady = !0, n.init(), n = null
      };
      var i = new window.XMLHttpRequest;
      return i.addEventListener("load", r), i.open("GET", e.file), i.send(), void 0
    }
    switch (t.constructor) {
      case window.SVGSVGElement:
      case window.SVGElement:
      case window.SVGGElement:
        this.el = t, this.isReady = !0;
        break;
      case window.HTMLObjectElement:
        n = this, r = function (e) {
          if (!n.isReady) {
            if (n.el = t.contentDocument && t.contentDocument.querySelector("svg"), !n.el && e) throw new Error("Vivus [constructor]: object loaded does not contain any SVG");
            n.el && (t.getAttribute("built-by-vivus") && (n.parentEl.insertBefore(n.el, t), n.parentEl.removeChild(t), n.el.setAttribute("width", "100%"), n.el.setAttribute("height", "100%")), n.isReady = !0, n.init(), n = null)
          }
        }, r() || t.addEventListener("load", r);
        break;
      default:
        throw new Error('Vivus [constructor]: "element" parameter is not valid (or miss the "file" attribute)')
    }
  }, e.prototype.setOptions = function (t) {
    var r = ["delayed", "sync", "async", "nsync", "oneByOne", "scenario", "scenario-sync"],
      n = ["inViewport", "manual", "autostart"];
    if (void 0 !== t && t.constructor !== Object) throw new Error('Vivus [constructor]: "options" parameter must be an object');
    if (t = t || {}, t.type && -1 === r.indexOf(t.type)) throw new Error("Vivus [constructor]: " + t.type + " is not an existing animation `type`");
    if (this.type = t.type || r[0], t.start && -1 === n.indexOf(t.start)) throw new Error("Vivus [constructor]: " + t.start + " is not an existing `start` option");
    if (this.start = t.start || n[0], this.isIE = -1 !== window.navigator.userAgent.indexOf("MSIE") || -1 !== window.navigator.userAgent.indexOf("Trident/") || -1 !== window.navigator.userAgent.indexOf("Edge/"), this.duration = a(t.duration, 120), this.delay = a(t.delay, null), this.dashGap = a(t.dashGap, 1), this.forceRender = t.hasOwnProperty("forceRender") ? !!t.forceRender : this.isIE, this.reverseStack = !!t.reverseStack, this.selfDestroy = !!t.selfDestroy, this.onReady = t.onReady, this.map = [], this.frameLength = this.currentFrame = this.delayUnit = this.speed = this.handle = null, this.ignoreInvisible = t.hasOwnProperty("ignoreInvisible") ? !!t.ignoreInvisible : !1, this.animTimingFunction = t.animTimingFunction || e.LINEAR, this.pathTimingFunction = t.pathTimingFunction || e.LINEAR, this.delay >= this.duration) throw new Error("Vivus [constructor]: delay must be shorter than duration")
  }, e.prototype.setCallback = function (t) {
    if (t && t.constructor !== Function) throw new Error('Vivus [constructor]: "callback" parameter must be a function');
    this.callback = t || function () {}
  }, e.prototype.mapping = function () {
    var t, e, r, n, i, o, s, h;
    for (h = o = s = 0, e = this.el.querySelectorAll("path"), t = 0; t < e.length; t++) r = e[t], this.isInvisible(r) || (i = {
      el: r,
      length: Math.ceil(r.getTotalLength())
    }, isNaN(i.length) ? window.console && console.warn && console.warn("Vivus [mapping]: cannot retrieve a path element length", r) : (this.map.push(i), r.style.strokeDasharray = i.length + " " + (i.length + 2 * this.dashGap), r.style.strokeDashoffset = i.length + this.dashGap, i.length += this.dashGap, o += i.length, this.renderPath(t)));
    for (o = 0 === o ? 1 : o, this.delay = null === this.delay ? this.duration / 3 : this.delay, this.delayUnit = this.delay / (e.length > 1 ? e.length - 1 : 1), this.reverseStack && this.map.reverse(), t = 0; t < this.map.length; t++) {
      switch (i = this.map[t], this.type) {
        case "delayed":
          i.startAt = this.delayUnit * t, i.duration = this.duration - this.delay;
          break;
        case "oneByOne":
          i.startAt = s / o * this.duration, i.duration = i.length / o * this.duration;
          break;
        case "sync":
        case "async":
        case "nsync":
          i.startAt = 0, i.duration = this.duration;
          break;
        case "scenario-sync":
          r = i.el, n = this.parseAttr(r), i.startAt = h + (a(n["data-delay"], this.delayUnit) || 0), i.duration = a(n["data-duration"], this.duration), h = void 0 !== n["data-async"] ? i.startAt : i.startAt + i.duration, this.frameLength = Math.max(this.frameLength, i.startAt + i.duration);
          break;
        case "scenario":
          r = i.el, n = this.parseAttr(r), i.startAt = a(n["data-start"], this.delayUnit) || 0, i.duration = a(n["data-duration"], this.duration), this.frameLength = Math.max(this.frameLength, i.startAt + i.duration)
      }
      s += i.length, this.frameLength = this.frameLength || this.duration
    }
  }, e.prototype.drawer = function () {
    var t = this;
    if (this.currentFrame += this.speed, this.currentFrame <= 0) this.stop(), this.reset();
    else {
      if (!(this.currentFrame >= this.frameLength)) return this.trace(), this.handle = n(function () {
        t.drawer()
      }), void 0;
      this.stop(), this.currentFrame = this.frameLength, this.trace(), this.selfDestroy && this.destroy()
    }
    this.callback(this), this.instanceCallback && (this.instanceCallback(this), this.instanceCallback = null)
  }, e.prototype.trace = function () {
    var t, e, r, n;
    for (n = this.animTimingFunction(this.currentFrame / this.frameLength) * this.frameLength, t = 0; t < this.map.length; t++) r = this.map[t], e = (n - r.startAt) / r.duration, e = this.pathTimingFunction(Math.max(0, Math.min(1, e))), r.progress !== e && (r.progress = e, r.el.style.strokeDashoffset = Math.floor(r.length * (1 - e)), this.renderPath(t))
  }, e.prototype.renderPath = function (t) {
    if (this.forceRender && this.map && this.map[t]) {
      var e = this.map[t],
        r = e.el.cloneNode(!0);
      e.el.parentNode.replaceChild(r, e.el), e.el = r
    }
  }, e.prototype.init = function () {
    this.frameLength = 0, this.currentFrame = 0, this.map = [], new t(this.el), this.mapping(), this.starter(), this.onReady && this.onReady(this)
  }, e.prototype.starter = function () {
    switch (this.start) {
      case "manual":
        return;
      case "autostart":
        this.play();
        break;
      case "inViewport":
        var t = this,
          e = function () {
            t.isInViewport(t.parentEl, 1) && (t.play(), window.removeEventListener("scroll", e))
          };
        window.addEventListener("scroll", e), e()
    }
  }, e.prototype.getStatus = function () {
    return 0 === this.currentFrame ? "start" : this.currentFrame === this.frameLength ? "end" : "progress"
  }, e.prototype.reset = function () {
    return this.setFrameProgress(0)
  }, e.prototype.finish = function () {
    return this.setFrameProgress(1)
  }, e.prototype.setFrameProgress = function (t) {
    return t = Math.min(1, Math.max(0, t)), this.currentFrame = Math.round(this.frameLength * t), this.trace(), this
  }, e.prototype.play = function (t, e) {
    if (this.instanceCallback = null, t && "function" == typeof t) this.instanceCallback = t, t = null;
    else if (t && "number" != typeof t) throw new Error("Vivus [play]: invalid speed");
    return e && "function" == typeof e && !this.instanceCallback && (this.instanceCallback = e), this.speed = t || 1, this.handle || this.drawer(), this
  }, e.prototype.stop = function () {
    return this.handle && (i(this.handle), this.handle = null), this
  }, e.prototype.destroy = function () {
    this.stop();
    var t, e;
    for (t = 0; t < this.map.length; t++) e = this.map[t], e.el.style.strokeDashoffset = null, e.el.style.strokeDasharray = null, this.renderPath(t)
  }, e.prototype.isInvisible = function (t) {
    var e, r = t.getAttribute("data-ignore");
    return null !== r ? "false" !== r : this.ignoreInvisible ? (e = t.getBoundingClientRect(), !e.width && !e.height) : !1
  }, e.prototype.parseAttr = function (t) {
    var e, r = {};
    if (t && t.attributes)
      for (var n = 0; n < t.attributes.length; n++) e = t.attributes[n], r[e.name] = e.value;
    return r
  }, e.prototype.isInViewport = function (t, e) {
    var r = this.scrollY(),
      n = r + this.getViewportH(),
      i = t.getBoundingClientRect(),
      a = i.height,
      o = r + i.top,
      s = o + a;
    return e = e || 0, n >= o + a * e && s >= r
  }, e.prototype.getViewportH = function () {
    var t = this.docElem.clientHeight,
      e = window.innerHeight;
    return e > t ? e : t
  }, e.prototype.scrollY = function () {
    return window.pageYOffset || this.docElem.scrollTop
  }, r = function () {
    e.prototype.docElem || (e.prototype.docElem = window.document.documentElement, n = function () {
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (t) {
        return window.setTimeout(t, 1e3 / 60)
      }
    }(), i = function () {
      return window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || window.msCancelAnimationFrame || function (t) {
        return window.clearTimeout(t)
      }
    }())
  }, a = function (t, e) {
    var r = parseInt(t, 10);
    return r >= 0 ? r : e
  }, "function" == typeof define && define.amd ? define([], function () {
    return e
  }) : "object" == typeof exports ? module.exports = e : window.Vivus = e
}();

/*!
 * Flickity PACKAGED v2.1.2
 * Touch, responsive, flickable carousels
 *
 * Licensed GPLv3 for open source use
 * or Flickity Commercial License for commercial use
 *
 * https://flickity.metafizzy.co
 * Copyright 2015-2018 Metafizzy
 */

! function (t, e) {
  "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("jquery")) : t.jQueryBridget = e(t, t.jQuery)
}(window, function (t, e) {
  "use strict";

  function i(i, o, a) {
    function l(t, e, n) {
      var s, o = "$()." + i + '("' + e + '")';
      return t.each(function (t, l) {
        var h = a.data(l, i);
        if (!h) return void r(i + " not initialized. Cannot call methods, i.e. " + o);
        var c = h[e];
        if (!c || "_" == e.charAt(0)) return void r(o + " is not a valid method");
        var d = c.apply(h, n);
        s = void 0 === s ? d : s
      }), void 0 !== s ? s : t
    }

    function h(t, e) {
      t.each(function (t, n) {
        var s = a.data(n, i);
        s ? (s.option(e), s._init()) : (s = new o(n, e), a.data(n, i, s))
      })
    }
    a = a || e || t.jQuery, a && (o.prototype.option || (o.prototype.option = function (t) {
      a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t))
    }), a.fn[i] = function (t) {
      if ("string" == typeof t) {
        var e = s.call(arguments, 1);
        return l(this, t, e)
      }
      return h(this, t), this
    }, n(a))
  }

  function n(t) {
    !t || t && t.bridget || (t.bridget = i)
  }
  var s = Array.prototype.slice,
    o = t.console,
    r = "undefined" == typeof o ? function () {} : function (t) {
      o.error(t)
    };
  return n(e || t.jQuery), i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function () {
  function t() {}
  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {},
        n = i[t] = i[t] || [];
      return n.indexOf(e) == -1 && n.push(e), this
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {},
        n = i[t] = i[t] || {};
      return n[e] = !0, this
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      var n = i.indexOf(e);
      return n != -1 && i.splice(n, 1), this
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];
    if (i && i.length) {
      i = i.slice(0), e = e || [];
      for (var n = this._onceEvents && this._onceEvents[t], s = 0; s < i.length; s++) {
        var o = i[s],
          r = n && n[o];
        r && (this.off(t, o), delete n[o]), o.apply(this, e)
      }
      return this
    }
  }, e.allOff = function () {
    delete this._events, delete this._onceEvents
  }, t
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("get-size/get-size", e) : "object" == typeof module && module.exports ? module.exports = e() : t.getSize = e()
}(window, function () {
  "use strict";

  function t(t) {
    var e = parseFloat(t),
      i = t.indexOf("%") == -1 && !isNaN(e);
    return i && e
  }

  function e() {}

  function i() {
    for (var t = {
        width: 0,
        height: 0,
        innerWidth: 0,
        innerHeight: 0,
        outerWidth: 0,
        outerHeight: 0
      }, e = 0; e < h; e++) {
      var i = l[e];
      t[i] = 0
    }
    return t
  }

  function n(t) {
    var e = getComputedStyle(t);
    return e || a("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See https://bit.ly/getsizebug1"), e
  }

  function s() {
    if (!c) {
      c = !0;
      var e = document.createElement("div");
      e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style.boxSizing = "border-box";
      var i = document.body || document.documentElement;
      i.appendChild(e);
      var s = n(e);
      r = 200 == Math.round(t(s.width)), o.isBoxSizeOuter = r, i.removeChild(e)
    }
  }

  function o(e) {
    if (s(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == typeof e && e.nodeType) {
      var o = n(e);
      if ("none" == o.display) return i();
      var a = {};
      a.width = e.offsetWidth, a.height = e.offsetHeight;
      for (var c = a.isBorderBox = "border-box" == o.boxSizing, d = 0; d < h; d++) {
        var u = l[d],
          f = o[u],
          p = parseFloat(f);
        a[u] = isNaN(p) ? 0 : p
      }
      var g = a.paddingLeft + a.paddingRight,
        v = a.paddingTop + a.paddingBottom,
        m = a.marginLeft + a.marginRight,
        y = a.marginTop + a.marginBottom,
        b = a.borderLeftWidth + a.borderRightWidth,
        E = a.borderTopWidth + a.borderBottomWidth,
        S = c && r,
        C = t(o.width);
      C !== !1 && (a.width = C + (S ? 0 : g + b));
      var x = t(o.height);
      return x !== !1 && (a.height = x + (S ? 0 : v + E)), a.innerWidth = a.width - (g + b), a.innerHeight = a.height - (v + E), a.outerWidth = a.width + m, a.outerHeight = a.height + y, a
    }
  }
  var r, a = "undefined" == typeof console ? e : function (t) {
      console.error(t)
    },
    l = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
    h = l.length,
    c = !1;
  return o
}),
function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == typeof module && module.exports ? module.exports = e() : t.matchesSelector = e()
}(window, function () {
  "use strict";
  var t = function () {
    var t = window.Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";
    for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
      var n = e[i],
        s = n + "MatchesSelector";
      if (t[s]) return s
    }
  }();
  return function (e, i) {
    return e[t](i)
  }
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.matchesSelector)
}(window, function (t, e) {
  var i = {};
  i.extend = function (t, e) {
    for (var i in e) t[i] = e[i];
    return t
  }, i.modulo = function (t, e) {
    return (t % e + e) % e
  };
  var n = Array.prototype.slice;
  i.makeArray = function (t) {
    if (Array.isArray(t)) return t;
    if (null === t || void 0 === t) return [];
    var e = "object" == typeof t && "number" == typeof t.length;
    return e ? n.call(t) : [t]
  }, i.removeFrom = function (t, e) {
    var i = t.indexOf(e);
    i != -1 && t.splice(i, 1)
  }, i.getParent = function (t, i) {
    for (; t.parentNode && t != document.body;)
      if (t = t.parentNode, e(t, i)) return t
  }, i.getQueryElement = function (t) {
    return "string" == typeof t ? document.querySelector(t) : t
  }, i.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, i.filterFindElements = function (t, n) {
    t = i.makeArray(t);
    var s = [];
    return t.forEach(function (t) {
      if (t instanceof HTMLElement) {
        if (!n) return void s.push(t);
        e(t, n) && s.push(t);
        for (var i = t.querySelectorAll(n), o = 0; o < i.length; o++) s.push(i[o])
      }
    }), s
  }, i.debounceMethod = function (t, e, i) {
    i = i || 100;
    var n = t.prototype[e],
      s = e + "Timeout";
    t.prototype[e] = function () {
      var t = this[s];
      clearTimeout(t);
      var e = arguments,
        o = this;
      this[s] = setTimeout(function () {
        n.apply(o, e), delete o[s]
      }, i)
    }
  }, i.docReady = function (t) {
    var e = document.readyState;
    "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t)
  }, i.toDashed = function (t) {
    return t.replace(/(.)([A-Z])/g, function (t, e, i) {
      return e + "-" + i
    }).toLowerCase()
  };
  var s = t.console;
  return i.htmlInit = function (e, n) {
    i.docReady(function () {
      var o = i.toDashed(n),
        r = "data-" + o,
        a = document.querySelectorAll("[" + r + "]"),
        l = document.querySelectorAll(".js-" + o),
        h = i.makeArray(a).concat(i.makeArray(l)),
        c = r + "-options",
        d = t.jQuery;
      h.forEach(function (t) {
        var i, o = t.getAttribute(r) || t.getAttribute(c);
        try {
          i = o && JSON.parse(o)
        } catch (a) {
          return void(s && s.error("Error parsing " + r + " on " + t.className + ": " + a))
        }
        var l = new e(t, i);
        d && d.data(t, n, l)
      })
    })
  }, i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/cell", ["get-size/get-size"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("get-size")) : (t.Flickity = t.Flickity || {}, t.Flickity.Cell = e(t, t.getSize))
}(window, function (t, e) {
  function i(t, e) {
    this.element = t, this.parent = e, this.create()
  }
  var n = i.prototype;
  return n.create = function () {
    this.element.style.position = "absolute", this.element.setAttribute("aria-selected", "false"), this.x = 0, this.shift = 0
  }, n.destroy = function () {
    this.element.style.position = "";
    var t = this.parent.originSide;
    this.element.removeAttribute("aria-selected"), this.element.style[t] = ""
  }, n.getSize = function () {
    this.size = e(this.element)
  }, n.setPosition = function (t) {
    this.x = t, this.updateTarget(), this.renderPosition(t)
  }, n.updateTarget = n.setDefaultTarget = function () {
    var t = "left" == this.parent.originSide ? "marginLeft" : "marginRight";
    this.target = this.x + this.size[t] + this.size.width * this.parent.cellAlign
  }, n.renderPosition = function (t) {
    var e = this.parent.originSide;
    this.element.style[e] = this.parent.getPositionValue(t)
  }, n.wrapShift = function (t) {
    this.shift = t, this.renderPosition(this.x + this.parent.slideableWidth * t)
  }, n.remove = function () {
    this.element.parentNode.removeChild(this.element)
  }, i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/slide", e) : "object" == typeof module && module.exports ? module.exports = e() : (t.Flickity = t.Flickity || {}, t.Flickity.Slide = e())
}(window, function () {
  "use strict";

  function t(t) {
    this.parent = t, this.isOriginLeft = "left" == t.originSide, this.cells = [], this.outerWidth = 0, this.height = 0
  }
  var e = t.prototype;
  return e.addCell = function (t) {
    if (this.cells.push(t), this.outerWidth += t.size.outerWidth, this.height = Math.max(t.size.outerHeight, this.height), 1 == this.cells.length) {
      this.x = t.x;
      var e = this.isOriginLeft ? "marginLeft" : "marginRight";
      this.firstMargin = t.size[e]
    }
  }, e.updateTarget = function () {
    var t = this.isOriginLeft ? "marginRight" : "marginLeft",
      e = this.getLastCell(),
      i = e ? e.size[t] : 0,
      n = this.outerWidth - (this.firstMargin + i);
    this.target = this.x + this.firstMargin + n * this.parent.cellAlign
  }, e.getLastCell = function () {
    return this.cells[this.cells.length - 1]
  }, e.select = function () {
    this.changeSelected(!0)
  }, e.unselect = function () {
    this.changeSelected(!1)
  }, e.changeSelected = function (t) {
    var e = t ? "add" : "remove";
    this.cells.forEach(function (i) {
      i.element.classList[e]("is-selected"), i.element.setAttribute("aria-selected", t.toString())
    })
  }, e.getCellElements = function () {
    return this.cells.map(function (t) {
      return t.element
    })
  }, t
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/animate", ["fizzy-ui-utils/utils"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("fizzy-ui-utils")) : (t.Flickity = t.Flickity || {}, t.Flickity.animatePrototype = e(t, t.fizzyUIUtils))
}(window, function (t, e) {
  var i = {};
  return i.startAnimation = function () {
    this.isAnimating || (this.isAnimating = !0, this.restingFrames = 0, this.animate())
  }, i.animate = function () {
    this.applyDragForce(), this.applySelectedAttraction();
    var t = this.x;
    if (this.integratePhysics(), this.positionSlider(), this.settle(t), this.isAnimating) {
      var e = this;
      requestAnimationFrame(function () {
        e.animate()
      })
    }
  }, i.positionSlider = function () {
    var t = this.x;
    this.options.wrapAround && this.cells.length > 1 && (t = e.modulo(t, this.slideableWidth), t -= this.slideableWidth, this.shiftWrapCells(t)), t += this.cursorPosition, t = this.options.rightToLeft ? -t : t;
    var i = this.getPositionValue(t);
    this.slider.style.transform = this.isAnimating ? "translate3d(" + i + ",0,0)" : "translateX(" + i + ")";
    var n = this.slides[0];
    if (n) {
      var s = -this.x - n.target,
        o = s / this.slidesWidth;
      this.dispatchEvent("scroll", null, [o, s])
    }
  }, i.positionSliderAtSelected = function () {
    this.cells.length && (this.x = -this.selectedSlide.target, this.velocity = 0, this.positionSlider())
  }, i.getPositionValue = function (t) {
    return this.options.percentPosition ? .01 * Math.round(t / this.size.innerWidth * 1e4) + "%" : Math.round(t) + "px"
  }, i.settle = function (t) {
    this.isPointerDown || Math.round(100 * this.x) != Math.round(100 * t) || this.restingFrames++, this.restingFrames > 2 && (this.isAnimating = !1, delete this.isFreeScrolling, this.positionSlider(), this.dispatchEvent("settle", null, [this.selectedIndex]))
  }, i.shiftWrapCells = function (t) {
    var e = this.cursorPosition + t;
    this._shiftCells(this.beforeShiftCells, e, -1);
    var i = this.size.innerWidth - (t + this.slideableWidth + this.cursorPosition);
    this._shiftCells(this.afterShiftCells, i, 1)
  }, i._shiftCells = function (t, e, i) {
    for (var n = 0; n < t.length; n++) {
      var s = t[n],
        o = e > 0 ? i : 0;
      s.wrapShift(o), e -= s.size.outerWidth
    }
  }, i._unshiftCells = function (t) {
    if (t && t.length)
      for (var e = 0; e < t.length; e++) t[e].wrapShift(0)
  }, i.integratePhysics = function () {
    this.x += this.velocity, this.velocity *= this.getFrictionFactor()
  }, i.applyForce = function (t) {
    this.velocity += t
  }, i.getFrictionFactor = function () {
    return 1 - this.options[this.isFreeScrolling ? "freeScrollFriction" : "friction"]
  }, i.getRestingPosition = function () {
    return this.x + this.velocity / (1 - this.getFrictionFactor())
  }, i.applyDragForce = function () {
    if (this.isDraggable && this.isPointerDown) {
      var t = this.dragX - this.x,
        e = t - this.velocity;
      this.applyForce(e)
    }
  }, i.applySelectedAttraction = function () {
    var t = this.isDraggable && this.isPointerDown;
    if (!t && !this.isFreeScrolling && this.slides.length) {
      var e = this.selectedSlide.target * -1 - this.x,
        i = e * this.options.selectedAttraction;
      this.applyForce(i)
    }
  }, i
}),
function (t, e) {
  if ("function" == typeof define && define.amd) define("flickity/js/flickity", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./cell", "./slide", "./animate"], function (i, n, s, o, r, a) {
    return e(t, i, n, s, o, r, a)
  });
  else if ("object" == typeof module && module.exports) module.exports = e(t, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./cell"), require("./slide"), require("./animate"));
  else {
    var i = t.Flickity;
    t.Flickity = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, i.Cell, i.Slide, i.animatePrototype)
  }
}(window, function (t, e, i, n, s, o, r) {
  function a(t, e) {
    for (t = n.makeArray(t); t.length;) e.appendChild(t.shift())
  }

  function l(t, e) {
    var i = n.getQueryElement(t);
    if (!i) return void(d && d.error("Bad element for Flickity: " + (i || t)));
    if (this.element = i, this.element.flickityGUID) {
      var s = f[this.element.flickityGUID];
      return s.option(e), s
    }
    h && (this.$element = h(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e), this._create()
  }
  var h = t.jQuery,
    c = t.getComputedStyle,
    d = t.console,
    u = 0,
    f = {};
  l.defaults = {
    accessibility: !0,
    cellAlign: "center",
    freeScrollFriction: .075,
    friction: .28,
    namespaceJQueryEvents: !0,
    percentPosition: !0,
    resize: !0,
    selectedAttraction: .025,
    setGallerySize: !0
  }, l.createMethods = [];
  var p = l.prototype;
  n.extend(p, e.prototype), p._create = function () {
    var e = this.guid = ++u;
    this.element.flickityGUID = e, f[e] = this, this.selectedIndex = 0, this.restingFrames = 0, this.x = 0, this.velocity = 0, this.originSide = this.options.rightToLeft ? "right" : "left", this.viewport = document.createElement("div"), this.viewport.className = "flickity-viewport", this._createSlider(), (this.options.resize || this.options.watchCSS) && t.addEventListener("resize", this);
    for (var i in this.options.on) {
      var n = this.options.on[i];
      this.on(i, n)
    }
    l.createMethods.forEach(function (t) {
      this[t]()
    }, this), this.options.watchCSS ? this.watchCSS() : this.activate()
  }, p.option = function (t) {
    n.extend(this.options, t)
  }, p.activate = function () {
    if (!this.isActive) {
      this.isActive = !0, this.element.classList.add("flickity-enabled"), this.options.rightToLeft && this.element.classList.add("flickity-rtl"), this.getSize();
      var t = this._filterFindCellElements(this.element.children);
      a(t, this.slider), this.viewport.appendChild(this.slider), this.element.appendChild(this.viewport), this.reloadCells(), this.options.accessibility && (this.element.tabIndex = 0, this.element.addEventListener("keydown", this)), this.emitEvent("activate");
      var e, i = this.options.initialIndex;
      e = this.isInitActivated ? this.selectedIndex : void 0 !== i && this.cells[i] ? i : 0, this.select(e, !1, !0), this.isInitActivated = !0, this.dispatchEvent("ready")
    }
  }, p._createSlider = function () {
    var t = document.createElement("div");
    t.className = "flickity-slider", t.style[this.originSide] = 0, this.slider = t
  }, p._filterFindCellElements = function (t) {
    return n.filterFindElements(t, this.options.cellSelector)
  }, p.reloadCells = function () {
    this.cells = this._makeCells(this.slider.children), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize()
  }, p._makeCells = function (t) {
    var e = this._filterFindCellElements(t),
      i = e.map(function (t) {
        return new s(t, this)
      }, this);
    return i
  }, p.getLastCell = function () {
    return this.cells[this.cells.length - 1]
  }, p.getLastSlide = function () {
    return this.slides[this.slides.length - 1]
  }, p.positionCells = function () {
    this._sizeCells(this.cells), this._positionCells(0)
  }, p._positionCells = function (t) {
    t = t || 0, this.maxCellHeight = t ? this.maxCellHeight || 0 : 0;
    var e = 0;
    if (t > 0) {
      var i = this.cells[t - 1];
      e = i.x + i.size.outerWidth
    }
    for (var n = this.cells.length, s = t; s < n; s++) {
      var o = this.cells[s];
      o.setPosition(e), e += o.size.outerWidth, this.maxCellHeight = Math.max(o.size.outerHeight, this.maxCellHeight)
    }
    this.slideableWidth = e, this.updateSlides(), this._containSlides(), this.slidesWidth = n ? this.getLastSlide().target - this.slides[0].target : 0
  }, p._sizeCells = function (t) {
    t.forEach(function (t) {
      t.getSize()
    })
  }, p.updateSlides = function () {
    if (this.slides = [], this.cells.length) {
      var t = new o(this);
      this.slides.push(t);
      var e = "left" == this.originSide,
        i = e ? "marginRight" : "marginLeft",
        n = this._getCanCellFit();
      this.cells.forEach(function (e, s) {
        if (!t.cells.length) return void t.addCell(e);
        var r = t.outerWidth - t.firstMargin + (e.size.outerWidth - e.size[i]);
        n.call(this, s, r) ? t.addCell(e) : (t.updateTarget(), t = new o(this), this.slides.push(t), t.addCell(e))
      }, this), t.updateTarget(), this.updateSelectedSlide()
    }
  }, p._getCanCellFit = function () {
    var t = this.options.groupCells;
    if (!t) return function () {
      return !1
    };
    if ("number" == typeof t) {
      var e = parseInt(t, 10);
      return function (t) {
        return t % e !== 0
      }
    }
    var i = "string" == typeof t && t.match(/^(\d+)%$/),
      n = i ? parseInt(i[1], 10) / 100 : 1;
    return function (t, e) {
      return e <= (this.size.innerWidth + 1) * n
    }
  }, p._init = p.reposition = function () {
    this.positionCells(), this.positionSliderAtSelected()
  }, p.getSize = function () {
    this.size = i(this.element), this.setCellAlign(), this.cursorPosition = this.size.innerWidth * this.cellAlign
  };
  var g = {
    center: {
      left: .5,
      right: .5
    },
    left: {
      left: 0,
      right: 1
    },
    right: {
      right: 0,
      left: 1
    }
  };
  return p.setCellAlign = function () {
    var t = g[this.options.cellAlign];
    this.cellAlign = t ? t[this.originSide] : this.options.cellAlign
  }, p.setGallerySize = function () {
    if (this.options.setGallerySize) {
      var t = this.options.adaptiveHeight && this.selectedSlide ? this.selectedSlide.height : this.maxCellHeight;
      this.viewport.style.height = t + "px"
    }
  }, p._getWrapShiftCells = function () {
    if (this.options.wrapAround) {
      this._unshiftCells(this.beforeShiftCells), this._unshiftCells(this.afterShiftCells);
      var t = this.cursorPosition,
        e = this.cells.length - 1;
      this.beforeShiftCells = this._getGapCells(t, e, -1), t = this.size.innerWidth - this.cursorPosition, this.afterShiftCells = this._getGapCells(t, 0, 1)
    }
  }, p._getGapCells = function (t, e, i) {
    for (var n = []; t > 0;) {
      var s = this.cells[e];
      if (!s) break;
      n.push(s), e += i, t -= s.size.outerWidth
    }
    return n
  }, p._containSlides = function () {
    if (this.options.contain && !this.options.wrapAround && this.cells.length) {
      var t = this.options.rightToLeft,
        e = t ? "marginRight" : "marginLeft",
        i = t ? "marginLeft" : "marginRight",
        n = this.slideableWidth - this.getLastCell().size[i],
        s = n < this.size.innerWidth,
        o = this.cursorPosition + this.cells[0].size[e],
        r = n - this.size.innerWidth * (1 - this.cellAlign);
      this.slides.forEach(function (t) {
        s ? t.target = n * this.cellAlign : (t.target = Math.max(t.target, o), t.target = Math.min(t.target, r))
      }, this)
    }
  }, p.dispatchEvent = function (t, e, i) {
    var n = e ? [e].concat(i) : i;
    if (this.emitEvent(t, n), h && this.$element) {
      t += this.options.namespaceJQueryEvents ? ".flickity" : "";
      var s = t;
      if (e) {
        var o = h.Event(e);
        o.type = t, s = o
      }
      this.$element.trigger(s, i)
    }
  }, p.select = function (t, e, i) {
    if (this.isActive && (t = parseInt(t, 10), this._wrapSelect(t), (this.options.wrapAround || e) && (t = n.modulo(t, this.slides.length)), this.slides[t])) {
      var s = this.selectedIndex;
      this.selectedIndex = t, this.updateSelectedSlide(), i ? this.positionSliderAtSelected() : this.startAnimation(), this.options.adaptiveHeight && this.setGallerySize(), this.dispatchEvent("select", null, [t]), t != s && this.dispatchEvent("change", null, [t]), this.dispatchEvent("cellSelect")
    }
  }, p._wrapSelect = function (t) {
    var e = this.slides.length,
      i = this.options.wrapAround && e > 1;
    if (!i) return t;
    var s = n.modulo(t, e),
      o = Math.abs(s - this.selectedIndex),
      r = Math.abs(s + e - this.selectedIndex),
      a = Math.abs(s - e - this.selectedIndex);
    !this.isDragSelect && r < o ? t += e : !this.isDragSelect && a < o && (t -= e), t < 0 ? this.x -= this.slideableWidth : t >= e && (this.x += this.slideableWidth)
  }, p.previous = function (t, e) {
    this.select(this.selectedIndex - 1, t, e)
  }, p.next = function (t, e) {
    this.select(this.selectedIndex + 1, t, e)
  }, p.updateSelectedSlide = function () {
    var t = this.slides[this.selectedIndex];
    t && (this.unselectSelectedSlide(), this.selectedSlide = t, t.select(), this.selectedCells = t.cells, this.selectedElements = t.getCellElements(), this.selectedCell = t.cells[0], this.selectedElement = this.selectedElements[0])
  }, p.unselectSelectedSlide = function () {
    this.selectedSlide && this.selectedSlide.unselect()
  }, p.selectCell = function (t, e, i) {
    var n = this.queryCell(t);
    if (n) {
      var s = this.getCellSlideIndex(n);
      this.select(s, e, i)
    }
  }, p.getCellSlideIndex = function (t) {
    for (var e = 0; e < this.slides.length; e++) {
      var i = this.slides[e],
        n = i.cells.indexOf(t);
      if (n != -1) return e
    }
  }, p.getCell = function (t) {
    for (var e = 0; e < this.cells.length; e++) {
      var i = this.cells[e];
      if (i.element == t) return i
    }
  }, p.getCells = function (t) {
    t = n.makeArray(t);
    var e = [];
    return t.forEach(function (t) {
      var i = this.getCell(t);
      i && e.push(i)
    }, this), e
  }, p.getCellElements = function () {
    return this.cells.map(function (t) {
      return t.element
    })
  }, p.getParentCell = function (t) {
    var e = this.getCell(t);
    return e ? e : (t = n.getParent(t, ".flickity-slider > *"), this.getCell(t))
  }, p.getAdjacentCellElements = function (t, e) {
    if (!t) return this.selectedSlide.getCellElements();
    e = void 0 === e ? this.selectedIndex : e;
    var i = this.slides.length;
    if (1 + 2 * t >= i) return this.getCellElements();
    for (var s = [], o = e - t; o <= e + t; o++) {
      var r = this.options.wrapAround ? n.modulo(o, i) : o,
        a = this.slides[r];
      a && (s = s.concat(a.getCellElements()))
    }
    return s
  }, p.queryCell = function (t) {
    return "number" == typeof t ? this.cells[t] : ("string" == typeof t && (t = this.element.querySelector(t)), this.getCell(t))
  }, p.uiChange = function () {
    this.emitEvent("uiChange")
  }, p.childUIPointerDown = function (t) {
    this.emitEvent("childUIPointerDown", [t])
  }, p.onresize = function () {
    this.watchCSS(), this.resize()
  }, n.debounceMethod(l, "onresize", 150), p.resize = function () {
    if (this.isActive) {
      this.getSize(), this.options.wrapAround && (this.x = n.modulo(this.x, this.slideableWidth)), this.positionCells(), this._getWrapShiftCells(), this.setGallerySize(), this.emitEvent("resize");
      var t = this.selectedElements && this.selectedElements[0];
      this.selectCell(t, !1, !0)
    }
  }, p.watchCSS = function () {
    var t = this.options.watchCSS;
    if (t) {
      var e = c(this.element, ":after").content;
      e.indexOf("flickity") != -1 ? this.activate() : this.deactivate()
    }
  }, p.onkeydown = function (t) {
    var e = document.activeElement && document.activeElement != this.element;
    if (this.options.accessibility && !e) {
      var i = l.keyboardHandlers[t.keyCode];
      i && i.call(this)
    }
  }, l.keyboardHandlers = {
    37: function () {
      var t = this.options.rightToLeft ? "next" : "previous";
      this.uiChange(), this[t]()
    },
    39: function () {
      var t = this.options.rightToLeft ? "previous" : "next";
      this.uiChange(), this[t]()
    }
  }, p.focus = function () {
    var e = t.pageYOffset;
    this.element.focus({
      preventScroll: !0
    }), t.pageYOffset != e && t.scrollTo(t.pageXOffset, e)
  }, p.deactivate = function () {
    this.isActive && (this.element.classList.remove("flickity-enabled"), this.element.classList.remove("flickity-rtl"), this.unselectSelectedSlide(), this.cells.forEach(function (t) {
      t.destroy()
    }), this.element.removeChild(this.viewport), a(this.slider.children, this.element), this.options.accessibility && (this.element.removeAttribute("tabIndex"), this.element.removeEventListener("keydown", this)), this.isActive = !1, this.emitEvent("deactivate"))
  }, p.destroy = function () {
    this.deactivate(), t.removeEventListener("resize", this), this.emitEvent("destroy"), h && this.$element && h.removeData(this.element, "flickity"), delete this.element.flickityGUID, delete f[this.guid]
  }, n.extend(p, r), l.data = function (t) {
    t = n.getQueryElement(t);
    var e = t && t.flickityGUID;
    return e && f[e]
  }, n.htmlInit(l, "flickity"), h && h.bridget && h.bridget("flickity", l), l.setJQuery = function (t) {
    h = t
  }, l.Cell = s, l
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("unipointer/unipointer", ["ev-emitter/ev-emitter"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.Unipointer = e(t, t.EvEmitter)
}(window, function (t, e) {
  function i() {}

  function n() {}
  var s = n.prototype = Object.create(e.prototype);
  s.bindStartEvent = function (t) {
    this._bindStartEvent(t, !0)
  }, s.unbindStartEvent = function (t) {
    this._bindStartEvent(t, !1)
  }, s._bindStartEvent = function (e, i) {
    i = void 0 === i || i;
    var n = i ? "addEventListener" : "removeEventListener",
      s = "mousedown";
    t.PointerEvent ? s = "pointerdown" : "ontouchstart" in t && (s = "touchstart"), e[n](s, this)
  }, s.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, s.getTouch = function (t) {
    for (var e = 0; e < t.length; e++) {
      var i = t[e];
      if (i.identifier == this.pointerIdentifier) return i
    }
  }, s.onmousedown = function (t) {
    var e = t.button;
    e && 0 !== e && 1 !== e || this._pointerDown(t, t)
  }, s.ontouchstart = function (t) {
    this._pointerDown(t, t.changedTouches[0])
  }, s.onpointerdown = function (t) {
    this._pointerDown(t, t)
  }, s._pointerDown = function (t, e) {
    t.button || this.isPointerDown || (this.isPointerDown = !0, this.pointerIdentifier = void 0 !== e.pointerId ? e.pointerId : e.identifier, this.pointerDown(t, e))
  }, s.pointerDown = function (t, e) {
    this._bindPostStartEvents(t), this.emitEvent("pointerDown", [t, e])
  };
  var o = {
    mousedown: ["mousemove", "mouseup"],
    touchstart: ["touchmove", "touchend", "touchcancel"],
    pointerdown: ["pointermove", "pointerup", "pointercancel"]
  };
  return s._bindPostStartEvents = function (e) {
    if (e) {
      var i = o[e.type];
      i.forEach(function (e) {
        t.addEventListener(e, this)
      }, this), this._boundPointerEvents = i
    }
  }, s._unbindPostStartEvents = function () {
    this._boundPointerEvents && (this._boundPointerEvents.forEach(function (e) {
      t.removeEventListener(e, this)
    }, this), delete this._boundPointerEvents)
  }, s.onmousemove = function (t) {
    this._pointerMove(t, t)
  }, s.onpointermove = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerMove(t, t)
  }, s.ontouchmove = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerMove(t, e)
  }, s._pointerMove = function (t, e) {
    this.pointerMove(t, e)
  }, s.pointerMove = function (t, e) {
    this.emitEvent("pointerMove", [t, e])
  }, s.onmouseup = function (t) {
    this._pointerUp(t, t)
  }, s.onpointerup = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerUp(t, t)
  }, s.ontouchend = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerUp(t, e)
  }, s._pointerUp = function (t, e) {
    this._pointerDone(), this.pointerUp(t, e)
  }, s.pointerUp = function (t, e) {
    this.emitEvent("pointerUp", [t, e])
  }, s._pointerDone = function () {
    this._pointerReset(), this._unbindPostStartEvents(), this.pointerDone()
  }, s._pointerReset = function () {
    this.isPointerDown = !1, delete this.pointerIdentifier
  }, s.pointerDone = i, s.onpointercancel = function (t) {
    t.pointerId == this.pointerIdentifier && this._pointerCancel(t, t)
  }, s.ontouchcancel = function (t) {
    var e = this.getTouch(t.changedTouches);
    e && this._pointerCancel(t, e)
  }, s._pointerCancel = function (t, e) {
    this._pointerDone(), this.pointerCancel(t, e)
  }, s.pointerCancel = function (t, e) {
    this.emitEvent("pointerCancel", [t, e])
  }, n.getPointerPoint = function (t) {
    return {
      x: t.pageX,
      y: t.pageY
    }
  }, n
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("unidragger/unidragger", ["unipointer/unipointer"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("unipointer")) : t.Unidragger = e(t, t.Unipointer)
}(window, function (t, e) {
  function i() {}
  var n = i.prototype = Object.create(e.prototype);
  n.bindHandles = function () {
    this._bindHandles(!0)
  }, n.unbindHandles = function () {
    this._bindHandles(!1)
  }, n._bindHandles = function (e) {
    e = void 0 === e || e;
    for (var i = e ? "addEventListener" : "removeEventListener", n = e ? this._touchActionValue : "", s = 0; s < this.handles.length; s++) {
      var o = this.handles[s];
      this._bindStartEvent(o, e), o[i]("click", this), t.PointerEvent && (o.style.touchAction = n)
    }
  }, n._touchActionValue = "none", n.pointerDown = function (t, e) {
    var i = this.okayPointerDown(t);
    i && (this.pointerDownPointer = e, t.preventDefault(), this.pointerDownBlur(), this._bindPostStartEvents(t), this.emitEvent("pointerDown", [t, e]))
  };
  var s = {
      TEXTAREA: !0,
      INPUT: !0,
      SELECT: !0,
      OPTION: !0
    },
    o = {
      radio: !0,
      checkbox: !0,
      button: !0,
      submit: !0,
      image: !0,
      file: !0
    };
  return n.okayPointerDown = function (t) {
    var e = s[t.target.nodeName],
      i = o[t.target.type],
      n = !e || i;
    return n || this._pointerReset(), n
  }, n.pointerDownBlur = function () {
    var t = document.activeElement,
      e = t && t.blur && t != document.body;
    e && t.blur()
  }, n.pointerMove = function (t, e) {
    var i = this._dragPointerMove(t, e);
    this.emitEvent("pointerMove", [t, e, i]), this._dragMove(t, e, i)
  }, n._dragPointerMove = function (t, e) {
    var i = {
      x: e.pageX - this.pointerDownPointer.pageX,
      y: e.pageY - this.pointerDownPointer.pageY
    };
    return !this.isDragging && this.hasDragStarted(i) && this._dragStart(t, e), i
  }, n.hasDragStarted = function (t) {
    return Math.abs(t.x) > 3 || Math.abs(t.y) > 3
  }, n.pointerUp = function (t, e) {
    this.emitEvent("pointerUp", [t, e]), this._dragPointerUp(t, e)
  }, n._dragPointerUp = function (t, e) {
    this.isDragging ? this._dragEnd(t, e) : this._staticClick(t, e)
  }, n._dragStart = function (t, e) {
    this.isDragging = !0, this.isPreventingClicks = !0, this.dragStart(t, e)
  }, n.dragStart = function (t, e) {
    this.emitEvent("dragStart", [t, e])
  }, n._dragMove = function (t, e, i) {
    this.isDragging && this.dragMove(t, e, i)
  }, n.dragMove = function (t, e, i) {
    t.preventDefault(), this.emitEvent("dragMove", [t, e, i])
  }, n._dragEnd = function (t, e) {
    this.isDragging = !1, setTimeout(function () {
      delete this.isPreventingClicks
    }.bind(this)), this.dragEnd(t, e)
  }, n.dragEnd = function (t, e) {
    this.emitEvent("dragEnd", [t, e])
  }, n.onclick = function (t) {
    this.isPreventingClicks && t.preventDefault()
  }, n._staticClick = function (t, e) {
    this.isIgnoringMouseUp && "mouseup" == t.type || (this.staticClick(t, e), "mouseup" != t.type && (this.isIgnoringMouseUp = !0, setTimeout(function () {
      delete this.isIgnoringMouseUp
    }.bind(this), 400)))
  }, n.staticClick = function (t, e) {
    this.emitEvent("staticClick", [t, e])
  }, i.getPointerPoint = e.getPointerPoint, i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/drag", ["./flickity", "unidragger/unidragger", "fizzy-ui-utils/utils"], function (i, n, s) {
    return e(t, i, n, s)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("./flickity"), require("unidragger"), require("fizzy-ui-utils")) : t.Flickity = e(t, t.Flickity, t.Unidragger, t.fizzyUIUtils)
}(window, function (t, e, i, n) {
  function s() {
    return {
      x: t.pageXOffset,
      y: t.pageYOffset
    }
  }
  n.extend(e.defaults, {
    draggable: ">1",
    dragThreshold: 3
  }), e.createMethods.push("_createDrag");
  var o = e.prototype;
  n.extend(o, i.prototype), o._touchActionValue = "pan-y";
  var r = "createTouch" in document,
    a = !1;
  o._createDrag = function () {
    this.on("activate", this.onActivateDrag), this.on("uiChange", this._uiChangeDrag), this.on("childUIPointerDown", this._childUIPointerDownDrag), this.on("deactivate", this.onDeactivateDrag), this.on("cellChange", this.updateDraggable), r && !a && (t.addEventListener("touchmove", function () {}), a = !0)
  }, o.onActivateDrag = function () {
    this.handles = [this.viewport], this.bindHandles(), this.updateDraggable()
  }, o.onDeactivateDrag = function () {
    this.unbindHandles(), this.element.classList.remove("is-draggable")
  }, o.updateDraggable = function () {
    ">1" == this.options.draggable ? this.isDraggable = this.slides.length > 1 : this.isDraggable = this.options.draggable, this.isDraggable ? this.element.classList.add("is-draggable") : this.element.classList.remove("is-draggable")
  }, o.bindDrag = function () {
    this.options.draggable = !0, this.updateDraggable()
  }, o.unbindDrag = function () {
    this.options.draggable = !1, this.updateDraggable()
  }, o._uiChangeDrag = function () {
    delete this.isFreeScrolling
  }, o._childUIPointerDownDrag = function (t) {
    t.preventDefault(), this.pointerDownFocus(t)
  }, o.pointerDown = function (e, i) {
    if (!this.isDraggable) return void this._pointerDownDefault(e, i);
    var n = this.okayPointerDown(e);
    n && (this._pointerDownPreventDefault(e), this.pointerDownFocus(e), document.activeElement != this.element && this.pointerDownBlur(), this.dragX = this.x, this.viewport.classList.add("is-pointer-down"), this.pointerDownScroll = s(), t.addEventListener("scroll", this), this._pointerDownDefault(e, i))
  }, o._pointerDownDefault = function (t, e) {
    this.pointerDownPointer = e, this._bindPostStartEvents(t), this.dispatchEvent("pointerDown", t, [e])
  };
  var l = {
    INPUT: !0,
    TEXTAREA: !0,
    SELECT: !0
  };
  return o.pointerDownFocus = function (t) {
    var e = l[t.target.nodeName];
    e || this.focus()
  }, o._pointerDownPreventDefault = function (t) {
    var e = "touchstart" == t.type,
      i = "touch" == t.pointerType,
      n = l[t.target.nodeName];
    e || i || n || t.preventDefault()
  }, o.hasDragStarted = function (t) {
    return Math.abs(t.x) > this.options.dragThreshold
  }, o.pointerUp = function (t, e) {
    delete this.isTouchScrolling, this.viewport.classList.remove("is-pointer-down"), this.dispatchEvent("pointerUp", t, [e]), this._dragPointerUp(t, e)
  }, o.pointerDone = function () {
    t.removeEventListener("scroll", this), delete this.pointerDownScroll
  }, o.dragStart = function (e, i) {
    this.isDraggable && (this.dragStartPosition = this.x, this.startAnimation(), t.removeEventListener("scroll", this), this.dispatchEvent("dragStart", e, [i]))
  }, o.pointerMove = function (t, e) {
    var i = this._dragPointerMove(t, e);
    this.dispatchEvent("pointerMove", t, [e, i]), this._dragMove(t, e, i)
  }, o.dragMove = function (t, e, i) {
    if (this.isDraggable) {
      t.preventDefault(), this.previousDragX = this.dragX;
      var n = this.options.rightToLeft ? -1 : 1;
      this.options.wrapAround && (i.x = i.x % this.slideableWidth);
      var s = this.dragStartPosition + i.x * n;
      if (!this.options.wrapAround && this.slides.length) {
        var o = Math.max(-this.slides[0].target, this.dragStartPosition);
        s = s > o ? .5 * (s + o) : s;
        var r = Math.min(-this.getLastSlide().target, this.dragStartPosition);
        s = s < r ? .5 * (s + r) : s
      }
      this.dragX = s, this.dragMoveTime = new Date,
        this.dispatchEvent("dragMove", t, [e, i])
    }
  }, o.dragEnd = function (t, e) {
    if (this.isDraggable) {
      this.options.freeScroll && (this.isFreeScrolling = !0);
      var i = this.dragEndRestingSelect();
      if (this.options.freeScroll && !this.options.wrapAround) {
        var n = this.getRestingPosition();
        this.isFreeScrolling = -n > this.slides[0].target && -n < this.getLastSlide().target
      } else this.options.freeScroll || i != this.selectedIndex || (i += this.dragEndBoostSelect());
      delete this.previousDragX, this.isDragSelect = this.options.wrapAround, this.select(i), delete this.isDragSelect, this.dispatchEvent("dragEnd", t, [e])
    }
  }, o.dragEndRestingSelect = function () {
    var t = this.getRestingPosition(),
      e = Math.abs(this.getSlideDistance(-t, this.selectedIndex)),
      i = this._getClosestResting(t, e, 1),
      n = this._getClosestResting(t, e, -1),
      s = i.distance < n.distance ? i.index : n.index;
    return s
  }, o._getClosestResting = function (t, e, i) {
    for (var n = this.selectedIndex, s = 1 / 0, o = this.options.contain && !this.options.wrapAround ? function (t, e) {
        return t <= e
      } : function (t, e) {
        return t < e
      }; o(e, s) && (n += i, s = e, e = this.getSlideDistance(-t, n), null !== e);) e = Math.abs(e);
    return {
      distance: s,
      index: n - i
    }
  }, o.getSlideDistance = function (t, e) {
    var i = this.slides.length,
      s = this.options.wrapAround && i > 1,
      o = s ? n.modulo(e, i) : e,
      r = this.slides[o];
    if (!r) return null;
    var a = s ? this.slideableWidth * Math.floor(e / i) : 0;
    return t - (r.target + a)
  }, o.dragEndBoostSelect = function () {
    if (void 0 === this.previousDragX || !this.dragMoveTime || new Date - this.dragMoveTime > 100) return 0;
    var t = this.getSlideDistance(-this.dragX, this.selectedIndex),
      e = this.previousDragX - this.dragX;
    return t > 0 && e > 0 ? 1 : t < 0 && e < 0 ? -1 : 0
  }, o.staticClick = function (t, e) {
    var i = this.getParentCell(t.target),
      n = i && i.element,
      s = i && this.cells.indexOf(i);
    this.dispatchEvent("staticClick", t, [e, n, s])
  }, o.onscroll = function () {
    var t = s(),
      e = this.pointerDownScroll.x - t.x,
      i = this.pointerDownScroll.y - t.y;
    (Math.abs(e) > 3 || Math.abs(i) > 3) && this._pointerDone()
  }, e
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("tap-listener/tap-listener", ["unipointer/unipointer"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("unipointer")) : t.TapListener = e(t, t.Unipointer)
}(window, function (t, e) {
  function i(t) {
    this.bindTap(t)
  }
  var n = i.prototype = Object.create(e.prototype);
  return n.bindTap = function (t) {
    t && (this.unbindTap(), this.tapElement = t, this._bindStartEvent(t, !0))
  }, n.unbindTap = function () {
    this.tapElement && (this._bindStartEvent(this.tapElement, !0), delete this.tapElement)
  }, n.pointerUp = function (i, n) {
    if (!this.isIgnoringMouseUp || "mouseup" != i.type) {
      var s = e.getPointerPoint(n),
        o = this.tapElement.getBoundingClientRect(),
        r = t.pageXOffset,
        a = t.pageYOffset,
        l = s.x >= o.left + r && s.x <= o.right + r && s.y >= o.top + a && s.y <= o.bottom + a;
      if (l && this.emitEvent("tap", [i, n]), "mouseup" != i.type) {
        this.isIgnoringMouseUp = !0;
        var h = this;
        setTimeout(function () {
          delete h.isIgnoringMouseUp
        }, 400)
      }
    }
  }, n.destroy = function () {
    this.pointerDone(), this.unbindTap()
  }, i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/prev-next-button", ["./flickity", "tap-listener/tap-listener", "fizzy-ui-utils/utils"], function (i, n, s) {
    return e(t, i, n, s)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("./flickity"), require("tap-listener"), require("fizzy-ui-utils")) : e(t, t.Flickity, t.TapListener, t.fizzyUIUtils)
}(window, function (t, e, i, n) {
  "use strict";

  function s(t, e) {
    this.direction = t, this.parent = e, this._create()
  }

  function o(t) {
    return "string" == typeof t ? t : "M " + t.x0 + ",50 L " + t.x1 + "," + (t.y1 + 50) + " L " + t.x2 + "," + (t.y2 + 50) + " L " + t.x3 + ",50  L " + t.x2 + "," + (50 - t.y2) + " L " + t.x1 + "," + (50 - t.y1) + " Z"
  }
  var r = "http://www.w3.org/2000/svg";
  s.prototype = Object.create(i.prototype), s.prototype._create = function () {
    this.isEnabled = !0, this.isPrevious = this.direction == -1;
    var t = this.parent.options.rightToLeft ? 1 : -1;
    this.isLeft = this.direction == t;
    var e = this.element = document.createElement("button");
    e.className = "flickity-button flickity-prev-next-button", e.className += this.isPrevious ? " previous" : " next", e.setAttribute("type", "button"), this.disable(), e.setAttribute("aria-label", this.isPrevious ? "Previous" : "Next");
    var i = this.createSVG();
    e.appendChild(i), this.on("tap", this.onTap), this.parent.on("select", this.update.bind(this)), this.on("pointerDown", this.parent.childUIPointerDown.bind(this.parent))
  }, s.prototype.activate = function () {
    this.bindTap(this.element), this.element.addEventListener("click", this), this.parent.element.appendChild(this.element)
  }, s.prototype.deactivate = function () {
    this.parent.element.removeChild(this.element), i.prototype.destroy.call(this), this.element.removeEventListener("click", this)
  }, s.prototype.createSVG = function () {
    var t = document.createElementNS(r, "svg");
    t.setAttribute("class", "flickity-button-icon"), t.setAttribute("viewBox", "0 0 100 100");
    var e = document.createElementNS(r, "path"),
      i = o(this.parent.options.arrowShape);
    return e.setAttribute("d", i), e.setAttribute("class", "arrow"), this.isLeft || e.setAttribute("transform", "translate(100, 100) rotate(180) "), t.appendChild(e), t
  }, s.prototype.onTap = function () {
    if (this.isEnabled) {
      this.parent.uiChange();
      var t = this.isPrevious ? "previous" : "next";
      this.parent[t]()
    }
  }, s.prototype.handleEvent = n.handleEvent, s.prototype.onclick = function (t) {
    var e = document.activeElement;
    e && e == this.element && this.onTap(t, t)
  }, s.prototype.enable = function () {
    this.isEnabled || (this.element.disabled = !1, this.isEnabled = !0)
  }, s.prototype.disable = function () {
    this.isEnabled && (this.element.disabled = !0, this.isEnabled = !1)
  }, s.prototype.update = function () {
    var t = this.parent.slides;
    if (this.parent.options.wrapAround && t.length > 1) return void this.enable();
    var e = t.length ? t.length - 1 : 0,
      i = this.isPrevious ? 0 : e,
      n = this.parent.selectedIndex == i ? "disable" : "enable";
    this[n]()
  }, s.prototype.destroy = function () {
    this.deactivate()
  }, n.extend(e.defaults, {
    prevNextButtons: !0,
    arrowShape: {
      x0: 10,
      x1: 60,
      y1: 50,
      x2: 70,
      y2: 40,
      x3: 30
    }
  }), e.createMethods.push("_createPrevNextButtons");
  var a = e.prototype;
  return a._createPrevNextButtons = function () {
    this.options.prevNextButtons && (this.prevButton = new s((-1), this), this.nextButton = new s(1, this), this.on("activate", this.activatePrevNextButtons))
  }, a.activatePrevNextButtons = function () {
    this.prevButton.activate(), this.nextButton.activate(), this.on("deactivate", this.deactivatePrevNextButtons)
  }, a.deactivatePrevNextButtons = function () {
    this.prevButton.deactivate(), this.nextButton.deactivate(), this.off("deactivate", this.deactivatePrevNextButtons)
  }, e.PrevNextButton = s, e
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/page-dots", ["./flickity", "tap-listener/tap-listener", "fizzy-ui-utils/utils"], function (i, n, s) {
    return e(t, i, n, s)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("./flickity"), require("tap-listener"), require("fizzy-ui-utils")) : e(t, t.Flickity, t.TapListener, t.fizzyUIUtils)
}(window, function (t, e, i, n) {
  function s(t) {
    this.parent = t, this._create()
  }
  s.prototype = new i, s.prototype._create = function () {
    this.holder = document.createElement("ol"), this.holder.className = "flickity-page-dots", this.dots = [], this.on("tap", this.onTap), this.on("pointerDown", this.parent.childUIPointerDown.bind(this.parent))
  }, s.prototype.activate = function () {
    this.setDots(), this.bindTap(this.holder), this.parent.element.appendChild(this.holder)
  }, s.prototype.deactivate = function () {
    this.parent.element.removeChild(this.holder), i.prototype.destroy.call(this)
  }, s.prototype.setDots = function () {
    var t = this.parent.slides.length - this.dots.length;
    t > 0 ? this.addDots(t) : t < 0 && this.removeDots(-t)
  }, s.prototype.addDots = function (t) {
    for (var e = document.createDocumentFragment(), i = [], n = this.dots.length, s = n + t, o = n; o < s; o++) {
      var r = document.createElement("li");
      r.className = "dot", r.setAttribute("aria-label", "Page dot " + (o + 1)), e.appendChild(r), i.push(r)
    }
    this.holder.appendChild(e), this.dots = this.dots.concat(i)
  }, s.prototype.removeDots = function (t) {
    var e = this.dots.splice(this.dots.length - t, t);
    e.forEach(function (t) {
      this.holder.removeChild(t)
    }, this)
  }, s.prototype.updateSelected = function () {
    this.selectedDot && (this.selectedDot.className = "dot", this.selectedDot.removeAttribute("aria-current")), this.dots.length && (this.selectedDot = this.dots[this.parent.selectedIndex], this.selectedDot.className = "dot is-selected", this.selectedDot.setAttribute("aria-current", "step"))
  }, s.prototype.onTap = function (t) {
    var e = t.target;
    if ("LI" == e.nodeName) {
      this.parent.uiChange();
      var i = this.dots.indexOf(e);
      this.parent.select(i)
    }
  }, s.prototype.destroy = function () {
    this.deactivate()
  }, e.PageDots = s, n.extend(e.defaults, {
    pageDots: !0
  }), e.createMethods.push("_createPageDots");
  var o = e.prototype;
  return o._createPageDots = function () {
    this.options.pageDots && (this.pageDots = new s(this), this.on("activate", this.activatePageDots), this.on("select", this.updateSelectedPageDots), this.on("cellChange", this.updatePageDots), this.on("resize", this.updatePageDots), this.on("deactivate", this.deactivatePageDots))
  }, o.activatePageDots = function () {
    this.pageDots.activate()
  }, o.updateSelectedPageDots = function () {
    this.pageDots.updateSelected()
  }, o.updatePageDots = function () {
    this.pageDots.setDots()
  }, o.deactivatePageDots = function () {
    this.pageDots.deactivate()
  }, e.PageDots = s, e
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/player", ["ev-emitter/ev-emitter", "fizzy-ui-utils/utils", "./flickity"], function (t, i, n) {
    return e(t, i, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(require("ev-emitter"), require("fizzy-ui-utils"), require("./flickity")) : e(t.EvEmitter, t.fizzyUIUtils, t.Flickity)
}(window, function (t, e, i) {
  function n(t) {
    this.parent = t, this.state = "stopped", this.onVisibilityChange = this.visibilityChange.bind(this), this.onVisibilityPlay = this.visibilityPlay.bind(this)
  }
  n.prototype = Object.create(t.prototype), n.prototype.play = function () {
    if ("playing" != this.state) {
      var t = document.hidden;
      if (t) return void document.addEventListener("visibilitychange", this.onVisibilityPlay);
      this.state = "playing", document.addEventListener("visibilitychange", this.onVisibilityChange), this.tick()
    }
  }, n.prototype.tick = function () {
    if ("playing" == this.state) {
      var t = this.parent.options.autoPlay;
      t = "number" == typeof t ? t : 3e3;
      var e = this;
      this.clear(), this.timeout = setTimeout(function () {
        e.parent.next(!0), e.tick()
      }, t)
    }
  }, n.prototype.stop = function () {
    this.state = "stopped", this.clear(), document.removeEventListener("visibilitychange", this.onVisibilityChange)
  }, n.prototype.clear = function () {
    clearTimeout(this.timeout)
  }, n.prototype.pause = function () {
    "playing" == this.state && (this.state = "paused", this.clear())
  }, n.prototype.unpause = function () {
    "paused" == this.state && this.play()
  }, n.prototype.visibilityChange = function () {
    var t = document.hidden;
    this[t ? "pause" : "unpause"]()
  }, n.prototype.visibilityPlay = function () {
    this.play(), document.removeEventListener("visibilitychange", this.onVisibilityPlay)
  }, e.extend(i.defaults, {
    pauseAutoPlayOnHover: !0
  }), i.createMethods.push("_createPlayer");
  var s = i.prototype;
  return s._createPlayer = function () {
    this.player = new n(this), this.on("activate", this.activatePlayer), this.on("uiChange", this.stopPlayer), this.on("pointerDown", this.stopPlayer), this.on("deactivate", this.deactivatePlayer)
  }, s.activatePlayer = function () {
    this.options.autoPlay && (this.player.play(), this.element.addEventListener("mouseenter", this))
  }, s.playPlayer = function () {
    this.player.play()
  }, s.stopPlayer = function () {
    this.player.stop()
  }, s.pausePlayer = function () {
    this.player.pause()
  }, s.unpausePlayer = function () {
    this.player.unpause()
  }, s.deactivatePlayer = function () {
    this.player.stop(), this.element.removeEventListener("mouseenter", this)
  }, s.onmouseenter = function () {
    this.options.pauseAutoPlayOnHover && (this.player.pause(), this.element.addEventListener("mouseleave", this))
  }, s.onmouseleave = function () {
    this.player.unpause(), this.element.removeEventListener("mouseleave", this)
  }, i.Player = n, i
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/add-remove-cell", ["./flickity", "fizzy-ui-utils/utils"], function (i, n) {
    return e(t, i, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("./flickity"), require("fizzy-ui-utils")) : e(t, t.Flickity, t.fizzyUIUtils)
}(window, function (t, e, i) {
  function n(t) {
    var e = document.createDocumentFragment();
    return t.forEach(function (t) {
      e.appendChild(t.element)
    }), e
  }
  var s = e.prototype;
  return s.insert = function (t, e) {
    var i = this._makeCells(t);
    if (i && i.length) {
      var s = this.cells.length;
      e = void 0 === e ? s : e;
      var o = n(i),
        r = e == s;
      if (r) this.slider.appendChild(o);
      else {
        var a = this.cells[e].element;
        this.slider.insertBefore(o, a)
      }
      if (0 === e) this.cells = i.concat(this.cells);
      else if (r) this.cells = this.cells.concat(i);
      else {
        var l = this.cells.splice(e, s - e);
        this.cells = this.cells.concat(i).concat(l)
      }
      this._sizeCells(i), this.cellChange(e, !0)
    }
  }, s.append = function (t) {
    this.insert(t, this.cells.length)
  }, s.prepend = function (t) {
    this.insert(t, 0)
  }, s.remove = function (t) {
    var e = this.getCells(t);
    if (e && e.length) {
      var n = this.cells.length - 1;
      e.forEach(function (t) {
        t.remove();
        var e = this.cells.indexOf(t);
        n = Math.min(e, n), i.removeFrom(this.cells, t)
      }, this), this.cellChange(n, !0)
    }
  }, s.cellSizeChange = function (t) {
    var e = this.getCell(t);
    if (e) {
      e.getSize();
      var i = this.cells.indexOf(e);
      this.cellChange(i)
    }
  }, s.cellChange = function (t, e) {
    var i = this.selectedElement;
    this._positionCells(t), this._getWrapShiftCells(), this.setGallerySize();
    var n = this.getCell(i);
    n && (this.selectedIndex = this.getCellSlideIndex(n)), this.selectedIndex = Math.min(this.slides.length - 1, this.selectedIndex), this.emitEvent("cellChange", [t]), this.select(this.selectedIndex), e && this.positionSliderAtSelected()
  }, e
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/lazyload", ["./flickity", "fizzy-ui-utils/utils"], function (i, n) {
    return e(t, i, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("./flickity"), require("fizzy-ui-utils")) : e(t, t.Flickity, t.fizzyUIUtils)
}(window, function (t, e, i) {
  "use strict";

  function n(t) {
    if ("IMG" == t.nodeName) {
      var e = t.getAttribute("data-flickity-lazyload"),
        n = t.getAttribute("data-flickity-lazyload-src"),
        s = t.getAttribute("data-flickity-lazyload-srcset");
      if (e || n || s) return [t]
    }
    var o = "img[data-flickity-lazyload], img[data-flickity-lazyload-src], img[data-flickity-lazyload-srcset]",
      r = t.querySelectorAll(o);
    return i.makeArray(r)
  }

  function s(t, e) {
    this.img = t, this.flickity = e, this.load()
  }
  e.createMethods.push("_createLazyload");
  var o = e.prototype;
  return o._createLazyload = function () {
    this.on("select", this.lazyLoad)
  }, o.lazyLoad = function () {
    var t = this.options.lazyLoad;
    if (t) {
      var e = "number" == typeof t ? t : 0,
        i = this.getAdjacentCellElements(e),
        o = [];
      i.forEach(function (t) {
        var e = n(t);
        o = o.concat(e)
      }), o.forEach(function (t) {
        new s(t, this)
      }, this)
    }
  }, s.prototype.handleEvent = i.handleEvent, s.prototype.load = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this);
    var t = this.img.getAttribute("data-flickity-lazyload") || this.img.getAttribute("data-flickity-lazyload-src"),
      e = this.img.getAttribute("data-flickity-lazyload-srcset");
    this.img.src = t, e && this.img.setAttribute("srcset", e), this.img.removeAttribute("data-flickity-lazyload"), this.img.removeAttribute("data-flickity-lazyload-src"), this.img.removeAttribute("data-flickity-lazyload-srcset")
  }, s.prototype.onload = function (t) {
    this.complete(t, "flickity-lazyloaded")
  }, s.prototype.onerror = function (t) {
    this.complete(t, "flickity-lazyerror")
  }, s.prototype.complete = function (t, e) {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
    var i = this.flickity.getParentCell(this.img),
      n = i && i.element;
    this.flickity.cellSizeChange(n), this.img.classList.add(e), this.flickity.dispatchEvent("lazyLoad", t, n)
  }, e.LazyLoader = s, e
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity/js/index", ["./flickity", "./drag", "./prev-next-button", "./page-dots", "./player", "./add-remove-cell", "./lazyload"], e) : "object" == typeof module && module.exports && (module.exports = e(require("./flickity"), require("./drag"), require("./prev-next-button"), require("./page-dots"), require("./player"), require("./add-remove-cell"), require("./lazyload")))
}(window, function (t) {
  return t
}),
function (t, e) {
  "function" == typeof define && define.amd ? define("flickity-as-nav-for/as-nav-for", ["flickity/js/index", "fizzy-ui-utils/utils"], e) : "object" == typeof module && module.exports ? module.exports = e(require("flickity"), require("fizzy-ui-utils")) : t.Flickity = e(t.Flickity, t.fizzyUIUtils)
}(window, function (t, e) {
  function i(t, e, i) {
    return (e - t) * i + t
  }
  t.createMethods.push("_createAsNavFor");
  var n = t.prototype;
  return n._createAsNavFor = function () {
    this.on("activate", this.activateAsNavFor), this.on("deactivate", this.deactivateAsNavFor), this.on("destroy", this.destroyAsNavFor);
    var t = this.options.asNavFor;
    if (t) {
      var e = this;
      setTimeout(function () {
        e.setNavCompanion(t)
      })
    }
  }, n.setNavCompanion = function (i) {
    i = e.getQueryElement(i);
    var n = t.data(i);
    if (n && n != this) {
      this.navCompanion = n;
      var s = this;
      this.onNavCompanionSelect = function () {
        s.navCompanionSelect()
      }, n.on("select", this.onNavCompanionSelect), this.on("staticClick", this.onNavStaticClick), this.navCompanionSelect(!0)
    }
  }, n.navCompanionSelect = function (t) {
    if (this.navCompanion) {
      var e = this.navCompanion.selectedCells[0],
        n = this.navCompanion.cells.indexOf(e),
        s = n + this.navCompanion.selectedCells.length - 1,
        o = Math.floor(i(n, s, this.navCompanion.cellAlign));
      if (this.selectCell(o, !1, t), this.removeNavSelectedElements(), !(o >= this.cells.length)) {
        var r = this.cells.slice(n, s + 1);
        this.navSelectedElements = r.map(function (t) {
          return t.element
        }), this.changeNavSelectedClass("add")
      }
    }
  }, n.changeNavSelectedClass = function (t) {
    this.navSelectedElements.forEach(function (e) {
      e.classList[t]("is-nav-selected")
    })
  }, n.activateAsNavFor = function () {
    this.navCompanionSelect(!0)
  }, n.removeNavSelectedElements = function () {
    this.navSelectedElements && (this.changeNavSelectedClass("remove"), delete this.navSelectedElements)
  }, n.onNavStaticClick = function (t, e, i, n) {
    "number" == typeof n && this.navCompanion.selectCell(n)
  }, n.deactivateAsNavFor = function () {
    this.removeNavSelectedElements()
  }, n.destroyAsNavFor = function () {
    this.navCompanion && (this.navCompanion.off("select", this.onNavCompanionSelect), this.off("staticClick", this.onNavStaticClick), delete this.navCompanion)
  }, t
}),
function (t, e) {
  "use strict";
  "function" == typeof define && define.amd ? define("imagesloaded/imagesloaded", ["ev-emitter/ev-emitter"], function (i) {
    return e(t, i)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.imagesLoaded = e(t, t.EvEmitter)
}("undefined" != typeof window ? window : this, function (t, e) {
  function i(t, e) {
    for (var i in e) t[i] = e[i];
    return t
  }

  function n(t) {
    if (Array.isArray(t)) return t;
    var e = "object" == typeof t && "number" == typeof t.length;
    return e ? h.call(t) : [t]
  }

  function s(t, e, o) {
    if (!(this instanceof s)) return new s(t, e, o);
    var r = t;
    return "string" == typeof t && (r = document.querySelectorAll(t)), r ? (this.elements = n(r), this.options = i({}, this.options), "function" == typeof e ? o = e : i(this.options, e), o && this.on("always", o), this.getImages(), a && (this.jqDeferred = new a.Deferred), void setTimeout(this.check.bind(this))) : void l.error("Bad element for imagesLoaded " + (r || t))
  }

  function o(t) {
    this.img = t
  }

  function r(t, e) {
    this.url = t, this.element = e, this.img = new Image
  }
  var a = t.jQuery,
    l = t.console,
    h = Array.prototype.slice;
  s.prototype = Object.create(e.prototype), s.prototype.options = {}, s.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this)
  }, s.prototype.addElementImages = function (t) {
    "IMG" == t.nodeName && this.addImage(t), this.options.background === !0 && this.addElementBackgroundImages(t);
    var e = t.nodeType;
    if (e && c[e]) {
      for (var i = t.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var s = i[n];
        this.addImage(s)
      }
      if ("string" == typeof this.options.background) {
        var o = t.querySelectorAll(this.options.background);
        for (n = 0; n < o.length; n++) {
          var r = o[n];
          this.addElementBackgroundImages(r)
        }
      }
    }
  };
  var c = {
    1: !0,
    9: !0,
    11: !0
  };
  return s.prototype.addElementBackgroundImages = function (t) {
    var e = getComputedStyle(t);
    if (e)
      for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(e.backgroundImage); null !== n;) {
        var s = n && n[2];
        s && this.addBackground(s, t), n = i.exec(e.backgroundImage)
      }
  }, s.prototype.addImage = function (t) {
    var e = new o(t);
    this.images.push(e)
  }, s.prototype.addBackground = function (t, e) {
    var i = new r(t, e);
    this.images.push(i)
  }, s.prototype.check = function () {
    function t(t, i, n) {
      setTimeout(function () {
        e.progress(t, i, n)
      })
    }
    var e = this;
    return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (e) {
      e.once("progress", t), e.check()
    }) : void this.complete()
  }, s.prototype.progress = function (t, e, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && l && l.log("progress: " + i, t, e)
  }, s.prototype.complete = function () {
    var t = this.hasAnyBroken ? "fail" : "done";
    if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var e = this.hasAnyBroken ? "reject" : "resolve";
      this.jqDeferred[e](this)
    }
  }, o.prototype = Object.create(e.prototype), o.prototype.check = function () {
    var t = this.getIsImageComplete();
    return t ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
  }, o.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth
  }, o.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.img, e])
  }, o.prototype.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t)
  }, o.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents()
  }, o.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents()
  }, o.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype = Object.create(o.prototype), r.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
    var t = this.getIsImageComplete();
    t && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
  }, r.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
  }, r.prototype.confirm = function (t, e) {
    this.isLoaded = t, this.emitEvent("progress", [this, this.element, e])
  }, s.makeJQueryPlugin = function (e) {
    e = e || t.jQuery, e && (a = e, a.fn.imagesLoaded = function (t, e) {
      var i = new s(this, t, e);
      return i.jqDeferred.promise(a(this))
    })
  }, s.makeJQueryPlugin(), s
}),
function (t, e) {
  "function" == typeof define && define.amd ? define(["flickity/js/index", "imagesloaded/imagesloaded"], function (i, n) {
    return e(t, i, n)
  }) : "object" == typeof module && module.exports ? module.exports = e(t, require("flickity"), require("imagesloaded")) : t.Flickity = e(t, t.Flickity, t.imagesLoaded)
}(window, function (t, e, i) {
  "use strict";
  e.createMethods.push("_createImagesLoaded");
  var n = e.prototype;
  return n._createImagesLoaded = function () {
    this.on("activate", this.imagesLoaded)
  }, n.imagesLoaded = function () {
    function t(t, i) {
      var n = e.getParentCell(i.img);
      e.cellSizeChange(n && n.element), e.options.freeScroll || e.positionSliderAtSelected()
    }
    if (this.options.imagesLoaded) {
      var e = this;
      i(this.slider).on("progress", t)
    }
  }, e
});

/*!
 * VERSION: 0.5.6
 * DATE: 2017-01-17
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * SplitText is a Club GreenSock membership benefit; You must have a valid membership to use
 * this code without violating the terms of use. Visit http://greensock.com/club/ to sign up or get more details.
 * This work is subject to the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
! function (a) {
  "use strict";
  var b = a.GreenSockGlobals || a,
    c = function (a) {
      var c, d = a.split("."),
        e = b;
      for (c = 0; c < d.length; c++) e[d[c]] = e = e[d[c]] || {};
      return e
    },
    d = c("com.greensock.utils"),
    e = function (a) {
      var b = a.nodeType,
        c = "";
      if (1 === b || 9 === b || 11 === b) {
        if ("string" == typeof a.textContent) return a.textContent;
        for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
      } else if (3 === b || 4 === b) return a.nodeValue;
      return c
    },
    f = document,
    g = f.defaultView ? f.defaultView.getComputedStyle : function () {},
    h = /([A-Z])/g,
    i = function (a, b, c, d) {
      var e;
      return (c = c || g(a, null)) ? (a = c.getPropertyValue(b.replace(h, "-$1").toLowerCase()), e = a || c.length ? a : c[b]) : a.currentStyle && (c = a.currentStyle, e = c[b]), d ? e : parseInt(e, 10) || 0
    },
    j = function (a) {
      return a.length && a[0] && (a[0].nodeType && a[0].style && !a.nodeType || a[0].length && a[0][0]) ? !0 : !1
    },
    k = function (a) {
      var b, c, d, e = [],
        f = a.length;
      for (b = 0; f > b; b++)
        if (c = a[b], j(c))
          for (d = c.length, d = 0; d < c.length; d++) e.push(c[d]);
        else e.push(c);
      return e
    },
    l = /(?:\r|\n|\t\t)/g,
    m = /(?:\s\s+)/g,
    n = 55296,
    o = 56319,
    p = 56320,
    q = 127462,
    r = 127487,
    s = 127995,
    t = 127999,
    u = function (a) {
      return (a.charCodeAt(0) - n << 10) + (a.charCodeAt(1) - p) + 65536
    },
    v = f.all && !f.addEventListener,
    w = " style='position:relative;display:inline-block;" + (v ? "*display:inline;*zoom:1;'" : "'"),
    x = function (a, b) {
      a = a || "";
      var c = -1 !== a.indexOf("++"),
        d = 1;
      return c && (a = a.split("++").join("")),
        function () {
          return "<" + b + w + (a ? " class='" + a + (c ? d++ : "") + "'>" : ">")
        }
    },
    y = d.SplitText = b.SplitText = function (a, b) {
      if ("string" == typeof a && (a = y.selector(a)), !a) throw "cannot split a null element.";
      this.elements = j(a) ? k(a) : [a], this.chars = [], this.words = [], this.lines = [], this._originals = [], this.vars = b || {}, this.split(b)
    },
    z = function (a, b, c) {
      var d = a.nodeType;
      if (1 === d || 9 === d || 11 === d)
        for (a = a.firstChild; a; a = a.nextSibling) z(a, b, c);
      else(3 === d || 4 === d) && (a.nodeValue = a.nodeValue.split(b).join(c))
    },
    A = function (a, b) {
      for (var c = b.length; --c > -1;) a.push(b[c])
    },
    B = function (a) {
      var b, c = [],
        d = a.length;
      for (b = 0; b !== d; c.push(a[b++]));
      return c
    },
    C = function (a, b, c) {
      for (var d; a && a !== b;) {
        if (d = a._next || a.nextSibling) return d.textContent.charAt(0) === c;
        a = a.parentNode || a._parent
      }
      return !1
    },
    D = function (a) {
      var b, c, d = B(a.childNodes),
        e = d.length;
      for (b = 0; e > b; b++) c = d[b], c._isSplit ? D(c) : (b && 3 === c.previousSibling.nodeType ? c.previousSibling.nodeValue += 3 === c.nodeType ? c.nodeValue : c.firstChild.nodeValue : 3 !== c.nodeType && a.insertBefore(c.firstChild, c), a.removeChild(c))
    },
    E = function (a, b, c, d, e, h, j) {
      var k, l, m, n, o, p, q, r, s, t, u, v, w = g(a),
        x = i(a, "paddingLeft", w),
        y = -999,
        B = i(a, "borderBottomWidth", w) + i(a, "borderTopWidth", w),
        E = i(a, "borderLeftWidth", w) + i(a, "borderRightWidth", w),
        F = i(a, "paddingTop", w) + i(a, "paddingBottom", w),
        G = i(a, "paddingLeft", w) + i(a, "paddingRight", w),
        H = .2 * i(a, "fontSize"),
        I = i(a, "textAlign", w, !0),
        J = [],
        K = [],
        L = [],
        M = b.wordDelimiter || " ",
        N = b.span ? "span" : "div",
        O = b.type || b.split || "chars,words,lines",
        P = e && -1 !== O.indexOf("lines") ? [] : null,
        Q = -1 !== O.indexOf("words"),
        R = -1 !== O.indexOf("chars"),
        S = "absolute" === b.position || b.absolute === !0,
        T = b.linesClass,
        U = -1 !== (T || "").indexOf("++"),
        V = [];
      for (P && 1 === a.children.length && a.children[0]._isSplit && (a = a.children[0]), U && (T = T.split("++").join("")), l = a.getElementsByTagName("*"), m = l.length, o = [], k = 0; m > k; k++) o[k] = l[k];
      if (P || S)
        for (k = 0; m > k; k++) n = o[k], p = n.parentNode === a, (p || S || R && !Q) && (v = n.offsetTop, P && p && Math.abs(v - y) > H && "BR" !== n.nodeName && (q = [], P.push(q), y = v), S && (n._x = n.offsetLeft, n._y = v, n._w = n.offsetWidth, n._h = n.offsetHeight), P && ((n._isSplit && p || !R && p || Q && p || !Q && n.parentNode.parentNode === a && !n.parentNode._isSplit) && (q.push(n), n._x -= x, C(n, a, M) && (n._wordEnd = !0)), "BR" === n.nodeName && n.nextSibling && "BR" === n.nextSibling.nodeName && P.push([])));
      for (k = 0; m > k; k++) n = o[k], p = n.parentNode === a, "BR" !== n.nodeName ? (S && (s = n.style, Q || p || (n._x += n.parentNode._x, n._y += n.parentNode._y), s.left = n._x + "px", s.top = n._y + "px", s.position = "absolute", s.display = "block", s.width = n._w + 1 + "px", s.height = n._h + "px"), !Q && R ? n._isSplit ? (n._next = n.nextSibling, n.parentNode.appendChild(n)) : n.parentNode._isSplit ? (n._parent = n.parentNode, !n.previousSibling && n.firstChild && (n.firstChild._isFirst = !0), n.nextSibling && " " === n.nextSibling.textContent && !n.nextSibling.nextSibling && V.push(n.nextSibling), n._next = n.nextSibling && n.nextSibling._isFirst ? null : n.nextSibling, n.parentNode.removeChild(n), o.splice(k--, 1), m--) : p || (v = !n.nextSibling && C(n.parentNode, a, M), n.parentNode._parent && n.parentNode._parent.appendChild(n), v && n.parentNode.appendChild(f.createTextNode(" ")), b.span && (n.style.display = "inline"), J.push(n)) : n.parentNode._isSplit && !n._isSplit && "" !== n.innerHTML ? K.push(n) : R && !n._isSplit && (b.span && (n.style.display = "inline"), J.push(n))) : P || S ? (n.parentNode && n.parentNode.removeChild(n), o.splice(k--, 1), m--) : Q || a.appendChild(n);
      for (k = V.length; --k > -1;) V[k].parentNode.removeChild(V[k]);
      if (P) {
        for (S && (t = f.createElement(N), a.appendChild(t), u = t.offsetWidth + "px", v = t.offsetParent === a ? 0 : a.offsetLeft, a.removeChild(t)), s = a.style.cssText, a.style.cssText = "display:none;"; a.firstChild;) a.removeChild(a.firstChild);
        for (r = " " === M && (!S || !Q && !R), k = 0; k < P.length; k++) {
          for (q = P[k], t = f.createElement(N), t.style.cssText = "display:block;text-align:" + I + ";position:" + (S ? "absolute;" : "relative;"), T && (t.className = T + (U ? k + 1 : "")), L.push(t), m = q.length, l = 0; m > l; l++) "BR" !== q[l].nodeName && (n = q[l], t.appendChild(n), r && n._wordEnd && t.appendChild(f.createTextNode(" ")), S && (0 === l && (t.style.top = n._y + "px", t.style.left = x + v + "px"), n.style.top = "0px", v && (n.style.left = n._x - v + "px")));
          0 === m ? t.innerHTML = "&nbsp;" : Q || R || (D(t), z(t, String.fromCharCode(160), " ")), S && (t.style.width = u, t.style.height = n._h + "px"), a.appendChild(t)
        }
        a.style.cssText = s
      }
      S && (j > a.clientHeight && (a.style.height = j - F + "px", a.clientHeight < j && (a.style.height = j + B + "px")), h > a.clientWidth && (a.style.width = h - G + "px", a.clientWidth < h && (a.style.width = h + E + "px"))), A(c, J), A(d, K), A(e, L)
    },
    F = function (a, b, c, d) {
      var g, h, i, j, k, p, v, w, x, y = b.span ? "span" : "div",
        A = b.type || b.split || "chars,words,lines",
        B = (-1 !== A.indexOf("words"), -1 !== A.indexOf("chars")),
        C = "absolute" === b.position || b.absolute === !0,
        D = b.wordDelimiter || " ",
        E = " " !== D ? "" : C ? "&#173; " : " ",
        F = b.span ? "</span>" : "</div>",
        G = !0,
        H = f.createElement("div"),
        I = a.parentNode;
      for (I.insertBefore(H, a), H.textContent = a.nodeValue, I.removeChild(a), a = H, g = e(a), v = -1 !== g.indexOf("<"), b.reduceWhiteSpace !== !1 && (g = g.replace(m, " ").replace(l, "")), v && (g = g.split("<").join("{{LT}}")), k = g.length, h = (" " === g.charAt(0) ? E : "") + c(), i = 0; k > i; i++)
        if (p = g.charAt(i), p === D && g.charAt(i - 1) !== D && i) {
          for (h += G ? F : "", G = !1; g.charAt(i + 1) === D;) h += E, i++;
          i === k - 1 ? h += E : ")" !== g.charAt(i + 1) && (h += E + c(), G = !0)
        } else "{" === p && "{{LT}}" === g.substr(i, 6) ? (h += B ? d() + "{{LT}}</" + y + ">" : "{{LT}}", i += 5) : p.charCodeAt(0) >= n && p.charCodeAt(0) <= o || g.charCodeAt(i + 1) >= 65024 && g.charCodeAt(i + 1) <= 65039 ? (w = u(g.substr(i, 2)), x = u(g.substr(i + 2, 2)), j = w >= q && r >= w && x >= q && r >= x || x >= s && t >= x ? 4 : 2, h += B && " " !== p ? d() + g.substr(i, j) + "</" + y + ">" : g.substr(i, j), i += j - 1) : h += B && " " !== p ? d() + p + "</" + y + ">" : p;
      a.outerHTML = h + (G ? F : ""), v && z(I, "{{LT}}", "<")
    },
    G = function (a, b, c, d) {
      var e, f, g = B(a.childNodes),
        h = g.length,
        j = "absolute" === b.position || b.absolute === !0;
      if (3 !== a.nodeType || h > 1) {
        for (b.absolute = !1, e = 0; h > e; e++) f = g[e], (3 !== f.nodeType || /\S+/.test(f.nodeValue)) && (j && 3 !== f.nodeType && "inline" === i(f, "display", null, !0) && (f.style.display = "inline-block", f.style.position = "relative"), f._isSplit = !0, G(f, b, c, d));
        return b.absolute = j, void(a._isSplit = !0)
      }
      F(a, b, c, d)
    },
    H = y.prototype;
  H.split = function (a) {
    this.isSplit && this.revert(), this.vars = a = a || this.vars, this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;
    for (var b, c, d, e = this.elements.length, f = a.span ? "span" : "div", g = ("absolute" === a.position || a.absolute === !0, x(a.wordsClass, f)), h = x(a.charsClass, f); --e > -1;) d = this.elements[e], this._originals[e] = d.innerHTML, b = d.clientHeight, c = d.clientWidth, G(d, a, g, h), E(d, a, this.chars, this.words, this.lines, c, b);
    return this.chars.reverse(), this.words.reverse(), this.lines.reverse(), this.isSplit = !0, this
  }, H.revert = function () {
    if (!this._originals) throw "revert() call wasn't scoped properly.";
    for (var a = this._originals.length; --a > -1;) this.elements[a].innerHTML = this._originals[a];
    return this.chars = [], this.words = [], this.lines = [], this.isSplit = !1, this
  }, y.selector = a.$ || a.jQuery || function (b) {
    var c = a.$ || a.jQuery;
    return c ? (y.selector = c, c(b)) : "undefined" == typeof document ? b : document.querySelectorAll ? document.querySelectorAll(b) : document.getElementById("#" === b.charAt(0) ? b.substr(1) : b)
  }, y.version = "0.5.6"
}(_gsScope),
function (a) {
  "use strict";
  var b = function () {
    return (_gsScope.GreenSockGlobals || _gsScope)[a]
  };
  "function" == typeof define && define.amd ? define([], b) : "undefined" != typeof module && module.exports && (module.exports = b())
}("SplitText");

/*! ScrollMagic v2.0.6 | (c) 2018 Jan Paepke (@janpaepke) | license & info: http://scrollmagic.io */
! function (e, t) {
  "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t() : e.ScrollMagic = t()
}(this, function () {
  "use strict";
  var e = function () {};
  e.version = "2.0.6", window.addEventListener("mousewheel", function () {});
  var t = "data-scrollmagic-pin-spacer";
  e.Controller = function (r) {
    var o, s, a = "ScrollMagic.Controller",
      l = "FORWARD",
      c = "REVERSE",
      f = "PAUSED",
      u = n.defaults,
      d = this,
      h = i.extend({}, u, r),
      g = [],
      p = !1,
      v = 0,
      m = f,
      w = !0,
      y = 0,
      S = !0,
      b = function () {
        for (var e in h) u.hasOwnProperty(e) || delete h[e];
        if (h.container = i.get.elements(h.container)[0], !h.container) throw a + " init failed.";
        w = h.container === window || h.container === document.body || !document.body.contains(h.container), w && (h.container = window), y = z(), h.container.addEventListener("resize", T), h.container.addEventListener("scroll", T);
        var t = parseInt(h.refreshInterval, 10);
        h.refreshInterval = i.type.Number(t) ? t : u.refreshInterval, E()
      },
      E = function () {
        h.refreshInterval > 0 && (s = window.setTimeout(A, h.refreshInterval))
      },
      x = function () {
        return h.vertical ? i.get.scrollTop(h.container) : i.get.scrollLeft(h.container)
      },
      z = function () {
        return h.vertical ? i.get.height(h.container) : i.get.width(h.container)
      },
      C = this._setScrollPos = function (e) {
        h.vertical ? w ? window.scrollTo(i.get.scrollLeft(), e) : h.container.scrollTop = e : w ? window.scrollTo(e, i.get.scrollTop()) : h.container.scrollLeft = e
      },
      F = function () {
        if (S && p) {
          var e = i.type.Array(p) ? p : g.slice(0);
          p = !1;
          var t = v;
          v = d.scrollPos();
          var n = v - t;
          0 !== n && (m = n > 0 ? l : c), m === c && e.reverse(), e.forEach(function (e) {
            e.update(!0)
          })
        }
      },
      L = function () {
        o = i.rAF(F)
      },
      T = function (e) {
        "resize" == e.type && (y = z(), m = f), p !== !0 && (p = !0, L())
      },
      A = function () {
        if (!w && y != z()) {
          var e;
          try {
            e = new Event("resize", {
              bubbles: !1,
              cancelable: !1
            })
          } catch (t) {
            e = document.createEvent("Event"), e.initEvent("resize", !1, !1)
          }
          h.container.dispatchEvent(e)
        }
        g.forEach(function (e) {
          e.refresh()
        }), E()
      };
    this._options = h;
    var N = function (e) {
      if (e.length <= 1) return e;
      var t = e.slice(0);
      return t.sort(function (e, t) {
        return e.scrollOffset() > t.scrollOffset() ? 1 : -1
      }), t
    };
    return this.addScene = function (t) {
      if (i.type.Array(t)) t.forEach(function (e) {
        d.addScene(e)
      });
      else if (t instanceof e.Scene)
        if (t.controller() !== d) t.addTo(d);
        else if (g.indexOf(t) < 0) {
        g.push(t), g = N(g), t.on("shift.controller_sort", function () {
          g = N(g)
        });
        for (var n in h.globalSceneOptions) t[n] && t[n].call(t, h.globalSceneOptions[n])
      }
      return d
    }, this.removeScene = function (e) {
      if (i.type.Array(e)) e.forEach(function (e) {
        d.removeScene(e)
      });
      else {
        var t = g.indexOf(e);
        t > -1 && (e.off("shift.controller_sort"), g.splice(t, 1), e.remove())
      }
      return d
    }, this.updateScene = function (t, n) {
      return i.type.Array(t) ? t.forEach(function (e) {
        d.updateScene(e, n)
      }) : n ? t.update(!0) : p !== !0 && t instanceof e.Scene && (p = p || [], -1 == p.indexOf(t) && p.push(t), p = N(p), L()), d
    }, this.update = function (e) {
      return T({
        type: "resize"
      }), e && F(), d
    }, this.scrollTo = function (n, r) {
      if (i.type.Number(n)) C.call(h.container, n, r);
      else if (n instanceof e.Scene) n.controller() === d && d.scrollTo(n.scrollOffset(), r);
      else if (i.type.Function(n)) C = n;
      else {
        var o = i.get.elements(n)[0];
        if (o) {
          for (; o.parentNode.hasAttribute(t);) o = o.parentNode;
          var s = h.vertical ? "top" : "left",
            a = i.get.offset(h.container),
            l = i.get.offset(o);
          w || (a[s] -= d.scrollPos()), d.scrollTo(l[s] - a[s], r)
        }
      }
      return d
    }, this.scrollPos = function (e) {
      return arguments.length ? (i.type.Function(e) && (x = e), d) : x.call(d)
    }, this.info = function (e) {
      var t = {
        size: y,
        vertical: h.vertical,
        scrollPos: v,
        scrollDirection: m,
        container: h.container,
        isDocument: w
      };
      return arguments.length ? void 0 !== t[e] ? t[e] : void 0 : t
    }, this.loglevel = function () {
      return d
    }, this.enabled = function (e) {
      return arguments.length ? (S != e && (S = !!e, d.updateScene(g, !0)), d) : S
    }, this.destroy = function (e) {
      window.clearTimeout(s);
      for (var t = g.length; t--;) g[t].destroy(e);
      return h.container.removeEventListener("resize", T), h.container.removeEventListener("scroll", T), i.cAF(o), null
    }, b(), d
  };
  var n = {
    defaults: {
      container: window,
      vertical: !0,
      globalSceneOptions: {},
      loglevel: 2,
      refreshInterval: 100
    }
  };
  e.Controller.addOption = function (e, t) {
    n.defaults[e] = t
  }, e.Controller.extend = function (t) {
    var n = this;
    e.Controller = function () {
      return n.apply(this, arguments), this.$super = i.extend({}, this), t.apply(this, arguments) || this
    }, i.extend(e.Controller, n), e.Controller.prototype = n.prototype, e.Controller.prototype.constructor = e.Controller
  }, e.Scene = function (n) {
    var o, s, a = "BEFORE",
      l = "DURING",
      c = "AFTER",
      f = r.defaults,
      u = this,
      d = i.extend({}, f, n),
      h = a,
      g = 0,
      p = {
        start: 0,
        end: 0
      },
      v = 0,
      m = !0,
      w = function () {
        for (var e in d) f.hasOwnProperty(e) || delete d[e];
        for (var t in f) L(t);
        C()
      },
      y = {};
    this.on = function (e, t) {
      return i.type.Function(t) && (e = e.trim().split(" "), e.forEach(function (e) {
        var n = e.split("."),
          r = n[0],
          i = n[1];
        "*" != r && (y[r] || (y[r] = []), y[r].push({
          namespace: i || "",
          callback: t
        }))
      })), u
    }, this.off = function (e, t) {
      return e ? (e = e.trim().split(" "), e.forEach(function (e) {
        var n = e.split("."),
          r = n[0],
          i = n[1] || "",
          o = "*" === r ? Object.keys(y) : [r];
        o.forEach(function (e) {
          for (var n = y[e] || [], r = n.length; r--;) {
            var o = n[r];
            !o || i !== o.namespace && "*" !== i || t && t != o.callback || n.splice(r, 1)
          }
          n.length || delete y[e]
        })
      }), u) : u
    }, this.trigger = function (t, n) {
      if (t) {
        var r = t.trim().split("."),
          i = r[0],
          o = r[1],
          s = y[i];
        s && s.forEach(function (t) {
          o && o !== t.namespace || t.callback.call(u, new e.Event(i, t.namespace, u, n))
        })
      }
      return u
    }, u.on("change.internal", function (e) {
      "loglevel" !== e.what && "tweenChanges" !== e.what && ("triggerElement" === e.what ? E() : "reverse" === e.what && u.update())
    }).on("shift.internal", function () {
      S(), u.update()
    }), this.addTo = function (t) {
      return t instanceof e.Controller && s != t && (s && s.removeScene(u), s = t, C(), b(!0), E(!0), S(), s.info("container").addEventListener("resize", x), t.addScene(u), u.trigger("add", {
        controller: s
      }), u.update()), u
    }, this.enabled = function (e) {
      return arguments.length ? (m != e && (m = !!e, u.update(!0)), u) : m
    }, this.remove = function () {
      if (s) {
        s.info("container").removeEventListener("resize", x);
        var e = s;
        s = void 0, e.removeScene(u), u.trigger("remove")
      }
      return u
    }, this.destroy = function (e) {
      return u.trigger("destroy", {
        reset: e
      }), u.remove(), u.off("*.*"), null
    }, this.update = function (e) {
      if (s)
        if (e)
          if (s.enabled() && m) {
            var t, n = s.info("scrollPos");
            t = d.duration > 0 ? (n - p.start) / (p.end - p.start) : n >= p.start ? 1 : 0, u.trigger("update", {
              startPos: p.start,
              endPos: p.end,
              scrollPos: n
            }), u.progress(t)
          } else T && h === l && N(!0);
      else s.updateScene(u, !1);
      return u
    }, this.refresh = function () {
      return b(), E(), u
    }, this.progress = function (e) {
      if (arguments.length) {
        var t = !1,
          n = h,
          r = s ? s.info("scrollDirection") : "PAUSED",
          i = d.reverse || e >= g;
        if (0 === d.duration ? (t = g != e, g = 1 > e && i ? 0 : 1, h = 0 === g ? a : l) : 0 > e && h !== a && i ? (g = 0, h = a, t = !0) : e >= 0 && 1 > e && i ? (g = e, h = l, t = !0) : e >= 1 && h !== c ? (g = 1, h = c, t = !0) : h !== l || i || N(), t) {
          var o = {
              progress: g,
              state: h,
              scrollDirection: r
            },
            f = h != n,
            p = function (e) {
              u.trigger(e, o)
            };
          f && n !== l && (p("enter"), p(n === a ? "start" : "end")), p("progress"), f && h !== l && (p(h === a ? "start" : "end"), p("leave"))
        }
        return u
      }
      return g
    };
    var S = function () {
        p = {
          start: v + d.offset
        }, s && d.triggerElement && (p.start -= s.info("size") * d.triggerHook), p.end = p.start + d.duration
      },
      b = function (e) {
        if (o) {
          var t = "duration";
          F(t, o.call(u)) && !e && (u.trigger("change", {
            what: t,
            newval: d[t]
          }), u.trigger("shift", {
            reason: t
          }))
        }
      },
      E = function (e) {
        var n = 0,
          r = d.triggerElement;
        if (s && (r || v > 0)) {
          if (r)
            if (r.parentNode) {
              for (var o = s.info(), a = i.get.offset(o.container), l = o.vertical ? "top" : "left"; r.parentNode.hasAttribute(t);) r = r.parentNode;
              var c = i.get.offset(r);
              o.isDocument || (a[l] -= s.scrollPos()), n = c[l] - a[l]
            } else u.triggerElement(void 0);
          var f = n != v;
          v = n, f && !e && u.trigger("shift", {
            reason: "triggerElementPosition"
          })
        }
      },
      x = function () {
        d.triggerHook > 0 && u.trigger("shift", {
          reason: "containerResize"
        })
      },
      z = i.extend(r.validate, {
        duration: function (e) {
          if (i.type.String(e) && e.match(/^(\.|\d)*\d+%$/)) {
            var t = parseFloat(e) / 100;
            e = function () {
              return s ? s.info("size") * t : 0
            }
          }
          if (i.type.Function(e)) {
            o = e;
            try {
              e = parseFloat(o())
            } catch (n) {
              e = -1
            }
          }
          if (e = parseFloat(e), !i.type.Number(e) || 0 > e) throw o ? (o = void 0, 0) : 0;
          return e
        }
      }),
      C = function (e) {
        e = arguments.length ? [e] : Object.keys(z), e.forEach(function (e) {
          var t;
          if (z[e]) try {
            t = z[e](d[e])
          } catch (n) {
            t = f[e]
          } finally {
            d[e] = t
          }
        })
      },
      F = function (e, t) {
        var n = !1,
          r = d[e];
        return d[e] != t && (d[e] = t, C(e), n = r != d[e]), n
      },
      L = function (e) {
        u[e] || (u[e] = function (t) {
          return arguments.length ? ("duration" === e && (o = void 0), F(e, t) && (u.trigger("change", {
            what: e,
            newval: d[e]
          }), r.shifts.indexOf(e) > -1 && u.trigger("shift", {
            reason: e
          })), u) : d[e]
        })
      };
    this.controller = function () {
      return s
    }, this.state = function () {
      return h
    }, this.scrollOffset = function () {
      return p.start
    }, this.triggerPosition = function () {
      var e = d.offset;
      return s && (e += d.triggerElement ? v : s.info("size") * u.triggerHook()), e
    };
    var T, A;
    u.on("shift.internal", function (e) {
      var t = "duration" === e.reason;
      (h === c && t || h === l && 0 === d.duration) && N(), t && O()
    }).on("progress.internal", function () {
      N()
    }).on("add.internal", function () {
      O()
    }).on("destroy.internal", function (e) {
      u.removePin(e.reset)
    });
    var N = function (e) {
        if (T && s) {
          var t = s.info(),
            n = A.spacer.firstChild;
          if (e || h !== l) {
            var r = {
                position: A.inFlow ? "relative" : "absolute",
                top: 0,
                left: 0
              },
              o = i.css(n, "position") != r.position;
            A.pushFollowers ? d.duration > 0 && (h === c && 0 === parseFloat(i.css(A.spacer, "padding-top")) ? o = !0 : h === a && 0 === parseFloat(i.css(A.spacer, "padding-bottom")) && (o = !0)) : r[t.vertical ? "top" : "left"] = d.duration * g, i.css(n, r), o && O()
          } else {
            "fixed" != i.css(n, "position") && (i.css(n, {
              position: "fixed"
            }), O());
            var f = i.get.offset(A.spacer, !0),
              u = d.reverse || 0 === d.duration ? t.scrollPos - p.start : Math.round(g * d.duration * 10) / 10;
            f[t.vertical ? "top" : "left"] += u, i.css(A.spacer.firstChild, {
              top: f.top,
              left: f.left
            })
          }
        }
      },
      O = function () {
        if (T && s && A.inFlow) {
          var e = h === l,
            t = s.info("vertical"),
            n = A.spacer.firstChild,
            r = i.isMarginCollapseType(i.css(A.spacer, "display")),
            o = {};
          A.relSize.width || A.relSize.autoFullWidth ? e ? i.css(T, {
            width: i.get.width(A.spacer)
          }) : i.css(T, {
            width: "100%"
          }) : (o["min-width"] = i.get.width(t ? T : n, !0, !0), o.width = e ? o["min-width"] : "auto"), A.relSize.height ? e ? i.css(T, {
            height: i.get.height(A.spacer) - (A.pushFollowers ? d.duration : 0)
          }) : i.css(T, {
            height: "100%"
          }) : (o["min-height"] = i.get.height(t ? n : T, !0, !r), o.height = e ? o["min-height"] : "auto"), A.pushFollowers && (o["padding" + (t ? "Top" : "Left")] = d.duration * g, o["padding" + (t ? "Bottom" : "Right")] = d.duration * (1 - g)), i.css(A.spacer, o)
        }
      },
      _ = function () {
        s && T && h === l && !s.info("isDocument") && N()
      },
      P = function () {
        s && T && h === l && ((A.relSize.width || A.relSize.autoFullWidth) && i.get.width(window) != i.get.width(A.spacer.parentNode) || A.relSize.height && i.get.height(window) != i.get.height(A.spacer.parentNode)) && O()
      },
      D = function (e) {
        s && T && h === l && !s.info("isDocument") && (e.preventDefault(), s._setScrollPos(s.info("scrollPos") - ((e.wheelDelta || e[s.info("vertical") ? "wheelDeltaY" : "wheelDeltaX"]) / 3 || 30 * -e.detail)))
      };
    this.setPin = function (e, n) {
      var r = {
        pushFollowers: !0,
        spacerClass: "scrollmagic-pin-spacer"
      };
      if (n = i.extend({}, r, n), e = i.get.elements(e)[0], !e) return u;
      if ("fixed" === i.css(e, "position")) return u;
      if (T) {
        if (T === e) return u;
        u.removePin()
      }
      T = e;
      var o = T.parentNode.style.display,
        s = ["top", "left", "bottom", "right", "margin", "marginLeft", "marginRight", "marginTop", "marginBottom"];
      T.parentNode.style.display = "none";
      var a = "absolute" != i.css(T, "position"),
        l = i.css(T, s.concat(["display"])),
        c = i.css(T, ["width", "height"]);
      T.parentNode.style.display = o, !a && n.pushFollowers && (n.pushFollowers = !1);
      var f = T.parentNode.insertBefore(document.createElement("div"), T),
        d = i.extend(l, {
          position: a ? "relative" : "absolute",
          boxSizing: "content-box",
          mozBoxSizing: "content-box",
          webkitBoxSizing: "content-box"
        });
      if (a || i.extend(d, i.css(T, ["width", "height"])), i.css(f, d), f.setAttribute(t, ""), i.addClass(f, n.spacerClass), A = {
          spacer: f,
          relSize: {
            width: "%" === c.width.slice(-1),
            height: "%" === c.height.slice(-1),
            autoFullWidth: "auto" === c.width && a && i.isMarginCollapseType(l.display)
          },
          pushFollowers: n.pushFollowers,
          inFlow: a
        }, !T.___origStyle) {
        T.___origStyle = {};
        var h = T.style,
          g = s.concat(["width", "height", "position", "boxSizing", "mozBoxSizing", "webkitBoxSizing"]);
        g.forEach(function (e) {
          T.___origStyle[e] = h[e] || ""
        })
      }
      return A.relSize.width && i.css(f, {
        width: c.width
      }), A.relSize.height && i.css(f, {
        height: c.height
      }), f.appendChild(T), i.css(T, {
        position: a ? "relative" : "absolute",
        margin: "auto",
        top: "auto",
        left: "auto",
        bottom: "auto",
        right: "auto"
      }), (A.relSize.width || A.relSize.autoFullWidth) && i.css(T, {
        boxSizing: "border-box",
        mozBoxSizing: "border-box",
        webkitBoxSizing: "border-box"
      }), window.addEventListener("scroll", _), window.addEventListener("resize", _), window.addEventListener("resize", P), T.addEventListener("mousewheel", D), T.addEventListener("DOMMouseScroll", D), N(), u
    }, this.removePin = function (e) {
      if (T) {
        if (h === l && N(!0), e || !s) {
          var n = A.spacer.firstChild;
          if (n.hasAttribute(t)) {
            var r = A.spacer.style,
              o = ["margin", "marginLeft", "marginRight", "marginTop", "marginBottom"],
              a = {};
            o.forEach(function (e) {
              a[e] = r[e] || ""
            }), i.css(n, a)
          }
          A.spacer.parentNode.insertBefore(n, A.spacer), A.spacer.parentNode.removeChild(A.spacer), T.parentNode.hasAttribute(t) || (i.css(T, T.___origStyle), delete T.___origStyle)
        }
        window.removeEventListener("scroll", _), window.removeEventListener("resize", _), window.removeEventListener("resize", P), T.removeEventListener("mousewheel", D), T.removeEventListener("DOMMouseScroll", D), T = void 0
      }
      return u
    };
    var R, k = [];
    return u.on("destroy.internal", function (e) {
      u.removeClassToggle(e.reset)
    }), this.setClassToggle = function (e, t) {
      var n = i.get.elements(e);
      return 0 !== n.length && i.type.String(t) ? (k.length > 0 && u.removeClassToggle(), R = t, k = n, u.on("enter.internal_class leave.internal_class", function (e) {
        var t = "enter" === e.type ? i.addClass : i.removeClass;
        k.forEach(function (e) {
          t(e, R)
        })
      }), u) : u
    }, this.removeClassToggle = function (e) {
      return e && k.forEach(function (e) {
        i.removeClass(e, R)
      }), u.off("start.internal_class end.internal_class"), R = void 0, k = [], u
    }, w(), u
  };
  var r = {
    defaults: {
      duration: 0,
      offset: 0,
      triggerElement: void 0,
      triggerHook: .5,
      reverse: !0,
      loglevel: 2
    },
    validate: {
      offset: function (e) {
        if (e = parseFloat(e), !i.type.Number(e)) throw 0;
        return e
      },
      triggerElement: function (e) {
        if (e = e || void 0) {
          var t = i.get.elements(e)[0];
          if (!t || !t.parentNode) throw 0;
          e = t
        }
        return e
      },
      triggerHook: function (e) {
        var t = {
          onCenter: .5,
          onEnter: 1,
          onLeave: 0
        };
        if (i.type.Number(e)) e = Math.max(0, Math.min(parseFloat(e), 1));
        else {
          if (!(e in t)) throw 0;
          e = t[e]
        }
        return e
      },
      reverse: function (e) {
        return !!e
      }
    },
    shifts: ["duration", "offset", "triggerHook"]
  };
  e.Scene.addOption = function (e, t, n, i) {
    e in r.defaults || (r.defaults[e] = t, r.validate[e] = n, i && r.shifts.push(e))
  }, e.Scene.extend = function (t) {
    var n = this;
    e.Scene = function () {
      return n.apply(this, arguments), this.$super = i.extend({}, this), t.apply(this, arguments) || this
    }, i.extend(e.Scene, n), e.Scene.prototype = n.prototype, e.Scene.prototype.constructor = e.Scene
  }, e.Event = function (e, t, n, r) {
    r = r || {};
    for (var i in r) this[i] = r[i];
    return this.type = e, this.target = this.currentTarget = n, this.namespace = t || "", this.timeStamp = this.timestamp = Date.now(), this
  };
  var i = e._util = function (e) {
    var t, n = {},
      r = function (e) {
        return parseFloat(e) || 0
      },
      i = function (t) {
        return t.currentStyle ? t.currentStyle : e.getComputedStyle(t)
      },
      o = function (t, n, o, s) {
        if (n = n === document ? e : n, n === e) s = !1;
        else if (!u.DomElement(n)) return 0;
        t = t.charAt(0).toUpperCase() + t.substr(1).toLowerCase();
        var a = (o ? n["offset" + t] || n["outer" + t] : n["client" + t] || n["inner" + t]) || 0;
        if (o && s) {
          var l = i(n);
          a += "Height" === t ? r(l.marginTop) + r(l.marginBottom) : r(l.marginLeft) + r(l.marginRight)
        }
        return a
      },
      s = function (e) {
        return e.replace(/^[^a-z]+([a-z])/g, "$1").replace(/-([a-z])/g, function (e) {
          return e[1].toUpperCase()
        })
      };
    n.extend = function (e) {
      for (e = e || {}, t = 1; t < arguments.length; t++)
        if (arguments[t])
          for (var n in arguments[t]) arguments[t].hasOwnProperty(n) && (e[n] = arguments[t][n]);
      return e
    }, n.isMarginCollapseType = function (e) {
      return ["block", "flex", "list-item", "table", "-webkit-box"].indexOf(e) > -1
    };
    var a = 0,
      l = ["ms", "moz", "webkit", "o"],
      c = e.requestAnimationFrame,
      f = e.cancelAnimationFrame;
    for (t = 0; !c && t < l.length; ++t) c = e[l[t] + "RequestAnimationFrame"], f = e[l[t] + "CancelAnimationFrame"] || e[l[t] + "CancelRequestAnimationFrame"];
    c || (c = function (t) {
      var n = (new Date).getTime(),
        r = Math.max(0, 16 - (n - a)),
        i = e.setTimeout(function () {
          t(n + r)
        }, r);
      return a = n + r, i
    }), f || (f = function (t) {
      e.clearTimeout(t)
    }), n.rAF = c.bind(e), n.cAF = f.bind(e);
    var u = n.type = function (e) {
      return Object.prototype.toString.call(e).replace(/^\[object (.+)\]$/, "$1").toLowerCase()
    };
    u.String = function (e) {
      return "string" === u(e)
    }, u.Function = function (e) {
      return "function" === u(e)
    }, u.Array = function (e) {
      return Array.isArray(e)
    }, u.Number = function (e) {
      return !u.Array(e) && e - parseFloat(e) + 1 >= 0
    }, u.DomElement = function (e) {
      return "object" == typeof HTMLElement ? e instanceof HTMLElement : e && "object" == typeof e && null !== e && 1 === e.nodeType && "string" == typeof e.nodeName
    };
    var d = n.get = {};
    return d.elements = function (t) {
      var n = [];
      if (u.String(t)) try {
        t = document.querySelectorAll(t)
      } catch (r) {
        return n
      }
      if ("nodelist" === u(t) || u.Array(t))
        for (var i = 0, o = n.length = t.length; o > i; i++) {
          var s = t[i];
          n[i] = u.DomElement(s) ? s : d.elements(s)
        } else(u.DomElement(t) || t === document || t === e) && (n = [t]);
      return n
    }, d.scrollTop = function (t) {
      return t && "number" == typeof t.scrollTop ? t.scrollTop : e.pageYOffset || 0
    }, d.scrollLeft = function (t) {
      return t && "number" == typeof t.scrollLeft ? t.scrollLeft : e.pageXOffset || 0
    }, d.width = function (e, t, n) {
      return o("width", e, t, n)
    }, d.height = function (e, t, n) {
      return o("height", e, t, n)
    }, d.offset = function (e, t) {
      var n = {
        top: 0,
        left: 0
      };
      if (e && e.getBoundingClientRect) {
        var r = e.getBoundingClientRect();
        n.top = r.top, n.left = r.left, t || (n.top += d.scrollTop(), n.left += d.scrollLeft())
      }
      return n
    }, n.addClass = function (e, t) {
      t && (e.classList ? e.classList.add(t) : e.className += " " + t)
    }, n.removeClass = function (e, t) {
      t && (e.classList ? e.classList.remove(t) : e.className = e.className.replace(RegExp("(^|\\b)" + t.split(" ").join("|") + "(\\b|$)", "gi"), " "))
    }, n.css = function (e, t) {
      if (u.String(t)) return i(e)[s(t)];
      if (u.Array(t)) {
        var n = {},
          r = i(e);
        return t.forEach(function (e) {
          n[e] = r[s(e)]
        }), n
      }
      for (var o in t) {
        var a = t[o];
        a == parseFloat(a) && (a += "px"), e.style[s(o)] = a
      }
    }, n
  }(window || {});
  return e
});


/* Font Face Observer v2.0.13 - Â© Bram Stein. License: BSD-3-Clause */
(function () {
  'use strict';
  var f, g = [];

  function l(a) {
    g.push(a);
    1 == g.length && f()
  }

  function m() {
    for (; g.length;) g[0](), g.shift()
  }
  f = function () {
    setTimeout(m)
  };

  function n(a) {
    this.a = p;
    this.b = void 0;
    this.f = [];
    var b = this;
    try {
      a(function (a) {
        q(b, a)
      }, function (a) {
        r(b, a)
      })
    } catch (c) {
      r(b, c)
    }
  }
  var p = 2;

  function t(a) {
    return new n(function (b, c) {
      c(a)
    })
  }

  function u(a) {
    return new n(function (b) {
      b(a)
    })
  }

  function q(a, b) {
    if (a.a == p) {
      if (b == a) throw new TypeError;
      var c = !1;
      try {
        var d = b && b.then;
        if (null != b && "object" == typeof b && "function" == typeof d) {
          d.call(b, function (b) {
            c || q(a, b);
            c = !0
          }, function (b) {
            c || r(a, b);
            c = !0
          });
          return
        }
      } catch (e) {
        c || r(a, e);
        return
      }
      a.a = 0;
      a.b = b;
      v(a)
    }
  }

  function r(a, b) {
    if (a.a == p) {
      if (b == a) throw new TypeError;
      a.a = 1;
      a.b = b;
      v(a)
    }
  }

  function v(a) {
    l(function () {
      if (a.a != p)
        for (; a.f.length;) {
          var b = a.f.shift(),
            c = b[0],
            d = b[1],
            e = b[2],
            b = b[3];
          try {
            0 == a.a ? "function" == typeof c ? e(c.call(void 0, a.b)) : e(a.b) : 1 == a.a && ("function" == typeof d ? e(d.call(void 0, a.b)) : b(a.b))
          } catch (h) {
            b(h)
          }
        }
    })
  }
  n.prototype.g = function (a) {
    return this.c(void 0, a)
  };
  n.prototype.c = function (a, b) {
    var c = this;
    return new n(function (d, e) {
      c.f.push([a, b, d, e]);
      v(c)
    })
  };

  function w(a) {
    return new n(function (b, c) {
      function d(c) {
        return function (d) {
          h[c] = d;
          e += 1;
          e == a.length && b(h)
        }
      }
      var e = 0,
        h = [];
      0 == a.length && b(h);
      for (var k = 0; k < a.length; k += 1) u(a[k]).c(d(k), c)
    })
  }

  function x(a) {
    return new n(function (b, c) {
      for (var d = 0; d < a.length; d += 1) u(a[d]).c(b, c)
    })
  };
  window.Promise || (window.Promise = n, window.Promise.resolve = u, window.Promise.reject = t, window.Promise.race = x, window.Promise.all = w, window.Promise.prototype.then = n.prototype.c, window.Promise.prototype["catch"] = n.prototype.g);
}());

(function () {
  function l(a, b) {
    document.addEventListener ? a.addEventListener("scroll", b, !1) : a.attachEvent("scroll", b)
  }

  function m(a) {
    document.body ? a() : document.addEventListener ? document.addEventListener("DOMContentLoaded", function c() {
      document.removeEventListener("DOMContentLoaded", c);
      a()
    }) : document.attachEvent("onreadystatechange", function k() {
      if ("interactive" == document.readyState || "complete" == document.readyState) document.detachEvent("onreadystatechange", k), a()
    })
  };

  function r(a) {
    this.a = document.createElement("div");
    this.a.setAttribute("aria-hidden", "true");
    this.a.appendChild(document.createTextNode(a));
    this.b = document.createElement("span");
    this.c = document.createElement("span");
    this.h = document.createElement("span");
    this.f = document.createElement("span");
    this.g = -1;
    this.b.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
    this.c.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
    this.f.style.cssText = "max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
    this.h.style.cssText = "display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";
    this.b.appendChild(this.h);
    this.c.appendChild(this.f);
    this.a.appendChild(this.b);
    this.a.appendChild(this.c)
  }

  function t(a, b) {
    a.a.style.cssText = "max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:" + b + ";"
  }

  function y(a) {
    var b = a.a.offsetWidth,
      c = b + 100;
    a.f.style.width = c + "px";
    a.c.scrollLeft = c;
    a.b.scrollLeft = a.b.scrollWidth + 100;
    return a.g !== b ? (a.g = b, !0) : !1
  }

  function z(a, b) {
    function c() {
      var a = k;
      y(a) && a.a.parentNode && b(a.g)
    }
    var k = a;
    l(a.b, c);
    l(a.c, c);
    y(a)
  };

  function A(a, b) {
    var c = b || {};
    this.family = a;
    this.style = c.style || "normal";
    this.weight = c.weight || "normal";
    this.stretch = c.stretch || "normal"
  }
  var B = null,
    C = null,
    E = null,
    F = null;

  function G() {
    if (null === C)
      if (J() && /Apple/.test(window.navigator.vendor)) {
        var a = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);
        C = !!a && 603 > parseInt(a[1], 10)
      } else C = !1;
    return C
  }

  function J() {
    null === F && (F = !!document.fonts);
    return F
  }

  function K() {
    if (null === E) {
      var a = document.createElement("div");
      try {
        a.style.font = "condensed 100px sans-serif"
      } catch (b) {}
      E = "" !== a.style.font
    }
    return E
  }

  function L(a, b) {
    return [a.style, a.weight, K() ? a.stretch : "", "100px", b].join(" ")
  }
  A.prototype.load = function (a, b) {
    var c = this,
      k = a || "BESbswy",
      q = 0,
      D = b || 3E3,
      H = (new Date).getTime();
    return new Promise(function (a, b) {
      if (J() && !G()) {
        var M = new Promise(function (a, b) {
            function e() {
              (new Date).getTime() - H >= D ? b() : document.fonts.load(L(c, '"' + c.family + '"'), k).then(function (c) {
                1 <= c.length ? a() : setTimeout(e, 25)
              }, function () {
                b()
              })
            }
            e()
          }),
          N = new Promise(function (a, c) {
            q = setTimeout(c, D)
          });
        Promise.race([N, M]).then(function () {
          clearTimeout(q);
          a(c)
        }, function () {
          b(c)
        })
      } else m(function () {
        function u() {
          var b;
          if (b = -1 !=
            f && -1 != g || -1 != f && -1 != h || -1 != g && -1 != h)(b = f != g && f != h && g != h) || (null === B && (b = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent), B = !!b && (536 > parseInt(b[1], 10) || 536 === parseInt(b[1], 10) && 11 >= parseInt(b[2], 10))), b = B && (f == v && g == v && h == v || f == w && g == w && h == w || f == x && g == x && h == x)), b = !b;
          b && (d.parentNode && d.parentNode.removeChild(d), clearTimeout(q), a(c))
        }

        function I() {
          if ((new Date).getTime() - H >= D) d.parentNode && d.parentNode.removeChild(d), b(c);
          else {
            var a = document.hidden;
            if (!0 === a || void 0 === a) f = e.a.offsetWidth,
              g = n.a.offsetWidth, h = p.a.offsetWidth, u();
            q = setTimeout(I, 50)
          }
        }
        var e = new r(k),
          n = new r(k),
          p = new r(k),
          f = -1,
          g = -1,
          h = -1,
          v = -1,
          w = -1,
          x = -1,
          d = document.createElement("div");
        d.dir = "ltr";
        t(e, L(c, "sans-serif"));
        t(n, L(c, "serif"));
        t(p, L(c, "monospace"));
        d.appendChild(e.a);
        d.appendChild(n.a);
        d.appendChild(p.a);
        document.body.appendChild(d);
        v = e.a.offsetWidth;
        w = n.a.offsetWidth;
        x = p.a.offsetWidth;
        I();
        z(e, function (a) {
          f = a;
          u()
        });
        t(e, L(c, '"' + c.family + '",sans-serif'));
        z(n, function (a) {
          g = a;
          u()
        });
        t(n, L(c, '"' + c.family + '",serif'));
        z(p, function (a) {
          h = a;
          u()
        });
        t(p, L(c, '"' + c.family + '",monospace'))
      })
    })
  };
  "object" === typeof module ? module.exports = A : (window.FontFaceObserver = A, window.FontFaceObserver.prototype.load = A.prototype.load);
}());