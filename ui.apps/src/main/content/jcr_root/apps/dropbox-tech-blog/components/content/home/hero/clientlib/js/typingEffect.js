$(document).ready(function() {

var i = 0;
var txt = $(".hero_contanier").attr('data-prompt-text');
var speed = 50;
typeWriter();

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("hero_prompt").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }
}
});