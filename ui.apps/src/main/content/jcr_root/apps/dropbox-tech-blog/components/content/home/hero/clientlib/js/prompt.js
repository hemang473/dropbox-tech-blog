class PromptType {
    constructor(element, categories, promptText, period = 2000) {
        this.element = element;
        this.categories = categories;
        this.promptText = promptText;
        this.loopNum = 0;
        this.period = period;
        this.text = '';
        this.tick();
        this.isDeleting = false;    
    }

    tick () {
        let i = this.loopNum % this.categories.length;
        let fullTxt = this.categories[i].text;
        if (this.isDeleting) {
            this.text = fullTxt.substring(0, this.text.length - 1);
        } else {
            this.text = fullTxt.substring(0, this.text.length + 1);
        }
        this.element.innerHTML = this .promptText
                                        .replace(
                                           "${categoryName}", 
                                           `<span class="hero__interactive">${this.text.trim()}</span>`
                                        );
        document.querySelector(".hero__interactive").style.color = this.categories[i].color;
        document.querySelector(".hero__interactive").style.borderRightColor = this.categories[i].color;
        let delta = 200 - Math.random() * 100;
        if (this.isDeleting) {
            delta /= 2;
        }
        if (!this.isDeleting && this.text === fullTxt) {
            delta = this.period;
            this.isDeleting = true;
        } else if (this.isDeleting && this.text === '') {
            this.isDeleting = false;
           this.loopNum++;
            delta = 500;
        }
        setTimeout(() => this.tick(), delta);
    };
}

window.addEventListener("load", (event) => {
    const categories = [];
    const promptElement = document.querySelector(".hero__prompt");
    const promptText = document .querySelector(".hero__contanier")
                                .getAttribute("data-prompt-text");
    document.querySelectorAll('.hero__category')
            .forEach( category =>  {
                categories.push({
                    text  : category.textContent,
                    color : category.getAttribute("data-color")
                })
            });
    if (categories.length > 0) {
        new PromptType(promptElement, categories, promptText);
    }
});