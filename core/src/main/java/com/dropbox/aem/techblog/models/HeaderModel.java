package com.dropbox.aem.techblog.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderModel {
    
    @Inject
    private List<Resource> headerList;

    private List<HeaderItems> headerItemsList = new ArrayList<>();

    public List<HeaderItems> getHeaderItemsList() {
        return headerItemsList;
    }

    @PostConstruct
    protected void init() {
        if (headerList != null) {
            headerList.forEach(resource -> {
                    HeaderItems header = resource.adaptTo(HeaderItems.class);
                    if (header != null) {
                        headerItemsList.add(header);
                    }
                });
        }
    }
}