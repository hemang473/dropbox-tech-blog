package com.dropbox.aem.techblog.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageCardsModel {

    private static Logger log = LoggerFactory.getLogger(ImageCardsModel.class);

    @Inject
    private List<Resource> cardList;

    public List<Resource> getCardList() {
        return cardList;
    }
}