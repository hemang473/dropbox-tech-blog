package com.dropbox.aem.techblog.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticleModel {

    private static Logger log = LoggerFactory.getLogger(ArticleModel.class);

    @Inject
    List<Resource> articleList;

    public List<Resource> getArticleList() {
        return articleList;
    }

    @PostConstruct
    protected void init() {
        log.info("Article Component initialized");
    }
}