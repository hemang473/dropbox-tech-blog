package com.dropbox.aem.techblog.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface FooterItems {

    @Inject
    public List<LinkList> getLinkList();

    @Model(adaptables = Resource.class, 
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    class LinkList {

        @ValueMapValue
        private String text;

        @ValueMapValue
        private String link;

        public String getText() {
            return text;
        }
        
        public String getLink() {
            return link;
        }
    }
}