package com.dropbox.aem.techblog.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface HeaderItems {

    @ValueMapValue
    public String getHeading();

    @ValueMapValue
    public String getHeadingLink();

    @ValueMapValue
    public String getCheckbox();

    @Inject
    public List<NestedList> getNestedList();

    @Model(adaptables = Resource.class, 
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    class NestedList {

        @ValueMapValue
        private String subHeading;

        @ValueMapValue
        private String subHeadingLink;

        public String getSubHeading() {
            return subHeading;
        }
        
        public String getSubHeadingLink() {
            return subHeadingLink;
        }
    }
}