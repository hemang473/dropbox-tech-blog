package com.dropbox.aem.techblog.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterModel {

    @Inject
    private List<Resource> columnList;

    private List<FooterItems> footerItemsList = new ArrayList<>();

    public List<FooterItems> getFooterItemsList() {
        return footerItemsList;
    }

    @PostConstruct
    protected void init() {
        if (columnList != null) {
            columnList.forEach(resource -> {
                    FooterItems footer = resource.adaptTo(FooterItems.class);
                    if (footer != null) {
                        footerItemsList.add(footer);
                    }
                });
        }
    }
}